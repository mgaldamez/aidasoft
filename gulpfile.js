/**
 * SASS
 */
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var gulp = require('gulp');
var watch = require('gulp-watch');
var sass = require('gulp-sass');


// Utilities
var gutil = require('gulp-util'); // Gulp Utilities
// var rename       = require('gulp-rename'); // Rename Files
var notify = require('gulp-notify'); // OSX Notifications
var autoprefixer = require('gulp-autoprefixer');

// Directories
var SRC = 'resources/assets';
var BUILD = 'public';
var BOWER = 'vendor/bower_components';


// Browser Sync
gulp.task('browser-sync', function () {
    browserSync({
        proxy: "127.0.0.1:8000",
        notify: false
    });
});

// Sass
gulp.task('sass', function () {
    return gulp.src(SRC + '/sass/**.scss')
    //   .pipe(
    //     sass({
    //       outputStyle: 'expanded',
    //       debugInfo: true,
    //       lineNumbers: true,
    //       errLogToConsole: false,
    //       onError: function(err) {
    //         gutil.beep();
    //         notify().write('Gulp Sass Error on line ' + err.line);
    //         gutil.log(
    //           'Error ' + gutil.colors.red.inverse(err.message +
    //             '\nin ' + err.file + ' on line ' + err.line));
    //     }
    // })
    //     )
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(BUILD + '/assets/css')) // plain css output before extra builds
        // .pipe(minifycss()) // minifycss has to happen before prefix and pixrem
        .pipe(autoprefixer())
        // .pipe(pixrem('16px', { replace: false }))
        // .pipe(rename({suffix: '.min'}))
        // .pipe(filesize())
        // .pipe(gulp.dest(BUILD + '/css'))
        .pipe(reload({stream: true}));
});


// Javascript
gulp.task("js", function () {
    return gulp.src([
        BUILD + '/assets/js/*.js'
    ])
//   .pipe(jshint())
//   .pipe(jshint.reporter(stylish))
//   .pipe(uglify().on('error', swallowError ))
//   .pipe(concat('app.js'))
//   .pipe(rename({suffix: '.min'}))
//   .pipe(gulp.dest(BUILD + '/js'))
//   .pipe(filesize());
});


// Watch
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
gulp.task('watch', function () {
    gulp.watch(SRC + '/sass/app/*.scss', ['sass']);
    // gulp.watch(SRC + '/js/*.js', ['js', browserSync.reload]);
    gulp.watch(BUILD + '/assets/js/*.js', ['js', browserSync.reload]);
    gulp.watch('resources/views/**/*.php').on("change", browserSync.reload);
});

// Default
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
gulp.task('default', [
    'browser-sync',
    'sass',
    'js',
    'watch'
]);


/**
 * VUE JS
 */
// var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */


// require('laravel-elixir-vue-2');
// // var browserify = require('laravel-elixir-webpack-official');
//
//  elixir(function(mix) {
//      // mix.browserify('main.js');
//      mix.webpack('main.js');
// });