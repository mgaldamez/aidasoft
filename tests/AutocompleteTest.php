<?php

// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AutocompleteTest extends TestCase
{

	use DatabaseTransactions;

	protected $baseUrl = "127.0.0.1:800";

	public function testExample()
	{


		// $color = seed('Attribute', [
		// 	'description' => 'Color'
		// ]);

		// $talla = seed('Attribute', [
		// 	'description' => 'Talla'
		// ]);

		$tamano = seed('Attribute', [
			'description' => 'Tamano'
		]);


		$this->get('autocomplete/attribute?term=Color')
		->seeStatusCode(200)
		->seeJsonEquals([
			[
				'id' => 1, 
				'description' => 'Color'
			]
		]);



		$this->get('autocomplete/attribute?term=ta')
		->seeStatusCode(200)
		->seeJsonEquals([
			[
				'id' => 2, 
				'description' => 'Talla'
			],
			[
				'id' => $tamano->id, 
				'description' => 'Tamano'
			]
		]);


	}
}
