@extends('layout_principal') 

@section('extra_scriptHead')
{{-- <link href="{{asset('select2/css/select2.css')}}" rel="stylesheet" />
<script src="{{asset('select2/js/select2.js')}}"></script>
<script src="{{asset('select2/js/i18n/es.js')}}" type="text/javascript"></script> --}}

{{-- <link href="{!! asset('EasyAutocomplete-1.3.5/easy-autocomplete.min.css') !!}" rel="stylesheet"> --}}
{{-- <script src="{!! asset('EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js') !!}" type="text/javascript"></script> --}}

@endsection

@section('container')


<div class="col-md-10 col-md-offset-1">
	<div class="panel">
		<div class="panel-heading">
			<h2>Nuevo Atributo</h2>
		</div>

		<div class="panel-body">

			{!!Form::open(['url' => 'attribute', 'class' => 'form-horizontal', 'method' => 'POST', 'id' => 'form-attribute'] )!!}

			<div class="form-group">
				{!!Form::label('attribute', 'Atributo', ['class' => 'control-table col-md-2'])!!}
				<div class="col-md-10">
					
					{!!Form::text('attribute', old('attribute'), ['class' => 'form-control', 'autocomplete', 'placeholder' => 'Nombre del atributo', 'autofocus'])!!}
				</div>
			</div>

			<div class="form-group">
				{!!Form::label('', 'Valores', ['class' => 'control-table col-md-2'])!!}
				<div class="col-md-10">
					<div class="row">
						
						<table id="__attributes" class="table table-condensed table-bordered text-middle">
							<tbody>

								<tr>
									<td  style="border-bottom: 0;">
										<input type="text" id="attribute_descripction" class="form-control" placeholder="Descripctión">
									</td>
									<td  style="border-bottom: 0;">
										<input type="text" id="attribute_value" class="form-control" placeholder="Valor">
									</td>
									<td  style="border-bottom: 0;">
										<a href="#" class="btn btn-primary btn-fab btn-fab-sm mdb " role="add"><i class="material-icons">add</i></a>
									</td>
								</tr>

							</tbody>
						</table>

					</div>
				</div>
			</div>

			{!!Form::submit('Crear', ['class' => 'btn btn-primary tbn-lg pull-right mdb'] )!!}

			{!!Form::close()!!}

		</div>
	</div>
</div>


@endsection


@section('extra_scriptBody')

<script src="{!! asset('assets/js/attributes.js') !!}"></script>

{{-- 	<script>
		$(document).ready(function(event){
		
		});
	</script> --}}

	@endsection

