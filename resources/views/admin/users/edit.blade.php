@extends('layout_principal')

@section('container')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li><a href="{!! url('/') !!}">Inicio</a></li>
                        <li><a href="{!! route('admin.user.index') !!}">usuarios</a></li>
                        <li class="active">Editar usuario</li>
                    </ol>

                    <div class="navbar-btn">
                        <a href="{!! URL::previous() !!}" class="btn mdb" >Cancelar</a>
                        <button class="btn btn-primary mdb" onclick="document.getElementById('form-user').submit()">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1">

        <div class="panel">
            <div class="panel-heading">
                <p class="text-info">
                    <i class="material-icons md-18">info_outline</i> Los campos marcados con (*) son obligatorios.
                </p>
            </div>

            <div class="panel-body">
                {!! Form::model( $user, ['route' => ['admin.user.update', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal form-condensed', 'id' => 'form-user']) !!}

                @include('admin.users.form.create')


                <div class="form-group mdb  @if($errors->has('role_id')) has-error @endif"">
                    {!! Form::label('role_id', 'Rol', ['class' => 'control-label col-md-3']) !!}
                    <div class="col-md-9">

                        {!! Form::select('role_id', $roleList, old('role_id'), ['class' => 'form-control'] ) !!}

                        {!! $errors->first('role_id', '<p class="text-danger">:message</p>')  !!}
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col -->

@endsection