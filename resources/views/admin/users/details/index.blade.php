@extends('layout_principal')

@section('extra_scriptHead')

<style>
.list-group-item .row-content,
.list-group-item-heading {
	border-bottom: 1px solid #c0c5ca;
}
</style>
@endsection


@section('container')
<div class="col-xs-12 col-sm-12 col-md-12">
   <div class="row-sm">
      	<div class="panel __panel">
       		<div class="__panel-heading">
           		<h3>Perfil de Usuario</h3>       
        	</div>
        	<!--.panel-heading-->
        
       		<div class="__panel-body">
       			<div class="row">
       				<div class="col-sm-12 col-md-3">
						<div class="profile">
						  
            				@foreach( $user as $user  )
						    
						    <a href="#" class="">
						      <img class="img-thumbnail" src="http://lorempixel.com/250/250/people" alt="...">
						    </a>
						  
						  	<h4>{{ $user->name }} </h4>
						 	<ul>
							  {{-- <li><span class="glyphicon glyphicon-globe"></span> {{ $user->address }}</li> --}}
							  {{-- <li><span class="glyphicon glyphicon-earphone"></span> {{ $user->phone }}</li> --}}
							  <li class="text-uppercase"><span class="glyphicon glyphicon-certificate"></span><strong> {{ $user->role }}</strong></li>
							  <li><span class="glyphicon glyphicon-envelope"></span> {{ $user->email }}</li>
							</ul>
								
							@endforeach
						</div>
					</div>
					<!-- .col -->	
       		
					
					<div class="col-sm-12 col-md-9">

						<ul class="nav nav-pills" role="tablist">
						    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Actividades Recientes</a></li>

						    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Informacion</a></li>

						    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>

						    <li role="presentation" class="pull-right"><a role="button" data-toggle="modal" data-target="#myModal"> <span class="glyphicon glyphicon-cog"></span></a></li>
						</ul>

						<div class="tab-content">
						    <div role="tabpanel" class="tab-pane fade in active" id="home">
						    	<br>
								<div class="media">
								  <div class="media-left media-middle">
								    <a href="#">
								      <img class="media-object" src="http://lorempixel.com/50/50/people" alt="...">
								    </a>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">Middle aligned media</h4>
								    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea incidunt fugit, autem, harum possimus quisquam maxime! Molestiae doloribus fugit distinctio ratione praesentium delectus velit debitis optio ullam a. Odit, incidunt?</p>
								  </div>
								</div>

								<br>
								<div class="media">
								  <div class="media-left media-middle">
								    <a href="#">
								      <img class="media-object" src="http://lorempixel.com/50/50/people" alt="...">
								    </a>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">Middle aligned media</h4>
								    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea incidunt fugit, autem, harum possimus quisquam maxime! Molestiae doloribus fugit distinctio ratione praesentium delectus velit debitis optio ullam a. Odit, incidunt?</p>
								  </div>
								</div>
						    		
						    	<br>
								<div class="media">
								  <div class="media-left media-middle">
								    <a href="#">
								      <img class="media-object" src="http://lorempixel.com/50/50/people" alt="...">
								    </a>
								  </div>
								  <div class="media-body">
								    <h4 class="media-heading">Middle aligned media</h4>
								    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea incidunt fugit, autem, harum possimus quisquam maxime! Molestiae doloribus fugit distinctio ratione praesentium delectus velit debitis optio ullam a. Odit, incidunt?</p>
								  </div>
								</div>

								<div class="list-group">
								  <div class="list-group-item">
								    <div class="row-action-primary">
								      <i class="material-icons">folder</i>
								    </div>
								    <div class="row-content">
								      <div class="least-content">15m</div>
								      <h4 class="list-group-item-heading">Tile with a label</h4>

								      <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus.</p>
								    </div>	
								  </div>
								  <div class="list-group-separator"></div>
								  <div class="list-group-item">
								    <div class="row-action-primary">
								      <i class="material-icons">folder</i>
								    </div>
								    <div class="row-content">
								      <div class="least-content">10m</div>
								      <h4 class="list-group-item-heading">Tile with a label</h4>

								      <p class="list-group-item-text">Maecenas sed diam eget risus varius blandit.</p>
								    </div>
								  </div>
								  <div class="list-group-separator"></div>
								  <div class="list-group-item">
								    <div class="row-action-primary">
								      <i class="material-icons">folder</i>
								    </div>
								    <div class="row-content">
								      <div class="least-content">8m</div>
								      <h4 class="list-group-item-heading">Tile with a label</h4>

								      <p class="list-group-item-text">Maecenas sed diam eget risus varius blandit.</p>
								    </div>
								  </div>
								  <div class="list-group-separator"></div>
								</div>

						    </div>
							<!-- .tab-pane  -->
							<div role="tabpanel" class="tab-pane fade" id="profile">
							
							
								<div class="list-group">
								<br>
								   <div class="list-group-item">  
								    <div class="row-content">
								      <h4 class="list-group-item-heading">Datos de Usuario</h4>
									  
									  <form class="form-horizontal">
										  <div class="form-group">
										    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
										    <div class="col-sm-10">
										      <input type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ $user->email }}">
										    </div>
										  </div>	 
										  <div class="form-group">
										    <div class="col-sm-offset-2 col-sm-10">
										      <button type="submit" class="btn btn-raised btn-primary">Guardar Cambios</button>
										       <button type="submit" class="btn btn-raised btn-default">Cancelar</button>
										    </div>
										  </div>
										</form>

								    </div>	
								   <!-- .row-content -->
								  </div>
								  <!-- .list-group-item -->

								  <div class="list-group-item">
								  <br>  
								    <div class="row-content">
								      <h4 class="list-group-item-heading">Contraseña</h4>
									  
									  <form class="form-horizontal">
									  	<div class="form-group">
										    <label for="inputEmail3" class="col-sm-2 control-label">Contraseña actual</label>
										    <div class="col-sm-10">
										      <input type="password" class="form-control" id="inputEmail3" placeholder="Contraseña" value="{{ $user->password }}">
										    </div>
										  </div>
										  <div class="form-group">
										    <label for="inputEmail3" class="col-sm-2 control-label">Contraseña nueva</label>
										    <div class="col-sm-10">
										      <input type="password" class="form-control" id="inputEmail3" placeholder="Contraseña" value="">
										    </div>
										  </div>	
										  <div class="form-group">
										    <label for="inputEmail3" class="col-sm-2 control-label">Confirmar contraseña</label>
										    <div class="col-sm-10">
										      <input type="password" class="form-control" id="inputEmail3" placeholder="Contraseña" value="">
										    </div>
										  </div> 
										  <div class="form-group">
										    <div class="col-sm-offset-2 col-sm-10">
										      <button type="submit" class="btn btn-raised btn-primary">Guardar Cambios</button>
										       <button type="submit" class="btn btn-raised btn-default">Cancelar</button>
										    </div>
										  </div>
										</form>

								    </div>	
								   <!-- .row-content -->
								  </div>
								  <!-- .list-group-item -->
								</div>
								<!-- .list-group -->
							</div> 
							<!-- .tab-pane  -->
							<div role="tabpanel" class="tab-pane fade" id="messages">C...</div>
							<!-- .tab-pane  -->
						    <div role="tabpanel" class="tab-pane fade" id="settings">D...</div>
						    <!-- .tab-pane  -->
						</div>
						  		
					</div>
					<!-- .col -->
				</div>
				<!-- .row -->
			</div>
			<!-- .panel-body -->
	  	</div>
	  	<!-- .panel -->
	</div>
	<!-- .row-sm -->
</div>
<!-- .col -->

<!-- MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Roles de Usuario</h4>
      </div>
      <div class="modal-body">
        <p>Rol actual {{ $user->role }}</p>


		<table class="table table-condensed vertical-text-center" style="100%">
			<thead>
			<tr>
				<th colspan="2">Tipo de Usuario</th>
				<th class="text-center">Leer</th>
				<th class="text-center">Escribir</th>
				<th class="text-center">Modificar</th>
			</tr>
			</thead>
			<tbody>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
				@foreach($role as $key => $role)
				<tr>
					<td>
						<div class="radio radio-primary">
				          <label>
				            <input type="radio" name="roleOption" id="optionsRadios{{$key+1}}" value="{{$role->id}}"   @if($user->role_id == $role->id) checked="" @endif >
				          </label>
				        </div>
					</td>
					<td class="">{{ $role->description}}</td>
					<td class="text-center">
					@if($role->read == 1)
						<span class="glyphicon glyphicon-ok"></span>
					@endif	
					</td>
					<td class="text-center">
					@if($role->write == 1)
						<span class="glyphicon glyphicon-ok"></span>
					@endif	
					</td>
					<td class="text-center">
					@if($role->modify == 1)
						<span class="glyphicon glyphicon-ok"></span>
					@endif	
					</td>	
				</tr>
				@endforeach
			</tbody>
		</table>
		



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="save" class="btn btn-primary"  onclick="saveRole()">Guardar Cambios</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection


@section('extra_scriptBody')
<script>
function saveRole(){
	$role =  $('input:radio[name=roleOption]:checked').val();
	// $token = $('#token').val();
	$url  = "/save/user/role/"
	
	$.ajax({
	  url: $url,
	  method: "GET",
	  data: { role : $role },
	  dataType: "html",
	  success: function(res){
	  	 // Recargo la página
            location.reload();
	  },
	  error: function(res){
	  	console.log(res);
	  }
	});
}
</script>
@endsection
