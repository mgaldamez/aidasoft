@extends('layout_principal')



@section('container')
{!! csrf_field() !!}
    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <ol class="breadcrumb">
                        <li><a href="{!! url('/') !!}">Inicio</a></li>
                        <li><a href="{!! route('admin.warehouse.index') !!}">usuarios</a></li>
                        <li class="active">Nuevo usuario</li>
                    </ol>

                    <div class="navbar-btn bg-default">


                        <a href="{!! URL::previous() !!}" class="btn mdb">
                            <!-- arrow back -->
                            <i class="material-icons md-18">&#xE5C4;</i>Atras
                        </a>


                        <button type="submit" class="btn btn-primary mdb" form="form_create">
                            <i class="material-icons md-18">save</i> Crear usuario
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10 col-md-offset-1">

     


	<form id="form_create" method="POST"  action="{{action('UserController@init')}}"  onsubmit="return validar(this);">
		{{ csrf_field() }}
	<div class="form-row">
    <div class="form-group col-md-6">
      <label for="txt_nombre">Nombre</label>
      <input type="text" class="form-control" id="txt_nombre" name="txt_nombre" placeholder="Nombre">
    </div>
    <div class="form-group col-md-4">
      <label for="txt_apellido">Apellido</label>
       <input type="text" class="form-control" id="txt_apellido" name="txt_apellido" placeholder="Apellido">
      </select>
    </div>
    <div class="form-group col-md-2">
      <label for="txt_usuario">Usuario</label>
      <input type="text" class="form-control" id="txt_usuario" name="txt_usuario" placeholder="US0000" >
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="txt_email" name="txt_email" placeholder="Email">
    </div>
     <div class="form-group col-md-6">
      <label for="inputEmail4">Telefono</label>
      <input type="phone" class="form-control" id="txt_telefono" name="txt_telefono" placeholder="Telefono">
    </div>
	</div>
		<div class="form-row">
   
    <div class="form-group col-md-6">
      <label for="inputPassword4">Direccion</label>
      <input type="text" class="form-control" id="txt_direccion" name="txt_direccion" placeholder="1234 Main St">
    </div>
	  <div class="form-group col-md-6">
      <label for="txt_rol">Rol</label>
      <select id="txt_rol"  name="txt_rol" class="form-control">
        <option selected>Choose...</option>
		<option>admin</option>
		<option>editor</option>
      </select>
    </div>
	</div>
	
	<div class="form-row">
	<div class="form-group col-md-6">
	<label for="inputCity">Activo</label>
      <div class="togglebutton">
	  
		<label>
		<input type="checkbox" id="chk_active" name="chk_active" checked="checked">
		</label>
		</div>
	</div>
	  

	</div>

 

</form>
         
         
    </div>







@endsection

@section('extra_scriptBody')


    @include('partials.dataTables_script')
	
<script type="text/javascript">
  	function validar(frm) { 
		if(frm.txt_nombre.value  == "") { alert('Favor ingrese el nombre de usuario para poder continuar') ; return false ;} 
	
		if(frm.txt_apellido.value  == "") { alert('Favor ingrese el apellido de usuario para poder continuar') ; return false ;} 
	
		if(frm.txt_usuario.value  == "") { alert('Favor ingrese el usuario para poder continuar') ; return false ;} 
	
		if(frm.txt_email.value  == "") { alert('Favor ingrese un correo para poder continuar') ; return false ;} 
	
		if(frm.txt_telefono.value == "Choose...") { alert('Debes elejir un rol para poder continuar') ; return false ; } 
		
		if(frm.txt_rol.value == "Choose...") { alert('Debes elejir un rol para poder continuar') ; return false ; } 
				
		

		} 
</script>
    <script>
        $(document).ready(function () {
            $('#table-users').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>

    <script>
        var route = '{!! route('admin.user.nose', ':USER_ID') !!}';

        $('button.add-user').on('click', function (event) {

            var button = $(event.target);

            var agent = button.data('agent');

            var newRoute = route.replace(':USER_ID', agent);
            var modal = $(this);
            var form = modal.find('form');


            $.get(newRoute, function (response) {

                var body = document.getElementsByClassName('container-wrapper')[0];
                body.insertAdjacentHTML('afterbegin', response.alert);

                button.remove();
            });
        });
    </script>

    {{--<!-- Input Mask -->--}}
    {{--<script src="{{ asset('inputmask/inputmask.js') }}"></script>--}}
    {{--<script src="{{ asset('inputmask/inputmask.phone.Extensions.js') }}"></script>--}}
    {{--<script src="{{ asset('inputmask/jquery.inputmask.js') }}"></script>--}}
    {{--<script>--}}
    {{--$(document).ready(function () {--}}
    {{--$("input[name='phone']").inputmask({"mask": "+(999) 9999-9999"});--}}
    {{--});--}}
    {{--</script>--}}


@endsection




{{--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">--}}
{{--<div class="modal-dialog" role="document">--}}
{{--<div class="modal-content">--}}
{{--<div class="modal-header">--}}
{{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
{{--aria-hidden="true">&times;</span></button>--}}
{{--<h4 class="modal-title" id="exampleModalLabel">New message</h4>--}}
{{--</div>--}}
{{--<div class="modal-body">--}}

{{--<p class="text-info">--}}
{{--<i class="material-icons md-18">info_outline</i> Los campos marcados con (*) son obligatorios.--}}
{{--</p>--}}

{{--{!! Form::open(array('route' => ['admin.user.store'], 'method' => 'post', 'id' => 'form-user', 'class' => 'form-horizontal form-condensed')) !!}--}}

{{--@include('admin.users.form.create')--}}

{{--{!! Form::close() !!}--}}

{{--</div>--}}
{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-default mdb" data-dismiss="modal">Cerrar</button>--}}
{{--<button type="button" class="btn btn-primary mdb"--}}
{{--onclick="document.getElementById('form-user').submit()">Crear--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}