@extends('layout_principal')

@section('extra_scriptHead')
    <!-- Croppie JS -->
    <link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}"/>
    <script src="{{ asset('croppie/croppie.js') }}"></script>
    <script src="{{ asset('assets/js/demo_image.js')}}"></script>
@endsection

@section('container')

    <section class="col-md-10 col-md-offset-1">
        <div class="row-sm">

            {!! Form::open(['route' => ['admin.article.store'], 'method' => 'POST', 'role' => 'create', 'id' => 'article_create']) !!}

            <section class="panel">

                <div class="panel-heading">
                    <h1>Nuevo Articulo</h1>
                </div>

                <div class="panel-body">

                    <div class="btn-group btn-group-sm pull-right">
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#categoryModal">
                            <i class="material-icons md-18">add_circle_outline</i> Categoría
                        </button>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#brandModal">
                            <i class="material-icons md-18">add_circle_outline</i> Marca
                        </button>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#measurementModal">
                            <i class="material-icons md-18">add_circle_outline</i> Unidad de medida
                        </button>
                    </div>


                    <legend style="width: auto">Datos Generales</legend>
                </div>


                <div class="panel-body form-horizontal">
                    @include('admin.forms.article_general')
                </div>
                <!-- .panel-body -->


                <div class="panel-body">

                    <button type="button" class="btn btn-sm btn-link pull-right" data-toggle="modal"
                            data-target="#attributeModal">
                        <i class="material-icons md-18">add_circle_outline</i> Nuevo Atributo
                    </button>

                    <legend style="width: auto;">Atributos y Variantes</legend>
                    @include('admin.forms.article_attributes')
                </div>
                <!-- .panel-body -->


                <div class="panel-body form-horizontal">
                    <legend>Inventario</legend>
                    @include('admin.forms.article_stock')
                </div>
                <!-- .panel-body -->


                <div class="panel-body">

                    <legend>Carga una imagen</legend>
                    {{--<p class="text-muted">Distinge a tus articulos....</p>--}}

                    @include('admin.forms.upload_image')
                </div>
                <!-- .panel-body -->
            </section>
            <!-- .panel -->

            {!! Form::submit('send', ['id' => 'button-save', 'class' => 'hidden']) !!}
            {!! Form::close() !!}


        </div>
        <!-- .row -->
    </section>
    <!-- .col -->




@endsection

@section('extra_scriptBody')

    @include('admin.forms.new_caracteristic_modal')


    @include('partials.select2_script')
    <script>
        $(function () {
            $('.select2').select2({
                tags: true,
                maximumSelectionLength: 1,
                minimumResultsForSearch: Infinity,
                multiple: true,
                allowClear: true
            });
        });


        //         	$(document).ready(function() {
        //             // run test on initial page load
        //             checkSize();
        //
        //             // run test on resize of the window
        //             $(window).resize(checkSize);
        //         });
        //
        //         	function checkSize(){
        //         		if (window.matchMedia("(min-width: 1024px)").matches) {
        //         			$("select").select2();
        //         		} else {
        //         			$("select").select2("destroy");
        //         		}
        //         	}
    </script>
{{--    <script src="{!! asset('assets/js/variants2.js') !!}"></script>--}}
    {{--    <script src="{!! asset('assets/js/warehouse.js') !!}"></script>--}}
{{--    <script src="{!! asset('assets/js/attributes.js') !!}"></script>--}}

    @include('admin.forms.variant_script')

    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
    {{--    <script src="{!! asset('assets/js/demo_insert_after_node.js') !!}"></script>--}}
    <script>
        $(document).ready(function () {

            var form = $('#article_create');
          
            $(form).sendToAjax({
               
                method: $(form)[0].method,
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                success: function (response) {

                    if (response.redirectTo) {
                        var route = '{!! route('article.show', ':ARTICLE_ID') !!}';
                        location.href =  route.replace(':ARTICLE_ID', response.redirectTo);
                    }
                }
            });
        });
    </script>


    <script>
        // Confirmar que hubo algun cambio  en la tabla
        $(document).delegate('#tbl-atrributes tbody', 'DOMSubtreeModified', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

            rows.forEach(function (items, key) {

                var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

                elements.forEach(function (item) {

                    var nameAttr = item.getAttribute('name');

                    var split = nameAttr.split("]");

                    // var split_2 = nameAttr.split("[")
                    var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + '][]';

                    // change attribute name
                    item.setAttribute('name', newName);
                });
            });
        });
    </script>



    <script src="{!! asset('assets/js/demo_textarea_handler.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $('#long_description').textAreaHandler();
        })
    </script>
@endsection



@section('header')

    <div class="navbar">
        <div class="container-fluid">

            <div class="navbar-btn">

                <a href="{!! URL::previous() !!}" class="btn">
                    <i class="material-icons md-18">&#xE5C4;</i><span class="hidden-xs">Atras</span>
                </a>

                <button type="button" id="btnSubmit" class="btn"
                        onclick="document.getElementById('button-save').click();">
                    <i class="material-icons md-18">save</i>Guardar
                </button>

                <a href="{!! route('import.excel') !!}" class="btn pull-right">
                    <i class="material-icons md-18">&#xE2C6;</i>Agregar desde excel
                </a>

                <a href="{!! route('article.index') !!}" class="btn pull-right">
                    <i class="material-icons md-18">list</i>Ir a la lista
                </a>

            </div>
        </div>
    </div>

@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('article.index') !!}">{!! trans('app.attributes.articles') !!}</a></li>
        <li class="active">Nuevo Artículo</li>
    </ol>
@endsection