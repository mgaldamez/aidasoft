@extends('layout_principal')

@section('container')


    <div class="panel">
        <div class="panel-body">

            <p class="text-muted">Puedes asginar a tu articulo atributos como (color, talla, ...)</p>


            <table id="tbl-atrributes" class="table table-condensed table-bordered table-form text-middle">
                <tbody>

                @if(isset($variants))
                    @foreach($variants as $key => $variant)
                    <tr>
                        <td>
                            <div class="">
                                    <div class="form-inline">
                                    <div class="form-group col-xs-12 col-sm-4" style="margin:0px; padding-right:3px;">
                                        <select name="attribute[][id]" class="form-control attr">' + attributes +
                                            </select>

                                        {!! Form::select('attribute', ) !!}
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-8" style="margin:0px; padding-right:3px;">
                                        <select name="attribute[][value][]"
                                                class="form-control attr-values name_handler"
                                                ultiple="multiple"></select>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td style="padding-left:8px; padding-right:8px;" width="20">
                            <div class="form-group" style="margin:0px;">
                                <a href="#" class="btn btn-xs btn-danger" role="_delete" data-toggle="tooltip"
                                   data-placement="top" itle="Quitar el atributo"><i
                                            class="material-icons md-18">clear</i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @endif

                </tbody>
            </table>


            <button type="button" id="add_atribute" class="btn btn-sm btn-link" data-toggle="tooltip"
                    data-placement="top"
                    title="Agrega un atributo al artículo"><i class="material-icons">add</i>Atributo
            </button>

            <hr>

            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="check-variants" name="check-variants"> Artículo con variantes
                    </label>
                </div>

                <p class="help-block">Selecciona esta opción si el articulo tiene mas de una variante.</p>
            </div>

            <p class="alert alert-info">
                <i class="material-icons md-18">info_outline</i>
                <small>
                    Puedes generar hasta 50 variantes a la vez.
                    <br>Si se llegara a sobrepasar esta cantidad el sistema sólo generará las 50 primeras de forma
                    automatica.
                </small>
            </p>
        </div>
    </div>


@endsection


@section('extra_scriptBody')
    <script src="{!! asset('assets/js/variants2.js') !!}"></script>
    <script src="{!! asset('assets/js/attributes.js') !!}"></script>

    @include('partials.select2_script')
    <script>
        $(function () {
            $('.select2').select2({
                tags: true,
                maximumSelectionLength: 1,
                minimumResultsForSearch: Infinity,
                multiple: true,
                allowClear: true
            });
        });
    </script>
@endsection