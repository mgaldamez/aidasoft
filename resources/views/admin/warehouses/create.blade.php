@extends('layout_principal')

@section('container')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <ol class="breadcrumb">
                        <li><a href="{!! url('/') !!}">Inicio</a></li>
                        <li><a href="{!! route('admin.warehouse.index') !!}">Bodegas</a></li>
                        <li class="active">Nueva Bodega</li>
                    </ol>

                    <div class="navbar-btn bg-default">


                        <a href="{!! URL::previous() !!}" class="btn mdb">
                            <!-- arrow back -->
                            <i class="material-icons md-18">&#xE5C4;</i>Atras
                        </a>


                        <button type="button" class="btn btn-primary mdb"
                                onclick="document.getElementById('form-submit').click();">
                            <i class="material-icons md-18">save</i> Guardar
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10 col-md-offset-1">

        <div class="panel">

            <div class="panel-heading">
            </div>


            <div class="panel-body">

                {!! Form::open(['route' => ['admin.warehouse.store'], 'method' => 'POST', 'id' => 'form-warehouse-create', 'class' => 'form-horizontal']) !!}

                @include('admin.warehouses.partials.save')

                {!! Form::submit('Send', ['id' => 'form-submit', 'class' => 'hidden']) !!}
                {!! Form::close() !!}

            </div>
        </div>

    </div>


@endsection


@section('extra_scriptBody')


    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
    <script>
        $(document).ready(function () {
            var form = $('#form-warehouse-create');
            $(form).sendToAjax({
                method: 'POST',
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                success: function (response) {
                    console.log(response);
                    $(form).reset;
                    location.href = '{!! URL::previous() !!}';
                }
            });
        });
    </script>


    @include('partials.select2_script')
    <script>
        $(document).ready(function () {
            $('#sucursal').select2({
                placeholder: 'Sucursal'
            });
            $('#users').select2({
                placeholder: 'Asignar usuarios a esta bodega',
//                tags: true,
                tokenSeparators: [',', ' '],
                allowClear: true,
                multiple: true,
//                templateResult: formatState
            });
//            function formatState (state) {
//
//                var $state = $(
//                    '<span><img src="http://lorempixel.com/30/30/people/"'+ Math.floor((Math.random() * 10) + 1) +' class="img-circle" /> ' + state.text + '</span>'
//                );
//                return $state;
//            };
        });
    </script>

@endsection