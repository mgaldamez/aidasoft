@extends('layout_principal')

@section('container')





    <div class="col-md-14" >

        <div class="navbar" style="background-color: transparent">
            <div class="container-fluid">

                <div class="navbar-form navbar-right">
                    @include('partials.dataTables_filter', ['table' => 'table-warehouse'])
                </div>


                <ol class="breadcrumb">
                    <li><a href="{!! url('/') !!}">Inicio</a></li>
                    <li class="active">Bodegas</li>
                </ol>

                <div class="navbar-btn">
                    <a href="{!! route('admin.warehouse.create') !!}" class="btn">
                        <i class="material-icons">&#xE145;</i>Nueva Bodega
                    </a>
                </div>
            </div>
        </div>
    </div>






    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">

                <table class="table table-condensed table-bordered text-middle small" id="table-warehouses">
                    <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Sucursal</th>
                        <th>Región</th>
                        <th>Usuario</th>
                        <th>Activo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @foreach($warehouses as $key => $warehouse)
                        <tr class="{!! !$warehouse->active ? 'inactive' : '' !!}">
                            <td class="warehouse">
                                <a href="{!! route('warehouse.show', $warehouse->id ) !!}">{!! $warehouse->description !!}
                                </a>

                                @if($warehouse->main)
                                    <i class="material-icons md-18" style="color:#ffc107;"
                                       title="Bodega Principal">star</i>
                                @endif
                            </td>

                            <td class="sucursal">
                                {!! $warehouse->sucursal->DESCR_SUCURSAL !!}
                            </td>

                            <td>{!! $warehouse->sucursal->region->DESCR_REGION !!}</td>

                            <td class="user">
                                @foreach( $warehouse->usersActive as $user )

                                    <span hidden>{!! $user->fullName !!}</span>

                                    @include('partials.contact-chip',  [
                                           'role' => $user->role->description,
                                           'shortName' => $user->nameProfile,
                                           'mini' => true
                                           ])

                                @endforeach
                            </td>

                            <td width="20">
                                <span hidden>{!! $warehouse->active ? 'active' : 'inactive' !!}</span>


                                <div class="togglebutton">
                                    <label>
                                        <input type="checkbox" name="active" data-warehouse="{!! $warehouse->id !!}"
                                               @if($warehouse->active)
                                               checked="checked"
                                                @endif>
                                    </label>
                                </div>
                            </td>

                            <td width="20">
                                <a href="{!! route('admin.warehouse.edit', $warehouse->id) !!}" class="btn btn-xs">
                                    <!-- mode edit -->
                                    <i class="material-icons md-18">&#xE254;</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <!--.panel-->
    </div>


    {!! Form::open(['route' => ['admin.warehouse.active', ':WAREHOUSE_ID'], 'method' => 'PUT', 'id' => 'form-warehouse-active']) !!}
    {!! Form::close() !!}
@endsection


@section('extra_scriptBody')
    @include('admin.warehouses.partials.modal')

    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#table-warehouses').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ],
                "order": []
            });
        });
    </script>


    <script>
        $('#table-warehouses').on('change', 'input[name="active"]', function (event) {

            var button = $(event.target);
            var warehouse_id = button.attr('data-warehouse');
            var form = $('#form-warehouse-active');
            var route = $(form)[0].action.replace(':WAREHOUSE_ID', warehouse_id);

            var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));

            alerts.forEach(function (item) {
                item.remove();
            });

            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': $(form).find('input[name="_token"]').val()},
                type: $(form).find('input[name="_method"]').val(),
                data: button,
                dataType: 'json',
                success: function (response) {
                    console.log(response);

                    $(button.closest('tr')).toggleClass('inactive');


                    var body = document.getElementsByClassName('container-wrapper')[0];
                    body.insertAdjacentHTML('afterbegin', response.alert);
                },
                error: function (errors) {
                    console.log(errors)
                }
            });
        });
    </script>
@endsection