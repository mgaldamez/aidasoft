<p class="text-muted">Puedes asginar a tu articulo atributos como (color, talla, ...)</p>


<table id="tbl-atrributes" class="table table-condensed table-bordered table-form text-middle">
    <tbody>
    </tbody>
</table>


{{-- @if(isset($attributeValues))
<table class="table">
	<tbody>

		@foreach(  $attributeValues as $value )

		<tr>

			<td>

				{!!  $value->attribute->description  !!}

			</td>
			<td>
				{!! Form::select('attribute_value_id',  \TradeMarketing\Models\AttributeValue::where('attribute_id', $value->attribute->id )->lists('description', 'id'),

					$value->description  

				, ['class' => 'form-control']) !!}
				
			</td>

		</tr>
		@endforeach

	</tbody>
</table>

@endif --}}


<button type="button" id="add_atribute" class="btn btn-sm btn-link" data-toggle="tooltip" data-placement="top"
        title="Agrega un atributo al artículo">
    <i class="material-icons">add</i>Atributo
</button>

<hr>

{{--<div class="pull-right">

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-app" data-toggle="modal" data-target="#variantsModal" id="btn-show-variants">
        <span id="number-variants" class="badge"></span> <span class="glyphicon glyphicon-random"></span>Variantes
    </button>
</div>--}}


<div class="form-group">

    <div class="checkbox ">
        <label>
            <input type="checkbox" id="check-variants" name="check-variants"  checked="checked"> Artículo con variantes
        </label>
    </div>

    <p class="help-block">Selecciona esta opción si el artículo tiene mas de una variante.</p>

</div>


<p class="alert alert-info">
    <i class="material-icons md-18">info_outline</i>
    <small>
        Puedes generar hasta 50 variantes a la vez.
        <br>Si se llegara a sobrepasar esta cantidad el sistema sólo generará las 50 primeras de forma automatica.
    </small>

</p>

<!-- Modal -->
{{--<div class="modal fade" id="variantsModal" tabindex="-1" role="dialog">--}}

{{--<div class="modal-dialog modal-lg" role="document">--}}

{{--<div class="modal-content">--}}

{{--<div class="modal-header">--}}
{{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
{{--aria-hidden="true">&times;</span></button>--}}
{{--<h4 class="modal-title" id="myModalLabel">Variantes</h4>--}}
{{--<p class="help-block">Puedes editar los nombres de la variantes</p>--}}
{{--</div>--}}

{{--<div class="modal-body">--}}

{{--<table id="show-variants" class="table table-condensed table-striped text-middle">--}}
{{--<thead>--}}
{{--<tr class="text-uppercase">--}}
{{--<th>Descripción</th>--}}
{{--<th></th>--}}
{{--<th>Atributos</th>--}}
{{--<th></th>--}}
{{--<th>Activo</th>--}}
{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}

{{--</tbody>--}}
{{--</table>--}}

{{--</div>--}}

{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>--}}
{{--<button type="button" class="btn btn-primary mdb" data-dismiss="modal">Aceptar</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}







