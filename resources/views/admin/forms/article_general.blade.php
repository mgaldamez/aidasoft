<div class="col-sm-6">

    <div class="form-group @if($errors->has('name')) has-error @endif">

        {!!Form::label('name', 'Nombre (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('name', old('name'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'Ejm: Articulo', 'autofocus'])!!}


            {!! $errors->first('name', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Agrega el nombre del artículo; ejm: </p>
        </div>
    </div>
    <!--.form-group -->
	

    <div class="form-group @if($errors->has('category_id')) has-error @endif">

        {!!Form::label('category_id', 'Categoria (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::select('category_id', $categoryList, old('category_id'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}

            {!! $errors->first('category_id', '<p class="text-danger">:message</p>')  !!}
            <br>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('brand_id')) has-error @endif">

        {!!Form::label('brand_id', 'Marca (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::select('brand_id', $brandList, old('brand_id'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}


            {!! $errors->first('brand_id', '<p class="text-danger">:message</p>')  !!}

            <br>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('measurement_unit_id')) has-error @endif">

        {!!Form::label('measurement_unit_id', 'Unidad de medida (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">

            <div class="form-control" disabled="true">{!! (old('measurement_unit_id')) ? $measurementUnitList[old('measurement_unit_id')] : $measurementUnitList[1]  !!}</div>

            {!!Form::select('measurement_unit_id', $measurementUnitList, old('measurement_unit_id') | 1, ['class' => 'form-control hidden',])!!}

            {!! $errors->first('measurement_unit_id', '<p class="text-danger">:message</p>')  !!}
        </div>
    </div>
    <!--.form-group -->


</div>
<!-- .col -->


<div class="col-sm-6">

    <div class="form-group @if($errors->has('barcode')) has-error @endif">

        {!!Form::label('barcode', 'Código de barras', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('barcode', old('barcode'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('barcode', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Agrega el codigo de barras del producto.</p>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('internal_reference')) has-error @endif">

        {!!Form::label('internal_reference', trans('app.attributes.internal_reference'  ), ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('internal_reference', old('internal_reference'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('internal_reference', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Agrega un codigo interno.</p>
        </div>
    </div>
    <div class="form-group @if($errors->has('gestion')) has-error @endif">

        {!!Form::label('gestion', 'Articulo gestionado por', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::select('gestion', \TradeMarketing\tmk_tipo_gestion_articulo::listing(), old('gestion'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}


            {!! $errors->first('gestion', '<p class="text-danger">:message</p>')  !!}

            <br>
        </div>
    </div>


    <!--.form-group -->
</div>


<div class="col-sm-12">
    <div class="form-group mdb @if($errors->has('description')) has-error @endif">

        {!!Form::label('long_description', 'Descripción', ['class' => 'control-label'])!!}

        {!!Form::textarea('long_description', old('long_description'), ['class' => 'form-control', 'placeholder' => 'Detalla mas información sobre el articulo', 'rows' => '3', 'maxlength' => '500'])!!}

        <span class="form-control-feedback"><i class="material-icons md-18">mode_edit</i></span>


        {!! $errors->first('long_description', '<p class="text-danger">:message</p>')  !!}

    </div>
    <!--.form-group -->

</div>


