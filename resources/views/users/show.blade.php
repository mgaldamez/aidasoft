@extends('layout_principal')

@section('container')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li><a href="{!! url('/') !!}">Inicio</a></li>
                        @if($user->role_id < 3)
                        <li><a href="{!! route('admin.user.index') !!}">usuarios</a></li>
                          @endif
                        <li class="active">{!! $user->username !!}</li>
                    </ol>
                    
                    <div class="navbar-btn">
                    <!--SOLO APARECE EL BOTON DE EDITAR SI ES ADMIN O SUPERADMIN-->
                       @if($user->role_id < 3)
                        <a href="{!! route('user.edit', $user->id) !!}" class="btn btn-primary mdb">
                            <i class="material-icons md-18">edit</i>
                            <span class="hidden-xs"> Editar </span>

                        </a>

                        @endif
                        <!-- CAMBIAR CONTRASEÑA LE APARECERA A TODO LOS USUARIOS-->
                            <a href="{!! route('user.password', $user->id) !!}" class="btn btn-primary mdb">
                            <i class="material-icons md-18">edit</i>
                            <span class="hidden-xs"> Cambiar contraseña</span>

                        </a>



                    </div>
                 
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-10 col-md-offset-1">
        <div class="panel">
            <div class="panel-heading">
                <h2>{!! $user->fullName !!}</h2>
            </div>

            <div class="panel-body">
                <p><strong>Nombre:</strong> {!!$user->fullname !!}</p>
                <p><strong>Direccion: </strong>{!!$user->address!!}</p>
                <p><strong>Telefono: </strong>{!!$user->phone!!}</p>
                <p><strong>Email: </strong>{!!$user->email!!}</p>
                <p><strong>Rol: </strong>{!! trans('auth.roles.'.$user->role->description)!!}</p>
            </div>
        </div>
    </div>
@endsection