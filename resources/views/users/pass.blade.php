@extends('layout_principal')

@section('container')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">
                    <ol class="breadcrumb">
                        <li><a href="{!! url('/') !!}">Inicio</a></li>
                        <li><a href="{!! route('admin.user.index') !!}">usuarios</a></li>
                        <li class="active">Cambiar Contraseña</li>
                    </ol>

                    <div class="navbar-btn">
                        <a href="{!! URL::previous() !!}" class="btn mdb" >Cancelar</a>
                        <button class="btn btn-primary mdb" onclick="document.getElementById('form-user').submit()">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-10 col-sm-offset-1">

        <div class="panel">
            <div class="panel-heading">
                <p class="text-info">
                    <i class="material-icons md-18">info_outline</i> Los campos marcados con (*) son obligatorios.
                </p>
            </div>

            <div class="panel-body">


                {!! Form::model( $user, ['route' => ['admin.user.update', $user->id], 'method' => 'PUT', 'class' => 'form-horizontal form-condensed', 'id' => 'form-user']) !!}



                @include('users.password')


         

                {!! Form::close() !!}










            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col -->

@endsection