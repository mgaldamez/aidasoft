

{{-- PASSWORD --}}
<div class="form-group mdb @if ($errors->first('password') != '') has-error @endif">
    {!! Form::label('password', 'Contraseña (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off']) !!}

        {!! $errors->first('password', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- PASSWORD CONFIRM --}}
<div class="form-group mdb @if ($errors->first('password_confirmation') != '') has-error @endif">
    {!! Form::label('password_confirmation', 'Confirmar Contraseña', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off']) !!}

        {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>









<!--  ESTTOS CAMPOS SE MANTIENEN OCULTOS-->
<div class="form-group mdb @if($errors->has('tipo')) has-error @endif" style="display: none;">
    {!! Form::label('tipo', 'tipo (*)', ['class' => 'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('tipo', 'change_password', ['class' => 'form-control', 'autofocus']) !!}

        {!! $errors->first('tipo', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>



{{-- FIRST NAME --}}
<div class="form-group mdb @if($errors->has('firstname')) has-error @endif" style="display: none;">
    {!! Form::label('firstname', 'Nombre (*)', ['class' => 'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('firstname', old('firstname'), ['class' => 'form-control', 'autofocus']) !!}

        {!! $errors->first('firstname', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- LAST NAME --}}
<div class="form-group mdb @if($errors->has('lastname')) has-error @endif" style="display: none;">
    {!! Form::label('lastname', 'Apellido (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('lastname', old('lastname'), ['class' => 'form-control']) !!}

        {!! $errors->first('lastname', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>



{{-- PHONE --}}
<div class="form-group mdb @if($errors->has('phone')) has-error @endif" style="display: none;">
    {!! Form::label('phone', 'Teléfono', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}

        {!! $errors->first('phone', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- ROL --}}
<div class="form-group mdb @if($errors->has('phone')) has-error @endif" style="display: none;">
    {!! Form::label('role_id', 'role_id', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('role_id', old('role_id'), ['class' => 'form-control']) !!}

        {!! $errors->first('role_id', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>



{{-- ADDRESS --}}
<div class="form-group mdb @if($errors->has('address')) has-error @endif" style="display: none;">
    {!! Form::label('address', 'Dirección', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}

        {!! $errors->first('address', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- EMAIL   --}}
<div class="form-group mdb @if($errors->has('email')) has-error @endif" style="display: none;">
    {!! Form::label('email', 'Correo Electrónico (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}

        {!! $errors->first('email', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>
