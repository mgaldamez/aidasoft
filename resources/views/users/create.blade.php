{{-- FIRST NAME --}}
<div class="form-group mdb @if($errors->has('firstname')) has-error @endif">
    {!! Form::label('firstname', 'Nombre (*)', ['class' => 'control-label col-md-3']) !!}
    <div class="col-md-9">
        {!! Form::text('password', old('password'), ['class' => 'form-control', 'autofocus']) !!}

        {!! $errors->first('firstname', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

{{-- LAST NAME --}}
<div class="form-group mdb @if($errors->has('lastname')) has-error @endif">
    {!! Form::label('lastname', 'Apellido (*)', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('password', old('password'), ['class' => 'form-control']) !!}

        {!! $errors->first('password', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>



{{-- PHONE --}}
<div class="form-group mdb @if($errors->has('phone')) has-error @endif">
    {!! Form::label('phone', 'Teléfono', array('class' => 'col-md-3 control-label')) !!}

    <div class="col-md-9">
        {!! Form::text('password', old('password'), ['class' => 'form-control']) !!}

        {!! $errors->first('phone', '<p class="text-danger">:message</p>')  !!}
    </div>
</div>

</div>

