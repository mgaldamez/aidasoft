@extends('layout_principal')


@section('extra_scriptHead')


    <!-- Bootstrap DatePiker Pluging CSS -->
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">

    <!-- Bootstrap DatePiker Pluging JS -->
    <script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>




@include('partials.dataTables_script')

@endsection


@section('container')


    <section class="col-md-12">

        <div class="page-header">
            <h3>Inventario</h3>
        </div>

        {!! Form::model(Request::all(), [ 'url' => '/inventory', 'role' => 'search', 'method' => 'GET']) !!}

        <div class="form-inline">

            <div class="input-group" style="width: 100%">

                {!! Form::select('filterBy', config('enums.searchFilter'), null, [ 'class' => 'form-control', 'placeholder' => 'Filtrar por']) !!}
                {!! Form::select('filter', [], null, [ 'class' => 'form-control', 'placeholder' =>'Debe seleccionar un filtro']) !!}


                {!! Form::submit('Filtrar', ['class' => 'btn btn-primary btn-raised mdb']) !!}
            </div>


        </div>

        {!! Form::close() !!}

    </section>



    <section class="col-md-12">
        <div class="panel">
            <table class="table table-bordered table-condensed text-middle">
                <thead>
                <tr class="text-uppercase">
                    <th>Articulo</th>
                    <th>Min Stock</th>
                    <th>Max Stock</th>
                    <th>Stock</th>
                    <th>Fecha</th>
                </tr>
                </thead>
                <tbody id="inventory-data">

                @include('inventories.partials.inventoryRender')

                </tbody>
            </table>
        </div>
    </section>


@endsection


@section('extra_scriptBody')
    <script>
        $(function () {
            $('input.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoClose: true,
                title: 'hola',
                todayHighlight: true,
                defaultViewDate: 'today',
                clearBtn: true
            });
        });
    </script>

    <script src="{!! asset('assets/js/inventory_filter.js') !!}"></script>

@endsection