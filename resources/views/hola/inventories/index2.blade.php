@extends('layout_principal')
@section('extra_scriptHead')
<!-- css DataTables-1.10.11-->
<link rel="stylesheet" href="{{ asset('dataTables/1.10.11/css/dataTables.bootstrap.min.css')}}" >
<!--
<link rel="stylesheet" href="https://cdn.datatables.net/colreorder/1.3.1/css/colReorder.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.1.1/css/rowReorder.dataTables.min.css">
-->
<!--js Chart JS-->
<script src="{{ asset('Chart.js/2.0-dev/Chart.min.js')}}"></script>
@endsection 

@section('container')


<div class="col-md-12">
    <div class="row-sm">

        <div class="panel panel-default">
            <div class="panel-body">
                <h1>Inventario</h1>  

                <div class="col-md-12">

                    <div class="row">

                        <div class="col-sm-8 col-md-9">

                            <div class="row">
                                <canvas id="myChart" style="width:100%; max-height:300px; min-height:250px"></canvas>
                            </div>
                        </div>

                        <div id="js-legend" class="col-sm-4 col-md-3 chart-legend"></div>

                    </div>
                </div>
            </div>

        </div>

    </div>

</div>


<div class="col-md-12">

    <div class="row-sm">

        <div class="panel">
            <div class="panel-body">

                <table class="table">            
                    <thead>
                        <tr class="text-uppercase">
                            <th>Articulo</th>
                            <th>Inicial</th>
                            <th>Actual</th>
                            <th>Min</th>
                            <th>Max</th>
                            <th>Ultimo Movimiento</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach( $inventory as $inventory )
                        <tr>
                            <td>
                                <div  class="media-content">
                                    {{--@if( $inventory->image == null )--}}
                                    {{--<img src="{{ asset('assets/img/trade.jpg') }}"> --}}
                                    {{--@else --}}
                                    {{--<img src="{{ asset('assets/img/'.$inventory->image ) }}">   --}}
                                    {{--@endif--}}

                                    <div class="body">

                                        <a href="article/{{ $inventory->article_id }}" class="text-uppercase"><strong>{{ $inventory->barcode }}</strong></a>
                                        <br>
                                        <a href="/article/{{ $inventory->article_id }}"><span class="title">{{ $inventory->article }}</span></a>
                                    </div>

                                </div>
                            </td>
                            <td>{{ $inventory->initial }}</td>
                            <td>{{ $inventory->stock }}</td>
                            <td>{{ $inventory->min_stock }} </td>
                            <td>{{ $inventory->max_stock }}</td>
                            <td>
                                <?= date_format( date_create( $inventory->last_update ) , 'd M Y'); 
                                ?>       
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </table>

            </div>
        </div>
    </div>
</div>


<div class="col-xs-12 col-sm-12 col-md-8">
    <div class="row-sm">
        <div class="panel x-panel">
            <div class="panel-heading">

                <div class="col-xs-6">
                   <h3>Inventario</h3>
               </div>

               <div class="col-xs-6">
                <a class="btn btn-success" type="submit" href="/inventory/create">
                    <span class="glyphicon glyphicon-plus"></span>
                    <span class="hidden-xs"> Nuevo </span>Inventario
                </a>  
            </div>

        </div>
        <!--.panel-heading-->

        <div class="panel-body">
            <div class="row-sm">

                <div class="table-responsive">
                    <table id="" class="table table-hover"  width="100%" >

                        <thead>
                            <tr class="">
                                <th class="">#</th>                           
                                <th class="text-nowrap">Fecha</th>
                                <th class="text-nowrap">Cant. Articulos</th>
                                <th class="text-nowrap">Monto</th>
                                <th class="text-nowrap">Opciones</th>
                            </tr>
                        </thead>  

                        <tbody>
                            @foreach($inventories as $key => $inventories  )
                            <tr>
                                <td class="text-right active">{{$key+1}}</td>
                                <td class="">{{$inventories->date}}</td>
                                <td class="text-right">{{$inventories->quantity_items}}</td>
                                <td class="text-right">{{$inventories->amount}}</td>
                                <td class="">
                                    <ul class="btn-option-table">
                                        <li class="show" >
                                            <a href="/inventory/{{$inventories->id}}" role="button">
                                                <span class="glyphicon glyphicon-share"></span>
                                            </a>
                                        </li>
                                        <li class="edit">
                                            <a href=""  role="button">
                                                <span class="glyphicon glyphicon-edit"></span>
                                            </a>
                                        </li>
                                        <li class="delete">
                                            <a href=""  role="button">
                                                <span class="glyphicon glyphicon-trash"></span>
                                            </a>
                                        </li>
                                    </ul>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    <!--table-->
                </div>

            </div>

        </div>      
        <!--.panel-body-->

    </div>
    <!--.panel-->
</div>
<!--.row-->
</div>
<!--.col-->

@endsection

@section('extra_scriptBody')
<!-- js DataTables-1.10.11-->
<script src="{{ asset('dataTables/1.10.11/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('dataTables/1.10.11/js/dataTables.bootstrap.min.js')}}"></script>
<!--
<script src="https://cdn.datatables.net/colreorder/1.3.1/js/dataTables.colReorder.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.1.1/js/dataTables.rowReorder.min.js"></script>
-->
<script src="{{ asset('assets/js/dataTable_config.js')}}"></script>
<script src="{{ asset('assets/js/demo_convert_to_rgb.js')}}"></script>
<script src="{{ asset('assets/js/charts/amount_bodega.js')}}"></script>

<script type="text/javascript">

    $(document).ready(function(){
        dataTableConfig();

        traer_data();
    });

</script>
@endsection