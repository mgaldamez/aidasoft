@extends('layout_principal')
@section('extra_scriptHead')

<script src="{{ asset('assets/js/tabla.js')}}"></script>
@endsection 

@section('container')
<div class="col-xs-12 col-sm-12 col-md-12">    
<div class="row-sm">     
<div class="panel x-panel">
   <div class="panel-heading">
        <h2><small> Nuevo Inventario </small> #007612</h2>
    </div><!--.panel-heading-->

    <div class="panel-body">
       <div class="col-md-6">
       <div class="row">
              <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

             
                <div class="form-group">
                    <label for="brand">Seleccione una Bodega</label>
                     <select id="bodega" class="bodega form-control" name="bodega"></select>
                    <span id="bodegaHelpBlock" class="help-block"></span>
                </div><!--.form-group -->
                
                <button type="button" id="load-tabla" class="btn btn-success btn-lg">Crear</button>
        </div><!--.row-->
        </div><!--.col--> 
    </div><!--.panel-body-->
</div><!--.panel-->
</div><!--.row-->
</div><!--.col-->

<div class="col-xs-12 col-md-12" >
    <div class="row-sm">
        <div class="panel x-panel">
            <div class="panel-body">
               <section>

                <!-- Table row -->
                <div class="row">
                  <div class="col-xs-12">
                  <div class="row-sm">
                    <div class="table-responsive">
                    
                     <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                     <table id="table-inventary"  class="table">
                      <thead>
                        <tr>
                          <th class="text-nowrap">Codigo de barras</th>
                          <th class="text-nowrap">Articulo</th>
                          <th class="text-nowrap">Descripcion</th>
                          <th class="text-nowrap">Costo/U</th>
                          <th class="text-nowrap">Cant. Actual</th>
                          <th class="text-nowrap">Stock Actual</th>
                          <th class="text-nowrap">Cant. Comprometidas</th>
                          <th class="text-nowrap">Cant. Saldo</th>
                          <th class="text-nowrap">Subtotal</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr style="background-color: rgba(52,73,94,0.80); color: #FFFFFF;">
                          <th class="text-nowrap" colspan="3" >TOTAL</th>              
                          <th class="text-right">$0.00</th>
                          <th class="text-right">00</th>
                          <th class="text-right">$0.00</th>
                          <th id="articulos" class="text-right">00</th>
                          <th class="text-right">00</th>
                          <th id="total" class="text-right">$0.00</th>
                        </tr>
                      </tfoot>
                    </table> 
                    <button id="save" class="btn btn-warning">
                        Enviar
                    </button>

                   </div><!--.table-responsive-->
                  </div><!--.row-sm-->
                               
                  </div><!--.col -->
                </div><!--.row -->
       
              </section><!--.content -->
            </div><!--.panel-body-->
        </div>
    </div>
</div>

@endsection



@section('extra_scriptBody')
<script src="{{ asset('assets/js/load_select_rgtr_article.js')}}"></script>
<script src="{{ asset('table-to-json/0.11.1-3/jquery.tabletojson.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
    bodegas('bodega');    
});
</script>

<script type="text/javascript">    
$('#load-tabla').click(function (e) {

    e.preventDefault;

    $("#table-inventary tbody tr").remove(); // borrar los datos dentro de  la tabla

    var bodega = $('#bodega').val();
    var route = '/inventory/bodega/' + bodega;
    var datos = $('table tbody');

    $.get(route, function (res) {
        $(res).each(function (key, value) {
            datos.append("<tr class='dato'>"+
                         "<td class='text-uppercase'><dt>"+ value.barcode +"</dt></td>"+
                         "<td><a href='/"+ value.id +"'>"+ value.article +"</a>"+
                             "<input type='text' name='_article[]' value='"+ value.id +"' hidden>"+
                             "<input type='text' name='_bodega[]' value='"+ value.idbodega +"' hidden>"+
                         "</td>"+
                         "<td> Lorem ipsum dolor sit amet, consectetur adipisicing elit </td>"+
                         "<td class='text-right' id='costou'>"+ value.cost +"</td>"+
                         "<td class='text-right info'>"+ value.cant +"</td>"+
                         "<td class='text-right info'>"+ value.subtotal +"</td>"+
//                         "<td class='text-right info'><input type='number' class='form-control input-sm text-right'  style='width:80% !important; margin: auto auto'/></td>"+
                         "<td class='text-right form-group active' id='comprometidas'>0</td>"+
                         "<td class='text-right'>0.00</td>"+
                         "<td class='text-right' id='sub_total'>0.00</td>"+
                         "</tr>")
        });

        iniciarTabla(); // iniciar la funcion para cargar los input en la tabla
        sumar();
    });
});
    
$('#save').click(function(){

    var DATA 	= [], // array principal 
        TABLA 	= $("#table-inventary tbody tr"); // tabla

    // recorre todas las filas "TR" de la tabla
    TABLA.each(function(){
        // se extraen los datos necesarios por cada fila "TR"
        var article = $(this).find("input[name='_article[]']").val(),
            bodega  = $(this).find("input[name='_bodega[]']").val(),
            compr  = $(this).find("td[id='comprometidas']").text(),
            costou  = $(this).find("td[id='costou']").text(),
            subtotal  = $(this).find("td[id='sub_total']").text();

        // declaro un array para almacenar los items obtenidos estos datos son remplazados en cada iteracion
        item = {};
        item ["article"] 	= article;
        item ["bodega"] 	= bodega;
        item ["compro"] 	= compr;
        item ["costou"] 	= costou;
        item ["subtotal"] 	= subtotal;
        
        // cargo los datos del array en un el  array principal que almacenara los datos de forma permanente
        DATA.push(item);
    });

    var data = JSON.stringify(DATA), // convierto el array principal en un objeto JSON
        articulos = $('#articulos').text(), // total de articulos en el inventario
        total = $('#total').text(), // monto total del inventario
        route = '/inventory',
        token = $('#token').val();  
    
    $.ajax({
        url: route,
        headers: { 'X-CSRF-TOKEN': token },
        type: 'POST',
        dataType: 'json',
        data: {
            data: data,
            articulos: articulos,
            total: total
        }, 
        success: function(res){
            console.log(res.message);
            window.location.href = "/inventory/"+res.inventario;
        },
        error: function(res){
            console.log(res.message);
        }
    });
});
    
</script>
@endsection
