<table id="table-movements" class="table table-condensed table-bordered small">
    <thead>
    <tr>
        <th>{!! trans('app.attributes.date') !!}</th>
        <th>{!! trans('app.attributes.movement') !!}</th>
        <th>{!! trans('app.attributes.reference') !!}</th>
        <th>{!! trans('app.attributes.warehouse') !!}</th>
        <th class="hidden-xs">{!! trans('app.attributes.article') !!}</th>
        <th class="hidden-xs">{!! trans('app.attributes.measurement_unit') !!}</th>
        <th class="hidden-xs">{!! trans('app.attributes.quantity') !!}</th>
        <th class="cell-unit-cost hidden-xs">{!! trans('app.attributes.unit_cost') !!}</th>
    </tr>
    </thead>
    <tbody class="text-uppercase">
    @foreach( $movements as $movement)

        <tr>
            <td title="{!! $movement->created_at !!}" data-sort="{!! $movement->sortCreatedAt() !!}">
                {!! $movement->createdAt() !!}
            </td>

            <td>
                @if($movement->movement_type == 'entry')
                    <span class="label label-success">{!! trans('app.attributes.'. $movement->movement_type) !!}</span>
                @else
                    <span class="label label-warning">{!! trans('app.attributes.'. $movement->movement_type) !!}</span>
                @endif
                <br>{!! $movement->movement !!}
            </td>

            <td>
                @if( $movement->reference_id)
                    <a href="{!! route($movement->movement_tag.'.show', $movement->reference_id) !!}"
                       title="{!! $movement->reference !!}">
                        {!! $movement->reference !!}
                    </a>
                @endif
            </td>

            <td>{!! $movement->warehouse !!}
            </td>

            <td class="hidden-xs">
                <a href="{!! route('article.show', $movement->article_master_id) !!}">
                    {{ $movement->article }}
                </a><br>
                <span>{{ $movement->internal_reference }}</span>
            </td>

            <td class="hidden-xs">{!! $movement->unity !!}
            </td>

            @if($movement->movement_type == 'entry')

                <td class="hidden-xs" title="{!! trans('app.attributes.quantity_entry') !!}">
                    {!! $movement->quantity !!}
                </td>
            @else
                <td class="hidden-xs" title="{!! trans('app.attributes.quantity_exit') !!}">
                    - {!!  $movement->quantity_exit !!}
                </td>
            @endif

            <td class="cell-unit-cost hidden-xs">{{ $movement->unit_cost }}
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
