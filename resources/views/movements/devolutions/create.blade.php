@extends('layout_principal')


@section('extra_scriptHead')
<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">
<!-- Bootstrap DatePiker Pluging JS -->
<script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>

<!-- inpput in table -->
<style type="text/css">
	table.table tbody tr td input.form-control{
		border: 2px solid rgba(0,0,0, .54);
		background-image: none !important;
		padding: 6px 12px !important;
		    border-radius: 2px !important;
	}
	table.table tbody tr td .form-group.is-focused input.form-control{
		background-image: none !important;
		outline: 3px !important;
	}
</style>
@endsection


@section('container')

@include('movements.layout_movement')


<div class="col-md-8">
	<div class="panel">
		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@foreach( $invoice as $invoice )
					<tr>
						<td>{{ $invoice->date }}</td>
						<td>{{ $invoice->invoice_code }}</td>	
						<td>{{ $invoice->unit_cost }}</td>
						<td>{{ $invoice->total }}</td>
						<td>{{ $invoice->count_article }}</td>
						<td>{{ $invoice->count_real }}</td>
						<td>{{ $invoice->company }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>
</div>




<div class="col-md-12">
	<div class="row-sm">
		<form method="POST" action="/devolution/purchase">

		<input type="hidden" id="token" name="_token"  value="{{ csrf_token() }}">
		<input type="hidden" id="invoice" name="invoice" value="1">
		<input type="hidden" id="bodega" name="bodega" value="8">
		<input type="hidden" id="movement" name="movement" value="23">
			<div class="panel">
				<div class="panel-body">

					<div class="col-md-4">
						<div class="row">

							<div class="form-group">
								<label class="control-label sr-only" for="document">Documento</label>
								<input type="text" id="document" class="form-control" name="document" placeholder="Documento" value="{{ old('document') }}">
							</div>

							<div class="form-group">
								<label class="control-label sr-only" for="date">Fecha</label>
								<input type="text" id="date" class="form-control datepicker" name="date" placeholder="Fecha" value="{{ old('date') }}">
							</div>

							<div class="form-group">
								<label class="control-label sr-only" for="description">Descripción</label>
								<textarea id="description" class="form-control" name="description" placeholder="Descripción" value="{{ old('description') }}"></textarea>
							</div>
						</div>
					</div>


					<div class="col-md-12">
						<div class="row">
							<br>
							<br>
							<h3>Articulos</h3>
							<div class="row-sm">
								<table id="tbl-devolution" class="table table-condensed">
									<thead>
										<tr>
											<th>Articulo</th>
											<th>Recibido</th>
											<th>Devuelto</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>

				<div class="panel-footer">
					<button type="submit" class="btn btn-success btn-raised">Confirmar</button>
				</div>
			</div>
		</form>
	</div>
</div>	

@endsection

@section('extra_scriptBody')

<script type="text/javascript">
var invoice = 1;	

var table = $('#tbl-devolution.table> tbody');

$(document).ready( function(){
	$.get( '/invoice/article/'+invoice, function(res){
		$(res).each( function(key, value){
			table.append(
				'<tr>'+
				'<td>'+ 
				'<div class="checkbox" style="margin:0">'+
				'<label>'+
				'<input type="checkbox" id="check'+key+'"> '+ value.name +
				'<input type="number" name="check[]" class="hidden" value="0">'+
				'<input type="text" class="hidden" name="article[]" value="'+ value.id +'">'+
				'<input type="text" class="hidden" name="reference[]" value="'+ value.article +'">'+
				'</label>'+
				'</div>'+
				'</td>'+
				'<td>'+ value.quantity +'</td>'+
				'<td>'+
				'<div id="check'+key+'" class="form-group hidden" style="margin:0; padding:0; max-width: 100px">'+
				'<input type="text" class="form-control input-sm" name="devolution[]">'+ 
				'</div>'+
				'</td>'+
				'</tr>');
		})

		$.material.init();
	})
})
</script>

<script type="text/javascript">
	$(document).delegate( 'input[type="checkbox"]', 'click', function( event ){
		var checkbox = $(this);
		var checkboxId = checkbox.attr('id');
		var parentContainer =  $('table tbody td div#'+checkboxId+'');
		var input = parentContainer.children()[0];
		var brotherInput =  checkbox.siblings('input[name="check[]"]')[0];
		var row = event.target.closest('tr')

		if( checkbox.is(':checked') ) {
			brotherInput.value = 1;
			input.value = 1;
			parentContainer.toggleClass('hidden');
			parentContainer.children().focus();
			row.classList.add('active') 
		} else {
			brotherInput.value = 0;
			input.value = 1;
			parentContainer.toggleClass('hidden');
			row.classList.remove('active')
		}
	})
</script>

<script>
	$('input[name="date"]').datepicker({
		format: 'yyyy/mm/dd',
		autoclose: true,
		title: 'hola',
		todayHighlight: false,
		defaultViewDate: 'today',
		clearBtn: true
	});	
</script>

@endsection