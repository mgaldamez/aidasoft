@extends('layout_principal')

@section('container')

    <div class="col-md-10 col-md-offset-1">

        <div class="panel">

            <div class="panel-heading">
                <h1>{!! trans('app.attributes.external_movement_entry') !!}</h1>
            </div>


            {!! Form::open(['route' => ['external.movement.in'], 'id' => 'form-movement', 'class' => 'form-horizontal form-condensed' ]) !!}
            {!! Form::hidden('warehouse', $warehouse_id) !!}


            <div class="panel-body">
                @include('movements.external.partials.general')
            </div>
            <small style="display:none">
                <div >
     
               
                {!! Form::text('num_doc_temp',isset($num_doc['num_doc_temp'])?($num_doc['num_doc_temp']):'' , ['class' => 'input-pending','id'=>'num_doc_temp'] ) !!}
                </div>
            </small>
            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                @include('movements.external.partials.table_editing')
            </div>

            <div class="panel-body">
                <div class="col-md-12">
                    @include('partials.observation_field')
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('extra_scriptBody')

    @include('partials.select2_script')

    <script>
        $(document).ready(function () {
            $('#agent').select2();
        });
    </script>


    @include('movements.external.partials.table_script')

@endsection



@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">


                    <div class="navbar-btn">



                        <a href="{!! route('external.cancelar_in',[$num_doc['num_doc_temp'],$warehouse_id]) !!}" class="btn">
                            <i class="material-icons md-18">&#xE5C4;</i>Atras
                        </a>

                        <button type="button"
                                class="btn"
                                onclick="document.getElementById('form-movement').submit();">Guardar
                            <i class="material-icons md-18">&#xE161;</i>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! url('admin/my/warehouse') !!}">{!! trans('app.attributes.warehouses') !!}</a></li>
        <li><a href="{!! route('warehouse.show', $warehouse_id) !!}">{!! TradeMarketing\Models\Warehouse::find($warehouse_id)->description !!}</a>
        </li>
        <li class="active">{!! trans('app.attributes.external_movement_entry') !!}</li>
    </ol>
@endsection