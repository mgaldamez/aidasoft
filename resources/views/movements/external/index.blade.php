@extends('layout_principal')


@section('extra_scriptHead')
    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#table-movements').dataTableConfig();
        });
    </script>
@endsection


@section('container')
    <div class="col-md-10 col-md-offset-1">

        @include('partials.dataTables_filter', ['table' => 'table-movements'])

        <div class="panel">
            <div class="panel-body">

                <table id="table-movements" class="table table-condensed table-bordered small">
                    <thead>
                    <tr>
                        <th>{!! trans('app.attributes.date') !!}</th>
                        <th>{!! trans('app.attributes.reference') !!}</th>
                        <th>{!! trans('app.attributes.supervisor') !!}</th>
                        <th>{!! trans('app.attributes.warehouse') !!}</th>
                        <th>{!! trans('app.attributes.user') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($movements as $movement)
                        <tr>
                            <td>{!! $movement->created_at !!}</td>
                            <td>
                                <a href="{!! route('external.movement.show', $movement->id) !!}">{!! $movement->external_movement_num !!}</a>
                            </td>
                            <td>{!! $movement->agentName !!}</td>

                            <td>{!! $movement->warehouse->description !!}</td>
                            <td>{!! $movement->createdBy->fullName !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection