<style>
body{
  padding:20px 20px;
}

.results tr[visible='false'],
.no-result{
  display:none;
}

.results tr[visible='true']{
  display:table-row;
}

.counter{
  padding:8px; 
  color:#ccc;
}
</style>
<script>
$(document).ready(function() {
  $(".search").keyup(function () {
    var searchTerm = $(".search").val();
    var listItem = $('.results tbody').children('tr');
    var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
    
  $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
        return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });
    
  $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','false');
  });

  $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
    $(this).attr('visible','true');
  });

  var jobCount = $('.results tbody tr[visible="true"]').length;
    $('.counter').text();

  if(jobCount == '0') {$('.no-result').show();}
    else {$('.no-result').hide();}
		  });
});
</script>

<div class="form-group ">
    <input type="text" class="search form-control" placeholder="Buscar serie">
</div>
<span class="counter pull-right"></span>
<table  id="table_series" class="table table-hover table-bordered results">
  <thead>
    <tr>
    <th>.</th>
    <th>#</th>
    <th>Series</th>
    <th></th>
    </tr>
    <tr class="warning no-result">
      <td colspan="4"><i class="fa fa-warning"></i> No result</td>
    </tr>
  </thead>
  <tbody>

@foreach($series as $key => $series_out)

    <tr>
    <td>
    @if($series_out->existe>0)
    {!!Form::checkbox('input_serie['.$series_out->id.']', true,['checked'=>'checked'])!!}
    @else
    {!!Form::checkbox('input_serie['.$series_out->id.']', true)!!}
    @endif
    </td>
    <td >{!!$series_out->id!!} </td>
    <td >{!!$series_out->serie!!}</td>
    <td style="color:transparent">{!!$series_out->num_doc_temp!!}</td>
    </tr>
@endforeach
</tbody>
</table>