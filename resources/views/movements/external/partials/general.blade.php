<div class="col-sm-6">
    <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
        {!! Form::label('agent', trans('app.attributes.supervisor'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">
            {!! Form::select('agent', $userList, old('agent'), ['class' => 'form-control']) !!}

            {!! $errors->first('agent', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>


    <div class="form-group mdb @if($errors->has('external_agent')) has-error @endif">

        {!! Form::label('external_agent', trans('app.attributes.external_agent'), ['class' => 'col-md-3 control-label']) !!}

        <div class="col-md-9">
            {!! Form::select('external_agent', $particularList, old('external_agent'), ['class' => 'form-control']) !!}

            {!! $errors->first('external_agent', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>


    <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
        {!! Form::label('warehouse', trans('app.attributes.warehouse'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">

            <div class="form-control">
                {!! isset($warehouse_id)
                ? \TradeMarketing\Models\Warehouse::findOrFail($warehouse_id)->description
                : ''  !!}
            </div>

        </div>
    </div>
</div>


<div class="col-sm-6">
    <div class="form-group mdb @if($errors->has('agent')) has-error @endif">
        {!! Form::label('date', trans('app.attributes.date'), ['class' => 'col-md-3 control-label']) !!}
        <div class="col-md-9">

            <div class="form-control">
                {!! \Carbon\Carbon::today()->format('d-m-Y') !!}
            </div>

        </div>
    </div>
</div>