


<script>
$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        html: true
    });
});
</script>


<!-- Modal -->
<div class="modal fade" id="showVariantModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
            ...
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary mdb" data-dismiss="modal">Aceptar</button>
        </div>
    </div>
</div>
</div>

<script>
$('#showVariantModal').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget) // Button that triggered the modal
    var article = button.data('article') // Extract info from data-* attributes
    var title = button.data('title');
    var route = button.data('route');
    var modal = $(this);
    var body = modal.find('.modal-body');
    body.html('');
    modal.find('.modal-title').text(title)

    $.ajax({
        url: route,
        type: 'GET',
        dataType: 'json',
        beforeSend: function () {
            body.addClass('ajax loader');
        },
        success: function (res) {
            body.html(res);
        },
        complete: function () {
            body.removeClass('ajax loader');
        }
    });
})
</script>