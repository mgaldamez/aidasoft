@extends('layout_principal')


@section('extra_scriptHead')
<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">
<!-- Bootstrap DatePiker Pluging JS -->
<script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>

@endsection


@section('container')

@include('movements.layout_movement')


<form action="/article/movement/purchase" method="POST" id="form-purchase" class=""> 
	<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
	<input type="text" id="" name="movement"  class="hidden" value="1">

	<div class="col-md-12">	
		<div class="row-sm">				

			<div class="panel x-panel">

				<div class="panel-body">

					<button type="button" class="btn btn-info btn-link btn-sm pull-right" data-toggle="modal" data-target="#articleModal"><span class="glyphicon glyphicon-plus"></span> Articulo
					</button>

					<legend>Detalles de la compra</legend>

					<div class="col-md-4 ">
						<div class="row">

							<div class="form-group label-floating">
								<div class="input-group">
									<label for="code" class="control-label">Factura</label>

									<input type="text" name="code" class="form-control " autofocus="true"> 
									<span class="input-group-btn">
										<button type="button" class="btn btn-raised btn-fab btn-fab-mini" data-toggle="modal" data-target="#docModal">
											<span class="glyphicon glyphicon-file"></span>
										</button>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="row">

							<div class="form-inline">
								<div class="form-group">
									<label for="" class="control-label sr-only">Fecha</label>
									<input type="text" name="date" class="form-control datepicker" placeholder="Fecha de la factura">
								</div>

								<div class="form-group label-floating">
									<label for="provider" class="control-label">Proveedor</label>
									<select name="provider" id="providerBrand" class="form-control"></select>
								</div>

								<div class="form-group label-floating">
									<label for="bodega" class="control-label">Bodega</label>
									<select name="bodega" id="bodega_out" class="form-control"></select>
								</div>
							</div>

						</div>
					</div>

				</div>
				<!-- .panel-body -->
			</div>
			<!-- .panel -->
		</div>
	</div>

	<div class="col-md-12">

		<div class="row">

			<div class="col-md-4">
				<div class="row-sm">
					<div class="panel">
						<div class="panel-body">

							<div class="form-group">
								<label for="_article" class="control-label sr-only">Articulo</label>
								<select id="_article"  data-live-search="true" class="form-control" title="Articulos"></select>
							</div>

							<div class="form-group">
								<label for="_quantity" class="control-label sr-only">Cantidad</label>
								<input type="text" id="_quantity" class="form-control" placeholder="Cantidad de articulos">
							</div>

							<div class="form-group">
								<label for="_unit_cost" class="control-label sr-only">Costo unitario</label>
								<input type="text" id="_unit_cost" class="form-control" placeholder="Costo por unidad">	
							</div>

							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" checked="checked"  id="_tax"> Impuesto
									</label>
								</div>
							</div>

							<button type="button" id="add-article" class="btn btn-raised"  data-toggle="tooltip" data-placement="bottom" title="Agregar a la lista" data-original-title="Agregar a la lista" >Agregar</button>

							<button type="button" id="btnAceptUpdate" class="btn btn-warning btn-raised update-controls hidden">Cambiar</button>
							<button type="button" id="btnCancelUpdate" class="btn btn-raised update-controls hidden">Cancelar</button>

						</div>
					</div>
				</div>

			</div>
			<!-- .col -->

			<div class="col-md-8">
				<div class="row-sm">
					<div class="panel">
						<div class="panel-body">

							<div class="btn-group" role="group" aria-label="...">
								<button type="button" id="remove-item" class="btn btn-default btn-raised"><span class="glyphicon glyphicon-trash"></span></button>
								<button type="button" class="btn btn-default btn-raised hidden">Middle</button>
								<button type="button" class="btn btn-default btn-raised hidden">Right</button>
							</div>
						</div>

						<table id="tbl-article" class="article table text-right text-middle">
							<thead>
								<tr>
									<td class="text-left">ARTICULOS</td>
									<td>CANT<span class="hidden-xs hidden-sm">IDAD</span></td>
									<td>COST<span class="hidden-xs hidden-sm">O</span></td>
									<td>IMP<span class="hidden-xs hidden-sm">UESTO</span></td>
									<td>TOTAL</td>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
								</tr>	
							</tfoot>
						</table>

						<table class="table">
							<tbody class="text-right">
								<tr>
									{{-- <th>Subtotal:</th> --}}
									<td><strong>Subtotal:</strong><input type="text" id="subtotal" name="subtotal" class="text-right" value="0.00"></td>
								</tr>
								<tr>
									{{-- <th style="width:50%">Impuesto:</th> --}}
									<td><strong>Impuesto:</strong><input type="text" id="totalTax" name="totalTax" class="text-right" value="0.00"></td>
								</tr>
								<tr>
									{{-- <th style="width:50%">Descuento:</th> --}}
									<td><strong>Descuento:</strong><input type="text" id="totalDiscount" name="totalDiscount" class="text-right" value="0.00"></td>
								</tr>
								<tr>
									{{-- <th>Total:</th> --}}
									<td class="total"><strong>Total:</strong><input type="text" id="total" name="total" class="text-right" value="0.00"></td>
								</tr>	
							</tbody>
						</table>

					</div>
					<!-- .panel -->
				</div>
				<!-- .row-sm -->
			</div>
			<!-- .col -->
			



		</div>
		<!-- .row -->	
	</div>	
	<!--.col -->

	<button class="btn btn-primary btn-raised btn-lg pull-right" type="submit">Terminar</button>

</form>





<!-- Modal -->
<div class="modal fade" id="articleModal" tabindex="-1" role="dialog" aria-labelledby="articleModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="articleModalLabel">Nuevo Articulo</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12">
					<div class="row">
						<form action="POST" id="form-article" class="form-horizontal">
							<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
							<input type="file" id="image" class="hidden" name="image" value='{{ old('image') }}'/>

							<div class="col-md-4">
								<div class="row-sm">				
									<div class="well">
										<div class="__image_container-preview center-block">
											<div id="btn-cargar" class="__img-preview">
												<div id="spin" class="load-rotate" hidden><span class="glyphicon glyphicon-refresh"></span></div>
												<span class="glyphicon glyphicon-picture"></span>
												{{-- <i class="fa fa-image" aria-hidden="true"></i> --}}
											</div>
											<a id="btn-quitar" class="img-remove"></a>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-8">
								<div class="row-sm">
									@include('admin.forms.general')
									@include('articles.form.pakage')	
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">	
					<hr>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" id="btn-article" class="btn btn-primary">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="docModal" tabindex="-1" role="dialog" aria-labelledby="docModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Documento</h4>
			</div>
			<div class="modal-body">
				<div class="row">

					<div class="col-md-12">
						<div class="row">
							<table id="tbl-invoice-list" class="table table-hover">
								<thead>
									<tr class="text-uppercase">
										<td><strong>Fecha</strong></td>
										<td><strong>Codigo</strong></td>
										<td><strong>Monto</strong></td>
									</tr>
								</thead>
								<tbody>
									@foreach($invoices as $invoice)
									<tr>	
										<td class="hidden">									
											<input id="invoice-code"  value="{{ $invoice->id }}">
										</td>
										<td>{{ $invoice->date}}</td>
										<td>{{ $invoice->invoice_code}}</td>
										<td>{{ $invoice->total}} <span class="glyphicon glyphicon-menu-right pull-right"></span></td>
									</tr>
									@endforeach
								</tbody>
							</table>	

						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-12">	
					<hr>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" id="btn-article" class="btn btn-primary">Guardar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmAddModal" tabindex="-1" role="dialog" aria-labelledby="confirmAddModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-center" id="">Este Articulo ya existe en la lista, elija una de las opciones</h4>
			</div>
			<div class="modal-body min-height-25">
				<div class="col-md-12">

					<button type="button" id="confirmReplace" class="col-md-12 btn btn-primary btn-raised btn-lg btn-block">Remplazar</button>
					<button type="button" id="confirmBlend" class="col-md-12 btn btn-primary btn-raised btn-lg btn-block">Combinar</button>
					<button type="button" class="col-md-12 btn btn-default btn-raised btn-lg btn-block" data-dismiss="modal">Cancelar</button>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection


@section('extra_scriptBody')
<script src="{{ asset('assets/js/load_select_rgtr_article.js')}}"></script>
<script src="{{ asset('assets/js/demo_imagen.js')}}"></script>

<script src="{{ asset('assets/js/table.js')}}"></script>

<script>
	/*RADIO PAKAGE*/    
	$("input:radio[name=pakageOption]").click(function () {  
		if($('input:radio[name=pakageOption]:checked').val() == 2 ){
			$('.pakageDetaills').removeClass('hidden');
			$('label[for="quantity"').addClass('sr-only');
			$('label[for="box"]').removeClass('sr-only')
		} else {
			$('.pakageDetaills').addClass('hidden');
			$('label[for="quantity"').removeClass('sr-only');
			$('label[for="box"]').addClass('sr-only')
		}
	});
</script>

<script>
	/*ALMACENAR EL ARTICULO*/
	$('#btn-article').click( function (e) {
		e.preventDefault();

		var token = $('#token').val();
		var route = '/article';
		var data = new FormData(document.getElementById("form-article"));

		console.log(data)
		$.ajax({
			url: route,
			headers: { 'X-CSRF-TOKEN': token },
			type: 'POST',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
				console.log('enviando...')
			},
			success: function(res){ 
				console.log(res)
				select_articles()
			},
			error: function (err){
				console.log(err)
			}
    }) // end first ajax
	})
</script>

<script>
	$(document).ready( function() {
		select_bodegas();
		select_articles("_article");
		brandsToProviders( '#providerBrand' );	

		$('#box').val(1);
		$('#unit_cost').val(1);
		$('#quantity').val(1);
	});
</script>

<script>

	$('#_article').change( function(){
		var addArticle = $('#add-article').removeClass('hidden')
		$('.btn.update-controls').addClass('hidden')
	})
	
</script>

<!-- Modal Actions -->
<script>
	function replaceValueArticleList (){
		var article   = $('#form-purchase select[id="_article"]'); 
		var quantity  = $('#form-purchase input[id="_quantity"]');
		var unit_cost = $('#form-purchase input[id="_unit_cost"]');

		$("table#tbl-article tbody> tr").each( function() {

			var tableBody = $(this).children('td');
			var articleId = tableBody[0].childNodes[1].value;

			if( articleId === article.val() ){
				tableBody[1].childNodes[0].textContent =  quantity.val();
				tableBody[1].childNodes[1].value = quantity.val();
				tableBody[2].childNodes[0].textContent =  parseFloat(unit_cost.val()).toFixed(2);
				tableBody[2].childNodes[1].value = parseFloat(unit_cost.val()).toFixed(2);
				
				calculate( 'tbl-article' )

				article.val('')
				quantity.val('')
				unit_cost.val('')

				$('#confirmAddModal').modal('hide');
			} 
		});
	}

	function blendValueArticleList (){
		var article   = $('#form-purchase select[id="_article"]'); 
		var quantity  = $('#form-purchase input[id="_quantity"]');
		var unit_cost = $('#form-purchase input[id="_unit_cost"]');

		$("table#tbl-article tbody> tr").each( function() {

			var tableBody = $(this).children('td');
			var articleId = tableBody[0].childNodes[1].value;

			if( articleId === article.val() ){
				var quantityActual = parseFloat( tableBody[1].childNodes[1].value ).toFixed(2);
				var unitCostActual = parseFloat( tableBody[2].childNodes[1].value ).toFixed(2);
				var blendQuantity  = parseFloat(  Number( quantityActual ) + Number( quantity.val() ) );

				tableBody[1].childNodes[0].textContent = blendQuantity;
				tableBody[1].childNodes[1].value = blendQuantity;
				tableBody[2].childNodes[0].textContent = parseFloat(unit_cost.val()).toFixed(2);
				tableBody[2].childNodes[1].value = parseFloat(unit_cost.val()).toFixed(2);
				
				calculate( 'tbl-article' )

				article.val('')
				quantity.val('')
				unit_cost.val('')

				$('#confirmAddModal').modal('hide');
			} 
		});
	}

	$('#confirmBlend').click( function() {
		blendValueArticleList()
	});

	$(document).delegate('#confirmReplace', 'click', function() {
		replaceValueArticleList()
	});
</script>

<!-- DatePiker -->
<script type="text/javascript">
	$('input[name="date"]').datepicker({
		format: 'yyyy/mm/dd',
		autoclose: true,
		title: 'hola',
		todayHighlight: false,
		defaultViewDate: 'today',
		clearBtn: true
	});	
</script>

<script type="text/javascript">
	$('#tbl-invoice-list tbody tr').click( function( event ) {

		var parent = $(this).closest('tr');
		var td = parent.children()[0];
		var inputValue = td.childNodes[1].value;
		var inputText = td.childNodes[1].textContent

		$.get( '/invoices/'+inputValue+'', function( res ){
			console.log( res )
		})

	})
</script>

@endsection

