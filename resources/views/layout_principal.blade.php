<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="../../favicon.ico">

    <title>Administracion y control de inventario </title>

    <!-- Bootstrap core CSS -->
    {!! Html::style('/bootstrap/3.3.7/css/bootstrap.css') !!}

    <!-- Bootstrap Material Design CSS -->
    {!! Html::style('/bootstrap/material-design/css/bootstrap-material-design.css') !!}
    {{--{!! Html::style('/bootstrap/material-design/css/ripples.min.css') !!}--}}

    {!! Html::style('/fonts.googleapis/material-icons.css') !!}


    {!! Html::style('/material-design-table/material_design_table.css') !!}

    <!-- JQuery Pluging -->
    {!! Html::script('/jquery/2.2.2/jquery.min.js') !!}


    {!! Html::script('/assets/js/demo_insert_after_node.js') !!}
    {!! Html::script('/assets/js/demo_array_handle.js') !!}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

{!! Html::script('/sweetalert/sweetalert.min.js') !!}
@include('sweet::alert')

@yield('extra_scriptHead')

{!! Html::style('/assets/css/app.css') !!}

<!--  Style CSS -->
    <style>
        .menu-fixed {
            position: fixed;
            z-index: 20;
            top: 0;
            box-shadow: 2px 1px 6px 0 rgba(0, 0, 0, 0.12), 2px 1px 6px 0 rgba(0, 0, 0, 0.12);
            padding: 10px;
            background-color: #eeeeee;
            margin: 0;
            margin-left: -15px;
            transition: width .3s;
        }
    </style>

</head>

<body>


<div id="app-container" class="__container">

    <ul id="main-menu" class="menu-main">

        <li>

            <div class="toggle-menu">

                <i class="icon icon-menu material-icons">&#xE5D2;</i>
            </div>

            <nav class="sidebar-menu">


                <div class="scroller">


                    @include('menu')


                </div>
                <!--.scroller-->

            </nav>
            <!--nav.sidebar-menu-->
        </li>

        <li class="title">
            <a href="{!! route('home') !!}">AidaSoft</a>
            {{--<div class="page-title visible-xs">--}}
            {{--@yield('page_title')--}}
            {{--</div>--}}
        </li>

        <li class="hidden-xs hidden-sm">
            @yield('breadcrumb')
        </li>


        {{--<li class="search-bar">--}}

        {{--{!! Form::model(Request::all(), ['url' => 'provider', 'method' => 'GET', 'class' => '', 'role' => 'search']) !!}--}}

        {{--<i class="material-icons control-search">search</i>--}}
        {{--<i class="material-icons control-back">arrow_back</i>--}}
        {{--{!! Form::text('search', null, ['id' =>'search', 'class' => '', 'placeholder' => 'Buscar...', 'autocomplete' => 'off', 'autofocus' => 'true']) !!}--}}
        {{--<i class="material-icons control-cancel">close</i>--}}

        {{--{!! Form::close() !!}--}}

        {{--</li>--}}

        <li>
            <ul>
                <li id="shopping-cart-icon" class="notification-icon">
                    @include('partials.order_cart')
                </li>

                <li class="notification-icon dropdown">


                    <a href="javascript:void(0)" class="contact-chip contact-chip-sm" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">

                        <div class="contact-chip-img contact-chip-role-{!! currentUser()->role->description !!}">
                            {!! currentUser()->nameProfile !!}
                        </div>

                        <div class="contact-chip-body">
                            <span class="contact-username">
                                <span class="hidden-xs">{!! currentUser()->username !!}</span>
                                <i class="material-icons md-18">&#xE5C5;</i>
                            </span>
                        </div>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="{!! route('user.show', currentUser()->id) !!}">
                                Perfil<i class="material-icons md-18 pull-right">&#xE853;</i>
                            </a>
                        </li>

                        <li>
                            <a href="{!! url('admin/my/warehouse') !!}">
                                Mi Bodegas <i class="material-icons md-18 pull-right">&#xE8D1;</i>
                            </a>
                        </li>

                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{!! route('logout') !!}">
                                <i class="material-icons md-18 pull-right">&#xE879;</i>Salir
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            @yield('notify_icon_bar')




        </li>

    </ul>
    <!--.menu-main-->


    <div class="container-wrapper open-part">

        <!-- HEADER -->
        <header class="headboard">

            @yield('header')

        </header>


        @include('alerts.flash_response')


        @yield('container')
    </div>


    <!-- FOOTER -->
    <footer>


        <p class="">© 2019 Jsgsoftware  Panama . · <a href="#">Privacy</a> · <a href="#">Terms</a>
        </p>

        <!-- BACK TO TOP BOOTOM -->
        <span id="top-link-block" class="hidden" style="position:absolute">

			<a href="#top" onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
				<i class="glyphicon glyphicon-arrow-up"></i>
			</a>

		</span>
        <!--.top-link-block -->


    </footer>


</div>
<!--/container -->

<!-- Bootstrap JS -->
{!! Html::script('bootstrap/3.3.7/js/bootstrap.min.js') !!}

<!-- Bootstrap Meterial Design JS -->
{!! Html::script('bootstrap/material-design/js/material.js') !!}
{{--{!! Html::script('bootstrap/material-design/js/ripples.min.js') !!}--}}

{!! Html::script('assets/js/demo_menu.js') !!}
{!! Html::script('assets/js/demo_search_bar.js') !!}
<script>
    $(function () {
        // init bootstrap material design
        $.material.init()

        // init tooltip
//        $('[data-toggle="tooltip"]').tooltip()


        if (($(window).height() + 100) < $(document).height()) {
            $('#top-link-block').removeClass('hidden');
        }


        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    });
</script>


<script>
    $(document).ready(function () {

        var nav = $('.headboard .navbar .navbar-btn');
        if (nav.length) {
            var altura = nav.offset().top;

            $(window).on('scroll', function () {
                if ($(window).scrollTop() > altura) {
                    nav.addClass('menu-fixed');
                } else {
                    nav.removeClass('menu-fixed');
                }
            });
        }

    });
</script>



@yield('extra_scriptBody')

</body>

</html>
