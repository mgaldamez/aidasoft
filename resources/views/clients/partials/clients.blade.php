@foreach($providers as $key => $provider)
    <tr data-provider-id="{!! $provider->id !!}">

        <td>
            <a href="{!! route('admin.provider.show', $provider->id) !!}">
                {!! $provider->Nombre !!}
                <small>{!! isset($provider->Nombre)? ', '. '<strong>'.$provider->Nombre .'</strong>' : '' !!}</small>
            </a>
        </td>

        <td>{!! $provider->Ruc !!} <br>
            {!! $provider->direccion1 !!}
        </td>

        <td>{!! $provider->telefono !!}</td>

        <td>{!! $provider->email !!}</td>



        <td width="80">
            <a href="{!! route('clientes.edit', $provider->id) !!}" class="btn btn-xs btn-default" title="Editar">
                <i class="material-icons md-18">mode_edit</i>
            </a>
			   
            <a href="#"
               class="btn btn-xs btn-default"
               data-toggle="modal"
               data-target="#deleteProviderModal"
               data-name="{!! $provider->Nombre !!}"
               data-id="{!! $provider->id !!}"
               title="Eliminar">
                <i class="material-icons md-18">delete</i>
            </a>
        </td>
    </tr>
@endforeach