@extends('layout_principal')


@section('extra_scriptHead')
   <link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}"/>
    <script src="{{ asset('croppie/croppie.js') }}"></script>
    <script src="{{ asset('assets/js/demo_image.js')}}"></script>
    <!-- Bootstrap Bootstrap-select Pluging CSS -->
    <link rel="stylesheet" href="{{asset('/bootstrap/bootstrap-select/css/bootstrap-select.css')}}">
    <!-- Bootstrap Bootstrap-select Pluging JS -->
    <script src="{{url('/bootstrap/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <link rel="stylesheet" href="{{asset('jquery.flexdatalist/jquery.flexdatalist.css')}}">
    <!-- JQuery UI Pluging CSS -->
    <link rel="stylesheet" href="{{ asset('jquery.ui/jquery-ui.min.css')}}">

    <script src="{{ asset('jquery.ui/jquery-ui.min.js')}}"></script>
	
	<script>
	function hidden_control(valor)
	{
	
		var elem = document.getElementById('empresa_natural');
		if(valor==1)
		{
			elem.style.visibility='hidden';
		}
			if(valor==2)
		{
			elem.style.visibility='visible';
		}
	}
	function isClient (valor)
	{
		
		var elem = document.getElementById('es_cliente');
		  // Get the checkbox
		var checkBox = document.getElementById("chk_cliente");
		if (checkBox.checked == true){
		elem.style.visibility='visible';
		} else {
		elem.style.visibility='hidden';
		}
	
	
		
	}
	</script>
@endsection

@section('page_title')
    Nueva cliente/proveedor
@endsection



@section('container')

    <div class="col-md-10 col-md-offset-1">
        <div class="row-sm">
            <div class="panel">

                <div class="panel-heading">
                    <h3>Nuevo cliente/proveedor</h3>
                </div>

                {!! Form::open( ['route' => ['clientes.add'], 'method' => 'POST','id'=>'create_cliente_proveedor']) !!}


                <div class="panel-body">
				</br>
				</br></br>
				</br>
 
		{{ csrf_field() }}
		<!--      GENERAL DATOS ------------------------------------------------------------>
		@include('clients.forms.general')

	
	<!--               AREA DE TAB           ----------------------------------------------------------------------------------------->
		@include('clients.forms.tab')
	<!--               AREA DE TAB           ----------------------------------------------------------------------------------------->
	
                </div>
                <!-- .panel-body -->

                {!! Form::close() !!}
            </div>
            <!-- .panel -->
        </div>
        <!-- .row-sm -->
    </div>
    <!-- .col offset-->

@endsection

@section('header')

    <div class="navbar">
        <div class="container-fluid">

            <div class="navbar-btn">

                <a href="{!! URL::previous() !!}" class="btn">
                    <i class="material-icons md-18">&#xE5C4;</i><span class="hidden-xs">Atras</span>
                </a>

                <button type="submit" class="btn btn-primary mdb" form='create_cliente_proveedor'>
                        <i class="material-icons md-18">save</i> Guardar
                    </button>


                <a href="{!! route('clientes.index') !!}" class="btn pull-right">
                    <i class="material-icons md-18">list</i>Ir a la lista
                </a>

            </div>
        </div>
    </div>

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('clientes.index') !!}"></a></li>
        <li class="active">crear cliente-proveedores</li>
    </ol>
@endsection
@section('extra_scriptBody')

    <script src="{{ asset('jquery.flexdatalist/jquery.flexdatalist.js') }}"></script>
@endsection