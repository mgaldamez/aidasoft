<!---------------------CONTENIDO-------------------->
<div id="content">

	<div class="form-row">
	<!--- 	<div id="contenidos">
		<div id="columna1"> -->
		<div class="form-group col-md-12">
				<hr/>
				@if(isset($provider->suc_id))
				
					{!! Form::text('suc_id', old('suc_id'), ['class' => 'form-control', 'autofocus','name'=>'suc_id','readonly']) !!}
				@endif
			</div>
			
		<div class="form-group col-md-12">
			{!! Form::label('lblnom_empresa', 'Nombre') !!}
			{!! Form::text('suc_nombre', old('suc_nombre'), ['class' => 'form-control', 'placeholder' => 'Nombre de sucursal', 'autofocus','name'=>'suc_nombre','id'=>'suc_nombre']) !!}
		</div>
		<div class="form-group col-md-12">
			{!! Form::label('lblnom_correo', 'Correo') !!}
			{!! Form::text('suc_correo', old('suc_correo'), ['class' => 'form-control', 'placeholder' => 'Correo', 'autofocus','name'=>'suc_correo']) !!}
		</div>
		<div class="form-group col-md-7">
			{!! Form::label('lblnom_telefono', 'Telefono') !!}
			{!! Form::text('suc_telefono', old('suc_telefono'), ['class' => 'form-control', 'placeholder' => 'Telefono', 'autofocus']) !!}
		</div>
		
		<div class="form-group col-md-7">
			{!! Form::label('lblnom_movil', 'Movil') !!}
			{!! Form::text('suc_movil', old('suc_movil'), ['class' => 'form-control', 'placeholder' => 'Movil', 'autofocus']) !!}
		</div>
	
	<!--- 	</div>

	<div id="columna2">
		<div class="form-group col-md-12">
			<div class="panel-body">
                    

                   @include('clients.forms.upload_image') --
            </div>
		</div>
		</div>
	</div>--->
	</div>
	
</div>
	<!---------------------CONTENIDO-------------------->
	<style type="text/css">
#contenedor {
    display: table;
    border: 0px solid #000;
    width: 333px;
    text-align: center;
    margin: 0 auto;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    border: 0px solid #000;
    vertical-align: middle;
    padding: 10px;
}
</style>