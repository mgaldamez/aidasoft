<!---------------------CONTENIDO-------------------->
<div id="content">
	<div class="form-row">
		<div class="form-group col-md-2">
			<div class="custom-control custom-radio custom-control-inline">
				{!! Form::label('lblnatural', 'Contacto', ['class' => 'custom-control-label']) !!}
				{!! Form::radio('id_tipo_cliente', '2' , true,['onchange' => 'hidden_control(2);']) !!}
	
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="custom-control custom-radio custom-control-inline">

			{!! Form::label('lbljuridica', 'Direccion Factura', ['class' => 'custom-control-label']) !!}
			{!! Form::radio('id_tipo_cliente', '1' , false,['onchange' => 'hidden_control(1);']) !!}
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="custom-control custom-radio custom-control-inline">

			{!! Form::label('lbljuridica', 'Direccion entrega', ['class' => 'custom-control-label']) !!}
			{!! Form::radio('id_tipo_cliente', '1' , false,['onchange' => 'hidden_control(1);']) !!}
			</div>
		</div>
		<div class="form-group col-md-4">
			<div class="custom-control custom-radio custom-control-inline">

			{!! Form::label('lbljuridica', 'otras direccion', ['class' => 'custom-control-label']) !!}
			{!! Form::radio('id_tipo_cliente', '1' , false,['onchange' => 'hidden_control(1);']) !!}
			</div>
		</div>
	</div>
	
</div>
	<!---------------------CONTENIDO-------------------->
	