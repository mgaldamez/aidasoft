	<ul class="nav nav-tabs" id="myTab" role="tablist">
	  <li class="nav-item">
    <a class="nav-link" id="sales_purchase" data-toggle="tab" href="#sales_purchases" role="tab" aria-controls="sales_purchases" aria-selected="false">Ventas y Facturas</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Contactos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="notas-tab" data-toggle="tab" href="#notas" role="tab" aria-controls="notas" aria-selected="true">Notas</a>
  </li>

</ul>
<div class="tab-content" id="myTabContent">
       <div class="tab-pane fade show active" id="homee" role="tabpanel" aria-labelledby="home-tab3">
 
	<hr/>
  
		</div>
    <!--               NOTAS                  -------------->
  <div class="tab-pane fade " id="notas" role="tabpanel" aria-labelledby="notas-tab">
  
  <div class="md-form">
  <i class="material-icons">
	speaker_notes</i>

   {!! Form::textarea('notas', old('notas'), ['class' => 'form-control', 'placeholder' => 'notas......', 'autofocus']) !!}
  
</div>
  
  </div>
  <!--               VENTAS Facturas-------------->







  
    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
   <div class="col-md-12">

      
        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#nuevocontactosmodal">
        <i class="material-icons md-18">save</i>   Agregar
       </button>
		@include('clients.forms.direcciones')
    </div>
  
  
  </div>

   <div class="tab-pane fade " id="sales_purchases" role="tabpanel" aria-labelledby="sales_purchase" style="margin-top:5px">
		 @include('clients.forms.ventas')
	 </div>

	</div>
  </div>
</div>