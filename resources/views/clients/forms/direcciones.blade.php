<div>


	<div class="form-row" id="panel_contactos">
@if(isset($contactos))
@foreach ($contactos as $contacto)


	<div class="card border-success mb-4" id="divcard-{{ $contacto->id}}" name="divcard-{{ $contacto->id}}"   style="max-width: 35rem; margin-left: 5%; margin-top: 5%; ">
     <div class="card-body" id="card-{{ $contacto->id}}" name="card-{{ $contacto->id}}">

	<!-- Title -->
	<div class="form-group col-md-12">
	<h4 class="card-title">

	{!!\TradeMarketing\ClientesTipocontacto::buscar_header($contacto->con_tipo_contacto)!!}

	<button type="button" class="btn btn-link" data-toggle="modal" data-target="#contactosmodal-{{ $contacto->id }}">
	<i class="material-icons">
	edit
	</i>
	<button/>
	<button 
	type="button" class="btn btn-link" 
               data-toggle="modal"
               data-target="#deletecontactoModal"
               data-name="{!! $contacto->con_nombre !!}"
               data-id="{!! $contacto->id !!}"
			   title="Eliminar"
			 
			   >
	
	<i class="material-icons">
	delete
	</i>
	<button/>
	</h4>
	
	</div>
    <!-- Text -->
    <p class="font-weight-normal">{!!$contacto->con_nombre!!}, {!!$contacto->con_puesto!!}</p>
	<p class="font-weight-normal">{!!$contacto->con_correo!!}</p>
	<p class="font-weight-normal">Telefono: {!!$contacto->con_telefono!!}, Movil: {!!$contacto->con_movil!!}</p>
	</div>
	</div>
    @endforeach
@endif
	
	</div>
</div>
	<!-- Modal -->

    <div class="modal fade" id="deletecontactoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="myModalLabel">Eliminar</h4>
                </div>
                <div class="modal-body text-center">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary mdb" role="delete" data-id=""
                            onclick="deleteProvider(this)">
                        Eliminar
                    </button>
                </div>
            </div>
        </div>
	</div>
	
	<script>
		
        function deleteProvider(button) {

            var modal = button.closest('.modal');
            var id = button.getAttribute('data-id');
			var $voz = button.getAttribute('data-id');
            var row = $('#cardview').find('tr[data-provider-id="' + id + '"]')[0];

            var form = $('#form-delete');
			var urls = '{{ route("clientes.destroy", ":id") }}';
				urls = urls.replace(':id', id);
				$("#divcard-"+id).empty();
			console.log("#divcard-"+id);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: urls,
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert.flash'));

                    alerts.forEach(function (item) {
                        item.remove();
                    });

                    $(modal).modal('hide');
                },
                error: function (xhr, status) {
                    console.log(xhr)
                },
                success: function (response) {
					
					console.log(response);
                    $(row).fadeOut();

                    var body = document.getElementsByClassName('container-wrapper')[0];
                    body.insertAdjacentHTML('afterbegin', response.alert);
                }
            });
        }


        $('#deletecontactoModal').on('show.bs.modal', function (event) {

            var provider = $(event.relatedTarget);
            var name = provider.data('name');
            var id = provider.data('id');

            var modal = $(this);

            modal.find('.modal-title').html('¿Estas seguro de eliminar a <strong>' + name + '</strong>?');
            var button = modal.find('.modal-footer button[role="delete"]');

            button.attr('data-id', id);
        });
    </script>