<!---------------------CONTENIDO-------------------->
<div id="content">


	<div class="form-row">
	<!--- 	<div id="contenidos">
		<div id="columna1"> -->
		<div class="form-group col-md-4">
		{!! Form::label('lblnom_empresa', 'Tipo de contacto') !!}
		{!! Form::select('con_tipo_contacto', \TradeMarketing\ClientesTipocontacto::listing(), old('con_tipo_contacto'), ['class'=>'selectpicker','data-live-search'=>'true', 'placeholder' => '--']) !!}
		{!! $errors->first('con_tipo_contacto', '<p class="text-danger">:message</p>') !!}
		</div>
		<div class="form-group col-md-1">
		</div>	
		<div class="form-group col-md-6">
			{!! Form::label('lblnom_empresa', 'Nombre completo') !!}
			
			{!! Form::text('con_nombre', '', ['class' => 'form-control', 'placeholder' => 'Nombre de sucursal', 'autofocus','name'=>'con_nombre','id'=>'con_nombre']) !!}
		
		</div>
		<div class="form-group col-md-12">
			{!! Form::label('lblnom_correo', 'Direccion') !!}
			{!! Form::text('con_direccion', old('con_direccion'), ['class' => 'form-control', 'placeholder' => 'Direccion', 'autofocus','name'=>'con_direccion']) !!}
		</div>
		
		<div class="form-group col-md-12">
			{!! Form::label('lblnom_correo', 'Cargo') !!}
			{!! Form::text('con_puesto', old('con_puesto'), ['class' => 'form-control', 'placeholder' => 'Eje: Analista de BI', 'autofocus','name'=>'con_puesto']) !!}
		</div>
		
		<div class="form-group col-md-4">
			{!! Form::label('lblnom_correo', 'Correo') !!}
			{!! Form::text('con_correo', old('con_correo'), ['class' => 'form-control', 'placeholder' => 'Correo', 'autofocus','name'=>'con_correo']) !!}
		</div>
		<div class="form-group col-md-1">
		</div>
		<div class="form-group col-md-3">
			{!! Form::label('lblnom_telefono', 'Telefono') !!}
			{!! Form::text('con_telefono', old('con_telefono'), ['class' => 'form-control', 'placeholder' => 'Telefono', 'autofocus']) !!}
		</div>
		<div class="form-group col-md-1">
		</div>
		<div class="form-group col-md-3">
			{!! Form::label('lblnom_movil', 'Movil') !!}
			{!! Form::text('con_movil', old('con_movil'), ['class' => 'form-control', 'placeholder' => 'Movil', 'autofocus']) !!}
		</div>
	
	<!--- 	</div>

	<div id="columna2">
		<div class="form-group col-md-12">
			<div class="panel-body">
                    

                   @include('clients.forms.upload_image') --
            </div>
		</div>
		</div>
	</div>--->
	</div>
	
</div>
	<!---------------------CONTENIDO-------------------->
	<style type="text/css">
#contenedor {
    display: table;
    border: 0px solid #000;
    width: 333px;
    text-align: center;
    margin: 0 auto;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    border: 0px solid #000;
    vertical-align: middle;
    padding: 10px;
}
</style>