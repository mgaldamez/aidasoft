

<!-- Modal -->
@if(isset($contactos))
@foreach ($contactos as $item)

<div class="modal fade" id="contactosmodal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar contacto</h4>
            </div>
            <div class="modal-body">

                 <form action="{!! route('clientes.editcontactos',$item->id) !!}" class="form-horizontal row" method="POST"
                      id="new-contact">

                     @include('clients.forms.edit_contacto')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- Modal -->
@endif


<div class="modal fade" id="nuevocontactosmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar contacto</h4>
            </div>
            <div class="modal-body">

                <form action="{!! route('clientes.addcontactos',isset($provider->id)) !!}" class="form-horizontal row" method="POST"
                      id="new-contact">

                     @include('clients.forms.new_contacto')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="sucursaleModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Categoria</h4>
            </div>
            <div class="modal-body">

                <form action="{!! route('clientes.addsucursal') !!}" class="form-horizontal row" method="POST"
                      id="new-category">
					
                    @include('clients.forms.new_sucursal')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="editsucursaleModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar sucursal</h4>
            </div>
              <div class="modal-body">

                <form action="{!! route('clientes.editsucursal') !!}" class="form-horizontal row" method="POST"
                      id="new-category">
					
                    @include('clients.forms.new_sucursal')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="attributeModal" tabindex="-1" role="dialog">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Atributo</h4>
                <p class="help-block">Puedes editar en la seccion de Attributos</p>
            </div>

            <div class="modal-body">

                <form action="{!! route('attribute.store') !!}" class="form-horizontal row"
                      method="POST"
                      id="new-attribute">


                    <div class="form-group mdb">
                        {!!Form::label('description', 'Descripción', ['class' => 'control-label col-sm-2'])!!}

                        <div class="col-sm-10">

                            {!! Form::text('description', old('description'), ['class' => 'form-control', 'autocomplete' => 'off', 'autofocus']) !!}
                        </div>
                    </div>
                </form>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" role="save">Guardar</button>
            </div>
        </div>
    </div>
</div>



<script>
    function showModal(modal, element, selected) {
			
        selected || ( selected = false );

        var button = modal.find('.modal-footer button[role="save"]');

        var form = modal.find('form')[0];
			console.log(form);
        $(form).submit(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
        });

        $(button).click(function (event) {
		
            event.preventDefault();
            event.stopImmediatePropagation();
            
                var ajax = save(form);
            
                
                ajax.success(function (response) {
                 //MODEL DE CONTACTOS AJAX
               console.log(response);
                if(element==='#panel_contactos')
                {
                 var card='<div class="card border-success mb-4" id="divcard-'+response.id+'" name="divcard-'+response.id+'"  style="max-width: 35rem; margin-left: 5%; margin-top: 5%; ">';
                card+='<div class="card-body">';
                card+='<h4 class="card-title">'+response.tipo_contacto+'';
                card+='<button type="button" class="btn btn-link" data-toggle="modal" data-target="#deletecontactoModal"';
                card+='data-name="'+response.con_nombre+'" data-id="'+response.id+'" title="Eliminar">';
                card+='<i class="material-icons">delete</i><button/>';
                card+='</h4>';
                card+='<p class="font-weight-normal">'+response.con_nombre+','+response.con_puesto+'</p>';
                card+='<p class="font-weight-normal">'+response.con_correo+'</p>';
                card+='<p class="font-weight-normal">Telefono: '+response.con_telefono+', Movil:'+response.con_movil+'</p>';
                card+='</div>';
                card+='</div>';
               
                var selectOption = $(element).append(card);

                }
                else if (element.indexOf('#card')> -1)
                {
                var card='<div class="card-body" id="card-"'+response.id+'">';
                card+='<h4 class="card-title">'+response.tipo_contacto+'';
                card+='<button type="button" class="btn btn-link" data-toggle="modal" data-target="#contactosmodal-'+response.id+'">';
                card+='<i class="material-icons">edit</i><button/>';
                card+='<button type="button" class="btn btn-link" data-toggle="modal" data-target="#editsucursaleModel">';
                card+='<i class="material-icons">delete</i><button/>';
                card+='</h4>';
                card+='<p class="font-weight-normal">'+response.con_nombre+','+response.con_puesto+'</p>';
                card+='<p class="font-weight-normal">'+response.con_correo+'</p>';
                card+='<p class="font-weight-normal">Telefono: '+response.con_telefono+', Movil:'+response.con_movil+'</p>';
                card+='</div>';
                console.log(element);
                var selectOption = $(element).empty().append(card);
                }

                
                else
                {
                
                var selectOption = $(element).empty().append('<option value="' + response.id + '">' + response.nombre + '</option>');

                if (selected)    {
                selectOption[0].lastChild.selected = true;
                                 }
                }
                

				if(modal[0].id!='editsucursaleModel')
				{
					form.reset();
				}
                
                modal.modal('hide')

            }).error(function (xhr, status) {
                if (xhr.status == 422) {
                    errors(xhr.responseJSON, modal);
                }
            })
        
        
        });

        modal.find('.modal-body input').focus();
    }


    function save(form) {
        return $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            dataType: 'json',
            error: function (xhr, status) {
                console.log(xhr)
            }
        });
    }

    function errors(json, modal) {
        modal.find('.text-danger').remove();

        $.each(json, function (item, message) {

            var p = document.createElement('P');

            p.appendChild(document.createTextNode(message));
            p.setAttribute('class', 'text-danger');

            var elem = modal.find('#' + item + '')[0];


            if (elem) {
                var parent = elem.closest('.form-group');
                parent.classList.add('has-error');

                $(parent).insertAfterNode(p, elem);
            }
        });
    }
    @if(isset($contactos))
    @foreach ($contactos as $item)
    $('#contactosmodal-{{ $item->id }}').on('show.bs.modal', function (event) {
        showModal($(this), '#card-{{ $item->id }}', true);
    });
    @endforeach
    @endif
    $('#nuevocontactosmodal').on('show.bs.modal', function (event) {
        showModal($(this), '#panel_contactos', true);
    });
    

    $('#sucursaleModel').on('show.bs.modal', function (event) {
        showModal($(this), '#sucursal_id', true);
    });


    $('#editsucursaleModel').on('show.bs.modal', function (event) {
        showModal($(this), '#sucursal_id', true);
    });


    $('#attributeModal').on('show.bs.modal', function (event) {
        showModal($(this), 'select.attr');
    });
    
</script>
