@extends('layout_principal')


@section('container')


    <section class="col-md-10 col-md-offset-1">

        {!! Form::model($transfer, ['route' => ['transfer.store',$transfer->id], 'method' => 'POST', 'role' => 'create', 'id' => 'form-transfer', 'class' => 'form-horizontal form-condensed']) !!}

        <section class="panel">
            <div class="panel-heading">


                @if($transfer)
                    @include('partials.contact-chip',  [
                    'role' => currentUser()->role->description,
                    'userName' =>  currentUser()->fullName,
                    'shortName' => currentUser()->nameProfile,
                    ])
                @endif


                <h1>
                    <small>{!! trans('app.attributes.transfer') !!}</small>
                </h1>
                <small>{!! \Carbon\Carbon::now()->format('D, d M  Y') !!}</small>
            </div>

            <div class="panel-body">
                Id orden: {!! $transfer->order_id !!}

                <div class="row">
                    @include('transfers.partials.general')
                </div>
            </div>

            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>


                @include('transfers.partials.table_editing')


            </div>


            <div class="panel-body">

                <div class="col-sm-12">
                    @include('partials.observation_field')
                </div>
            </div>
        </section>

        {!! Form::close() !!}

    </section>

@endsection


@section('extra_scriptBody')

    <script>
        $(document).ready(function () {

            var route_original = '{!! route('order.warehouse.details', ['order' => $transfer->order_id,  'warehouse' => ':WAREHOUSE_ID' ]) !!}';
            var tbody = $('#stock-transfer tbody');

            $('#bodega_id').on('change', function (event) {

                var route = route_original;

                route = route.replace(':WAREHOUSE_ID', this.value);

                tbody.empty();

                $.get(route, function (responce) {
                    tbody.append(responce);
                });
            });
        });
    </script>



    <table id="line-article" style="display: none">
        <tbody>
        <tr>
            <td class="cell-article">


                {{--                {!! Form::select('items[][article_id]', $articles) !!}--}}

                <input type="hidden" name="items[][article_id]" autocomplete="off" class="name_handler">
            </td>
            <td class="cell-measurement-unit">

            </td>
            <td class="cell-available">
                <input type="hidden">
            </td>
            <td class="cell-quantity" width="200">
                <input type="text" name="items[][quantity]" class="form-control name_handler" autocomplete="off"
                       value="1" autofocus="autofocus">
            </td>
            <td width="50">
                <a href="#" class="btn btn-danger btn-sm" role="delete-row">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
        </tr>
        </tbody>
    </table>

    @include('partials.select2_script')
    <script>
        $(document).ready(function () {
            $('select').select2();
        });
    </script>
@endsection



@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <a href="{!! URL::previous() !!}" class="btn">
                            Cancelar
                        </a>

                        <button type="submit" class="btn" form="form-transfer" >
                            <!-- save -->
                            Guardar<i class="material-icons md-18">&#xE161;</i>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection





@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('transfer.all') !!}">Transferencias</a></li>
        <li class="active">Nueva Transferencia</li>
    </ol>
@endsection


