@extends('layout_principal')


@section('extra_scriptHead')
@endsection

@section('container')

    <section class="col-md-10 col-md-offset-1">

        {!! Form::open(['url' => 'transfer/', 'method' => 'POST', 'role' => 'create']) !!}

        {!! isset($bodega_id)? Form::hidden('bodega_id', $bodega_id, ['id'=>'bodega_id']) : '' !!}



        <section class="panel">
            <div class="panel-body">

                <legend>Hola</legend>

                @include('transfers.partials.general')

            </div>
        </section>


        {{--VUE MAIN--}}
        <div id="vue-main">
            <section class="panel">
                <div class="panel-body">

                    <legend>Como Estas?</legend>

                    <p class="help-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda cupiditate
                        excepturi exercitationem facilis id.</p>

                    <table class="table table-condensed table-striped">
                        <tbody>
                        <tr v-for="article in articles"
                            is="article-row"
                            v-bind:article="article"
                            v-bind:article_list="article_list"
                        @update-article="updateArticle"
                        @remove-article="removeArticle"></tr>

                        </tbody>

                        <tfoot>
                        <tr>
                            <td>
                                <select-article v-bind:article_list="article_list"
                                                v-bind:article="add_article"
                                                v-bind:errors="errors"
                                                v-bind:class="{'has-error': errors.article_id}"></select-article>
                            </td>

                            <td>

                                <quantity-article v-bind:article="add_article"
                                                  v-bind:errors="errors"
                                                  v-bind:class="{'has-error': errors.quantity}"></quantity-article>
                            </td>

                            <td width="40">
                                <div class="btn-group btn-group-sm">
                                    <button type="button"
                                            class="btn"
                                            v-on:click="addArticle()">
                                        <span class="glyphicon glyphicon-plus"></span> Agregar
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </section>


        </div>
        {{--Vue main--}}


        <br>

        <div class="form-group">
            <div class="checkbox">
                <label for="transfer_confirm">
                    <input type="checkbox" name="transfer_confirm" id="transfer_confirm"> Iniciar Trasferencia
                </label>
            </div>
            <p class="help-block">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum,
                voluptates?</p>
        </div>

        {!! Form::submit('Crear Transferencia', [ 'class' => 'btn btn-primary btn-raised btn-lg mdb']) !!}
        {!! Form::close() !!}

    </section>

@endsection

@section('extra_scriptBody')

    {!! Html::script('js/main.js') !!}
    {!! Html::script('assets/js/send_to_ajax.js') !!}

    <script>
        $(document).ready(function () {

            var form = $('form[role="create"]');

            $(form).sendToAjax({
                method: $(form)[0].method,
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                success: function (res) {
                    location.href = '/transfer/' + res.id;
                }
            });
        });
    </script>


    <script>
        // Confirmar que hubo algun cambio  en la tabla
        $(document).delegate('#vue-main table tbody', 'DOMSubtreeModified', function (event) {
            event.preventDefault();
            event.stopPropagation();

            var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

            rows.forEach(function (items, key) {

                var inputs = Array.prototype.slice.call(items.querySelectorAll('input.name_handler'));

                inputs.forEach(function (items) {

                    var nameAttr = items.getAttribute('name');

                    var split = nameAttr.split("]");

                    var split2 = nameAttr.split("[")

                    var newName = split[0].split("[")[0] + '[' + key + '][' + split2.pop();

                    // change attribute name
                    items.setAttribute('name', newName);
                });
            });
        });
    </script>
@endsection
