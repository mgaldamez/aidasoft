@extends('layout_principal')



@section('container')

    <section class="col-md-10 col-md-offset-1">

        {!! Form::model($transfer, ['route' => ['transfer.update', $transfer->id], 'method' => 'PUT', 'role' => 'update', 'id' => 'form-transfer', 'class' => 'form-horizontal form-condensed']) !!}


        {!! isset($bodega_id)? Form::hidden('bodega_id', $bodega_id, ['id'=>'bodega_id']) : '' !!}


        <section class="panel">
            <div class="panel-heading">

                <div class="pull-right">
                    @include('partials.status', ['status' => $transfer->status])
                </div>

                <h1>
                    <small>{!! trans('app.attributes.transfer') !!}</small>
                    </br>{!! $transfer->transfer_num !!}
                </h1>

                <small>{!! \Carbon\Carbon::now()->format('D, d M  Y') !!}</small>
            </div>

            <div class="panel-body">
                @include('transfers.partials.general')
            </div>


            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                @include('transfers.partials.table_editing')
            </div>

            <div class="panel-body">
                <div class="col-sm-12">
                    @include('partials.observation_field')
                </div>
            </div>

        </section>


        {!! Form::close() !!}

    </section>

@endsection


@section('extra_scriptBody')


    <table id="line-article" style="display: none">
        <tbody>
        <tr>
            <td class="cell-article">
                <input type="hidden" name="items[][article_id]" autocomplete="off" class="name_handler">
            </td>
            <td class="cell-measurement-unit">
            </td>
            <td class="cell-stock"></td>
            <td class="cell-available">
            </td>
            <td class="cell-quantity" width="200">
                <input type="text" name="items[][quantity]" class="form-control name_handler" autocomplete="off"
                       value="1" autofocus="autofocus">
            </td>
            <td width="50">
                <a href="#" class="btn btn-danger btn-sm" role="delete-row">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
        </tr>
        </tbody>
    </table>

    {!! Html::script('assets/js/transfers-handler.js') !!}

    <script>
        $(document).ready(function () {
            $('#stock-transfer').transfersHandler({
                transfer: JSON.parse('{!! isset($transfer)? $transfer->id : '' !!}')
            });
        });
    </script>

    @include('partials.select2_script')
    <script>
        $(document).ready(function () {
            $('select').select2();
        });
    </script>


    <script>
        $(document).ready(function () {

            var route_original = '{!! route('order.warehouse.details', ['order' => $transfer->order_id,  'warehouse' => ':WAREHOUSE_ID' ]) !!}';
            var tbody = $('#stock-transfer tbody');

            $('#bodega_id').on('change', function (event) {

                var route = route_original;

                route = route.replace(':WAREHOUSE_ID', this.value);

                tbody.empty();

                $.get(route, function (responce) {
                    tbody.append(responce);
                });
            });
        });
    </script>




    {!! Html::script('assets/js/send_to_ajax.js') !!}

    <script>
        $(document).ready(function () {

            var form = $('form[role="update"]');

            $('').sendToAjax({
                method: $(form)[0].method,
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                resetForm: false
            });
        });
    </script>
@endsection



@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <a href="{!! route('transfer.show', $transfer->id) !!}" class="btn">
                            <i class="material-icons md-18">&#xE5C4;</i>Atras
                        </a>

                        <button type="button" class='btn' onclick="document.getElementById('form-transfer').submit()">
                            <i class="material-icons md-18">&#xE161;</i>Guardar
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! url('/') !!}">Inicio</a></li>
        <li><a href="{!! route('transfer.all') !!}">Transferencias</a></li>
        <li class="active">Editar {!! $transfer->transfer_num !!}</li>
    </ol>
@endsection