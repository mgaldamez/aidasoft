@if($status)

    <ul class="step-state">
        <li>
            <a href="javascript:void(0);"><span style="visibility: hidden">""</span></a>
        </li>

        <li {!! ($status === 'draft')? "class='active'" : ''!!}>
            <a href="javascript:void(0);">{!! trans('app.status.draft') !!}</a>
        </li>

        <li {!! ($status === 'in_revision')? "class='active'" : ''!!}>
            <a href="javascript:void(0);">{!! trans('app.status.in_revision') !!}</a>
        </li>

        @if($status === 'for_confirm')
            <li class="active">
                <a href="javascript:void(0);">{!! trans('app.status.for_confirm') !!}</a>
            </li>
        @endif

        @if($status === 'partially_confirmed')
            <li class="active">
                <a href="javascript:void(0);">{!! trans('app.status.partially_confirmed') !!}</a>
            </li>
        @endif

        @if($status === 'canceled')
            <li class="active">
                <a href="javascript:void(0);">{!! trans('app.status.cancel') !!}</a>
            </li>

        @else

            <li {!! ($status === 'confirmed')? "class='active'" : ''!!}>
                <a href="javascript:void(0);">{!! trans('app.status.confirmed') !!}</a>
            </li>
        @endif

        <li>
            <a href="javascript:void(0);"><span style="visibility: hidden">""</span></a>
        </li>
    </ul>
@endif