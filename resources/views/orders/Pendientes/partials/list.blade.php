<tr>

    <td data-sort="{!! $order->sortOrderedAt() !!}">
        <a href="{!! route('order.show', $order->id) !!}">{!! $order->order_num !!}</a>
    </td>

    <td title="{!! $order->ordered_at !!}">
        {!! $order->orderedAt() !!}
    </td>

    <td>
        @if(currentUser()->isAuthor($order))
            {!! isset($order->warehouse_destination)? $order->warehouseDestination() : '' !!}
            <i class="material-icons md-18">&#xE0B2;</i>
        @else
            {!! isset($order->warehouse_origen)? $order->warehouseOrigen() : '' !!}
            <i class="material-icons md-18">&#xE0B5;</i>
        @endif
    </td>


    <td title="{!! $order->require_date !!}" data-sort="{!! $order->sortRequiredAt() !!}">
        {!! $order->requiredAt() !!}
    </td>

    <td>
        @include('partials.status', ['status' => $order->status ])
    </td>

    <td>
        @if(currentUser()->isAuthor($order))
            PARA: {!! $order->forUser->fullName !!}
        @else
            {!! $order->fromUser->fullName !!}
        @endif
    </td>

    <td width="80">

        <div class="pull-right">
            @if($order->status == 'draft')

                <a href="{!! route('order.show', $order->id) !!}" class="btn btn-xs btn-default" title="Editar">
                    <i class="material-icons md-18">&#xE254;</i>
                </a>
            @endif

            <a href="" class="btn btn-xs btn-default" title="Eliminar">
                <i class="material-icons md-18">&#xE872;</i>
            </a>
        </div>

    </td>
</tr>

