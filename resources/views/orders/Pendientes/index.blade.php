@extends('layout_principal')

@section('container')

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <table id="tbl-orders" class="table table-bordered table-condensed text-middle small">
                    <thead>
                    <tr>
                        <th>{!! trans('app.attributes.order_num') !!}</th>
                        <th>{!! trans('app.attributes.date') !!}</th>
                        <th>{!! trans('app.attributes.warehouse') !!}</th>
                        <th>{!! trans('app.attributes.required_at') !!}</th>
                        <th>{!! trans('app.attributes.status') !!}</th>
                        <th>{!! trans('app.attributes.user') !!}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">

                    @foreach($orders as  $order)
             
                        @include('orders.Pendientes.partials.order_row')
               
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection


@section('extra_scriptBody')
    @include('partials.dataTables_script')
    <script>
        $(document).ready(function () {
            $('#tbl-orders').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ],
            });
        });
    </script>
@endsection



@section('header')

    <div class="col-md-12">
        @include('partials.dataTables_filter', ['table' => 'tbl-orders'])
        @include('orders.partials.navbar')
    </div>


@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li class="active">{!! trans('app.attributes.orders') !!}</li>
    </ol>
@endsection