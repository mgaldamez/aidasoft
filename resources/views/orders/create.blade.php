@extends('layout_principal')

@section('extra_scriptHead')
@endsection

@section('container')


    @if(isset($order))
        <div class="col-md-12">
            <div class="text-center text-muted">
                <h1>No has creado ningún pedido!</h1>
                <h4>Debes agregar artículos al carrito para crear un pedido.</h4>

                <br>
                <a href="{!! route('article.index') !!}" class="btn btn-info btn-lg">

                    Agrega articulos al carrito <i class="material-icons">&#xE854;</i>
                </a>
            </div>
        </div>

    @endif

    {{--{!! Form::open(['route' => ['order.store'] , 'class' =>  'form-horizontal form-condensed', 'id' => 'form-order']) !!}--}}

    {{--<div class="col-md-10 col-md-offset-1">--}}

    {{--<div class="panel">--}}

    {{--<div class="panel-heading">--}}
    {{--<span class="pull-right">--}}

    {{--@include('partials.status', ['status' => $order->status ])--}}
    {{--</span>--}}
    {{--<h1>--}}
    {{--<small>Pedidos</small>--}}
    {{--<br>Nueva Orden--}}
    {{--</h1>--}}
    {{--</div>--}}

    {{--<div class="panel-body">--}}
    {{--<div class="row">--}}
    {{--@include('orders.partials.order_general')--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--<div class="panel-body">--}}
    {{--@include('orders.partials.table_editing')--}}
    {{--</div>--}}


    {{--<div class="panel-body">--}}
    {{--<div class="col-sm-12">--}}
    {{--@include('partials.observation_field')--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}

    {{--</div>--}}
    {{--{!! Form::close() !!}--}}


@endsection


@section('extra_scriptBody')

    {{--@include('partials.date_script')--}}


    {{--@include('orders.partials.scripts')--}}

@endsection



@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    {{--<div class="navbar-btn">--}}
                    {{--                        @include('orders.partials.action_controls')--}}

                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('order.index') !!}">{!! trans('app.attributes.order') !!}</a></li>
        <li class="active">Nuevo Pedido</li>
    </ol>
@endsection