<div class="col-sm-6">

    {{--<div class="form-group mdb @if($errors->has('for_user_id')) has-error @endif">--}}

        {{--{!! Form::label('for_user_id', 'Para', ['class' => 'control-label col-md-3' ]) !!}--}}
        {{--<div class="col-md-9">--}}

            {{--@if($order->editing)--}}

                {{--{!! Form::select('for_user_id', \TradeMarketing\User::listing(), old('for_user_id'), ['class' => 'form-control']) !!}--}}
                {{--{!! $errors->first('for_user_id', '<p class="text-danger">:message</p>')  !!}--}}

            {{--@else--}}
                {{--<div class="form-control">{!! isset($order->for_user_id)? $order->forUser->fullName : '' !!}</div>--}}
            {{--@endif--}}

        {{--</div>--}}
    {{--</div>--}}


    <div class="form-group mdb @if($errors->has('warehouse_origen')) has-error @endif">

        {!! Form::label('warehouse_origen', trans('app.attributes.warehouse_origen'), ['class' => 'control-label col-md-3' ]) !!}
        <div class="col-md-9">

            @if($order->editing)

                {!! Form::select('warehouse_origen', \TradeMarketing\Models\Warehouse::listing() , old('warehouse_origen'), ['class' => 'form-control']) !!}


                {!! $errors->first('warehouse_origen', '<p class="text-danger">:message</p>')  !!}

            @else
                <div class="form-control">{!! isset($order->warehouse_origen)? $order->warehouseOrigen() : '' !!}</div>
            @endif

        </div>
    </div>


    <div class="form-group mdb @if($errors->has('warehouse_destination')) has-error @endif">

        {!! Form::label('warehouse_destination',  trans('app.attributes.warehouse_destination'), ['class' => 'control-label col-md-3' ]) !!}
        <div class="col-md-9">
            @if($order->editing)

                @if(currentUser()->isAdmin())
                    {!! Form::select('warehouse_destination',
                    \TradeMarketing\Models\Warehouse::listing() ,
                    old('warehouse_destination'),
                    ['class' => 'form-control']) !!}
                @else

                    {!! Form::select('warehouse_destination',
                    \TradeMarketing\Models\Warehouse::where('main', 1)->lists('description', 'id') ,
                    old('warehouse_destination'),
                    ['class' => 'form-control']) !!}
                @endif

                {!! $errors->first('warehouse_destination', '<p class="text-danger">:message</p>')  !!}

            @else
                <div class="form-control">{!! isset($order->warehouse_destination)? $order->warehouseDestination() : '' !!}</div>
            @endif


        </div>
    </div>
</div>


<div class="col-sm-6">
    <div class="form-group mdb @if($errors->has('require_date')) has-error @endif">

        {!! Form::label('require_date', 'Fecha requerida', ['class' => 'control-label col-md-3' ]) !!}
        <div class="col-md-9">

            @if($order->editing)

                {!! Form::text(
                    'require_date',
                    old('require_date') | (isset($order->require_date)? $order->requiredAt() : null),
                    ['class' => 'form-control datepicker']
                ) !!}

                {!! $errors->first('require_date', '<p class="text-danger">:message</p>')  !!}

            @else
                <div class="form-control">{!! isset($order->require_date)? $order->requiredAt() : '' !!}</div>
            @endif
        </div>
    </div>


</div>