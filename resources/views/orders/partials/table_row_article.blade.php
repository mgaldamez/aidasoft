<tr class="editing">

    <td class="active">
        <span>{!! isset($item->article_id) ? $item->article->description : $articleList[old('items.'.$key.'.article')]  !!}</span>


        <div class="@if($errors->has('items.'.$key.'.article')) has-error @endif">

            {!! Form::hidden('items['.$key.'][article]',isset($item->article_id) ? $item->article_id : old('items.'.$key.'.article') , ['class' => 'name_handler article']) !!}
            {{--            {!! Form::select('items['.$key.'][article]', $articleList, isset($item->article_id) ? $item->article_id : old('items.'.$key.'.article') , ['class' => 'form-control name_handler article', 'placeholder' => 'Selecciona un artículo de la lista'] ) !!}--}}

            {!! $errors->first('items.'.$key.'.article', '<p class="text-danger">:message</p>')  !!}
        </div>
    </td>

    <td class="active">
        <span class="internal_reference">{!! isset($item->article) ? $item->article->internal_reference : old('items.'.$key.'.internal_reference') !!}</span>
        {!! Form::hidden('items['.$key.'][internal_reference]', isset($item->article) ? $item->article->internal_reference : old('items.'.$key.'.internal_reference'), ['class' => 'name_handler internal_reference']) !!}

    </td>


    <td class="active">

        <span class="measurement_unit">{!! isset($item->article)? $item->article->measurementUnit : old('items.'.$key.'.measurement_unit') !!}</span>
        {!! Form::hidden('items['.$key.'][measurement_unit]', isset($item->article)? $item->article->measurementUnit : old('items.'.$key.'.measurement_unit'), ['class' => 'name_handler measurement_unit']) !!}

    </td>

    <td class="active">{!! $item->article->availableForOrders() !!}</td>

    <td class="cell-quantity col-sm-1">

        <span class="text-quantity">{!! isset($item->quantity)?  $item->quantity  : old('items.'.$key.'.quantity') !!}</span>

        <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">

            {!! Form::textarea('items['.$key.'][quantity]', isset($item->quantity) ? $item->quantity :  old('items.'.$key.'.quantity'), ['class' => 'form-control name_handler'] ) !!}

            {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>')  !!}
        </div>
    </td>


    <td>
        <button type="button" class="btn btn-xs btn-danger mdb remove-row" title="Quitar">
            <i class="material-icons md-18">&#xE872;</i>
        </button>
    </td>
</tr>


{{--<tr class="editing">--}}

{{--<td class="active">--}}
{{--<span>{!! isset($item->article_id) ? $item->article->description : $articleList[old('items.'.$key.'.article')]  !!}</span>--}}


{{--<div class="@if($errors->has('items.'.$key.'.article')) has-error @endif">--}}

{{--{!! Form::hidden('items['.$key.'][article]',isset($item->article_id) ? $item->article_id : old('items.'.$key.'.article') , ['class' => 'name_handler article']) !!}--}}
{{--            {!! Form::select('items['.$key.'][article]', $articleList, isset($item->article_id) ? $item->article_id : old('items.'.$key.'.article') , ['class' => 'form-control name_handler article', 'placeholder' => 'Selecciona un artículo de la lista'] ) !!}--}}

{{--{!! $errors->first('items.'.$key.'.article', '<p class="text-danger">:message</p>')  !!}--}}
{{--</div>--}}
{{--</td>--}}

{{--<td class="active">--}}
{{--<span class="internal_reference">{!! isset($item->article) ? $item->article->internal_reference : old('items.'.$key.'.internal_reference') !!}</span>--}}
{{--{!! Form::hidden('items['.$key.'][internal_reference]', isset($item->article) ? $item->article->internal_reference : old('items.'.$key.'.internal_reference'), ['class' => 'name_handler internal_reference']) !!}--}}

{{--</td>--}}


{{--<td class="active">--}}

{{--<span class="measurement_unit">{!! isset($item->article)? $item->article->measurementUnit : old('items.'.$key.'.measurement_unit') !!}</span>--}}
{{--{!! Form::hidden('items['.$key.'][measurement_unit]', isset($item->article)? $item->article->measurementUnit : old('items.'.$key.'.measurement_unit'), ['class' => 'name_handler measurement_unit']) !!}--}}

{{--</td>--}}

{{--<td class="cell-quantity col-sm-1">--}}

{{--<span class="text-quantity">{!! isset($item->quantity)?  $item->quantity  : old('items.'.$key.'.quantity') !!}</span>--}}

{{--<div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">--}}

{{--{!! Form::textarea('items['.$key.'][quantity]', isset($item->quantity) ? $item->quantity :  old('items.'.$key.'.quantity'), ['class' => 'form-control name_handler'] ) !!}--}}

{{--{!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>')  !!}--}}
{{--</div>--}}
{{--</td>--}}


{{--@if(!$order->editing)--}}
{{--<td class="active">--}}
{{--{!! isset($item->pending) ? $item->pending : 0 !!}--}}
{{--</td>--}}

{{--<td class="active">--}}

{{--@if(isset($order->status_id) && $order->status_id != 1)--}}

{{--@if(isset($order->for_user_id) && ($order->for_user_id == currentUser()->id  || currentUser()->authRole('admin') ) )--}}

{{--{!! Form::hidden('items['.$key.'][quantity]', isset($item->article_id) ? $item->article->availableToTransfer($item->quantity, currentUser()->bodega->id) : 0, ['class' => 'form-control']) !!}--}}

{{--{!! isset($item->article_id) ? $item->article->availableToTransfer($item->quantity, auth()->user()->bodega->id) : 0 !!}--}}


{{--{!! ' / ' !!}--}}
{{--{!! isset($item->article_id) ?  $item->article->availableQtyByWarehouse(currentUser()->bodega->id) : 0  !!}--}}
{{--{!! ' (en stock)' !!}--}}
{{--@endif--}}
{{--@endif--}}
{{--</td>--}}

{{--<td class="active"></td>--}}
{{--@endif--}}


{{--<td>--}}
{{--<button type="button" class="btn btn-xs btn-danger mdb remove-row" title="Quitar">--}}
{{--<i class="material-icons md-18">&#xE872;</i>--}}
{{--</button>--}}
{{--</td>--}}
{{--</tr>--}}
