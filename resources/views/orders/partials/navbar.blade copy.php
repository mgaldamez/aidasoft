<!-- Nav tabs -->
<!-- style="display:flex; justify-content:center; border-bottom:0" -->
<ul id="tab" class="nav nav-pills tabs-flex" role="tablist">

    <li role="presentation" class="{!! URL::current() == route('order.index') ? 'active' : ''  !!}">
        <a href="{!! route('order.index') !!}">Todos</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.inbox') ? 'active' : ''  !!}">
        <a href="{!! route('order.inbox') !!}">Entrantes</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.sent') ? 'active' : ''  !!}">
        <a href="{!! route('order.sent') !!}">Enviados</a>
    </li>

    <li role="presentation" class="{!! URL::current() == route('order.drafts') ? 'active' : ''  !!}">
        <a href="{!! route('order.drafts') !!}">Borradores</a>
    </li>
</ul>