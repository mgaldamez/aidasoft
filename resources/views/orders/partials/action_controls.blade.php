@if($order->isDraft())

    @if($order->editing)

        <a href="{!! route('order.show', $order->id) !!}" class="btn">
            Cancelar
        </a>

        <button type="button" class="btn" onclick=" document.getElementById('form-order').submit()">
            <!-- save -->
            Guardar<i class="material-icons md-18">&#xE161;</i>
        </button>

    @else

        <a href="{!! route('article.index') !!}" class="btn">
            <i class="material-icons md-18">&#xE15E;</i>Seguir agregando artículos
        </a>


        @can('edit', $order)
            <a href="{!! route('order.edit', $order->id) !!}" class="btn">
                Editar<i class="material-icons md-18">&#xE254;</i>
            </a>
        @endcan


        @can('send', $order)

            {!! Form::open([
            'route' => ['order.send', $order->id],
            'method' => 'PUT',
            'id' => 'form-send-order',
            'class' => 'hidden'
            ]) !!}
            {!! Form::close() !!}

            <button type="submit" class="btn" onclick="document.getElementById('form-send-order').submit();">
                <!-- send -->
                Enviar Pedido<i class="material-icons md-18">&#xE163;</i>
            </button>

        @endcan
    @endif
@endif



{{--@can('reject', $order)--}}
{{--<button type="button" class="btn">Rechazar</button>--}}
{{--@endcan--}}




@can('process', $order)
@if(!$order->transferCount())
    <a href="{!! route('order.transfer.create', $order->id) !!}" class="btn btn-default">
        Procesar Pedido <i class="material-icons md-18">&#xE0C3;</i>
    </a>
    @endif
@endcan





@if($order->transferCount())

    <a href="{!! route('order.transfers', $order->id) !!}" class="btn btn-warning">Confirmar Recepción</a>
@endif



@if(isset($order->id))

    @if(!$order->editing)
        @can('delete', $order)
            <a href="{!! route('order.delete', $order->id) !!}" class="btn delete-order pull-right">
                Eliminar<i class="material-icons md-18">&#xE92B;</i>
            </a>
        @endcan
    @endif

    @can('cancel', $order)
        <a href="{!! route('order.cancel', $order->id) !!}" title="Cancelar Envio" class="btn pull-right">
            Cancelar Pedido<i class="material-icons md-18">&#xE14C;</i>
        </a>
    @endcan

@endif



