@if($errors->first('items'))
    <div class="text-danger small">
        {!! $errors->first('items', '<p><i class="material-icons md-18">error_outline</i> :message</p>')  !!}
    </div>
@endif


<table id="table-orders" class="table table-bordered table-control small">
    <thead>
    <tr>
        <th>{!! trans('app.attributes.description') !!}</th>
        <th>{!! trans('app.attributes.internal_reference') !!}</th>
        <th>{!! trans('app.attributes.measurement_unit') !!}</th>
        <th>{!! trans('app.attributes.available') !!}</th>
        <th class="cell-quantity">{!! trans('app.attributes.quantity_requested') !!}</th>
        <th width="40"></th>
    </tr>
    </thead>

    <tbody class="text-uppercase">
    @if(old('items'))

        @foreach(old('items') as $key => $item)

            @include('orders.partials.table_row_article')

        @endforeach

    @elseif(!$errors->first('items') && isset($order->details))

        @foreach($order->details as $key => $item)

            @include('orders.partials.table_row_article')

        @endforeach

    @endif
    </tbody>

</table>

<script>

    function inArray(value, array) {

        if ($.inArray(value, array) != -1) {
            return true;
        }

        return false;
    }


    $(document).ready(function () {
        if (column = $('#table-orders thead tr').find('.cell-quantity'))
            only.push(column[0].cellIndex);
    });


    var only = [];

    $('#table-orders').on('click', 'tbody td', function () {

        if (inArray(this.cellIndex, only)) {
            displayForm($(this));
        }
    });


    function displayForm(cell) {

        var column = cell.attr('class').split(" ")[0],
            id = cell.closest('tr').attr('id'),
            cellWidth = cell.css('width'),//obtiene el ancho de la celda para el estilo de ancho del campo de entrada
            cellHeight = cell.css('height'),//obtiene el alto de la celda para el estilo de ancho del campo de entrada
            cellText = $(cell.find(column.replace('cell', '.text'))),//obtiene el texto actual de la celda para el campo de entrada
            prevContent = cellText.text();//almacena el valor anteror


        //borra el texto actual de la celda, mantiene la celda con el ancho actual
        cellText.html('');
        cell.css('width', cellWidth);

        var controls = cell.find('.form-control');
        var control;
        var select2;

        if (controls.length > 1) {
            control = $(controls[0]);
            select2 = $(controls[1]);
            select2.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'});
            control.select();

        } else {
            control = $(controls[0]);
            control.css({'width': cellWidth, 'height': cellHeight, 'display': 'block'}).select();
        }

        //desactiva el listener en la celda individual una vez hecho clic
        cell.on('click', function () {
            return false
        });

        //on keypress within td
        cell.on('keydown', function (event) {
            if (event.keyCode == 13) {//13 == enter

                cellText.text(control.val());
                control.css('display', 'none');

                if (select2 != null)
                    select2.css('display', 'none');


            } else if (event.keyCode == 27) {//27 == escape

                cellText.text(prevContent);//vuelve al valor original
                control.val(prevContent);//vuelve al valor original
                cell.off('click'); //reactivar edición
                control.css('display', 'none');

                if (select2 != null)
                    select2.css('display', 'none');
            }
        });


        control.blur(function () {

            cell.off('click'); //reactivar edición
            control.css('display', 'none');

            if (select2 != null) {

                if (control[0].options[control.val()])
                    cellText.text(control[0].options[control.val()].text);

                select2.css('display', 'none');

            } else {
                cellText.text(control.val());
            }
        });


        control.on('change', function (event) {
            event.preventDefault();
            cell.off('click'); //reactivar edición

            var text;

            if (select2 != null) {

                if (control[0].options[control.val()]) {
                    text = control[0].options[control.val()].text;//revert to original value
                }
                select2.css('display', 'none');

            } else {
                text = control.val();
            }

            cellText.text(text);
        });
    }


    $('#table-orders').on('mouseenter', 'tbody td', function () {

        if (inArray(this.cellIndex, only)) {
            $(this).css({
                'box-shadow': '0 0 1px 0 rgba(0, 0, 0, 0.5), 0 0 1px 0 rgba(0, 0, 0, 0.5)',
                'cursor': 'pointer',
                'z-index': '1'
            });
        }
    }).on('mouseleave', 'tbody td', function () {
        $(this).css({
            'box-shadow': 'none'
        });
    });


    $('textarea.form-control').on('keyup', function () {
        var actual_height = parseInt(this.style.height);

        if (actual_height <= (this.scrollHeight)) {
            this.style.height = "5px";
            this.style.height = (this.scrollHeight) + "px";
        }
    });
</script>