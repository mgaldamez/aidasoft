@foreach($articles as $key => $article)
    <tr>
{{--        <td>{!! $key+1 !!}</td>--}}
        <td class="cell-description">
            <span class="text-description">{!! $article->description !!}</span>

            <div class="form-group">
                {!! Form::textarea('items['.$key.'][name]', $article->description, ['id' => 'items.'.$key.'.name', 'class' => 'form-control'] ) !!}
            </div>
        </td>

        <td class="cell-internal-reference">

            <span class="text-internal-reference">{!! $article->code !!}</span>
            <div class="form-group">
                {!! Form::textarea('items['.$key.'][internal_reference]', $article->code, ['id' => 'items.'.$key.'.internal_reference', 'class' => 'form-control']  ) !!}
            </div>
        </td>

        <td class="cell-unit-cost">
            <span class="text-unit-cost">{!! $article->unit_cost !!}</span>
            <div class="form-group">

                {!! Form::textarea('items['.$key.'][unit_cost]', $article->unit_cost, ['id' => 'items.'.$key.'.unit_cost', 'class' => 'form-control']  ) !!}
            </div>
        </td>

        <td class="cell-brand">
            <span class="text-brand">{!! $article->brand !!}</span>
            <div class="form-group">
                {!! Form::textarea('items['.$key.'][brand]', $article->brand, ['id' => 'items.'.$key.'.brand', 'class' => 'form-control']  ) !!}
            </div>
        </td>

        {{--<td>--}}
            {{--<div class="form-group">--}}
                {{--{!! $article->attributes !!}--}}
            {{--</div>--}}
        {{--</td>--}}

        {{--<td>--}}
            {{--<div class="form-group">--}}
                {{--{!! $article->colour !!}--}}
            {{--</div>--}}
        {{--</td>--}}

        <td class="cell-category">

            <span class="text-category">{!! $article->category !!}</span>
            <div class="form-group">

                {!! Form::textarea('items['.$key.'][category]', $article->category, ['id' => 'items.'.$key.'.category', 'class' => 'form-control']  ) !!}
            </div>
        </td>

        <td style="max-width: 200px" class="cell-long_description">

            <span class="text-long_description">{!! $article->long_description !!}</span>
            <div class="form-group">

                {!! Form::textarea('items['.$key.'][long_description]', $article->long_description, ['id' => 'items.'.$key.'.long_description', 'class' => 'form-control']  ) !!}
            </div>
        </td>
    </tr>
@endforeach