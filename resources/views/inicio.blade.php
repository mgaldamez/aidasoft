
@section('extra_scriptHead')
    <style>
   .mt-0 {
  margin-top: 0 !important;
}

.ml-1 {
  margin-left: ($spacer-x * .25) !important;
}

.px-2 {
  padding-left: ($spacer-x * .5) !important;
  padding-right: ($spacer-x * .5) !important;
}

.p-3 {
  padding: $spacer-y $spacer-x !important;
}
    </style>


@endsection

@section('page_title')
    <span class="hidden-sm hidden-md hidden-lg">ACORNS</span>
@endsection

@section('container')

<div class="card border-success mb-3"  style="max-width: 30rem; margin-left: 5%; margin-top: 5%;  ">
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title"><a>
	<i class="material-icons">
arrow_downward
</i>
	Entradas de inventario</a></h4>
  <p class="card-text">Registrar entrada de articulo a bodegas</p>
   
    <a href="#" class="btn btn-primary">Ir</a>

  </div>

</div>
<div class="card border-success mb-3"  style="max-width: 30rem; margin-left: 5%; margin-top: 5%; ">
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title"><a>
	<i class="material-icons">
	arrow_upward
	</i>
Salidas de inventario</a></h4>
    <!-- Text -->
    <p class="card-text">Registrar Salida de articulo a bodegas</p>
    <!-- Button -->
    <a href="#" class="btn btn-primary">Ir</a>

  </div>

</div>


<div class="card border-success mb-3"  style="max-width: 30rem; margin-left: 5%; margin-top: 5%; ">
  <div class="card-body">

    <!-- Title -->
    <h4 class="card-title"><a>
	<i class="material-icons">
	shop
	</i>Compras pendientes</a></h4>
    <!-- Text -->
    <p class="card-text">Articulo</p>
    <!-- Button -->
    <a href="#" class="btn btn-primary">Ir</a>

  </div>

</div>
@endsection

