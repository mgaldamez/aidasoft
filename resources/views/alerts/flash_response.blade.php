@if (Session::has('alert-status'))

    <ul class="alert-container-layout">

        <li class="alert alert-flash

            @if(Session('alert-status') == 1)

                alert-success

            @elseif(Session('alert-status') == 2)

                    alert-danger

            @elseif(Session('alert-status') == 3)

                    alert-warning

            @elseif(Session('alert-status') == 4)

                    alert-info

            @endif">


            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>


            <div class="alert-container">
                @if(Session('alert-status') == 1)

                    <i class="material-icons">done_all</i>

                @elseif(Session('alert-status') == 2)

                    <i class="material-icons">error_outline</i>

                @elseif(Session('alert-status') == 3)

                    <i class="material-icons">warning</i>

                @elseif(Session('alert-status') == 4)

                    <i class="material-icons">info_outline</i>

                @endif

                <div class="alert-container-message">
                    @if(Session::has('alert-title'))
                      {!! Session::get('alert-title') !!}
                    @endif

                        <span>{!! Session::has('alert-message')? Session::get('alert-message') : '' !!}
                        </span>

                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>
                                <small>{!! $error  !!}</small>
                            </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </li>
    </ul>
@endif
