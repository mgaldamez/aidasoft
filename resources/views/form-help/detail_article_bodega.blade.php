
<div class="form-group">
    <label for="bodega" class="col-md-3 control-label">Bodega (*)</label>
    <div class="col-md-9">   
        <select id="bodega" name="bodega" value='{{ old('bodega') }}' class="form-control _form-control selectpicker" title="Seleccione una bodega de la lista"></select>
        <span id="classesHelpBlock" class="help-block"></span>

    </div>
</div>
<!--.form-group -->     
<div class="form-group">
    <label for="date" class="col-md-3 control-label">Fecha de Ingreso</label>
    <div class="col-md-9 date" data-provide="datepicker">   
        <input type="text" id="date" class="datepicker form-control _form-control" name="date" value='{{ old('date') }}' placeholder="Fecha de ingreso a la bodega  año/mes/día">
    </div>
</div>
<!--.form-group -->                             

@include('articles.form.pakage')
                    
<div class="form-group">
    <label for="unit_cost" class="col-md-3 control-label">Costo unitario (*)</label>
    <div class="col-md-7">
        <input type="text" name="unit_cost" value='{{ old('unit_cost') }}' class="form-control _form-control" aria-label="...">
    </div>
    <!--.col -->
</div>
<!--.form-group -->

<div class="form-group">
    <label for="tax" class="col-md-3 control-label">Impuesto</label>
    <div class="col-md-7">
        <div class="input-group">
            <input type="text" name="tax" value='{{ old('tax') }}' class="form-control _form-control" aria-label="..."> 
            <span class="input-group-addon" ><h4><strong>%</strong></h4></span>
        </div>
    </div>
    <!--.col -->
</div>
<!--.form-group -->

<div class="form-group">
    <label for="minvalue" class="col-md-3 control-label">Valor mínimo (*)</label>
    <div class="col-md-7">
        <input type="number" name="minvalue" value='{{ old('minvalue') }}' class="form-control _form-control" aria-label="...">
    </div>
    <!--.col -->
</div>
<!--.form-group -->

<div class="form-group">
    <label for="maxvalue" class="col-md-3 control-label">Valor máximo</label>
    <div class="col-md-7">
        <input type="number" name="maxvalue" value='{{ old('maxvalue') }}' class="form-control _form-control" aria-label="...">
    </div>
    <!--.col -->
</div>
<!--.form-group -->





