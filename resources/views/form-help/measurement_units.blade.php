
<label>Marca la unidad....</label>

<div class="form-group">     
<div class="checkbox ">
  <label class="col-xs-1 col-md-1 control-label">
    <input type="checkbox" id="check_weight" name="check_weight" >
  </label>
</div>      
    <div class="col-xs-11 col-md-11">
        <div class="row">
            <div class="col-xs-5">
                <input type="text" id="muWeight" name="muWeight" class="form-control _form-control" aria-label="..." placeholder="Peso">
            </div>                             
            <div class="col-xs-7">
                 <select name="weight" id="weight" class="form-control _form-control">
                    <option value="1">Kg - Kilogramos</option>
                    <option value="2">g - Gramos</option>
                    <option value="3">lb - Libras</option>
                    <option value="4">T - Toneladas</option>
                </select>
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col -->

</div>
<!--.form-group -->

 <div class="form-group">  
 <div class="checkbox">
  <label class="col-xs-1 col-md-1 control-label">
    <input type="checkbox" id="check_volume" name="check_volume" >
  </label>
</div>          
    <div class="col-xs-11 col-md-11">
        <div class="row">
            <div class="col-xs-5">
                <input type="text" id="muVolume" name="muVolume" class="form-control _form-control" aria-label="..." placeholder="Volumen">
            </div>                             
            <div class="col-xs-7">
                 <select name="volume" id="volume" class="form-control _form-control">
                    <option value="5">mm3 - Milimetros cubicos</option>
                    <option value="6">cm3 - Centimetros cubicos</option>
                </select>
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col -->

</div>
<!--.form-group -->


 <div class="form-group"> 
    <div class="checkbox">
      <label class="col-xs-1 col-md-1 control-label">
        <input type="checkbox" id="check_length" name="check_length" >
      </label>
    </div>          
    <div class="col-xs-11 col-md-11">
        <div class="row">
            <div class="col-xs-5">
                <input type="text" id="muLength" name="muLength" class="form-control _form-control" aria-label="..." placeholder="Longitud">
            </div>                             
            <div class="col-xs-7">
                 <select name="length" id="length" class="form-control _form-control">
                    <option value="7">m - Metros</option>
                    <option value="8">cm - Centimetros</option>
                    <option value="9">mm - Milimetros</option>
                </select>
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col -->

</div>
<!--.form-group -->

 <div class="form-group">   
   <div class="checkbox">
      <label class="col-xs-1 col-md-1 control-label">
        <input type="checkbox" id="check_temperature" name="check_temperature" >
      </label>
    </div>          
    <div class="col-xs-11 col-md-11">
        <div class="row">
            <div class="col-xs-5">
                <input type="text" id="muTemperature" name="muTemperature" class="form-control _form-control" aria-label="..." placeholder="Temperatura">
            </div>                             
            <div class="col-xs-7">
                 <select name="temperature" id="temperature" class="form-control _form-control">
                    <option value="10">°F</option>
                    <option value="11">°K</option>
                    <option value="12">°C</option>
                </select>
            </div>
            <!-- .col -->
        </div>
        <!-- .row -->
    </div>
    <!-- .col -->

</div>
<!--.form-group -->