@extends('layout_principal')
@section('extra_scriptHead')




    @include('partials.dataTables_script')

@endsection


@section('notify_icon_bar')
    {{--<a href="{{ url( '/article/create' ) }}">--}}
    {{--<i class="icon material-icons">library_add</i>--}}
    {{--</a>--}}
@endsection


@section('header-top')
    <div class="btn-group pull-right">
        <a href="/article/create" class="btn btn-success btn-raised btn-lg">
            <span class="glyphicon glyphicon-plus"></span> Nuevo articulo
        </a>
    </div>

    <ul class="breadcrumb">
        <li><a href="javascript:void(0)">Inicio</a></li>
        <li class="active">Articulos</li>
    </ul>
@endsection


@section('header-bottom')

    <!-- Nav tabs -->
    <ul class="nav nav-tabs tabs-flex">
        <li role="presentation" class="active">
            <a href="#tap1" aria-controls="tap1" role="tab" data-toggle="tab">Articulos</a>
        </li>
        <li role="presentation">
            <a href="#tap2" aria-controls="tap2" role="tab" data-toggle="tab">Lista</a>
        </li>
    </ul>
@endsection


@section('container')

    @if(!count($articles))

        <div class="col-xs-10 col-md-6 col-xs-offset-1 col-md-offset-3">

            <p class="center-block text-center">

                No has registrado articulos aún.
                <br>
                Crea un articulo dando click en el boton

                <i class="material-icons">library_add</i>
            </p>
        </div>

    @endif


    <section class="col-md-12">

        <div class="form-inline">
            {!! Form::model(Request::all(), ['role' => 'search']) !!}

            <div class="input-group">
                <div class="input-group-btn">
                    {!! Form::select('filter', \TradeMarketing\Category::listing(), null, [ 'class' => 'form-control', 'placeholder' => 'Todos']) !!}
                </div>
                {!! Form::text('search', null, ['id' =>'search', 'class' => 'form-control', 'placeholder' => 'Buscar...', 'autocomplete' => 'off']) !!}

            </div>
            <button type="button" class="btn btn-primary btn-raised mdb">Buscar</button>

            {!! Form::close() !!}
        </div>

    </section>


    <br>
    <section class="col-md-12">
        <div id="articlesRender">
            @include('articles.partials.articlesRender')
        </div>

    </section>



    <!-- Modal x-menu-option-->
    <div class="modal fade" id="x-menu-option" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <ul class="x-menu-option">
                    <li>
                        <a id="route" href="" class="btn btn-info btn-rounded" role="button">
                            <span class="glyphicon glyphicon-share"></span>
                        </a></br>
                        Ver
                    </li>
                    <li>
                        <a href="" class="btn btn-warning btn-rounded" role="button">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a></br>
                        Editar
                    </li>
                    <li>
                        <a href="" class="btn btn-success btn-rounded" role="button">
                            <span class=" glyphicon glyphicon-transfer"></span>
                        </a></br>
                        Transferir
                    </li>
                    <li>
                        <a href="" class="btn btn-danger btn-rounded" role="button">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a></br>
                        Eliminar
                    </li>
                </ul>

            </div>
            <!--.modal-content-->
        </div>
        <!--.modal-dialog-->
    </div>
    <!--.modal-->

@endsection


@section('extra_scriptBody')
    <!-- js DataTables-1.10.11-->
    <script src="{{ asset('dataTables/1.10.11/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('dataTables/1.10.11/js/dataTables.bootstrap.min.js')}}"></script>
    {{--<script src="https://cdn.datatables.net/colreorder/1.3.1/js/dataTables.colReorder.min.js"></script>--}}
    {{--<script src="https://cdn.datatables.net/rowreorder/1.1.1/js/dataTables.rowReorder.min.js"></script>--}}
    <script src="{{ asset('assets/js/dataTable_config.js')}}"></script>
    <script src="{!! asset('assets/js/demo_search.js') !!}"></script>

    <script>
        $(document).ready(function () {

//            dataTableConfig()
            $('#movement_e>tbody>tr').remove(); // borrar los datos dentro de  la tabla
            $('table#movements>tbody>tr').remove(); // borrar los datos dentro de  la tabla

            $('.dataTables_filter').addClass('form-group');
            $('.dataTables_length').addClass('form-group');
        });
    </script>
    <script type="text/javascript">

        // obtener el id del articulo y pasarlo al modal como una ruta
        function get_route_id(id) {
            $("#route").attr("href", "/article/" + id + "");
        }

        // mostrar modal
        $('#x-menu-option').on('shown.bs.modal');


        // mejorar las funcion del dataTable_filter
        //        $(document).ready(function () {
        //            var dataFilter = $('.dataTables_filter input[type="search"]');
        //            dataFilter.closest('.dataTables_filter').hide();
        //
        //            $('input[name="search"]').on('keyup', function () {
        //                dataFilter.val($(this).val());
        //                dataFilter.keyup();
        //            })
        //        })
    </script>




@endsection
