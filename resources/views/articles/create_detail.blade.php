@extends('layout_principal') 

@section('extra_scriptHead')
<!-- JQuery Validate Pluging JS -->
<script type="text/javascript" src="{{ asset('jqueryValidation/1.15.1/jquery.validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('jqueryValidation/1.15.1/jquery.validate.form.detail.js') }}"></script>

<!-- Bootstrap Select Pluging -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-select/css/bootstrap-select.css')}}">
<script src="{{ asset('bootstrap/bootstrap-select/js/bootstrap-select.js')}}"></script>

<style>
  /* COLOR */
  .color-container {
    width: 100%;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    margin-bottom: 20px;
  }

  .cont {
    display: block;
  }

  .item {
    list-style: none;
    width: 200px;
    margin-right: 20px;
    height: 60px;
  }

  .item:nth-child(n+2) {
    border-left: none;
  }

  .item:before {
    line-height: 60px;
    color: white;
    display: block;
    text-align: center;
    text-transform: uppercase;
    font-size: 1.1em;
    font-weight: bold;
    z-index: 1;
  }

  .item:active {
    -webkit-transform: scale(0.95);
    -ms-transform: scale(0.95);
    transform: scale(0.95);
  }
/*  legend:ntchild{
    margin-top: 50px;
    color: white;
    background-color: #607D8B;
    padding: .2em 10px;
    border-top: 1px solid #f6f6f6;
  }*/
</style>
@endsection

@section('container')

<div class="col-md-12">
  <div class="row-sm">

    @if( Session::has('status') == 1 )
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <h3><strong>¡Felicidades!</strong></h3>{{ Session::get('msj') }}
    </div>
    @endif

    @if( $quantity <= 0 )
    <div class="alert alert-warning">
      <h3><strong>¡Lo sentimos!</strong></h3>
      La acción que solicitas no es posible; no cuentas con artículos disponibles para detallar.
    </div>
    @else

    <div class="panel panel-primary x-panel">
      <div class="panel-heading"> 
        <h3> {{ $name }} <small>Subtext for header</small></h3>
      </div>
      <form action="" method="POST" id="formArticleDetailList" class="form-horizontal">
        <div class="panel-body">

          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          <div class="col-md-12">
            <div class="row">

              <div class="checkbox">
                <label>
                  <input type="checkbox"> Detalles para prendas de vestir
                </label>
              </div>
              <br>
              <br>

              <div class="clearfix pull-right">  
                <div class="col-x-12 col-sm-1 col-md-4">
                  <div style="width:150px; height:150px">
                    <div id="lbl-amount" class="alert alert-info text-center">
                      <h1><strong>{{ $quantity }}</strong></h1>Stock Actual
                    </div>
                    <input type="hidden" id="amount" value="{{ $quantity }}">
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-5 col-md-3">
                <div class="row">

                  <input type="hidden" id="article" name="article" value="{{ $id }}">

                  <div class="form-group">           
                    <label class="col-xs-2 control-label">Color</label>
                    <div class="col-xs-10" style="max-width:200px; min-width:200px">
                      <div class="input-group" >
                        <input type="text" id="colour" class="form-control _form-control" name="colour" data-toggle="modal" data-target="#colorModal" readonly >

                        <div class="input-group-btn btn-group">
                          <button type="button" id="btn-color" class="btn btn-default btn-raised dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-tint"></span>
                          </button>  
                          <ul class="dropdown-menu  dropdown-menu-right">
                            <li><a href="#" id="selectColor" data-toggle="modal" data-target="#colorModal">Seleccionar color</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" id="newColor">Nuevo color</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" id="removeColor">Quitar color</a></li>
                          </ul>
                        </div>
                        <!-- .btn-group -->
                      </div>
                      <!-- .input-group -->                                
                    </div>
                    <!-- .col -->
                  </div>
                  <!-- .form-group -->
                </div>
                <!-- .row -->
              </div>
              <!-- .col -->
            </div>
            <!-- .row -->
          </div>
          <!-- .col -->

          <div class="col-md-12 ">
            <hr>
            <div id="add_article" class="form-horizontal"></div>
            <button type="button" id="btn-addElement" class="btn btn-raised btn-info btn-md"><span class="glyphicon glyphicon-plus"></span><span class="hidden-xs"> Agregar</span>
            </button>
            <br><br><br>
            <p id="help-status" class="text-danger"></p>
          </div>
          <!-- .col -->
        </div>
        <!-- .panel-body -->

        <div class="panel-footer">
          <button type="submit" id="send" class="btn btn-primary btn-raised btn-lg">Enviar</button>
        </div>
        <!-- .panel-footer -->
      </form>
    </div>
    <!-- .panel -->

    @endif 
  </div>
  <!-- .row-sm -->
</div>
<!-- .col -->

<!-- Modal -->
<div class="modal fade" id="colorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Colores</h4>
      </div>
      <div class="modal-body">

        <div class="cont">
          <ul class="color-container">
            <li class="item" style="background-color:#10628a"></li>
            <li class="item" style="background-color:#00c6d7"></li>
            <li class="item" style="background-color:#48C400"></li>
            <li class="item" style="background-color:#e5a60d"></li>
            <li class="item" style="background-color:#e4007b"></li>
            <li class="item" style="background-color:#F7F8F9"></li>
          </ul>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
      </div>
    </div>
  </div>
</div>
{{-- INPUT COLOR --}}
<input type="color" id="color-piker" name="color-piker" style="visibility:hidden">
<p id="error"></p>
<br>

Total: <input type="text" id="total">
@endsection

@section('extra_scriptBody')
<script src="{{ asset('assets/js/demo_add_article.js')}}"></script>
<!-- COLOR CONVERT -->
<script src="{{ asset('assets/js/demo_color_convert.js')}}"></script>

<script type="text/javascript">
/*
* COLORS
*/
$(document).ready( function() {
  document.getElementById('color-piker').value = "";  
})

$('#newColor').click( function(){
 document.getElementById('color-piker').click();
});


$('#removeColor').click( function(){
 var piker = $('#color');
 piker.val('');
 piker.css({'background-color': '', 'color': ''});
});

$('#color-piker').change(function(){
  var color = this.value;
  console.log( color );

  var inputColor = $('#color');
  inputColor.val(color);
  inputColor.css({'background-color':color, 'color': color});

});

$(document).ready(function () {
  $(".item").click(function () {
    var color = $(this).css("background-color");
    var hexColor = convertRgbToHex(color);

    document.getElementById('color-piker').value = hexColor;   

    var inputColor = $('#colour');
    inputColor.val(hexColor);
    inputColor.css({'background-color':hexColor, 'color': hexColor});

    $('#colorModal').modal('toggle')
  });
});
</script>
<script>

function create_details(){
  var data = $('#formArticleDetailList').serializeArray();
  var token = $('#token').val();

  console.log(data)
  $.ajax({
    url: '/article/bodega',
    headers: { 'X-CSRF-TOKEN': token },
    type: 'POST',
    dataType: 'json',
    data:{ data: data} ,
    async: false,
    success: function (res) {
      console.log(res.message);
      location.reload()
    },
    error: function(err) {
      $('#error').html("")
      $('#error').html("Error: Ocurrio un error, revise el formulario y vuelva a intentarlo")
    },
    complete: function(){
    // setTimeout( function(){
      // $("#loaderDiv").hide();
    // HTTP redirect
    // window.location = "complete";
    // , 3000 )
    }
  }); // end ajax
}
</script>

<script>
  function compareAmounttLimit ( max, item ){
    var total =  0;

    item.each( function ( index, item ) {

      if(  $(item).val() == '' ||  $.isNumeric( $(item).val() ) == false ){
        total += 0
      } else {
        total += parseInt( $(item).val() )
      }
      $('#total').val(total)
    // console.log( index+' - '+ $(item).val())
  });

  // verificar si la suma total es menor que el maximo
  if( total > max ){
    $('#help-status').html("No puedes agregar más artículos de los disponibles en la bodega.")
    $('#btn-addElement').prop('disabled', true)
    $('#lbl-amount').removeClass('alert-info')
    $('#lbl-amount').addClass('alert-danger')
    return false 
  } 

  $('#help-status').html('')
  $('#btn-addElement').prop('disabled', false)
  $('#lbl-amount').addClass('alert-info')
  $('#lbl-amount').removeClass('alert-danger')
  return true
}

$(document).delegate('input[ name="quantity" ]', 'click', function(event) {
 compareAmounttLimit( $('#amount').val(), $('input[name="quantity"]') )
}); 

$(document).delegate('input[ name="quantity" ]', 'change', function(event) {
  compareAmounttLimit( $('#amount').val(), $('input[name="quantity"]') )
});

$(document).delegate('#btn-addElement','click', function(event){
  compareAmounttLimit( $('#amount').val(), $('input[name="quantity"]') )
});

$(document).delegate('#btn-deleteElement','click', function(event){
 compareAmounttLimit( $('#amount').val(), $('input[name="quantity"]') )
});

$(document).ready( function(){
 compareAmounttLimit( $('#amount').val(), $('input[name="quantity"]') )
});

</script>

@endsection