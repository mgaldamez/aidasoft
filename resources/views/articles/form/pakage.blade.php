<div class="form-group">
    <label for="pakageOption" class="col-md-3 control-label">Embalaje (*)</label>
    <div class="col-md-9">
        <div class="radio radio-primary">
            <label><input type="radio" name="pakageOption" id="optionsRadios1" value="1" checked="">Unidades individuales</label>
        </div>
        <div class="radio radio-primary">
            <label><input type="radio" name="pakageOption" id="optionsRadios2" value="2">Cajas / Paquetes</label>
        </div>
    </div>
</div>
<!--.form-group -->   
              
<div class="form-group pakageDetaills hidden">
    <label for="pakage" class="col-md-3 control-label">(*)</label>
    <div class="col-xs-12 col-md-7">
        <div class="row">
            <div class="col-xs-8 col-sm-10">          
                <select id="pakage" name="pakage" value='{{ old('pakage') }}' class="form-control _form-control selectpicker" title="Cajas / empaques">
                    {{-- @foreach($pakages as $pakages)
                    <option value="{{$pakages->id}}">{{$pakages->quantity.' unidades'}}</option>           
                    @endforeach --}} 
                </select>
            </div>
            <div class="col-xs-2 col-sm-2">
                <button type="button" class="btn btn-info btn-raised" data-toggle="tooltip" data-placement="top" title="Agregar nuevo"><span class="glyphicon glyphicon-plus"></span>
                </button>  
            </div>
        </div>
    </div>
    <!-- .col -->
</div>
<!-- .form-group -->

<div class="form-group hidden">           
    <label for="box" class="sr-only col-md-3 control-label">(*)</label>
    <div class="col-md-7">
        <input type="number" id="box" class="form-control _form-control" name="box" value='{{ old('box') }}' placeholder="Cantidad de cajas / empaques" min="1">
    </div>
    <!-- .col -->
</div>
<!--.form-group -->

<div class="form-group hidden">           
    <label for="quantity" class="col-md-3 control-label">(*)</label>
    <div class="col-md-7">
        <input type="number" id="quantity" class="form-control _form-control" name="quantity" value='{{ old('quantity') }}' placeholder="Unidades individuales" min="1">
    </div>
    <!-- .col -->
</div>
<!--.form-group --> 

