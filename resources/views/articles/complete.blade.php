
@extends('layout_principal') 

@section('container')
<style>
	.alert .icon span{
		/*border-radius: 50%;*/
		font-size: 46px;
		height: 56px;
		margin: auto;
		min-width: 56px;
		width: 56px;
		padding: 0;
		/*overflow: hidden;*/
		text-align: center;
		/* box-shadow: 0 1px 1.5px 0 rgba(0, 0, 0, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.24); */
		position: relative;
		line-height: normal;
		display: inline-block;
	}
</style>


@if(Session::has('msj'))
<div class="alert alert-success" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<strong> <h3>Felicidades!</h3></strong> 
	<div class="icon">
		<span class="glyphicon glyphicon-thumbs-up"></span>{{ Session::get('msj') }}
	</div>
	<br>
	Puedes agregar más detalles <a href="" class="btn btn-warning btn-link">aqui</a>
</div>
@endif

<a href="/article/create" class="btn btn-success btn-raised">Agregar nuevo</a>
<a href="" class="btn btn-warning btn-raised">ver</a> 
<a href="" class="btn btn-warning btn-raised">ver la bodega</a> 
@endsection