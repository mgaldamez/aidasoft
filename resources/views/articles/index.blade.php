@extends('layout_principal')



@section('container')

	<div class="col-md-12">
		<div class="row-sm">
			<div class="panel">
				<div class="panel-body">
					<div class="row-sm">
						<table id="table-articles"
							   class="table table-bordered table-condensed table-no-heading table-truncate-text text-middle small">
							<thead>
							<!--RUTA PARA MODIFICAR VISTA DE HACER PEDIDO: Partials/article_add_to_cart.blade.php  -->
							<tr>
							
								<th>{!! trans('app.attributes.article') !!}</th>
						   </tr>
							</thead>
							<tbody>
								@foreach($articles as $article)
								<tr>
									<td>
										@include('partials.article_add_to_cart')
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection



@section('extra_scriptBody')



	<script>
		$(document).ready(function () {

			$(document).on('submit', '.form-shopping-cart', function (event) {
				event.preventDefault();

				var row = $(event.target.closest('td'));

				var button = $(this).find('button').prop("disabled", true);

				var token = '{!! csrf_token() !!}';

				$.ajax({
					url: $(this).attr('action'),
					headers: {'X-CSRF-TOKEN': token},
					type: 'POST',
					data: $(this).serialize(),

					beforeSend: function () {
						var alerts = Array.prototype.slice.call(document.querySelectorAll('.alert-flash'));
						alerts.forEach(function (item) {
							item.remove();
						});
					},
					success: function (response) {

						if (response.html) {
							row.html(response.html);
						}
					},
					complete: function (response) {

						var body = document.getElementsByClassName('container-wrapper')[0];
						if (response.alert) {
							body.insertAdjacentHTML('afterbegin', response.alert);
						}

						if (response.responseJSON.alert) {
							body.insertAdjacentHTML('afterbegin', response.responseJSON.alert);
						}

						button.prop("disabled", false);

						// actualiza el estado del carrito en el menu
						refreshCart();
					}
				});
			});
		});

	</script>



	@include('partials.dataTables_script')
	<script>
		var oTable = $('#table-articles').dataTableConfig({
			"processing": true,
			"serverSide": true,
			"displayLength": 50,
			"ajax": {
				url: '{!! route('articles.filter') !!}',
				headers: {'X-CSRF-TOKEN': '{!! csrf_token() !!}'},
				type: 'POST',
				data: function (d) {
					d.filter = $('select[name="filter"]').val();
					d.param = $('input[name="param"]').val();
				},
				error: function (xhr, error, thrown) {
					console.log(thrown)
				}
			},
			"columns": [
				{data: 'article', name: 'article'}
			]
		});

		$('form[role="search"]').on('keyup', function (e) {
			e.preventDefault();
			oTable.draw();
		});


	</script>
@endsection



@section('header')
	<div class="col-md-12">
		<div class="navbar">
			<div class="container-fluid">

				<div class="navbar-btn">
					@include('partials.dataTables_filter', ['table' => 'table-articles'])

				</div>
			</div>
		</div>
	</div>
@endsection

@section('breadcrumb')
	<ol class="breadcrumb">
		<li><a href="{!! url('/') !!}">Inicio</a></li>
		<li class="active">Artículos</li>
	</ol>
@endsection