@if(isset($attributeValues))

    @foreach( $attributeValues as $value )

        <tr>

            <td>
                {!! Form::select('attribute_id[]', $attributeList, $value->attribute->id,  ['class' => 'form-control']) !!}
            </td>
            <td>
                {!! Form::select(
                'attribute_value_id[]',
                \TradeMarketing\Models\AttributeValue::listing($value->attribute->id),
                $value->id,
                ['class' => 'form-control'])
                !!}
            </td>
        </tr>
    @endforeach

@else
    <tr>
        <td>{!! Form::select('attribute[id][]', $attributeList, old('attribute_id'), ['class' => 'form-control']) !!}</td>
        <td>{!! Form::select(
            'attribute[value_id][]', \TradeMarketing\Models\AttributeValue::listing(1),
            [1],
            ['class' => 'form-control'])
            !!}
        </td>
        <td>
            <button type="button" class="btn btn-fab btn-fab-sm btn-danger mdb" role="buttonDelete">
                <i class="material-icons">delete</i>
            </button>
        </td>
    </tr>
@endif