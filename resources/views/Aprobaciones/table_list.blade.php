<tbody id="app-container" class="text-uppercase">


@foreach($transfers as $transfer)


    <tr>

        <td width="100" title="{!! $transfer->created_at !!}">
            {!! isset($transfer->created_at)!!}
        </td>

        <td>
            <a href="{!! route("external.movement.warehouse", $transfer->id) !!}">{!! $transfer->external_movement_num!!}</a>
        </td>


        <td class="hidden-xs">
            {!! $transfer->agent_id !!}

        </td>

        <td>
            {!! $transfer->warehouse_id!!}
            <i class="material-icons md-18">&#xE915;</i>
            {!! $transfer->warehouse_id!!}
        </td>

        <td>
           @if($transfer->status_id==7)
           
           @include('partials.status', ['status' => 'canceled'])
           @elseif($transfer->status_id==2)
           @include('partials.status', ['status' => 'in_revision'])
            @endif
         
        </td>

        <td class="hidden-xs">
           
               {!! $transfer->created_by !!}

         
        </td>
    </tr>


@endforeach

</tbody>
