@extends('layout_principal')

@section('extra_scriptHead')

<style>
	#tab.nav.nav-pills> li:nth-child(1) a {
		box-shadow: 0 -2px 0 #337ab7 inset;
		color: #777;
	}
</style>
@endsection 


@section('container')


    @include('bodegas.layout_bodega')


<div class="col-xs-12 col-md-12">

	<div class="row-sm">
		<div class="panel">

			<div class="panel-body">
				<div class="row-sm">
					<table class="table table-hover table-sm">
						<thead>
							<tr class="text-uppercase">
								<th class="hidden-xs hidden-sm">Codigo</th>
								<th>Descripción</th>
								<th class="hidden-xs hidden-sm">Entradas</th>
								<th class="hidden-xs hidden-sm">Salidas</th>
								<th class="hidden-xs hidden-sm">Minimo</th>
								<th class="hidden-xs hidden-sm">Maximo</th>
								<th>Stock</th>
							</tr>
						</thead>

						<tbody>
							@foreach( $stockBodega as $stockBodega )

							<tr onclick="location='/article/{{ $stockBodega->article_id }}'">
								<td class="text-uppercase hidden-xs hidden-sm">
									<strong>{{ $stockBodega->barcode }}</strong>
								</td>

								<td> 
									<a href="/article/{{ $stockBodega->article_id }}">{{ $stockBodega->article }} </a>
								</td>

								<td class="hidden-xs hidden-sm">{{ $stockBodega->entry }}</td>

								<td class="hidden-xs hidden-sm">{{ $stockBodega->exit }}</td>

								<td class="hidden-xs hidden-sm">{{ $stockBodega->min_stock }}</td>

								<td class="hidden-xs hidden-sm">{{ $stockBodega->max_stock }}</td>

								@if( ( $stockBodega->stock ) <= $stockBodega->min_stock )
								<td class="danger">
									<span class="glyphicon glyphicon-arrow-down"></span> {{ $stockBodega->stock }} 
								</td>
								@else

								<td>{{ $stockBodega->stock }}</td>
								@endif 
							</tr>
							@endforeach

						</tbody>

					</table>

				</div>
				<!-- .row-sm -->
			</div>
			<!--.panel-body-->
		</div>
		<!--.panel-->
	</div>
	<!--.row-->
</div>
<!--.col-->
@endsection



@section('extra_scriptBody')


@endsection