@extends('layout_principal')


@section('container')


    <section class="col-md-12">

        {!! Form::open(['url' => 'warehouse/admin', 'method' => 'POST', 'role' => 'create']) !!}

        @include('bodegas.partials.general')

        {!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-raised mdb']) !!}

        {!! Form::close() !!}
    </section>


    <section class="col-md-12">
        <table class="table table-striped table-condensed  text-middle">
            <tbody id="warehouses-table">

            @include('bodegas.partials.warehouses_table_render')

            </tbody>
        </table>
    </section>


@endsection


@section('extra_scriptBody')


    <!-- Modal -->
    <div id="editBodega" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="editBodegaLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="editBodegaTitle">Editar la bodega</h4>
                </div>

                <div class="modal-body">

                    {!! Form::open(['method'=>'PUT', 'role'=>'update', 'id' => 'form-update-bodega']) !!}

                    @include('bodegas.partials.general')

                    {!! Form::submit('Save',['id' => 'btnUpdateBodega', 'class'=>'hidden']) !!}
                    {!! Form::close() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary mdb" onclick="document.getElementById('btnUpdateBodega').click()">Guardar</button>
                </div>

            </div>

        </div>

    </div>
    <!-- .modal -->


    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>


    <script>
        $(function () {
            $('#editBodega').on("show.bs.modal", function (event) {
                var modal = this;
                $(modal).find('div[role="alert"]').remove();

                var bodega = $(event.relatedTarget).data('bodega_id');

                $.getJSON("/warehouse/admin/" + bodega + "/edit")
                    .done(function (data) {
                        $(modal).find('#description').val(data.description);
                        $(modal).find('#sucursal_id').val(data.sucursal_id).change();
                        $(modal).find('#colour').val(data.colour);


                        $('#form-update-bodega').sendToAjax({
                            method: $('#form-update-bodega').find('input[name="_method"]').val(),
                            token: $('#form-update-bodega').find('input[name="_token"]').val(),
                            route: document.location.href +'/update/'+ data.id,
                            success: function(data){
                                $('#warehouses-table').html(data);


                                $(modal).modal('hide');
                            }
                        });
                    })
                    .fail(function (xhr) {
//                        $(modal).find('.modal-body')
//                            .append('<div class="alert alert-danger" role="alert">No se encontrarn resultados</div>');
                        console.log(xhr);
                    });

            });
        });
    </script>

@endsection