@extends('bodegas.layout_bodega') 

@section('extra_scriptHead')
<link rel="stylesheet" href="{!! asset('material-design-table/material_design_table.css') !!}">
<!-- css DataTables-1.10.11-->
{{-- <link rel="stylesheet" href="{{ asset('dataTables/1.10.11/css/dataTables.bootstrap.min.css')}}" >
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.material.min.css"> --}}
<style>
	#tab.nav.nav-pills> li:nth-child(2) a {
		box-shadow: 0 -2px 0 #337ab7 inset;
		color: #777;
	}
</style>

@endsection 


@section('prueba')

<?php setlocale (LC_TIME, "es");
?>

<div class="col-xs-12 col-md-12">

	<div class="row-sm">

		<div class="panel">

			<div class="panel-body">
				
				<div class="row-sm">

					<table class="table table-hover table-sm">
						<thead>
							<tr class="text-uppercase">
								{{-- <th class="hidden-xs hidden-sm">#</th> --}}
								<th>F<span class="hidden-xs hidden-sm">e</span>ch<span class="hidden-xs hidden-sm">a</span></th>
								<th>Mov<span class="hidden-xs hidden-sm">imiento</span></th>
								<th class="hidden-xs hidden-sm">Codigo</th>
								<th>Desc<span class="hidden-xs hidden-sm">ripción</span></th>
								<th class="hidden-xs hidden-sm">Embalaje</th>
								<th class="text-right">Cant<span class="hidden-xs hidden-sm">idad</span></th>
								<th class="hidden-xs hidden-sm text-right">Costo U</th>
								<th class="hidden-xs hidden-sm text-right">Total</th>
							</tr>
						</thead>

						<tbody>

							@foreach( $articleBodega as $key => $articleBodega )
							<tr>
								{{-- <td class="hidden-xs hidden-sm">{{ $key+1 }}</td> --}}
								<td>
									<span class="hidden-xs hidden-sm">
										{{ Carbon\Carbon::parse( $articleBodega->created_at )->formatLocalized('%d %B %y') }} 
									</span>
									<span class="hidden-md hidden-lg">
										{{ Carbon\Carbon::parse( $articleBodega->created_at )->formatLocalized('%b %y') }} 
									</span>
								</td>
								<td>{{ $articleBodega->type_movement }}</td>
								<td class="hidden-xs hidden-sm text-uppercase">
									<strong>{{ $articleBodega->barcode }}</strong>
								</td>
								<td> 
									<a href="/article/{{ $articleBodega->article_id}}">{{ $articleBodega->name }} </a>
								</td>
								<td class="hidden-xs hidden-sm">{{ $articleBodega->unity }}</td>
								<td class="text-right">{{ $articleBodega->quantity }}</td>
								<td class="hidden-xs hidden-sm text-right">{{ $articleBodega->unit_cost }}</td>
								<td class="hidden-xs hidden-sm text-right">{{ $articleBodega->amount }}</td>
							</tr>

							@endforeach

						</tbody>

					</table>

				</div>
				<!-- .panel-body -->
			</div>
			<!--.panel-body-->
		</div>
		<!--.panel-->
	</div>
	<!--.row-->
</div>
<!--.col-->

@endsection




@section('extra_scriptBody')
{{-- <!-- js DataTables-1.10.11-->
<script src="{{ asset('dataTables/1.10.11/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('dataTables/1.10.11/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/dataTable_config.js')}}"></script>
<script>
	(function(d){
		dataTableConfig ('table')

	})(document);
</script> --}}
@endsection