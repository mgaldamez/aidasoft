@extends('bodegas.layout_bodega') 
@section('extra_scriptHead')
<script src="{{ asset('Chart.js/2.0-dev/Chart.bundle.min.js')}}"></script>
<style>
	#tab.nav.nav-pills> li:nth-child(3) a {
		box-shadow: 0 -2px 0 #337ab7 inset;
		color: #777;
	}
</style>
@endsection 

@section('prueba')
<div class="col-md-8">
	<canvas id="chartBodega"></canvas> 
</div>
@endsection

@section('extra_scriptBody')
<script src="{{ asset('assets/js/demo_color_convert.js')}}"></script>
<script src="{{ asset('assets/js/charts/articles_bodega.js')}}"></script>
<script src="{{ asset('assets/js/charts/bodega.js')}}"></script>
<script>
	$(document).ready(function (e) {
		e.preventDefault;
		var article = window.location.pathname.split('bodega/')[1];
		var route = '/bodega_articles/' + article;
		var datos = $('#tb_article_bodegas');
		var active;


		var  labels = [],  data = [];
		var _labels = '';
		var _data = '';
		$.get(route, function (res) {

		
			$(res).each(function (key, value) {
				if(value.active == 1){
					active = '<span class="label label-info">Activo</span>';
				} else {
					active = '<span class="label label-default">Inactivo</span>';
				}
				key += 1;
				datos.append("<tr><td>" + key +
					"</td><td class='text-uppercase'><dt>" + value.barcode +"</dt>"+
					"</td><td><a href='/article/" + value.idarticle + "'>" + value.article + "</a>" +
					"</td><td>" + active +
					"</td></tr>");

			})

			$(res).each(function (key, value) {
				_labels += '"' + value.articles + '"'; 
				_data += '"' + value.cant + '"';

				if (key < (res.length - 1)) {
					_labels += ',';
					_data += ',';

				}
			});

			labels[0] = _labels;
			data[0] = _data;

        // function muestra el grafico de los articulos que se encuentran esa bodega
        // la funcion resibe los siguentes paramatros labels, data, bodega, colour
        chart_article_bodega(labels, data, res[0].bodega,  ConvertToRGB(res[0].colour));
    });

		prueba();
	});
</script>
@endsection
