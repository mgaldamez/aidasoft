@extends('layout_principal')

@section('extra_scriptHead')
    @include('partials.dataTables_script')

    <style>
        td.details-control:before {
            content: "+";
            font-size: 18px;
            cursor: pointer;
        }

        tr.shown td.details-control:before {
            content: "-";
            font-size: 18px;
        }
    </style>


@endsection


@section('container')

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <table id="tbl-admin-articles"
                       class="table table-condensed table-responsive table-bordered text-middle small">
                    <thead>
                    <tr>
                        <th>
						</th>
						<th>ID</th>
                            <th></th>
                        <th>NOMBRE APELLIDO</th>
                        <th>DEPARTAMENTO</th>
                   		<th>FEC ENTREGA</td>
							<th>FEC DEVOLUCION</td>
							<td>ESTADO</td>
						
                        <th>
						</th>
                    </tr>
                    </thead>
                    <tbody class="text-uppercase">
                    @foreach($masters as $key => $master)
                        <tr data-article="{!! $master->id !!}">
                          
                            <td></td>

                            <td class="serial-code">{!! $master->ID !!}</td>
                            <td></td>
                            <td>{!! $master->NOMBRE_APELLIDO !!}</td>
                            <td>{!! $master->DEPARTAMENTO !!}</td>
							<td>{!! $master->FEC_ENTREGA !!}</td>
							<td>{!! $master->FEC_DEVOLUCION !!}</td>
                                @if($master->LIBERADO==1)
							<td>DEVUELTO</td>
                            @else
                            <td>PRESTADO</td>
                            @endif

						         <td width="20">
                      
                                <button type="button" class="btn btn-xs btn-info add-user"
                                            onclick="location.href='{!! route( 'Biblioteca.prestar3', $master->ID) !!}';">ver prestamo
                                </button>
                            
                                  
                            </td>




                            </td>
                           
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection


@section('extra_scriptBody')
    <script>



        /* Formatting function for row details - modify as you need */
        function format(d) {
            // `d` is the original data object for the row

            var row = '';

            d.forEach(function (item) {

                row += '<tr>' +
                    '<td></td>' +
                    '<td>' + item.article + '</td>' +
                    '<td>' + item.internal_reference + ' </td>' +
                    '<td>' + item.warehouse + '</td>' +
                    '<td>' + item.stock + '</td>' +
                    '</tr>';
            });

            return '<table class="table table-condensed table-hover text-middle" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' + row + '</table>';
        }


        $(document).ready(function () {

            var tableId = '#tbl-admin-articles';

            var table = $(tableId).dataTableConfig({
                "columns": [
                    {"orderable": false},
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });

            // Add event listener for opening and closing details
            $(tableId).on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var id = tr.data('article');

                var row = table.row(tr);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {

                    $.getJSON('/article/' + id + '/warehouses')
                        .done(function (json) {

                            // Open this row
                            row.child(format(json)).show();
                            tr.addClass('shown');

                        });
                }
            });
        });
    </script>
@endsection





@section('header')



    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">


                    <div class="navbar-btn">

                        <a href="{!! route('Biblioteca.create') !!}" class="btn">
                            <i class="material-icons">&#xE145;</i>Agregar Libro
                        </a>

                   
               <a href="{!! route('Biblioteca') !!}" class="btn btn-default btn-xs mdb pull-right">
                            <i class="material-icons md-18">list</i> <span class="hidden-xs">Ir a la lista</span>
                        </a>
                     


                        @include('partials.dataTables_filter', ['table' => 'tbl-admin-articles'])
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li class="active">Biblioteca</li>
    </ol>
@endsection