<div class="col-sm-6">

    <div class="form-group @if($errors->has('NOMBRE')) has-error @endif">

        {!!Form::label('NOMBRE', 'Nombre (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('NOMBRE', old('NOMBRE'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'Ejm: Libro', 'autofocus'])!!}


            {!! $errors->first('NOMBRE', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">nombre del Libro; ejm: </p>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('EDITORIAL')) has-error @endif">

        {!!Form::label('EDITORIAL', 'Editora', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('EDITORIAL', old('EDITORIAL'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'Ejm: Libro', 'autofocus'])!!}


            {!! $errors->first('EDITORIAL', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Casa editora del Libro; ejm: </p>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('AUTOR')) has-error @endif">

        {!!Form::label('AUTOR', 'Autor (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('AUTOR',  old('AUTOR'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}


            {!! $errors->first('AUTOR', '<p class="text-danger">:message</p>')  !!}

            <br>
        </div>
    </div>
    <!--.form-group -->


   <div class="form-group @if($errors->has('GENERO')) has-error @endif">

        {!!Form::label('GENERO', 'Genero (*)', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('GENERO',  old('GENERO'), ['class' => 'form-control', 'placeholder'=>'-- ninguno --'])!!}


            {!! $errors->first('GENERO', '<p class="text-danger">:message</p>')  !!}

            <br>
        </div>
    </div>
    <!--.form-group -->


</div>
<!-- .col -->


<div class="col-sm-6">

    <div class="form-group @if($errors->has('CODIGO_BARRAS')) has-error @endif">

        {!!Form::label('CODIGO_BARRAS', 'Código de barras', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('CODIGO_BARRAS', old('CODIGO_BARRAS'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('CODIGO_BARRAS', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Agrega el codigo de barras del producto.</p>
        </div>
    </div>
    <!--.form-group -->


    <div class="form-group @if($errors->has('COD_LIBRO')) has-error @endif">

        {!!Form::label('COD_LIBRO', 'Codigo Interno', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('COD_LIBRO', old('COD_LIBRO'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('COD_LIBRO', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">Agrega un codigo interno.</p>
        </div>
    </div>
	
	   <div class="form-group @if($errors->has('PAIS_AUTOR')) has-error @endif">

        {!!Form::label('PAIS_AUTOR', 'Nacionalidad Autor', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('PAIS_AUTOR', old('PAIS_AUTOR'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('PAIS_AUTOR', '<p class="text-danger">:message</p>')  !!}
<br>
        </div>
    </div>
		   <div class="form-group @if($errors->has('ANIO_EDICION')) has-error @endif">

        {!!Form::label('ANIO_EDICION', 'Año de edicion', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('ANIO_EDICION', old('ANIO_EDICION'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}

            {!! $errors->first('ANIO_EDICION', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block"></p>
        </div>
    </div>
    <!--.form-group -->
</div>

<!-- 
<div class="col-sm-12">
    <div class="form-group mdb @if($errors->has('description')) has-error @endif">

        {!!Form::label('long_description', 'Descripción', ['class' => 'control-label'])!!}

        {!!Form::textarea('long_description', old('long_description'), ['class' => 'form-control', 'placeholder' => 'Detalla mas información sobre el Libro', 'rows' => '3', 'maxlength' => '500'])!!}

        <span class="form-control-feedback"><i class="material-icons md-18">mode_edit</i></span>


        {!! $errors->first('long_description', '<p class="text-danger">:message</p>')  !!}

    </div>


</div> -->


