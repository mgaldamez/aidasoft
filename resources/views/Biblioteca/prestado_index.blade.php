@extends('layout_principal')

@section('extra_scriptHead')
    <!-- Croppie JS -->
    <link rel="stylesheet" href="{{ asset('croppie/croppie.css')}}"/>
    <script src="{{ asset('croppie/croppie.js') }}"></script>

    <!-- Select2  Pluging -->
    <link href="{{asset('select2/css/select2.css')}}" rel="stylesheet"/>
    <script src="{{asset('select2/js/select2.js')}}"></script>
    <script src="{{asset('select2/js/i18n/es.js')}}" type="text/javascript"></script>
@endsection

@section('container')

    <div class="col-md-10 col-md-offset-1">

        {!! Form::model( $pres, [ 'route' => [ 'Biblioteca.liberar', $master->ID ],  'method' => 'Post', 'role' => 'update', 'id' => 'form-Biblioteca-update']) !!}


        <section class="panel">

            <div class="panel-heading">
                <h1> {!!$master->NOMBRE!!}</h1>
              
            </div>

            <div class="panel-body">

    


                <legend style="width: auto">Datos de prestamos</legend>
            </div>


            <div class="panel-body form-horizontal">
         @include('Biblioteca.prestamos_general_1')
            </div>


      
        </section>
        <!-- .panel -->

        {!! Form::submit('Update', ['id' => 'btn-update', 'class' => 'hidden']) !!}
        {!! Form::close() !!}

    </div>
@endsection



@section('extra_scriptBody')

    @include('admin.forms.new_caracteristic_modal')
@include('partials.select2_script')
    <script src="{{ asset('assets/js/demo_image.js')}}"></script>

    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>
     <script>
        $(document).ready(function () {

            var form = $('#Biblioteca-update');

            $(form).sendToAjax({
                method: $(form)[0].method,
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
                success: function (response) {

                    if (response.redirectTo) {
                        var route = '{!! route('Biblioteca') !!}';
                        location.href = response.redirectTo;
                    }
                }
            });
        });
    </script>
<script>
        // Confirmar que hubo algun cambio  en la tabla
        $(document).delegate('#tbl-atrributes tbody', 'DOMSubtreeModified', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

            rows.forEach(function (items, key) {

                var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

                elements.forEach(function (item) {

                    var nameAttr = item.getAttribute('name');

                    var split = nameAttr.split("]");

                    // var split_2 = nameAttr.split("[")
                    var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + '][]';

                    // change attribute name
                    item.setAttribute('name', newName);
                });
            });
        });
    </script>
    {{--<script src="{!! asset('assets/js/variants.js') !!}"></script>--}}
    <script src="{!! asset('assets/js/warehouse.js') !!}"></script>
    {{--<script src="{!! asset('assets/js/attributes.js') !!}"></script>--}}

    
    <script>
        $('select.select2').select2();
           $(function () {
            $('.select2').select2({
                tags: true,
                maximumSelectionLength: 1,
                minimumResultsForSearch: Infinity,
                multiple: true,
                allowClear: true
            });
        });
    </script>
@endsection



@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar" style="background-color: transparent">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <a href="{!! route('Biblioteca') !!}" class="btn">
                            <i class="material-icons md-18">&#xE5C4;</i><span class="hidden-xs">Atras</span>
                        </a>

                        <button type="button" class="btn btn-default btn-sm mdb" onclick="document.location.reload()">
                            Cancelar
                        </button>
                     @if($pres->LIBERADO==0)
                        <button type="button" id="btnSubmit" class="btn btn-primary btn-sm mdb" onclick="document.getElementById('btn-update').click();">
                            <i class="material-icons md-18">save</i> Liberar Prestamo
                        </button>
                     @endif
                        <a href="{!! route('Biblioteca') !!}" class="btn btn-default btn-xs mdb pull-right">
                            <i class="material-icons md-18">list</i> <span class="hidden-xs">Ir a la lista</span>
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('Biblioteca') !!}">Biblioteca</a></li>
        <li class="active">Prestamos Libro</li>
    </ol>
@endsection