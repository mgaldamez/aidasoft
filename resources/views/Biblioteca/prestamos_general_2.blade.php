<div class="col-sm-6">

    <div class="form-group @if($errors->has('NOMBRE_APELLIDO')) has-error @endif">

        {!!Form::label('NOMBRE_APELLIDO', 'Nombre Apellido', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('NOMBRE_APELLIDO', old('NOMBRE_APELLIDO'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'Ejm:Juan Perez', 'autofocus'])!!}


            {!! $errors->first('NOMBRE_APELLIDO', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block">nombre de Solicitante ejmp: JUAN PEREZ </p>
        </div>
    </div>
 <div class="form-group @if($errors->has('NOMBRE_APELLIDO')) has-error @endif">

        {!!Form::label('NCT', 'Nct de empleado', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('NCT', old('NCT'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'NCT000', 'autofocus'])!!}


            {!! $errors->first('NCT', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block"> </p>
        </div>
    </div>




    <div class="form-group @if($errors->has('FEC_ENTREGA')) has-error @endif">

        {!!Form::label('FEC_ENTREGA', 'Fecha entrega', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
             
            {!!Form::text('FEC_ENTREGA', old('FEC_ENTREGA'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off'])!!}
           
            {!! $errors->first('FEC_ENTREGA', '<p class="text-danger">:message</p>')  !!}
           <p class="help-block"> </p>

        </div>
    </div>

</div>
<!-- .col -->




<div class="col-sm-6">


    <div class="form-group @if($errors->has('EDITORIAL')) has-error @endif">

        {!!Form::label('DEPARTAMENTO', 'Departamento', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9" >
            {!!Form::text('DEPARTAMENTO', old('DEPARTAMENTO'), ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder'  => 'Ejm: Libro', 'autofocus','id' => 'datepicker'])!!}


            {!! $errors->first('DEPARTAMENTO', '<p class="text-danger">:message</p>')  !!}
         <p class="help-block">Departamento Solicitante; ejm: TradeMarketing </p>
        </div>
    </div>


    <!--.form-group -->

  
    <div class="form-group @if($errors->has('FEC_DEVOLUCION')) has-error @endif">

        {!!Form::label('FEC_DEVOLUCION', 'Fecha Devolucion', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('FEC_DEVOLUCION', old('FEC_DEVOLUCION'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off' ,'id' => 'demo'])!!}

            {!! $errors->first('FEC_DEVOLUCION', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block"> </p>
        </div>
    </div>
<div class="form-group @if($errors->has('FEC_DEVOLUCION')) has-error @endif">

        {!!Form::label('FEC_MAX_DEVOLUCION', 'Fecha Maxima', ['class' => 'control-label col-md-2'])!!}

        <div class="col-md-9">
            {!!Form::text('FEC_MAX_DEVOLUCION', old('FEC_MAX_DEVOLUCION'), ['class' => 'form-control', 'autofocus' => 'true', 'autocomplete' => 'off' ,'id' => 'demo'])!!}

            {!! $errors->first('FEC_MAX_DEVOLUCION', '<p class="text-danger">:message</p>')  !!}
            <p class="help-block"> </p>
        </div>
    </div>

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

  
    <!--.form-group -->
  



