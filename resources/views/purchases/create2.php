@extends('layout_principal')

@section('extra_scriptHead')

<!-- JQuery UI Pluging CSS -->
<link rel="stylesheet" href="{{ asset('jquery.ui/jquery-ui.min.css')}}">

<script src="{{ asset('jquery.ui/jquery-ui.min.js')}}"></script>


<link rel="stylesheet" href="{{ asset('material-design-table/material_design_table.css')}}">

<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">

<!-- Bootstrap DatePiker Pluging JS -->
<script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>

<!-- Bootstrap Bootstrap-select Pluging CSS -->
<link rel="stylesheet" href="{{ asset('/bootstrap/bootstrap-select/css/bootstrap-select.css')}}">





@endsection

@section('container')

<?php setlocale (LC_TIME, "es");
?>


<div class="col-md-12">
	<div class="row-sm">
		<ul class="breadcrumb" style="margin-bottom: 5px;">
			<li><a href="{{ url('/') }}">Inicio</a></li>
			<li><a href="{{ url('/order/purchases') }}">Compras</a></li>
			<li class="active">Nueva compra</li>
		</ul>
	</div>
</div>



@if (count($errors) > 0)
<div class="col-md-12">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif


<div class="col-md-12 col-lg-10 col-lg-offset-1">

	<div class="row-sm">

		@if(Session::has('msj'))
		<div class="alert alert-{{ Session::get('status') }}" role="alert">

			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>

			{{-- <strong><h3>Felicidades!</h3></strong>  --}}

			<div class="icon">

				@if(Session::get('status') == 'success')

				<span class="glyphicon glyphicon-thumbs-up"></span>

				@elseif(Session::get('status') == 'danger')

				<span class="glyphicon glyphicon-thumbs-down"></span>

				@endif

			</div>

			<p>{{ Session::get('msj') }}</p>		

		</div>

		@endif

		<div class="panel">

			<div class="panel-body">
				<span class="label pull-right">Borrador</span>
				<h2>
					<small>Orden de compra</small> {{ $order }}
				</h2>				

				<small>	
					{{ Carbon\Carbon::now()->formatLocalized('%d de %B de %Y') }} 
				</small>

			</div>


			{!! Form::open(['url' => '/order/purchase/', 'method' => 'POST', 'role' => 'create']) !!}

			<input type="hidden" id="order" name="order" value="{{ $order }}">

			<div class="panel-body">

				<div class="form-horizontal">

					@include('purchases.forms.details')

				</div>
				<!-- .form-horizontal -->

			</div>
			<!-- .panel-body -->
			<br>

			<ul class="nav nav-pills tab-control-bottom hidden">
				<li>
					<a href="#" id="countSelect" class="btn btn-link"></a>
				</li>

				<li>
					<a href="#" id="remove-items" class="btn btn-link">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</li>

				<li>
					<a href="#" id="edit-item" class="btn btn-link"  v-if="removeDOM == true">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="#" id="edit-items" class="btn btn-link"  v-if="removeDOM == false">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</li>
			</ul>


			<table id="tbl-article" class="table table-bordered table-sm text-middle total-amount">

				@include('purchases.tables.purchase-items')

			</table>


			<div class="panel-body">

				<div class="col-md-8">

					<div class="row">

						@include('purchases.forms.description')
					</div>
				</div>
				
			</div>
			<!-- .panel-body -->

				<div class="panel-body">

				<div class="btn-group pull-right" role="group" aria-label="...">

					<a href="{{ URL::previous() }}" class="btn btn-default btn-lg mdb">
						<span class="hidden-xs hidden-sm">Atras</span>
						<span class="visible-xs visible-sm"> 
							<span class="glyphicon glyphicon-menu-left"></span> 
						</span> 
					</a>

					{!! Form::submit('Guardar', [ 'id' => 'sendOrder', 'class' => 'btn btn-primary btn-lg btn-raised mdb' ] ) !!}
				</div>
			</div>





			{!! Form::close() !!}

		</div>
		<!-- .panel -->	

	</div>
	<!-- .row-sm -->

</div>
<!-- .col -->



@endsection

@section('extra_scriptBody')


    <!-- Modal -->
    <div id="articleModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="articleModalLabel" v-if="removeDOM == true">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="articleModalLabel">Agregar</h4>
                </div>

                <div class="modal-body">

                    <div id="" class="table-row">

                        @include('purchases.modals.add-article')

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="agree" class="btn btn-primary">Aceptar</button>
                </div>

            </div>

        </div>

    </div>
    <!-- .modal -->




    <input type="hidden" id="add-article">


    <!-- Modal -->
    <div id="discountModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="discountModalLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="discountModal">Descuento</h4>
                </div>

                <div class="modal-body">

                    <div id="create-article">

                        <div class="form-group form-group-lg">

                            {!!Form::label('_discount', 'Descuento', ['class' => 'control-label sr-only'])!!}
                            {!!Form::text('_discount', old('_discount'), ['class' => 'form-control', 'placeholder' => '0.00', 'autocomplete' => 'off' ])!!}
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="add-discount" class="btn btn-primary">Aceptar</button>
                </div>

            </div>

        </div>

    </div>
    <!-- .modal -->




<!-- Bootstrap Bootstrap-select Pluging JS -->
<script src="{{ url('/bootstrap/bootstrap-select/js/bootstrap-select.js') }}"></script>

<script src="{{ asset('assets/js/table.js')}}"></script>

<script src="{{ asset('assets/js/reorder-rows.js')}}"></script>

<!-- DatePiker -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			title: 'hola',
			todayHighlight: false,
			defaultViewDate: 'today',
			clearBtn: true
		});	
	});
</script>

<!-- Bootstrap Bootstrap-select -->
<script>
	$(function(){

		$('select.selectpicker').selectpicker({
			'liveSearchPlaceholder': 'Buscar...',
		});
	});
</script>


<script>
	$(document).ready(function(){
		$('input[list="sucursals"]').change(function(){
			$('input[name="sucursal"]').val($("#sucursals option[value='" + $(this).val() + "']").attr('id'));
		});
	})
</script>


<script>
	// function autocomplete( element, data, focus, position = "fit" ){

	// 	$( element ).autocomplete({
	// 		source: data,
	// 		minLength: 0,
	// 		position: {
	// 			collision: position
	// 		},
	// 		create: function (event,ui) {

	// 			$('.ui-helper-hidden-accessible').remove();

	// 			$(this).data("ui-autocomplete")._renderItem = function (ul, item) {

	// 				var label = ( item.label ) ? "<span class='title'>" + item.label +"</span>" : '';
	// 				var desc = ( item.desc ) ? "<p>"+ item.desc +"</p>" : '';

	// 				return $("<li>")
	// 				.attr("item.autocomplete", item)
	// 				.append( label + desc )
	// 				.appendTo(ul);

	// 			};
	// 		},
	// 		focus: function( event, ui ) {
	// 			// $( this ).val( ui.item.label );
	// 			return false;
	// 		},
	// 		select: function(event, ui) {
	// 			$(this).val( ui.item.label );
	// 			// $(this).attr('title', "hola")
	// 			$(this).siblings('input').val( ui.item.value )

	// 			return false;
	// 		},
	// 		search: function( event, ui ) {
	// 			console.log( event.target.term )
	// 			console.log( ui.term )
	// 		}

	// 	}).focus(function(){   
	// 		( focus ) ? $(this).autocomplete("search") : false;

	// 	});
	// }

	// $(function(d) {
		// var providers = { !! $providers !!};
		// autocomplete("input[id='_provider']", providers, false);

		// var sucursals = { !! $sucursales !!};
		// autocomplete("input[id='_sucursal']", sucursals, false, "flap");

		// var articles = { !! $articles !!};
		// autocomplete(, articles, false);

	// });
</script>


<script src="{{ asset('vuejs/vue.js') }}"></script>

<script>
	var vue = new Vue({

		el: "#app-container", 

		data: {
			removeDOM: false

		},

		methods: {

			_removesm: function( ){

				function mediaQuery (mediaquery ){

					(mediaquery.matches) ? vue.removeDOM = true : vue.removeDOM = false;
				}

				var mediaquery = window.matchMedia("screen and (max-width: 991px)");

				mediaQuery(mediaquery);

				mediaquery.addListener( function(){

					mediaQuery(mediaquery);
				});
			}
		},

		created: function(){ 

			$(function(){
				vue._removesm();
			});
		}

	});
</script>


<script>
	$(function(){
		$('.tab-control-bottom li').click( function(event){
			event.preventDefault()
		});
	});
</script>

    <script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>

    <script>
        $(document).ready(function(){

            var form = document.querySelector('form[role="create"]');

            var options = {
                method: $(form)[0].method,
                token: $(form).find('input[name="_token"]').val(),
                route: $(form)[0].action,
            };

            $('form[role="create"]').sendToAjax(options);

        });
    </script>
@endsection
