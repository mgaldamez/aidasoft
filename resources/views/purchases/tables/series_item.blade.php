<button style='visibility:hidden' type="button" class="btn btn-link" data-toggle="modal" data-target="#editsucursaleModel">
      <i class="material-icons md-18" id="id_serial" name="id_serial" >add_circle_outline</i> Editar sucursal
</button>
<table id="table-quotation" class="table table-bordered table-control text-top small">

    <thead>
    <tr>
 
        <th>{!! trans('app.attributes.quantity') !!}</th>

  
        <th>delete </th>
        
    </tr>

    </thead>


    <tbody class="text-uppercase">
 
            @for($i = 0; $i < 3; $i++)
                @include('purchases.tables.detail_series')
            @endfor

    </tbody>
</table>



<script>

</script>


<!-- Modal -->
<div class="modal fade" id="editsucursaleModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ingresar series</h4>
            </div>
              <div class="modal-body">
              @include('purchases.tables.series')

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary mdb" onclick="ocultar_modal()">Guardar</button>
            </div>
        </div>
    </div>
</div>