<tr class="table-row">


    <td class="cell-description">
        <div class="form-group  @if($errors->has('items.'.$key.'.article')) has-error @endif">
            {!! Form::select('items.'.$key.'.article', $articleList,
            isset($item->article)
            ? $item->article->id
            : old('items.'.$key.'.article'),
            [
           
            'class' => 'article_id form-control input-description',
            'data-live-search'=>'true',
            'placeholder' => '--',
            'width' => '100%',
            'tabindex' => 1,
            
            ]) !!}


            {!! $errors->first('items.'.$key.'.article', '<p class="text-danger">:message</p>') !!}
        </div>

        <span class="text-description">

        @if(isset($articleList[old('items.'.$key.'.article')]))
                {!! $articleList[old('items.'.$key.'.article')] !!}
            @else

                @if(isset($item->article))
                   ' {!!  $item->article->description !!}'
                @else
              
                @endif
            @endif
       
            


        </span>



    </td>

    <td class="cell-internal-reference active hidden-xs">

        <span class="text-internal-reference">       
     
            {!! isset($item->article)
            ? $item->article->internal_reference
            : old('items.'.$key.'.internal_reference') !!}
            
        </span>

        {!! Form::hidden('items['.$key.'][internal_reference]',
        isset($item->article->internal_reference)
        ? $item->article->internal_reference
        : old('items.'.$key.'.internal_reference'),
        ['class' => 'form-control input-internal-reference name_handler']) !!}
    </td>


    <td class="cell-quantity">
    
        <span class="text-quantity" id="span:"  >{!! isset($item->quantity)?  $item->quantity  : old('items.'.$key.'.quantity') !!}</span>

        <div class="form-group @if($errors->has('items.'.$key.'.quantity')) has-error @endif">
            {!! Form::text('items['.$key.'][quantity]', isset($item->quantity)?  $item->quantity  : old('items.'.$key.'.quantity'), ['class' => 'form-control input-quantity  name_handler', 'rows' => 1, 'onFocus'=>'Model_series(this,key:)', 'id'=>'input:','autocomplete'=>'off'] ) !!}

            {!! $errors->first('items.'.$key.'.quantity', '<p class="text-danger">:message</p>') !!}
        </div>
        
    </td>


    <td class="cell-unit-cost">

        <span class="text-unit-cost">{!! isset($item->unit_cost)?  $item->unit_cost  : old('items.'.$key.'.unit_cost') !!}</span>

        <div class="form-group @if($errors->has('items.'.$key.'.unit_cost')) has-error @endif">

            {!! Form::text('items['.$key.'][unit_cost]', isset($item->unit_cost)?  $item->unit_cost  : old('items.'.$key.'.unit_cost'), ['class' => 'form-control input-unit-cost input-unit-cost name_handler', 'rows' => 1, 'tabindex' => 2]) !!}

            {!! $errors->first('items.'.$key.'.unit_cost', '<p class="text-danger">:message</p>') !!}
        </div>
    </td>


    <td class="cell-amount active hidden-xs">

        <span class="text-amount">{!! isset($item->amount)?  $item->amount  : old('items.'.$key.'.amount') !!}</span>

        {!! Form::hidden('items['.$key.'][amount]', isset($item->amount)?  $item->amount  : old('items.'.$key.'.amount'), ['class' => 'form-control input-amount name_handler', 'tabindex' => 3]) !!}
    </td>


    <td width="25">
        <button type="button" class="btn btn-xs text-danger remove" title="Quitar">
            <i class="material-icons md-18">&#xE872;</i>
        </button>
    </td>
</tr>
