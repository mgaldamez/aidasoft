<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
function addrow() {
    var tipo=document.getElementById('type_modal').innerHTML;
    urls='{!! route('get.row') !!}';
           
            $.get(urls, function (html) {
              
                var tbody = $('#'+tipo).find('tbody');
                var num = document.getElementById(tipo).rows.length;
            
                html = html.replace('1.', ''+num+'.');
                html = html.replace('input_serie[1]', 'input_serie['+num+']');
                html = html.replace('tabindex="1"', 'tabindex="'+num+'"');
                html = html.replace('num_doc_temp[1]', 'num_doc_temp['+num+']');
              
                
                
                tbody.append(html);
                var elemento=document.getElementById("num_doc_temp["+num+"]");
               elemento.focus();
               
                var last_row = tbody.find('tr').last();
                


            });
    
}



function addrow1(callback) {
    var num = document.getElementById("t1").rows.length;


    var x = document.createElement("tr");

    var a = document.createElement("td");
    a.setAttribute("scope","row");
    var anode = document.createTextNode(num+'.');
    
    a.appendChild(anode);
    x.appendChild(a);

    a = document.createElement("td");
    a.setAttribute('id','item['+num+'][serie]');
    anode = document.createElement("input");
    b = document.createAttribute("type");
    b.value = "text";
    anode.setAttribute("class","form-control");
    anode.setAttribute("colspan","2");
    anode.setAttribute('onkeydown','goToFirst();');
    anode.setAttribute('id','input['+num+'][series]');
    anode.setAttribute('name','input['+num+'][series]');
    anode.setAttribute("tabindex",num);
    anode.setAttributeNode(b);

    a.appendChild(anode);
   
    x.appendChild(a);
    
    a = document.createElement("td");
    anode = document.createElement('input');
    anode.setAttribute('type','button');
    anode.setAttribute('value','Delete Row');
    anode.setAttribute('onclick','deleteRow(this)');
    a.appendChild(anode);
    x.appendChild(a);
    document.getElementById("t1").appendChild(x);
    var elemento=document.getElementById("input["+num+"]");
   
    if(callback){ callback();}
}

function deleteRowf(e,v) {
  var tr = e.parentElement.parentElement;
  var tbl = e.parentElement.parentElement.parentElement;
  tbl.removeChild(tr);

}

function goToFirst(evt) {
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;


    if (charCode == 9 ) {
        addrow(function(){
       
                
        });

    }

    return false;
};
</script>
<style>
#ti {
    border-collapse: collapse;
    margin: 100px;
}

table, td, th {

    padding: 5px;
}
.ancho{
    width:'60%';
}
</style>
<div id="app">

</div>


<table id=":t1" class="table table-striped">
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Serie</th>
        
        <th scope="col"><input style="margin-top:-200px; padding:10px" type="button" value="Nueva serie" onclick="addrow()" /></th>
    </tr>
    @if(isset($series))
  
             @foreach($series as $key)
                @include('purchases.tables.detail_series')
            @endforeach
    @else
            
             @include('purchases.tables.detail_series')
    @endif
</table>
{{--{!! Html::script('assets/js/vue/seriado.js') !!}--}}