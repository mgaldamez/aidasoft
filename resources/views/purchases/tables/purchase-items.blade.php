<thead> 
	<tr class="text-uppercase">
		<th width="25px" class="">
			<div class="checkbox checkbox-mini" style="margin:0;">
				<label>
					{!! Form::checkbox('select-all', 0, false, [ 'id' => 'select-all'] ) !!}
				</label>
			</div>
		</th>


		<th colspan="6" class="tab-control-bottom hidden-head">
			{{-- <ul class="nav nav-pills"> --}}
				{{-- <li> --}}
					<a href="#" id="countSelect" class="btn btn-link"></a>
				{{-- </li> --}}

				{{-- <li> --}}
					<a href="#" id="remove-items" class="btn btn-link">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				{{-- </li> --}}

				{{-- <li> --}}
					<a href="#" id="edit-item" class="btn btn-link"  v-if="removeDOM == true">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="#" id="edit-items" class="btn btn-link"  v-if="removeDOM == false">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
				{{-- </li> --}}
			{{-- </ul> --}}
		</th>

		<th >
			<span class="hidden-xs hidden-sm">Descripción</span>
			<span class="hidden-md hidden-lg">Desc</span>
		</th>

		<th width="">
			<span class="hidden-xs hidden-sm">Cantidad</span>
			<span class="hidden-md hidden-lg">Cant</span>
		</th>

		<th width="30px" >
			<span class="hidden-xs hidden-sm">Unidad</span>
			<span class="hidden-md hidden-lg">Un</span>
		</th>

		<th width="" >
			<span class="hidden-xs hidden-sm">Costo</span>
			<span class="hidden-md hidden-lg">Cost</span>
		</th>

		<th width="30px" >
			<span class="hidden-xs hidden-sm">Impuesto</span>
			<span class="hidden-md hidden-lg">Imp</span>
		</th>

		<th class="hidden-xs" width="50px">
			<span class="hidden-xs hidden-sm">Monto</span>
			<span class="hidden-md hidden-lg">Mto</span>
		</th>
	</tr>

</thead>

<tbody>

	@if ($errors->has('article'))
	<tr>
		<td colspan="6">
			<p class="text-danger text-center">{!!$errors->first('article')!!}</p>
		</td>
	</tr>

	@endif


	@if( old('item') )

	@for( $i = 0; $i < count(old('item')['article']['id']); $i++ )

	<tr id="{!! old('item')['article']['id'][$i] !!}" class="table-row">
		<td>
			<div class="checkbox checkbox-mini"> 
				<label> 
					{!! Form::checkbox('', 0, false, [ 'name' => 'check' ] ) !!}
				</label> 
			</div>
		</td>

		<td class="cell-description">
			{!! old('item')['article']['text'][$i] !!}
			{!! Form::hidden('', old('item')['article']['text'][$i], [ 'name' => 'item[article][text][]' ] ) !!}
			{!! Form::hidden('', old('item')['article']['id'][$i], [ 'name' => 'item[article][id][]' ] ) !!}
		</td>

		<td>
			{!! Form::text('', old('item')['quantity'][$i], ['name' => 'item[quantity][]', 'readonly'] ) !!}
		</td>

		<td>
			{!! Form::text('', old('item')['unity'][$i], ['name' => 'item[unity][]', 'readonly'] ) !!}
		</td>

		<td>
			{!! Form::text('', old('item')['unit_cost'][$i], ['name' => 'item[unit_cost][]', 'readonly'] ) !!}
		</td>

		<td>
			{!! Form::text('', old('item')['tax']['text'][$i], ['name' => 'item[tax][text][]', 'readonly'] ) !!}
			{!! Form::hidden('', old('item')['tax']['id'][$i], [ 'name' => 'item[tax][id][]' ] ) !!}
		</td>

		<td class="hidden-xs">
			{!! Form::text('', old('item')['amount'][$i], ['name' => 'item[amount][]', 'readonly']) !!}
		</td>
	</tr>

	@endfor

	@else

	@if( isset($details) )

	@foreach( $details as $detail )

	<tr id="{!! $detail->article_id !!}" class="table-row" >
		<td>
			<div class="checkbox checkbox-mini">
				<label>
					{!! Form::checkbox('check', 0, false) !!}
				</label>
			</div>
		</td>

		<td>
			{!! $detail->article->description !!}
			{!! Form::hidden('item[article][text][]', $detail->article->description) !!}
			{!! Form::hidden('item[article][id][]', $detail->article->id ) !!}
		</td>

		<td>
			{!! Form::text('item[quantity][]', $detail->quantity, ['readonly']) !!}
		</td>

		<td>
			{!! Form::text('item[unity][]', $detail->unity_desc, ['readonly']) !!}
		</td>

		<td>
			{!! Form::text('item[unit_cost][]', $detail->unit_cost, ['readonly']) !!}
		</td>

		<td>
			{!! Form::text('item[tax][text][]', $detail->tax * 100 . '%', ['readonly']) !!}
			{!! Form::hidden('item[tax][id][]', $detail->tax) !!}
		</td>

		<td class="hidden-xs">
			{!! Form::text('item[amount][]', $detail->amount, ['readonly', 'style' => 'text-align: right;']) !!}
		</td>
	</tr>
	@endforeach

	@endif

	@endif 
</tbody>

<tfoot>		

	<tr class="hidden">
		<td colspan="7">

			<button id="btn-update" class="btn btn-default btn-raised">Actualizar</button>
		</td>
	</tr>

	<tr id="create-article" class="form-table">

		<td colspan="2">
			{!! Form::select('_article', $articleList, [], ['id' => '_article', 'class' => 'control-table', 'placeholder' => 'Selecciona un articulo' ] ) !!}
		</td>

		<td>
			{!!Form::number('_quantity', old('_quantity'), ['id' => '_quantity', 'class' => 'control-table', 'placeholder' => '0', 'autocomplete' => 'off' ])!!}
		</td>

		<td>
			<label id="_unity" name="_unity" class="control-table">U<span class="hidden-xs">nidad</span></label>
		</td>

		<td>
			{!!Form::number('_unit_cost', old('_unit_cost'), ['id' => '_unit_cost', 'class' => 'control-table', 'placeholder' => '0.00', 'autocomplete' => 'off' ])!!}
		</td>

		<td>
			{!! Form::select('_tax', config('enums.taxes'),  0,  ['id' => '_tax', 'class' => 'control-table'] ) !!}
		</td>

		<td class="hidden-xs">
			{{-- <a href="javascript:void(0)" class="btn btn-primary btn-fab btn-fab-mini mdb" onclick="add( $(this).parents('tr') );"> --}}
			<a href="javascript:void(0)" class="btn btn-primary btn-fab btn-fab-mini mdb" id="btn-add">
				<span class="glyphicon glyphicon-plus"></span>
			</a>
		</td>

	</tr>	

	<tr v-if="removeDOM == true">

		<td colspan="7" class="lead">
			<a href="" class="btn btn-primary btn-raised mdb" data-toggle="modal" data-target="#articleModal"> 
				<span class="glyphicon glyphicon-plus"></span> Artículo
			</a> 
		</td>
	</tr>

	<tr>
		<td class="hidden-xs"></td>
		<td colspan="3" class="lead">Monto</td>
		<td colspan="3"></td>
	</tr>			

	<tr>
		<td class="hidden-xs"></td>
		<td colspan="3">Subtotal</td>
		<td colspan="3">
			{!! Form::text('subtotal', old('subtotal'), ['readonly']) !!}
		</td>
	</tr>	

	<tr>
		<td class="hidden-xs"></td>
		<td colspan="3">
			<button type="button" class="btn btn-link" style="padding: 2px;">
				<span class="glyphicon glyphicon-plus"></span> 
			</button> Impuesto
		</td>

		<td colspan="3">
			{!! Form::text('tax', old('tax'), ['readonly']) !!}
		</td>
	</tr>

	<tr>
		<td class="hidden-xs"></td>
		<td colspan="3">
			<button type="button" class="btn btn-link"  data-toggle="modal" data-target="#discountModal" style="text-transform: capitalize; padding: 2px;">
				<span class="glyphicon glyphicon-pencil"></span> 
			</button> Descuento
		</td>

		<td colspan="3">
			{!! Form::text('discount', old('discount'), ['readonly']) !!}

		</td>
	</tr>

	<tr>
		<td class="hidden-xs"></td>
		<td colspan="3">TOTAL</td>
		<td colspan="3">
			{!! Form::text('total', old('total'), ['readonly'])!!}
		</td>
	</tr>

</tfoot>



