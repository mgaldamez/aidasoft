@can('delete', $purchase)
    @if(!$purchase->editing)
        <a href="{!! route('purchase.edit', $purchase->id) !!}" class="btn">Editar
            <i class="material-icons md-18">&#xE254;</i>
        </a>
    @endif
@endcan


@if($purchase->isDraft() and $purchase->editing)

    <a href="{!! route('purchase.show', $purchase->id) !!}" class="btn"
       onclick="document.getElementById('form-purchase').submit()"> Cancelar
    </a>

    <button type="button" class="btn" onclick="document.getElementById('form-purchase').submit()">Guardar
        <i class="material-icons md-18">&#xE161;</i>
    </button>
@endif



@if(!$purchase->editing)

    {{--<button type="button" class="btn"--}}
    {{--onclick="document.getElementById('confirm_reception').click();">Confirmar--}}
    {{--</button>--}}


    {{--{!! Form::open(['route' => ['purchase.confirm', $purchase->id], 'method' => 'POST', 'id' => 'form-purchase']) !!}--}}
    {{--{!! Form::close() !!}--}}


    <div class="pull-right">


        @can('delete', $purchase)
            <a href="{!! route('purchase.delete', $purchase->id) !!}" class="btn delete-purchase">Eliminar
                <i class="material-icons md-18">&#xE92B;</i>
            </a>
        @endcan
        {{--<button type="button" class="btn" data-toggle="modal" data-target="#cancelPurchaseModal"--}}
        {{--data-purchase="{!! $purchase->id !!}">Cancelar Compra--}}
        {{--</button>--}}

    </div>
@endif




