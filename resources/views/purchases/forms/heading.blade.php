<div class="col-sm-6">

    <div class="form-group mdb @if($errors->has('provider_id')) has-error @endif">

        {!! Form::label('provider_id', trans('app.attributes.provider'), ['class' => 'control-label col-md-3']) !!}

        <div class="col-md-9">
            {!! Form::select('provider_id', \TradeMarketing\Clients::lista_proveedores(), old('provider_id'), ['class'=>'form-control', 'placeholder' => '--']) !!}

            {!! $errors->first('provider_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>



    <div class="form-group mdb @if($errors->has('warehouse_id')) has-error @endif">

        {!! Form::label('warehouse_id', trans('app.attributes.warehouse_destination'), ['class' => 'control-label col-md-3']) !!}

        <div class="col-md-9">
            {!! Form::select('warehouse_id', $warehouses, old('warehouse_id'), ['class'=>'form-control']) !!}

            {!! $errors->first('warehouse_id', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>

</div>



<div class="col-sm-6">

    <div class="form-group mdb has-feedback @if($errors->has('require_date')) has-error @endif">

        {!! Form::label('require_date', trans('app.attributes.required_date'), ['class' => 'control-label col-md-3']) !!}

        <div class="col-md-9">
            {!! Form::text('require_date', old('require_date') | isset($purchase->require_date) ? $purchase->requiredAt() : null, ['class' => 'form-control datepicker', 'autocomplete' => 'off']) !!}

            <span class="glyphicon glyphicon-calendar form-control-feedback" aria-hidden="true"></span>
            {!! $errors->first('require_date', '<p class="text-danger">:message</p>') !!}
        </div>
    </div>
</div>