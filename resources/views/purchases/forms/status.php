if( $purchase->order_status_id == 1 ) {{--Draft--}}

<span class="label label-default">{!! isset($purchase->order_status)? trans('app.status.'.$purchase->order_status) :trans('app.status.'.$purchase->status->description )  !!}</span>
@elseif(  $purchase->order_status_id == 5)

    <span class="label label-info">{!! isset($purchase->order_status)? trans('app.status.'.$purchase->order_status) : trans('app.status.'.$purchase->status->description ) !!}</span>

@elseif(  $purchase->order_status_id == 6)
    <span class="label label-success">{!! isset($purchase->order_status)? trans('app.status.'.$purchase->order_status) : trans('app.status.'.$purchase->status->description )  !!}</span>

@elseif($purchase->order_status_id == 7)

    <span class="label label-danger">{!! isset($purchase->order_status)? trans('app.status.'.$purchase->order_status) : trans('app.status.'.$purchase->status->description ) !!}</span>

@elseif($purchase->order_status_id == 9)

    <span class="label label-info">{!! isset($purchase->order_status)? trans('app.status.'.$purchase->order_status) : trans('app.status.'.$purchase->status->description ) !!}</span>
@endif