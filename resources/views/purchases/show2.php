@extends('layout_principal')

@section('extra_scriptHead')
<link rel="stylesheet" href="{!! asset('material-design-table/material_design_table.css') !!}">
@endsection

@section('container')

<div class="col-md-12">
	<div class="row-sm">
		
		{!! Form::open( [ 'id' => 'myForm2', 'url' => '/received/purchase/cancel' ] ) !!}
		{!! Form::hidden('order_id', $order->id ) !!}
		{!! Form::close() !!}

		{!! Form::open( [ 'id' => 'myForm', 'url' => '/received/purchase/confirm' ] ) !!}
		{!! Form::hidden('order_id', $order->id ) !!}
		{!! Form::close() !!}


		<ul class="nav nav-tabs tabs-default">
			<li role="presentation">
				<a href="{!! url( '/order/create/purchases' ) !!}">Nueva Orden</a>
			</li>
			
			@if( $order->order_status_id == 1 )	
			<li role="presentation" class="pull-right">
				<a href="javascript:void(0)" class="btn btn-success btn-raised" onclick="document.getElementById('myForm2').submit()">Cancelar Orden</a>
			</li>
			@endif

			<li role="presentation" class="hidden">
				<a href="{!! url( '' ) !!}"  class="">Enviar 
					<i class="material-icons">email</i>
				</a>
			</li>

			@if( $order->order_status_id == 1 )		
			<li role="presentation">
				<a href="javascript:void(0)" onclick="document.getElementById('myForm').submit()">Confirmar recepción</a>
			</li>		
			@endif

			@if( $order->order_status_id == 5 )
			<li role="presentation">
				<a href="{!! url('/received/purchase/'.$order->id.'/create' ) !!}">Recibir
					<i class="material-icons">local_shipping</i>
					<span class="badge">{!! $order->missing !!}</span>
				</a>
			</li>
			@endif
			
			@if( $order->order_status_id == 1 )
			<li role="presentation">
				<a href="{!! url( '/order/purchase/edit', ['id' =>  $order->id] ) !!}">
					<span class="hidden-xs">Editar </span>
					<i class="material-icons">mode_edit</i>
				</a>
			</li>
			@endif
		</ul>
		<br>

	</div>
</div>

<div class="col-md-10 col-md-offset-1">
	<div class="row-sm">

		<div class="panel">

			<div class="panel-body">

				<ul class="panel_toolbox">

					<li class="dropdown">		
						<a class="collapse-link" data-target="#" class="btn dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-menu-down"></span>
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<a href="{!! url( '/order/purchase/edit', ['id' =>  $order->id] ) !!}">
									<span class="glyphicon glyphicon-pencil"></span> Editar
								</a>
							</li>
							<li><a href="javascript:void(0)">Another action</a></li>
							<li><a href="javascript:void(0)"><span class="glyphicon glyphicon-pencil"></span> Something else here</a></li>
							<li class="divider"></li>
							<li><a href="javascript:void(0)">Separated link</a></li>
						</ul>
					</li>
				</ul>
				{{-- <span class="label label-default pull-right">{{ $order->order_status }}</span> --}}
				<h2>Solicitud de compra</h2> 
			</div>	


			<div class="panel-body">

				<div class="form-horizontal">

					<div class="col-md-4">

						<div class="row">		

							<span class="lead"><strong>Telefónica Móviles Panamá, S.A.</strong></span>

							<address>
								Business Parck, Edificio Este<br>
								Ave La Rotonda<br>
								Urbanización Costa del Este<br>
								<span class="glyphicon glyphicon-earphone"></span> (507) 378-7500<br>
								<span class="glyphicon glyphicon-print"></span> (507) 300-6168<br>
								<span class="glyphicon glyphicon-envelope"></span>
								<a href="mailto:#"> movistar@telefonica.com</a>
							</address>

						</div>
					</div>


					<div class="col-md-4">

						<div class="row">		

							<span class="lead"><strong>Proveedor:</strong></span>

							<address>
								<strong>{!! isset($order->provider)? $order->provider->firstname : ''!!}</strong><br>
								{{--{!! $order->provider->address !!}<br>--}}

								{{--<span class="glyphicon glyphicon-earphone"></span>{!! $order->provider->phone !!}<br>--}}

								{{--<span class="glyphicon glyphicon-envelope"></span>--}}
								{{--<a href="mailto:#"> {!! $order->provider->email !!}</a>--}}
							</address>

						</div>
					</div>

					<div class="col-md-4">

						<div class="row">

							<span class="lead"><strong>Referencia:</strong></span> 						

							<address>								
								<strong>Orden: </strong>{!! $order->order !!}<br>
								<strong>Creada: </strong>{!! $order->created_at !!}<br>
								<br>
								<strong>Agente: </strong>{!! $order->user->agent_id !!}<br>
								<strong>Agente ID: </strong>{!! $order->user->agent_id !!}<br> 
							</address>


							<span class="lead"><strong>Engrega:</strong></span> 

							<address>								
								<abbr title="Fecha de entrega"><strong>Fecha: </strong></abbr> {!! $order->require_date !!}<br>
								<abbr title="Lugar de entrega"><strong>Lugar: </strong></abbr> {!! $order->sucursal->DESCR_SUCURSAL !!}<br>
							</address>

						</div>
					</div>

				</div>
				<!-- .form-horizontal -->

			</div>
			<!-- .panel-body -->

			<div class="panel-body">

				<div class="row-sm">

					<table id="tbl-article" class="article table table-striped table-sm text-middle total-amount">
						<thead>
							<tr class="text-uppercase">
								<th>Cant<span class="hidden-xs hidden-sm">idad</span></th>
								<th class="hidden-xs">Codigo</th>
								<th>Desc<span class="hidden-xs hidden-sm">ripción</span></th>
								<th>Cost<span class="hidden-xs hidden-sm">o</span></th>
								<th class="hidden-xs">Imp<span class="hidden-sm">uesto</span></th>
								<th>M<span class="hidden-xs hidden-sm">on</span>to</th>
							</tr>
						</thead>

						<tbody >
							@foreach( $details as $key => $detail )
							<tr>
								<td>{{ $detail->quantity }}</td>
								<td class="hidden-xs">
									<strong class="text-uppercase">{{ $detail->article->internal_reference }}</strong>
								</td>
								<td>{{ $detail->article->fullDescription }}</td>
								<td>{{ $detail->unit_cost }}</td>
								<td class="hidden-xs">{{ $detail->tax * 100 }}%</td>
								<td>{{ $detail->amount }}</td>
							</tr>
							@endforeach
						</tbody>


						<tfoot>	
							<tr>
								<td colspan="5" class="hidden-xs lead">Monto</td>
								<td colspan="3" class="visible-xs lead">Monto</td>
								<td></td>
							</tr>			

							<tr>
								<td colspan="5" class="hidden-xs">Subtotal</td>
								<td colspan="3" class="visible-xs">Subtotal</td>
								<td>{{ $order->subtotal }}</td>
							</tr>	

							<tr>
								<td colspan="5" class="hidden-xs">Impuesto</td>
								<td colspan="3" class="visible-xs">Impuesto</td>
								<td>{{ $order->tax }}</td>
							</tr>

							<tr>
								<td colspan="5" class="hidden-xs">Descuento</td>
								<td colspan="3" class="visible-xs">Descuento</td>
								<td>{{ $order->discount }}</td>
							</tr>

							<tr class="total">
								<td colspan="5" class="hidden-xs">TOTAL</td>
								<td colspan="3" class="visible-xs">TOTAL</td>
								<td>{{ $order->total }}</td>
							</tr>	

						</tfoot>


					</table>

				</div> 
				<!-- .row-sm -->
			</div>
			<!-- .panel-body -->

			<div class="panel-body">

				<div class="col-md-8">

					<div class="row">
						{!! Form::label('observation', 'Observaciones', ['class' => 'col-md-12 control-label'] ) !!}
						<div class="col-md-12">
							<p>{!! $order->observation !!}</p>

						</div>
					</div>
				</div>

			</div>
			<!-- .panel-body -->


			<div class="panel-body">
				<div class="btn-group btn-group-xs" role="group" aria-label="...">	
					<button class="btn">
						<span class="hidden-xs">Imprimir </span>
						<span class="glyphicon glyphicon-print"></span>
					</button>
					<button class="btn">
						<span class="hidden-xs">Crear </span>Excel 
						<span class="glyphicon glyphicon-export"></span>
					</button>
				</div>
			</div>

		</div>
		<!-- .panel -->	
	</div>
</div>

<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

@endsection

@section('extra_scriptBody')

<script>

	function approve(){
		var token =  $('#token').val()

		$.ajax({
			headers: { 'X-CSRF-TOKEN': token },
			url: '/order/purchase/approve/',
			method: 'PUT',
			dataType: 'json',
			data: { data : {{ $order->id }} } ,
			complete: function (res) {
				if (res.success) {

					$(document).ajax.reload();
				}
			}
		});

	}

</script>

@endsection