@extends('layout_principal')

@section('extra_scriptHead')

    {!! Html::style('material-design-table/material_design_table.css') !!}

    <!-- Select2 Plugin -->
    {!! Html::style('select2/css/select2.css') !!}
    {!! Html::script('select2/js/select2.js') !!}

@endsection

@section('container')

    <div class="col-md-8 col-md-offset-2">

        <div class="row-sm">

            @include('alerts.flash_response')

            @if( isset($order) )

                <div class="panel">

                    <div class="panel-heading">

                        <h2>Recepción del Pedido</h2>
                        <small>{!! $today !!}</small>

                        <h3 class="panel-title">
                            <small>Referencia</small> {!! $order->order !!}
                        </h3>

                        <p><strong>Fecha programada: </strong>{!! $order->require_date !!}</p>
                        {{-- <p class="panel-title">Recibodo por: {!! Auth::user()->agent_id !!}</p> --}}
                        <p><strong>Proveedor: </strong>{!!$order->provider->firstname!!}</p>
                        <p>{!!$order->sucursal->DESCR_SUCURSAL!!}</p>
                        <button class="btn btn-xs btn-link">Cambiar</button>
                    </div>
                    <!-- .panel-heading -->

                    @if( count($details) != 0 )
                        {!! Form::open( ['url' => '/received/purchase/', 'method' => 'POST', 'id' => 'myForm']) !!}


                        {!! Form::hidden('purchase_order_id', $order->id) !!}
                        {!! Form::hidden('provider', $order->provider_id) !!}
                        {!! Form::hidden('received_in', $order->require_place_id) !!}
                        {!! Form::hidden('backorder', 0) !!}

                        <div id="received">
                            <table class="table text-middle">

                                <thead>
                                <tr class="text-uppercase">
                                    <th>Articulo</th>
                                    <th width="250">Recibida</th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach( $details as $key => $detail )

                                    <tr>
                                        <td>
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <a href="#">

                                                        @if($detail->article->image)
                                                            <img src="{!! url('assets/img/'.$detail->article->image.'') !!}"
                                                                 alt="" class="media-object img-rounded" width="50">
                                                        @else

                                                            @if($detail->article->master->image)
                                                                <img src="{!! url('assets/img/'.$detail->article->master->image.'') !!}"
                                                                     alt="" class="media-object img-rounded" width="50">
                                                            @else
                                                                <img src="{!! url('http://lorempixel.com/sports/1/Dummy-Text') !!}"
                                                                     alt="" class="media-object img-rounded" width="50">

                                                            @endif
                                                        @endif

                                                    </a>
                                                </div>
                                                <div class="media-body hidden-xs">
                                                    <h4 class="media-heading">{!! $detail->article->description !!}</h4>

                                                    {!! Form::hidden('item['.$key.'][id]', $detail->id )!!}
                                                    {!! Form::hidden('item['.$key.'][text]', $detail->article->description)!!}
                                                </div>
                                            </div>
                                        </td>


                                        <td>
                                            <div class="form-group mdb">
                                                {!! Form::number(
                                                    'item['.$key.'][quantity]',
                                                    0 | $detail->missing,
                                                    [
                                                    'class' => 'form-control',
                                                    'placeholder' => '0000',
                                                    'autocomplete' => 'off',
                                                    'min' => 0,
                                                    'max' => abs( $detail->missing )
                                                    ]) !!}

                                                <p class="help-block">Se esperan {!! $detail->missing !!} unidades</p>
                                            </div>
                                            {!! Form::hidden('item['.$key.'][missing]',  abs( $detail->missing ), ['readonly']) !!}

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                        <div class="panel-body bg-info">

                            <div class="col-sm-5">

                                <p class="help-block">
                                    <i class="material-icons pull-right" style="margin-left: 5px;">info_outline</i>
                                    Puedes asignar directamente la bodega en la que se almacenaran los articulos
                                    recibidos
                                </p>

                                <div class="form-group">

                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="bodega_confirm"
                                                   value="{!! old('bodega_confirm') !!}"> Asignar a una bodega
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">

                                    @if(old('bodega_confirm'))

                                        {!! Form::select('bodega', $bodegas, old('bodega'), ['class' => 'form-control mdb', 'title' => 'Ninguno' ])!!}
                                    @else

                                        {!! Form::select('bodega', $bodegas, old('bodega'), ['class' => 'form-control mdb', 'title' => 'Ninguno', 'disabled' => 'true' ])!!}
                                    @endif

                                    @if ($errors->has('bodega'))

                                        <p class="text-danger">{!!$errors->first('bodega')!!}</p>
                                    @endif
                                </div>
                                <!-- .form-group -->
                            </div>
                            <!-- .col -->
                        </div>
                        <!-- .panel-body -->

                        @if( $order->order_status_id != 7 )
                            <div class="panel-body">

                                <div class="form-group has-feedback pull-left mdb" style="margin-top: 0;">
                                    {!! Form::textarea('observation', old('description'), ['class' => 'form-control', 'rows' => '3', 'placeholder' => 'Observaciones']) !!}
                                    <span class="glyphicon glyphicon-pencil form-control-feedback"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="btn-group pull-right">
                                    <a href="{!! URL::previous() !!}" class="btn btn-default btn-lg mdb">Cancelar</a>
                                    {!! Form::button('Confirmar', ['id' => 'confirm', 'class' => 'btn btn-success btn-raised btn-lg mdb']) !!}
                                </div>
                            </div>
                        @endif

                        {!! Form::close() !!}

                    @else
                        <i class="material-icons">sentiment_satisfied</i> Todos tus pedidos han sido recibidos
                    @endif

                </div>
                <!-- .panel -->
            @endif
        </div>
        <!-- .row -->

    </div>
    <!-- .col -->


    <div class="col-md-8 col-md-offset-2">
        <div class="row-sm">
            <div class="panel">
                @include('purchases.unassigned')
            </div>
        </div>
    </div>

@endsection




@section('extra_scriptBody')

    <script>
        $(document).ready(function (event) {

            $('select').select2({
                minimumResult: 20,
                minimumResultsForSearch: Infinity
            });

            /*SELECT CHECK */
            $(document).delegate('input[name="bodega_confirm"]', 'click', function (event) {

                var checkbox = $(this);

                if (checkbox.is(':checked')) {
                    $('select[name="bodega"]').prop('disabled', false);
                } else {
                    $('select[name="bodega"]').prop('disabled', true);
                    $('select[name="bodega"]').val('').change();
                }
                $('select[name="bodega"]').focus()
                // $('select[name="bodega"]').selectpicker('refresh');
            });
        });
    </script>

    {!! Html::script('assets/js/received.js') !!}

@endsection
