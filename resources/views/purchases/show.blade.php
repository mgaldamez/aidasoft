@extends('layout_principal')


@section('container')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel">

            <div class="panel-heading">

                <h1>
                    <small>{!! trans('app.attributes.purchase') !!}</small>
                    <br>{!! $purchase->order !!}
                </h1>
            </div>

            <div class="panel-body">

                <div class="form-horizontal form-condensed">
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="form-group mdb has-feedback @if($errors->has('require_date')) has-error @endif">
                                {!! Form::label('require_date', trans('app.attributes.required_date'), ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">{!! isset($purchase->require_date)? $purchase->requiredAt() : '' !!}</div>
                                </div>
                            </div>


                            <div class="form-group mdb @if($errors->has('warehouse_id')) has-error @endif">
                                {!! Form::label('warehouse_id', trans('app.attributes.warehouse_destination'), ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">{!! $purchase->warehouse->description !!}</div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-sm-6">
                            <div class="form-group mdb @if($errors->has('provider_id')) has-error @endif">
                                {!! Form::label('provider_id', 'Proveedor', ['class' => 'control-label col-md-3']) !!}

                                <div class="col-md-9">
                                    <div class="form-control">
                                        {!! isset($proveedor->Nombre)? $proveedor->Nombre : '' !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .form-horizontal -->

            </div>
            <!-- .panel-body -->

            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                <div class="row-sm">

                    <table class="article table table-striped table-bordered small">
                        <thead>
                        <tr>
                            <th>{!! trans('app.attributes.description') !!}</th>

                            <th>{!! trans('app.attributes.internal_reference') !!}</th>

                            <th>{!! trans('app.attributes.quantity') !!}</th>

                            <th>{!! trans('app.attributes.pending') !!}</th>

                            <th class="cell-unit-cost">{!! trans('app.attributes.unit_cost') !!}</th>

                            <th class="cell-amount">{!! trans('app.attributes.amount') !!}</th>
                        </tr>
                        </thead>

                        <tbody class="text-uppercase">

                        @foreach( $purchase->details as $key => $detail )

                            <tr class="table-row">
                                <td class="cell-description">{!! $detail->article->description !!}</td>

                                <td class="cell-internal-reference">{!! $detail->article->internal_reference !!}</td>

                                <td class="cell-quantity">{!! $detail->quantity !!}</td>

                                <td class="cell-pending">
                                    {!! $detail->pending !!}
                                </td>

                                <td class="cell-unit-cost">{!! $detail->unit_cost !!}</td>

                                <td class="cell-amount">{!! $detail->amount !!}</td>

                            </tr>
                        @endforeach
                        </tbody>


                        <tfoot>
                        <tr class="total">
                            <td colspan="5" class="cell-total">TOTAL $</td>
                            <td colspan="2" class="cell-total">{!! $purchase->total !!}</td>
                        </tr>
                        </tfoot>


                    </table>

                </div>
                <!-- .row-sm -->
            </div>
            <!-- .panel-body -->

            <div class="panel-body">
                @include('partials.observation_field', ['textarea' =>  $purchase->observation])
            </div>
            <!-- .panel-body -->

        </div>
        <!-- .panel -->
    </div>

@endsection





@section('header')

    <div class="col-md-12">
        <div class="row">
            <div class="navbar">

                @include('purchases.forms.step_state',  ['status' => $purchase->status ])
                <div class="clearfix"></div>

                <div class="container-fluid">

                    <div class="navbar-btn">

                        <div class="pull-left">
                            <a href="{!! route('purchase.index') !!}" class="btn">
                                <i class="material-icons md-18">&#xE15E;</i>Ir a la lista
                            </a>
                        </div>

                        @include('purchases.forms.control_actions')

                       @if($purchase->status=="draft")
                            <a href="{!! route('purchase.receipt', $purchase->id)  !!}" class="btn">Recibir Mercancía   
                                <i class="material-icons md-18">&#xE558;</i>
                            </a>
                       @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('purchase.index') !!}">Compras</a></li>
        <li class="active">{!! $purchase->order !!}</li>
    </ol>
@endsection


@section('extra_scriptBody')
    <script>
        $('.delete-purchase').on('click', function (event) {
            event.preventDefault();

            var route = $(this).attr('href');

            $.getJSON(route).fail(function (response) {

            }).done(function (response) {
                var body = document.getElementsByTagName('body')[0];
                body.insertAdjacentHTML('afterbegin', response.alert)
            }).fail(function (error) {

                var body = document.getElementsByTagName('body')[0];
                body.insertAdjacentHTML('afterbegin', response.alert);
            });
        });
    </script>
@endsection