@extends('layout_principal')


@section('container')
<div class="col-md-10 col-md-offset-1">
    <div class="row-sm">

        <div class="panel">

            <div class="panel-body">
                    <span class="pull-right">
                        @include('purchases.forms.status')
                    </span>

                {!! Form::open(['route' => ['purchase.confirm', $purchase->id], 'method' => 'POST', 'id' => 'form-purchase']) !!}
                @include('purchases.forms.control_actions')
                {!! Form::close() !!}

                <h2>Compra {!! $purchase->order !!}</h2>
            </div>


            <div class="panel-body">

                <div class="form-horizontal form-condensed">


                    <div class="col-sm-6">

                        <div class="form-group mdb has-feedback @if($errors->has('require_date')) has-error @endif">
                            {!! Form::label('require_date', 'Fecha de Entrega', ['class' => 'control-label col-md-3']) !!}

                            <div class="col-md-9">
                                <div class="form-control">{!! $purchase->require_date !!}</div>
                            </div>
                        </div>


                        <div class="form-group mdb @if($errors->has('warehouse_id')) has-error @endif">
                            {!! Form::label('warehouse_id', 'Bodega de  destino', ['class' => 'control-label col-md-3']) !!}

                            <div class="col-md-9">
                                <div class="form-control">{!! $purchase->warehouse->description !!}</div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group mdb @if($errors->has('provider_id')) has-error @endif">
                            {!! Form::label('provider_id', 'Proveedor', ['class' => 'control-label col-md-3']) !!}

                            <div class="col-md-9">
                                <div class="form-control">{!! isset($purchase->provider)? $purchase->provider->fullName : '' !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .form-horizontal -->

            </div>
            <!-- .panel-body -->

            <div class="panel-body">

                <div class="row-sm">

                    <table id="tbl-article" class="article table table-striped text-middle">
                        <thead>
                        <tr>
                            <th>Descripción</th>

                            <th>Referencia interna</th>

                            <th>Cantidad</th>

                            <th class="cell-unit-cost">Costo unitario</th>

                            <th class="cell-amount">Monto</th>
                        </tr>
                        </thead>

                        <tbody class="text-uppercase">

                        @foreach( $purchase->details as $key => $detail )

                        <tr class="table-row">
                            <td class="cell-description">{!! $detail->article->fullDescription !!}</td>

                            <td class="cell-internal-reference">{!! $detail->article->internal_reference !!}</td>

                            <td class="cell-quantity">{!! $detail->quantity !!}</td>

                            <td class="cell-unit-cost">{!! $detail->unit_cost !!}</td>

                            <td class="cell-amount">{!! $detail->amount !!}</td>
                        </tr>
                        @endforeach
                        </tbody>


                        <tfoot>
                        <tr class="total">
                            <td colspan="4" class="cell-total">TOTAL $</td>
                            <td colspan="2" class="cell-total">{!! $purchase->total !!}</td>
                        </tr>
                        </tfoot>


                    </table>

                </div>
                <!-- .row-sm -->
            </div>
            <!-- .panel-body -->

            <div class="panel-body">
                @include('partials.observation_field', ['textarea' =>  $purchase->observation])
            </div>
            <!-- .panel-body -->


            <div class="panel-body">
                <div class="btn-group btn-group-xs" role="group" aria-label="...">
                    <button class="btn">
                        <span class="hidden-xs">Imprimir </span>
                        <span class="glyphicon glyphicon-print"></span>
                    </button>
                    <button class="btn">
                        <span class="hidden-xs">Crear </span>Excel
                        <span class="glyphicon glyphicon-export"></span>
                    </button>
                </div>
            </div>

        </div>
        <!-- .panel -->
    </div>
</div>
@endsection
