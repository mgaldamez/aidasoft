@extends('layout_principal')

@section('extra_scriptHead')

    <!-- JQuery UI Pluging CSS -->
    <link rel="stylesheet" href="{{ asset('jquery.ui/jquery-ui.min.css')}}">

    <script src="{{ asset('jquery.ui/jquery-ui.min.js')}}"></script>
@endsection

@section('container')
    <div class="col-md-12 col-lg-10 col-lg-offset-1">

        <div class="panel">

            <div class="panel-heading">


                <h1>Nueva Compra</h1>

                <small>{{ Carbon\Carbon::now()->formatLocalized('%d de %B de %Y') }}</small>
             
                
                </div>

            {!! Form::open(['route' => ['purchase.store'], 'method' => 'POST', 'role' => 'create', 'class' => 'form-horizontal form-condensed', 'id' => 'form-quotation']) !!}

            <input type="hidden" id="order" name="order">
            <small style="display:none">
                <div >
     
               
                {!! Form::text('num_doc_temp',isset($num_doc['num_doc_temp'])?($num_doc['num_doc_temp']):'' , ['class' => 'input-pending','id'=>'num_doc_temp'] ) !!}
                </div>
            </small>
            <div class="panel-body">

                <div class="row">
                    @include('purchases.forms.heading')
                </div>
            </div>
            <!-- .panel-body -->


            <div class="panel-body">

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                @include('purchases.tables.table')
            </div>


            <div class="panel-body">
                <div class="col-md-12">
                    @include('partials.observation_field')
                </div>
            </div>
            <!-- .panel-body -->


            {!! Form::close() !!}

        </div>
        <!-- .panel -->

    </div>
    <!-- .col -->
@endsection



@section('extra_scriptBody')


    @include('purchases.tables.details_script')


    @include('partials.select2_script')
    <script>
        $(document).ready(function () {
            $('#provider_id, #warehouse_id').select2();
            $('.table select.form-control').select2();
        });
    </script>



    @include('partials.date_script')

    <script>
        $(function () {
            $('.tab-control-bottom li').click(function (event) {
                event.preventDefault()
            });
        });
    </script>

    {{--<script src="{!! asset('assets/js/send_to_ajax.js') !!}"></script>--}}

    <script>
//        $(document).ready(function () {
//
//            var form = document.querySelector('form[role="create"]');
//
//            var options = {
//                method: $(form)[0].method,
//                token: $(form).find('input[name="_token"]').val(),
//                route: $(form)[0].action,
//            };
//
//            $('').sendToAjax(options);
//
//        });
    </script>
@endsection



@section('header')
    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <a href="{!! route('purchase.cancelar',[$num_doc['num_doc_temp'],$bodega['id']]) !!}" class="btn">
                            Cancelar
                        </a>

                        <button type="button" id="sendOrder" class="btn"
                                onclick="document.getElementById('form-quotation').submit()"><i class="material-icons md-18">&#xE161;</i>Guardar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('purchase.index') !!}">Compras</a></li>
        <li class="active">Nueva Compra</li>
    </ol>
@endsection