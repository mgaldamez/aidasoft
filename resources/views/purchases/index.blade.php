@extends('layout_principal')


@section('container')


    <div class="col-md-12">
        <div class="row-sm">

            <div class="panel">

                <div class="panel-body">
                    <table class="table table-bordered table-condensed small" id="table-purchases">
                        <thead>
                        <tr>
                            <th>{!! trans('app.attributes.date') !!}</th>
                            <th>{!! trans('app.attributes.purchase_num') !!}</th>
                            <th>{!! trans('app.attributes.status') !!}</th>
                            <th>{!! trans('app.attributes.required_at') !!}</th>
                            <th>{!! trans('Confirmation') !!}</th>
                            <th>{!! trans('app.attributes.provider') !!}</th>
                            <th class="cell-amount">{!! trans('app.attributes.amount') !!}</th>
                            <th>{!! trans('app.attributes.created_by') !!}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody class="text-uppercase">
                        @foreach( $purchases as $purchase )
                            <tr>
                                <td title="{!! $purchase->created_at !!}"
                                    data-sort="{!! $purchase->sortCreatedAt() !!}">
                                    {!! $purchase->createdAt() !!}</td>
                                <td>
                                    <a href="{!! route('purchase.show', $purchase->id) !!}">{!! $purchase->order !!}</a>
                                </td>
                                <td>
                                    @include('partials.status', ['status' => $purchase->order_status ])
                                </td>
                                <td title="{!! $purchase->require_date !!}"
                                    data-sordt="{!! $purchase->sortRequiredAt() !!}">
                                    {!! $purchase->requiredAt() !!}
                                </td>
                                <td>{!! $purchase->confirmed_at !!}</td>

                                <td>{!! $purchase->provider !!}</td>
                                <td class="cell-amount">{!! $purchase->total !!}</td>

                                <td>{!! $purchase->created_by !!}</td>
                                <td width="20">

                                    @if( $purchase->order_status_id == 1 )
                                        <a href="{!! route('purchase.edit', $purchase->id) !!}"
                                           class="btn text-default btn-xs"
                                           title="Editar">
                                            <i class="material-icons md-18">mode_edit</i>
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- .panel -->

        </div>
    </div>

@endsection



@section('extra_scriptBody')

    @include('partials.dataTables_script')

    <script>
        $(document).ready(function () {
            $('#table-purchases').dataTableConfig({
                "columns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    {"orderable": false}
                ]
            });
        });
    </script>
@endsection



@section('header')


    <div class="col-md-12">
        <div class="row">
            <div class="navbar">
                <div class="container-fluid">

                    <div class="navbar-btn">

                        <div class="row-sm">

                            <a class="btn" href="{!! route( 'purchase.create' ) !!}">
                                <!-- add -->
                                <i class="material-icons">&#xE145;</i>Nueva Compra
                            </a>


                            <div class="col-sm-6 col-md-3 pull-right">
                                <div class="row">
                                    @include('partials.dataTables_filter', ['table' => 'table-purchases'])
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! url('/') !!}">Inicio</a></li>
        <li class="active">Compras</li>
    </ol>
@endsection
