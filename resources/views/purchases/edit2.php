@extends('layout_principal')

@section('extra_scriptHead')

<!-- JQuery UI Pluging CSS -->
<link rel="stylesheet" href="{{ asset('jquery.ui/jquery-ui.min.css')}}">

<script src="{{ asset('jquery.ui/jquery-ui.min.js')}}"></script>


<link rel="stylesheet" href="{{ asset('material-design-table/material_design_table.css')}}">

<!-- Bootstrap Bootstrap-select Pluging CSS -->
<link rel="stylesheet" href="{{ asset('/bootstrap/bootstrap-select/css/bootstrap-select.css')}}">

<!-- Bootstrap Bootstrap-select Pluging JS -->
<script src="{{ url('/bootstrap/bootstrap-select/js/bootstrap-select.js') }}"></script>

<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap/bootstrap-datetimepicker/css/datepicker.css')}}">

<!-- Bootstrap DatePiker Pluging JS -->
<script src="{{ asset('bootstrap/bootstrap-datetimepicker/js/bootstrap-datepicker.js')}}"></script>


@endsection


@section('container')

<?php setlocale (LC_TIME, "es");
?>


@include('alerts.flash_response')

<div class="col-md-12 col-lg-10 col-lg-offset-1">

	<div class="row-sm">

		<div class="panel">
			
			{!! Form::model($order, ['url'=>['/order/purchase'], 'method'=>'PUT']) !!}			
			{!! Form::hidden('id', old('id')) !!}


			<div class="panel-heading">	
				<div class="btn-group pull-right">
					<a href="{!! URL::previous() !!}" class="btn btn-default btn-lg">Cancelar</a>
					{!! Form::submit('Guardar Cambios', ['class' => 'btn btn-success btn-raised btn-lg']) !!}
				</div>
				
				<h2>
					<small>Orden de compra</small>
					{!! $order->order !!}
				</h2>
				
				<small>	
					{!! Carbon\Carbon::now()->formatLocalized('%d de %B de %Y') !!} 
				</small>
				
				<br>
				
				<small style="font-size: x-small">	
					Creado: {!! Carbon\Carbon::parse( $order->created_at )->formatLocalized('%d de %B de %Y') !!} 
				</small>
			</div>


			<div class="col-md-12">

				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{!! $error !!}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>


			<div class="panel-body">

				<div class="form-horizontal">

					@include('purchases.forms.details')
				</div>
				<!-- .form-horizontal -->
			</div>
			<!-- .panel-body -->

			<br>

			<ul class="nav nav-pills tab-control-bottom hidden">
				<li>
					<a href="#" id="countSelect" class="btn btn-link"></a>
				</li>

				<li>
					<a href="#" id="remove-items" class="btn btn-link">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
				</li>

				<li>
					<a href="#" id="edit-item" class="btn btn-link"  v-if="removeDOM == true">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
					<a href="#" id="edit-items" class="btn btn-link"  v-if="removeDOM == false">
						<span class="glyphicon glyphicon-pencil"></span>
					</a>
				</li>
			</ul>


			<table id="tbl-article" class="table table-sm text-middle form-table total-amount">

				@include('purchases.tables.purchase-items')
				
			</table>


			<div class="panel-body">

				<div class="col-md-8">

					<div class="row">

						@include('purchases.forms.description')
					</div>
				</div>
				
			</div>
			<!-- .panel-body -->


			{!!Form::close()!!} 

		</div>
	</div>
</div>


<!-- Modal -->
<div id="articleModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="articleModalLabel" v-if="removeDOM == true">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title" id="articleModalLabel">Agregar</h4>
			</div>

			<div class="modal-body">

				<div id="create-article">

					@include('purchases.modals.add-article')

				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" id="agree" class="btn btn-primary">Aceptar</button>
			</div>

		</div>

	</div>

</div>
<!-- .modal -->



<!-- Modal -->
<div id="editArticleModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="editArticleModalLabel" v-if="removeDOM == true">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">
				<h4 class="modal-title" id="editArticleModalLabel">Editar articulos</h4>
			</div>

			<div class="modal-body">
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" id="updates" class="btn btn-primary">Actualizar</button>
			</div>

		</div>

	</div>

</div>
<!-- .modal -->



<!-- Modal -->
<div id="discountModal" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="discountModalLabel">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-body">

				<div id="create-article">

					<div class="form-group form-group-lg">

						{!!Form::label('_discount', 'Descuento', ['class' => 'control-label'])!!}
						{!!Form::text('_discount', old('_discount'), ['class' => 'form-control', 'placeholder' => '0.00', 'autocomplete' => 'off' ])!!}
					</div>

				</div>

			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" id="add-discount" class="btn btn-primary">Aceptar</button>
			</div>

		</div>

	</div>

</div>
<!-- .modal -->


<input type="hidden" id="add-article">
@endsection

@section('extra_scriptBody')
<script src="{{ asset('assets/js/table.js')}}"></script>
{{-- <script src="{{ asset('assets/js/table2.js')}}"></script> --}}



<!-- DatePiker -->
<script type="text/javascript">
	$(function(){
		$('input.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			title: 'hola',
			todayHighlight: false,
			defaultViewDate: 'today',
			clearBtn: true
		});	
	})
</script>

<!-- Bootstrap Bootstrap-select -->
<script>
	$(function(){

		$('select.selectpicker').selectpicker({
			'liveSearchPlaceholder': 'Buscar...',
		});
	});
</script>



<script>
	$('#myModal').on('shown.bs.modal', function () {
		$('#myModal #_article').focus()
	})
</script>

<script src="{{ asset('assets/js/reorder-rows.js')}}"></script>


<script src="{{ asset('vuejs/vue.js') }}"></script>

<script>
	var vue = new Vue({

		el: "#app-container", 

		data: {
			removeDOM: false

		},

		methods: {

			_removesm: function( ){

				function mediaQuery (mediaquery ){

					(mediaquery.matches) ? vue.removeDOM = true : vue.removeDOM = false;
				}

				var mediaquery = window.matchMedia("screen and (max-width: 991px)");

				mediaQuery(mediaquery);

				mediaquery.addListener( function(){

					mediaQuery(mediaquery)
				});
			}
		},

		created: function(){ 

			$(function(){
				vue._removesm();
			})
		}

	});

</script>

<script>
	$(function(){
		$('.tab-control-bottom li').click( function(event){
			event.preventDefault()
		})
	})
</script>


<script src="{{ asset('assets/js/resize_textarea.js')}}"></script>
@endsection