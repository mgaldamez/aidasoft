@if($stock == 0)

    <span class="text-danger" style="width: 80px;">
        <span class="glyphicon glyphicon-arrow-down"></span> {!! $stock !!}
    </span>

@elseif($stock <= $min_stock)

    <span class="text-warning" style="width: 80px;">
        <span class="glyphicon glyphicon-arrow-down"></span> {!! $stock !!}
    </span>

@elseif($stock >= $max_stock )

    <span class="text-success" style="width: 80px;">
        <span class="glyphicon glyphicon-arrow-up"></span> {!! $stock !!}
    </span>

@endif