<!-- Moment.js 2.14.1 -->
<script src="{!! asset('moment_js/moment.js') !!}"></script>

<script src="{!! asset('moment_js/locale/es.js') !!}"></script>


<!-- DatePiker -->
<!-- Bootstrap DatePiker Pluging CSS -->
<link rel="stylesheet" href="{!! asset('bootstrap/bootstrap-datetimepicker-4/css/bootstrap-datetimepicker.css') !!}">

<!-- Bootstrap DatePiker Pluging JS -->
<script src="{!! asset('bootstrap/bootstrap-datetimepicker-4/js/bootstrap-datetimepicker.js') !!}"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $('.datepicker').datetimepicker({
            useCurrent: false,
            format: 'DD-MM-YYYY',
            locale: 'es',
            tooltips: {
                today: 'Ir a hoy',
                clear: 'Selección clara',
                close: 'Cierre el selector',
                selectMonth: 'Seleccione mes',
                prevMonth: 'Mes anterior',
                nextMonth: 'Próximo mes',
                selectYear: 'Seleccione Año',
                prevYear: 'Año anterior',
                nextYear: 'El próximo año',
                selectDecade: 'Seleccione Decade',
                prevDecade: 'Década anterior',
                nextDecade: 'Próxima Década',
                prevCentury: 'Siglo anterior',
                nextCentury: 'Próximo siglo'
            }
        });
    });
</script>