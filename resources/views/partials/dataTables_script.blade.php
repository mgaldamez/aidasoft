
<!-- css DataTables-1.10.11-->
<link rel="stylesheet" href="{!! URL::asset('dataTables/1.10.11/css/dataTables.bootstrap.css') !!}">

<script src="{!! URL::asset('dataTables/1.10.11/js/jquery.dataTables.js') !!}"></script>

<script src="https://cdn.datatables.net/plug-ins/1.10.13/sorting/date-euro.js"></script>
<script src="{!! URL::asset('dataTables/1.10.11/js/dataTables.bootstrap.js') !!}"></script>

<script src="{!! URL::asset('assets/js/dataTable_config.js') !!}"></script>



@if(isset($buttons) and $buttons === true )
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
@endif