<div class="row">
    <div class="list-group list-group-article">

        <div class="list-group-item">
            <div class="row-picture " width="130" height="130"  >

             

                    <img id="ign" src="{!! asset('assets/img/'.$article->image ) !!}"  width="130" height="130" 
                         onerror="this.src='{!! asset('assets/img/_not_available.png') !!}'"
                          class="img-rounded  img-responsive" 
                          onmouseover="javascript:this.height=300;this.width=300"
                          onmouseout="javascript:this.width=130;this.height=130"
                          >

            </div>

            <div class="row-content">

                <div class="pull-right text-center col-md-2">
<!-- RUTA DE ROUTER orderROUTES  HJTT/ROUTES//-->
                    {!! Form::open([ 'route' => ['add.article.cart'], 'class' => 'form-shopping-cart' ] ) !!}

                    {!! Form::hidden('article', $article->article_id ) !!}

                    @if($order = currentUser()->hasOrder())

                            @if($detail = $order->details()->article($article->article_id)->first())
                                {!! Form::hidden('quantity', $detail->quantity, ['class' => 'form-control']) !!}

                                    <button type="submit" class="btn btn-success btn-block" data-toggle="tooltip" data-placement="top" title="Agregado al carrito">
                                        <span class="hidden-xs">Agregado</span>
                                        <i class="material-icons md-18">&#xE8CC;</i>
                                    </button>

                            @else
                                {!! Form::hidden('quantity', 1, ['class' => 'form-control']) !!}

                                    <button type="submit" class="btn btn-default btn-block" data-loading-text="..."  data-toggle="tooltip" data-placement="top" title="Agregar al carrito">
                                        <span class="hidden-xs">Agregar</span>
                                        <i class="material-icons md-18">&#xE854;</i>
                                    </button>

                            @endif



                    @else


                            {!! Form::hidden('quantity',  1 , ['class' => 'form-control']) !!}
                            <button type="submit" class="btn btn-default btn-block" data-loading-text="..."  data-toggle="tooltip" data-placement="top" title="Agregar al carrito">
                               <span class="hidden-xs">Agregar</span>
                                <i class="material-icons md-18">&#xE854;</i>
                            </button>


                    @endif
                    {!! Form::close() !!}

                </div>

                <h4 class="list-group-item-heading text-truncate" title="{!! $article->article !!}">

                    <a href="{!! route('article.show', $article->article_master_id) !!}">
                        {!! $article->description !!}
                    </a>

                    <br>

                    <small>{!! $article->internal_reference !!}</small>

                </h4>

                <p class="list-group-item-text text-truncate multiline">

                    {!! $article->brand ? trans('app.attributes.brand') . ': '. $article->brand : ''  !!}
                    {!! $article->category ? ', '. trans('app.attributes.category') .': ' .$article->category: '' !!}
                

                </p>

                <div class="row-stock">
            
                   <span class="btn-link">{!! trans('app.attributes.stock') !!}
                       <strong>{!! $article->availableForOrders() !!}</strong>
                   </span>

                </div>

            </div>


        </div>

    </div>
     <div id="fade" class="black_overlay"></div>
</div>
