@if( $status == 'draft' ) {{--Draft--}}

<span class="label label-default text-capitalize">{!! trans('app.status.'.$status)  !!}</span>
@elseif($status == 'confirmed')

    <span class="label label-success text-capitalize">{!!  trans('app.status.'.$status) !!}</span>

@elseif($status == 'finalized')
    <span class="label label-success text-capitalize">{!!  trans('app.status.'.$status) !!}</span>

@elseif($status == 'completed')
    <span class="label label-success text-capitalize">{!!  trans('app.status.'.$status) !!}</span>

@elseif($status == 'in_revision')
    <span class="label label-warning text-capitalize">{!!  trans('app.status.'.$status) !!}</span>

@elseif($status == 'received')
    <span class="label label-success text-capitalize">{!!  trans('app.status.'.$status) !!}</span>

@elseif($status == 'canceled')

    <span class="label label-danger text-capitalize">{!!  trans('app.status.'.$status)  !!}</span>

@elseif($status == 'partially_confirmed')

    <span class="label label-info text-capitalize">{!! trans('app.status.'.$status)  !!}</span>

@elseif($status == 'in_transit')

    <span class="label label-info text-capitalize">{!! trans('app.status.'.$status)  !!}</span>

@elseif($status == 'for_transfer')

    <span class="label label-info text-capitalize">{!! trans('app.status.'.$status)  !!}</span>

@elseif($status == 'for_confirm')

    <span class="label label-info text-capitalize">{!! trans('app.status.'.$status)  !!}</span>
@endif



{{--'complete' => 'Completado',--}}
{{--'transit' => 'En transito',--}}
{{--'draft' => 'Borrador',--}}
{{--'cancel' => 'Cancelado',--}}
{{--'completed' => 'Completado',--}}
{{--'in_revision' => 'En revisión',--}}
{{--'rejected' => 'Rechazado',--}}
{{--'approved' => 'Aprovado',--}}
{{--'confirmed' => 'Confirmado',--}}
{{--'finalized' => 'Finalizado',--}}
{{--'canceled' => 'Cancelado',--}}
{{--'partially_approved' => 'Aprobado parcialmente',--}}
{{--'partially_confirmed' => 'Confirmado parcialmente'--}}