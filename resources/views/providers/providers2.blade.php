

<span class="hidden-xs hidden-sm">
	Encontrados {!! $providers->total() !!}
	<br>
	<br>
</span>

<div class="contact-list">
	
	@foreach($providers as $key => $provider)

	<div class="media">
		<div class="media-left">
			<div class="media-object">
				{{-- <i class="material-icons">business</i> --}}
				<div class="label label-default">{!! $provider->type !!}</div>
			</div>
		</div>
		<div  class="media-body">
			<a href="{{ url('/provider/'.$provider->id) }}">
				<h4 class="media-heading">{!! $provider->company !!}
					<small>	
						@if($provider->firstname)
						{!! $provider->firstname !!} {!! $provider->lastname !!}
						@endif
					</small>
				</h4>	
				<div class="media-content">

					@if($provider->phone)
					<i class="material-icons">phone</i> {!! $provider->phone !!}
					@endif

					@if($provider->email)
					<i class="material-icons">email</i> {!! $provider->email !!}
					@endif
				</div>
			</a>
		</div>
		<div class="media-right controls-action">

			<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Crear orden de compra">
				<i class="material-icons">add_shopping_cart</i>
			</a>
			<a href="" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar un email">
				<i class="material-icons">email</i>
			</a>
			<a href="{{ url('/provider/'.$provider->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
				<i class="material-icons">mode_edit</i>
			</a>
			<a href="{{ url('/provider/'.$provider->id) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
				<i class="material-icons">delete</i>
			</a>
		{{-- 	<div class="btn-group">
				<a href="" data-target="#" class="" data-toggle="dropdown" aria-expanded="true"><i class="material-icons">more_vert</i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li><a href="{{ url('/provider/'.$provider->id.'/edit') }}">Editar</a></li>
					<li><a href="javascript:void(0)">Hacer un pedido</a></li>
					<li><a href="javascript:void(0)">Ver pedidos</a></li>
					<li class="divider"></li>
					<li><a href="{{ url('/provider/'.$provider->id) }}" class="deleteProvider">Eliminar</a></li>
				</ul>
			</div> --}}
		</div>

	</div>
	<!-- .media -->

	@endforeach

</div>

{!! $providers->appends(Request::all())->render() !!}

