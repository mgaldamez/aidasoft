@foreach($providers as $key => $provider)
    <tr data-provider-id="{!! $provider->id !!}">

        <td>
            <a href="{!! route('admin.provider.show', $provider->id) !!}">
                {!! $provider->fullname !!}
                <small>{!! isset($provider->company)? ', '. '<strong>'.$provider->company .'</strong>' : '' !!}</small>
            </a>
        </td>

        <td>{!! $provider->email !!} <br>
            {!! $provider->phone !!}
        </td>

        <td>{!! $provider->type !!}</td>

        <td>{!! $provider->countPurchases !!}</td>

        <td>
{{--            {!! route('purchase', $provider->lastPurchase['id']) !!}--}}
            <a href="">
                {!! $provider->lastPurchase['order'] !!}
            </a><br>
            <small>{!! $provider->lastPurchase['updated_at'] !!}</small>
        </td>

        <td width="80">
            <a href="{!! route('admin.provider.edit', $provider->id) !!}" class="btn btn-xs btn-default" title="Editar">
                <i class="material-icons md-18">mode_edit</i>
            </a>
			   
            <a href="#"
               class="btn btn-xs btn-default"
               data-toggle="modal"
               data-target="#deleteProviderModal"
               data-name="{!! $provider->fullname !!}"
               data-id="{!! $provider->id !!}"
               title="Eliminar">
                <i class="material-icons md-18">delete</i>
            </a>
        </td>
    </tr>
@endforeach