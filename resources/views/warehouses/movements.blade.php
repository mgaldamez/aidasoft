@extends('layout_principal')



@section('extra_scriptHead')

    @include('partials.dataTables_script')
@endsection



@section('navbar-btn')
    <div class="pull-right">
        @include('partials.dataTables_exports')
    </div>
    @include('partials.dataTables_filter', ['table' => 'table-movements'])

@endsection



@section('container')

    <div class="col-md-12">
        <div class="panel">


            <div class="panel-heading">


                <legend>{!! trans('app.attributes.movements') !!}</legend>


                <div class="row">

                    <div class="col-md-6">@include('partials.dataTables_filter', ['table' => 'table-movements'])
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div id="date_filter" class="form-horizontal">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label id="date-label-from"
                                               class="date-label control-label col-xs-2 hidden-xs">
                                            {!! trans('app.attributes.from') !!}:
                                        </label>

                                        <div class="col-xs-10">
                                            <div class="input-group">
                                                <input id="datepicker_from"
                                                       class="date_range_filter date form-control"
                                                       type="text"
                                                       placeholder="{!! trans('app.attributes.initial_date') !!}"/>
                                                <span class="input-group-addon">
                                                        <i class="material-icons md-18">&#xE916;</i>
                                                    </span>
                                            </div>
                                            <!-- .input-group -->
                                        </div>
                                    </div>
                                </div>
                                <!-- .col -->

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label id="date-label-to"
                                               class="date-label control-label col-xs-2 hidden-xs">
                                            {!! trans('app.attributes.to') !!}:
                                        </label>

                                        <div class="col-xs-10">
                                            <div class="input-group">
                                                <input id="datepicker_to"
                                                       class="date_range_filter date form-control"
                                                       type="text"
                                                       placeholder="{!! trans('app.attributes.final_date') !!}"/>

                                                <span class="input-group-addon">
                                                        <i class="material-icons md-18">&#xE916;</i>
                                                    </span>
                                            </div>
                                            <!-- .input-group -->
                                        </div>
                                    </div>

                                </div>
                                <!-- .col -->
                            </div>
                        </div>
                    </div>
                    <!-- .col -->
                </div>
                <!-- .row -->

            </div>


            <div class="panel-body">
                @include('movements.table')
            </div>
        </div>
    </div>

@endsection


@section('extra_scriptBody')


    <!-- Moment.js 2.14.1 -->
    <script src="{!! asset('moment_js/moment.js') !!}"></script>

    <script src="{!! asset('moment_js/locale/es.js') !!}"></script>


    <!-- DatePiker -->
    <!-- Bootstrap DatePiker Pluging CSS -->
    <link rel="stylesheet"
          href="{!! asset('bootstrap/bootstrap-datetimepicker-4/css/bootstrap-datetimepicker.css') !!}">

    <!-- Bootstrap DatePiker Pluging JS -->
    <script src="{!! asset('bootstrap/bootstrap-datetimepicker-4/js/bootstrap-datetimepicker.js') !!}"></script>


    @include('partials.dataTables_script', ['buttons' => true])
    <script>

        var dpOptions = {
            useCurrent: false,
            format: 'DD-MM-YYYY',
            locale: 'es',
            tooltips: {
                today: 'Ir a hoy',
                clear: 'Selección clara',
                close: 'Cierre el selector',
                selectMonth: 'Seleccione mes',
                prevMonth: 'Mes anterior',
                nextMonth: 'Próximo mes',
                selectYear: 'Seleccione Año',
                prevYear: 'Año anterior',
                nextYear: 'El próximo año',
                selectDecade: 'Seleccione Decade',
                prevDecade: 'Década anterior',
                nextDecade: 'Próxima Década',
                prevCentury: 'Siglo anterior',
                nextCentury: 'Próximo siglo'
            }
        };

        function formatISODate(date) {
            var array = date.split("-");
            return array[1] + "/" + array[0] + "/" + array[2];
        }

        $(document).ready(function () {

            var oTable = $('#table-movements').dataTableConfig({
                "columns": [
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false},
                    {"orderable": false}
                ],
                "dom": 'Bfrtip',
                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#datepicker_from").datetimepicker(dpOptions)
                .on('dp.change', function (e) {
                    minDateFilter = new Date(formatISODate(e.target.value)).getTime();
                    oTable.draw();
                });

            $("#datepicker_to").datetimepicker(dpOptions)
                .on('dp.change', function (e) {
                    maxDateFilter = new Date(formatISODate(e.target.value)).getTime();
                    oTable.draw();
                });
        });


        // Date range filter
        minDateFilter = "";
        maxDateFilter = "";

        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {

                if (typeof aData._date == 'undefined') {
                    aData._date = new Date(formatISODate(aData[0])).getTime();
                }

                if (minDateFilter && !isNaN(minDateFilter)) {
                    if (aData._date < minDateFilter) {
                        return false;
                    }
                }

                if (maxDateFilter && !isNaN(maxDateFilter)) {
                    if (aData._date > maxDateFilter) {
                        return false;
                    }
                }

                return true;
            }
        );
    </script>
@endsection





@section('header')
    @include('warehouses.partials.page_header')
@endsection


@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! route('warehouse.show', $warehouse->id) !!}">{!! trans('app.attributes.warehouse') !!}</a></li>
        <li class="active">{!! trans('app.attributes.movements') !!}</li>
    </ol>
@endsection