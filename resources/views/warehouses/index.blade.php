@extends('layout_principal')


@section('container')


    <div class="col-md-10 col-md-offset-1">

        @if( count($warehouses) )
            <div class="panel">
                <div class="panel-body">
                    <table class="table table-condensed table-bordered text-middle small" id="table-warehouse">
                        <thead>
                        <tr class="text-uppercase">
                            <th>Descripcións</th>
                            <th>Sucursal</th>
                            <th>Región</th>
                            <th>Usuario</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($warehouses as $key => $warehouse)
                            <tr>
                                <td class="warehouse">
                                    <a href="{!! route('warehouse.inventory', $warehouse->id ) !!}">{!! $warehouse->description !!}
                                    </a>

                                    @if($warehouse->main)
                                        <i class="material-icons md-18" style="color:#ffc107;">star</i>
                                    @endif
                                </td>

                                <td class="sucursal" id="{!! $warehouse->sucursal->ID_SUCURSAL !!}">
                                    {!! $warehouse->sucursal->DESCR_SUCURSAL !!}
                                </td>

                                <td>{!! $warehouse->sucursal->region->DESCR_REGION !!}</td>

                                <td class="user"
                                    id="{!! isset($warehouse->user)? $warehouse->user->id : '' !!}">
                                    {!! isset($warehouse->user)? $warehouse->user->fullName : '' !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @else

            <div class="col-md-12">
                <div class="text-center text-muted">
                    <h1>No tienes bodegas asignadas!</h1>
                    <h4>Comunicate con tu administrador para solicitar una bodega.</h4>

                    <br>
                    <a href="{!! URL::previous() !!}" class="btn btn-link btn-lg">
                        <i class="material-icons">&#xE15E;</i>Volver atras
                    </a>
                </div>
            </div>
        @endif

    </div>
@endsection


{{--<div class="list-group list-group-hover">--}}

{{--@foreach($warehouses as $key => $warehouse)--}}

{{--<div class="list-group-item">--}}

{{--<div class="list-group-item-left">--}}
{{--<div class="list-group-item-object">--}}
{{--{!! $warehouse->articleMasters->count() !!}--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="list-group-item-body">--}}

{{--<a href="{!! route( 'warehouse_inventory', $warehouse->id  ) !!}">--}}
{{--<h4 class="list-group-item-heading">{!! $warehouse->description !!}</h4>--}}
{{--</a>--}}

{{--</div>--}}

{{--<div class="list-group-item-right">--}}

{{--<i class="material-icons">info</i>--}}

{{--</div>--}}


{{--<div class="list-gorup-item-bottom">--}}
{{--<span class="label label-default">{!!  $warehouse->articleMasters->count() !!}--}}
{{--<span class="glyphicon glyphicon-tags"></span>--}}
{{--</span>--}}


{{--<i class="material-icons">compare_arrows</i>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endforeach--}}

{{--</div>--}}
<!--.list-group -->