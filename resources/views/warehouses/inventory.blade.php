@extends('layout_principal')

@section('extra_scriptHead')
 
    @include('partials.dataTables_script', ['buttons' => true])

    <script>
        $(document).ready(function () {
            $('#table-inventory').dataTableConfig({
                "dom": 'Bfrtip',
                "buttons": [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        });
    </script>
@endsection







@section('container')

    <div class="col-md-12">
        <div class="panel">

            <div class="panel-heading">

                <div class="pull-right">
                    @include('partials.dataTables_exports')
                </div>

                <legend>{!! trans('app.attributes.articles') !!}</legend>

                <div class="col-md-3 pull-right">
                    @include('partials.dataTables_filter', ['table' => 'table-inventory'])
                </div>

                <div class="clearfix"></div>
            </div>

            <div class="panel-body">
                @include('warehouses.partials.inventory', ['warehouse_id' => 9])
          </div>
        </div>
    </div>


@endsection




@section('header')
    @include('warehouses.partials.page_header')
@endsection



@section('breadcrumb')

    <ol class="breadcrumb">
        <li><a href="{!! route('home') !!}">Inicio</a></li>
        <li><a href="{!! url('admin/my/warehouse') !!}">Bodegas</a></li>
        <li class="active">{!! $warehouse->description !!}</li>
    </ol>
@endsection