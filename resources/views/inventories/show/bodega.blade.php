@extends('layout_principal') 

@section('extra_scriptHead')
<!---->
@endsection 

@section('container')
<div class="col-md-12">
    <div class="row-sm">
        <div class="panel x-panel">
           <div class="panel-heading">
            <div class="col-xs-6">
                <h2>Inventario <small> #007612</small></h2>
            </div><!--.col-->
            <div class="col-xs-6">
               <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"> <span class="glyphicon glyphicon-menu-up"></span></a>
                </li>
                <li class=""><a href=""><span class="glyphicon glyphicon-share-alt"></span></a></li>
                <li><a class="close-link"><span class="glyphicon glyphicon-remove"></span></a>
                </li>
              </ul>
            </div><!--.col-->
            </div><!--.panel-heading-->
            <div class="panel-body">                
                <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                 
                  <div class="tile-stats">
                    <div class="icon"><span class="glyphicon glyphicon-usd"></span>
                    </div>
                    <div class="count total"></div>
                    <h3>VALOR TOTAL</h3>  
                    <p>Lorem ipsum psdea itgum rixt.</p>
                  </div>
                  
                </div><!--.col-->

                <div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">
                
                  <div class="tile-stats">
                    <div class="icon"><span class="glyphicon glyphicon-tags"></span>
                    </div>
                    <div class="count items"></div>
                    <h3>ARTICULOS</h3>
                    <p>Lorem ipsum psdea itgum rixt.</p>
                  </div>
               
                </div><!--.col-->
                
                
                
            </div><!--.panel-body-->
        </div><!--.panel-->
    </div><!--.row-sm-->
</div><!--.col-->



<div class="col-md-12" id="muestra">
    <div class="row-sm">
        <div class="panel x-panel">
            <div class="panel-body">
            
            <!--content invoice-->
            <section class=""> 
                <!-- title row -->
                <div class="row">
                  <div class="col-xs-12 invoice-header">
                  <h1>
                   <span class="glyphicon glyphicon-list-alt"></span> Inventario.
                   <small class="pull-right">Fecha: 
                    <?php
                        $hoy = getdate();
                        $d = $hoy['mday'];
                        $m = $hoy['mon'];
                        $y = $hoy['year'];
                        print_r($d."/".$m."/".$y);
                    ?>
                    </small>
                  </h1>
                  </div><!--.col -->
                </div>
                
                <!-- info row -->
                <div class="row invoice-info">
                   <div class="col-sm-4 invoice-col">
                       <address>
                        <strong>Telefónica Móviles Panamá (Movistar), SA.</strong>
                        <br>Ave. Central Santiago, Veraguas
                        <br>Panamá
                        <br>Telefono: (507) 999-8888
                        <br>Email: santiago@movistar.com
                       </address>
                  </div><!--.col -->
                  
                  <div class="col-sm-4 invoice-col">
                    <b>Inventario #007612</b>
                    <br>
<!--
                    <br>
                    <b>Order ID:</b> 4F3S8J
                    <br>
                    <b>Account:</b> 968-34567
-->
                  </div><!--.col -->
                </div><!--.row -->

                <!-- Table row -->
                <div class="row">
                  <div class="col-xs-12">
                  <div class="row-sm">
                    <div class="table-responsive">
                     <table class="table table-striped __table-default">
                      <thead>
                        <tr>
                          <th class="text-nowrap">Cant.</th>
                          <th class="text-nowrap">Articulo</th>
                          <th class="text-nowrap">Codigo de barras</th>
                          <th class="text-nowrap">Descripcion</th>
                          <th class="text-nowrap">Costo/U.</th>
                          <th class="text-nowrap">Subtotal</th>
                        </tr>
                      </thead>
                      <tbody>                   
                       @foreach($inventory as $inventory)
                        <tr class="dato" >
                          <td class="text-right">{{$inventory->cantidad}}</td>
                          <td><a href="/{{$inventory->article}}">{{$inventory->articlename}}</a></td>
                          <td class="text-uppercase"><dt>{{$inventory->barcode}}</dt></td>
                          <td class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                          <td class="text-right">{{$inventory->costo}}</td>
                          <td class="text-right">{{$inventory->subtotal}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>  
                   </div>  
                  </div>
                  
               
                  </div>
                  <!--.col -->
                </div>
                <!--.row -->

                <div class="row">
                  <div class="col-xs-12 col-md-6 pull-right">
                    <p class="lead">Monto</p>
                    <div class="table-responsive">
                      <table class="table">
                        <tbody>
                          <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td class="sub-total"></td>
                          </tr>
                          <tr>
                            <th>Total:</th>
                            <td class="total"></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!--.col -->
                </div>
                <!--.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-xs-12">
                    <button class="btn btn-default" onclick="window.print(muestra);"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
                    <button class="btn btn-primary" style="margin-right: 5px;"><span class="glyphicon glyphicon-download-alt"></span> Generar PDF</button>
                    
                          <a href="javascript:imprSelec('muestra')">Imprimir Tabla</a>
                  
                  </div>
                </div>
              </section><!--.content -->
              
            </div><!--.panel-body-->
        </div>
    </div>
</div>
@endsection


@section('extra_scriptBody')
<!---->
<script>

$(document).ready(function() {
    var items = 0,
        subTotal = 0.00,
        total = 0.00;
    
    $('.table tr.dato').each(function(){ 
      items += parseFloat($(this).find('td').eq(0).text()||0,10);
     
      subTotal += parseFloat($(this).find('td').eq(5).text()||0,10);
     
    })
    
    total = subTotal;
    $('.items').append(items);
    $('.sub-total').append("$"+subTotal);
    $('.total').append("$"+total);
} );
</script>

<script type="text/javascript">
function imprSelec(){
    var ficha=document.getElementById(muestra);
    var ventimp=window.open('','popimpr');
    
    ventimp.document.write(ficha.innerHTML);
    ventimp.document.close();
    ventimp.print();
    ventimp.close();}
</script>
@endsection