var Vue = require('vue');

Vue.component('select-article', require('./components/transfer-app/select-article.vue'));


Vue.component('quantity-article', require('./components/transfer-app/quantity-article.vue'));


Vue.component('article-row', require('./components/transfer-app/artticle-row.vue'));

// components: {
//     transfer_app: require('./components/transfer-app/app.vue')
// },

var vm = new Vue({
    el: '#vue-main',

    data: {
        add_article: [{
            article_id: '',
            quantity: '',
            stock: 0
        }],

        articles: [],

        article_list: [],

        errors: [],
    },

    methods: {
        addArticle: function () {

            this.errors = [];

            $.ajax({
                url: '/transfer/article/validate',
                type: 'GET',
                dataType: 'json',
                data: {
                    article_id: this.add_article.article_id,
                    quantity: this.add_article.quantity,
                    stock: this.add_article.stock
                },
                success: function (response) {
                    if (response.valid == true) {

                        this.articles.push(response.article);

                        this.add_article = {article_id: '', quantity: '', stock: 0};
                    }
                }.bind(this),
                error: function (jqXHR) {
                    this.errors = jqXHR.responseJSON;
                }.bind(this)

            });
        },

        updateArticle: function (component) {

            $.ajax({
                url: '/transfer/article/validate',
                type: 'GET',
                dataType: 'json',
                data: component.draft,
                success: function (response) {
                    if (response.valid == true) {
                        for (var key in response.article) {
                            component.article[key] = response.article[key];
                        }

                        component.editing = false;
                    }
                },
                error: function (jqXHR) {
                    component.errors = jqXHR.responseJSON;
                }
            });
        },

        removeArticle: function (article) {
//                this.$parent.articles.$remove(this.article); // deprecated in Vue v2
            var index = this.articles.indexOf(article);

            this.articles.splice(index, 1);
        }
    },

    created: function () {

        var href = window.location.href;
        var id = Number(href.split('transfer/')[1].split('/')[0]);

        if (id) {
            $.ajax({
                url: '/transfer/' + id + '/articles',
                type: 'GET',
                dataType: 'json',
                success: function (data) {

                    console.log(data.details);
                    this.articles = data.details;
                    // this.article_list = data.articles;


                }.bind(this),

                error: function (jqXHR) {

                    this.errors = jqXHR.responseJSON;
                }.bind(this)
            });
        }
    },

    mounted: function () {
        this.add_article = {article_id: '', quantity: '', stock: 0};

        var bodega = $('#bodega_id').val();

        $.getJSON('/warehouse/'+ bodega +'/stock', function (data) {

            this.article_list = data;

            console.log(this.article_list);
        }.bind(this));
    }
});