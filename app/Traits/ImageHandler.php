<?php
namespace TradeMarketing\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait ImageHandler{

    public function saveImage($base64, $imageName){

        $image = $base64;

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);

        $type = explode('/', $type)[1];

        $image = base64_decode($image);

        $imageName = ($imageName) ? $imageName : time().'.png';

        Storage::disk('images')->put($imageName,  $image);

        return $imageName;
    }


    public function getImageName($name){

        $name = explode(".", $name);

        $newName = Str::slug($name[0]).'-'.time() .'.' .$name[1];

       return $newName;
    }
}