<?php

namespace TradeMarketing\Traits;

use Illuminate\Support\Facades\Session;

trait ResponseHandler
{


    public static function alertView($status, $message, $title = '', $destroy = true)
    {

        Session::flash('alert-status', $status);

        ($title) ? Session::flash('alert-title', $title) : '';

        Session::flash('alert-message', $message);

        $view = view('alerts.flash_response')->render();

        if ($destroy) {
            Session::remove('alert-status');
        }

        return $view;
    }
}