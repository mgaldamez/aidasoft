<?php

namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

class InventarioGeneral extends Model
{
    protected $table = 'tmk_inventario_general';

    public static function buscar_inventario()
    {
         return DB::table('tmk_inventario_general')
            ->where('id',1)
            ->get();
    }

}
