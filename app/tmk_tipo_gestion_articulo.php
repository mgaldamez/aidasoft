<?php

namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

class tmk_tipo_gestion_articulo extends Model
{
    protected $table = "tmk_tipo_gestion_articulos_table";


    protected $fillable = [
		'id','gestion'
    ];


    public static function listing()
    {
        return static::orderBy('gestion')->lists('gestion', 'id');
    }
    public function up()
    {
        Schema::create('tmk_tipo_gestion_articulo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gestion');
            $table->timestamps();
        });
    }
}
