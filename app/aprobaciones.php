<?php

namespace TradeMarketing;

use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use TradeMarketing\Models\ArticleMasterAttribute;
use TradeMarketing\Models\ArticleMasterWarehouse;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\ArticleWarehouse;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\Traits\ImageHandler;
use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;
use Validator;
use DB; 
class aprobaciones extends Model
{
    //
     protected $table = "tmk_preaprobacionSalida";
         protected $fillable = [
         'id'
      ,'Pre_external_movement_num'
      ,'external_movement_num'
      ,'agent_id'
      ,'external_agent_id'
      ,'warehouse_id'
      ,'observation'
      ,'movement'
      ,'status_id'
      ,'created_by'
      ,'updated_by'
      ,'deleted_by'
      ,'created_at'
      ,'updated_at'
      ,'deleted_at'
      ,'aprobado'
      ,'actualizacion'
    ];
}
