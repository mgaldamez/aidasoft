<?php 
namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\Models\ArticleBodega;

class PurchaseArticleBodega extends Model{

	protected $table = "tmk_purchase_article_bodegas";
	protected $fillable = ['purchase_detail_id', 'article_bodega_id'];
	public $timestamps = false;


	public static function store($data = []){
	    dd($data);
		$articleBodega = ArticleBodega::create($data->only('article_id', 'bodega_id', 'quantity', 'movement_id'));

		PurchaseArticleBodega::create([
			'purchase_detail_id'    => $data->get('purchase_detail_id'),
			'article_bodega_id'     => $articleBodega->id
			]);
	}

}