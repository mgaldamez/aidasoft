<?php

namespace TradeMarketing\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use TradeMarketing\ArticleMaster;
use TradeMarketing\Models\Order;
use TradeMarketing\Models\Transfer;
use TradeMarketing\Models\Views\TransferView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Policies\ArticlePolicy;
use TradeMarketing\Policies\OrderPolicy;
use TradeMarketing\Policies\PurchasePolicy;
use TradeMarketing\Policies\TransferPolicy;
use TradeMarketing\Models\PurchaseOrder as Purchase;
use TradeMarketing\Policies\WarehousePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        ArticleMaster::class => ArticlePolicy::class,
        Order::class => OrderPolicy::class,
        Transfer::class => TransferPolicy::class,
        Purchase::class => PurchasePolicy::class,
        Warehouse::class => WarehousePolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {

//        $gate->before(function($user){
//            if($user->authRole('superadmin')){
//                return true;
//            }
//        });

        $this->registerPolicies($gate);
    }
}
