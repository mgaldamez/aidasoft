<?php

namespace TradeMarketing;

use Illuminate\Support\Facades\Storage;

class Image
{
	
	public static function save($base64){
		
		$image = $base64;

		list($type, $image) = explode(';', $image);
		list(, $image)      = explode(',', $image);

		$type = explode('/', $type)[1];
		
		$image = base64_decode($image);




		$imageName = time().'.png';

		Storage::disk('images')->put($imageName,  $image);

		return $imageName;
	}
}