<?php

namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{

    /*
	*	return: Array
	*/
	public static function object_to_array($object){
		return json_decode( json_encode( $object ), true );
	}


	public static function todayFormat (){

		$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

		$date =  $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

		return $date;
	}

	
	public static function array_push_assoc(array $array, array $values){
		return array_merge($array, $values);
	}
}
