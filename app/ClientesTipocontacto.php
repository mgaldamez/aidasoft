<?php

namespace TradeMarketing;
use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Traits\ImageHandler;
use Validator;
use DB;

class ClientesTipocontacto extends Model
{
    protected $table = "tmk_clientes_tipo_contactos";

    protected $fillable = [
		'id','descripcion'
    ];
    public function isValid($data)
    {
		 $rules = 
		 [
		 'id'=> 'required|max:100|unique:tmk_clientes_tipo_contactos',
		 'descripcion' => 'min:5|max:255',
		 ];
		    // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['id'] .= ',id,' . $this->id;
            $rules['descripcion'] .= ',descripcion,' . $this->id;
        }
		 $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
		  if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
	}
	public static function listing()
    {
        return static::orderBy('descripcion')->lists('descripcion', 'id');
    }
    public static function buscar_tipo_contacto($id)
    {
         return DB::table('tmk_clientes_tipo_contactos')
            ->where('id',$id)
            ->get();
    }
    public static function buscar_header($id)
    {
         $items = DB::table('tmk_clientes_tipo_contactos')
            ->select('descripcion')
            ->where('id',$id)
            ->get();
            $retorno;
            foreach($items as $item)
            {
              $retorno=$item->descripcion;  
            }
            return $retorno;
    }
    

}
