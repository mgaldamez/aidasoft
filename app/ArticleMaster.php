<?php

namespace TradeMarketing;

use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Models\Article;
use TradeMarketing\Models\ArticleMasterAttribute;
use TradeMarketing\Models\ArticleMasterWarehouse;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\ArticleWarehouse;

use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;
use TradeMarketing\Traits\ImageHandler;
use Validator;
use DB;

class ArticleMaster extends Model
{

    use ImageHandler, SoftDeletes;

    protected $table = "tmk_article_masters";


    protected $fillable = [
        'name', 'barcode', 'internal_reference', 'category_id', 'brand_id', 'measurement_unit_id','gestion', 'image', 'max_stock', 'min_stock', 'long_description', 'created_by', 'updated_by', 'deleted_by'
    ];


    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['attributes'];




    public function isValid($data)
    {
        $rules = [
            'name' => 'required|max:100|unique:tmk_article_masters',
            'brand_id' => 'required|exists:tmk_brands,id',
            'category_id' => 'required|exists:tmk_categories,id',
            'measurement_unit_id' => 'required|exists:tmk_measurement_units,id',
            'barcode' => 'min:6|max:100|unique:tmk_article_masters',
            'internal_reference' => 'min:6|max:100|unique:tmk_article_masters',
            'gestion' => 'numeric',
            'initial_stock' => 'numeric|min:0',
            'min_stock' => 'numeric|min:0',
            'max_stock' => 'numeric|min:0',
            'warehouse_id' => 'required_if:initial_stock,not_null|exists:tmk_bodegas,id',
            'long_description' => 'min:5|max:255',
            'image' => 'image_base64'
        ];


        // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['name'] .= ',name,' . $this->id;
            $rules['barcode'] .= ',barcode,' . $this->id;
            $rules['internal_reference'] .= ',internal_reference,' . $this->id;
        }

        $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);


        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }


    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }


    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }


    public function articles()
    {
        return $this->hasMany(Article::class);
    }


    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'tmk_article_master_attributes');
    }


    public function articleView()
    {
        return $this->hasMany(ArticleView::class);
    }


    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }


    public function measurementUnity()
    {
        return $this->belongsTo(MeasurementUnits::class, 'measurement_unit_id');
    }


    public function masterAttributes()
    {
        return $this->hasMany(ArticleMasterAttribute::class)
            ->select('article_master_id', 'attribute_id')
            ->groupBy('article_master_id', 'attribute_id');
    }

    public static function listing()
    {
        return static::orderBy('name')->lists('name', 'id');
    }


    public function articleByWarehouse($warehouse)
    {
        return $this->hasManyThrough(ArticleWarehouse::class, ArticleMasterWarehouse::class)->where('warehouse_id', $warehouse)->get();
    }


    public function getMeasurementUnitAttribute()
    {
        return $this->measurementUnity->description;
    }


    /**
     * @return mixed
     */
    public function getStockAttribute()
    {
        $res = DB::table('tmk_article_masters')
            ->join('tmk_article_master_warehouses', 'tmk_article_master_warehouses.article_master_id', '=', 'tmk_article_masters.id')
            ->join('tmk_article_warehouses', 'tmk_article_warehouses.article_master_warehouse_id', '=', 'tmk_article_master_warehouses.id')
            ->select(DB::raw('SUM(tmk_article_warehouses.stock) as stock'))
            ->where('tmk_article_masters.id', $this->id)
            ->first();

        return $res->stock;
    }


    public function lastPurchase()
    {
        return \DB::table('tmk_articles')
            ->join('tmk_purchase_order_details_view', 'tmk_purchase_order_details_view.article_id', '=', 'tmk_articles.id')
            ->select(
                'tmk_purchase_order_details_view.order_id',
                'tmk_purchase_order_details_view.id',
                \DB::raw('SUM(tmk_purchase_order_details_view.quantity) AS quantity'),
                'tmk_purchase_order_details_view.created_at'
            )
            ->where('tmk_articles.article_master_id', $this->id)
            ->groupby(
                'tmk_purchase_order_details_view.order_id',
                'tmk_purchase_order_details_view.id',
                'tmk_purchase_order_details_view.created_at'
            )
            ->first();
    }



}
