<?php

namespace TradeMarketing\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class WarehousePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }



    public function show($user, $warehouse){

        if($user->isAdmin()){
            return true;
        }

        return $user->isInCharge($warehouse->id);
    }

}
