<?php

namespace TradeMarketing\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class TransferPolicy
{
    use HandlesAuthorization;


//    public function before($user, $transfer)
//    {
//        if ($user->authRole('admin')) {
//            return true;
//        }
//    }


    public function show($user, $transfer)
    {

        
        if($user->isAdmin()){
            return true;
        }
      
        if ($user->isInCharge($transfer->bodega_id) and !$transfer->isDraft()) {

             return true;
        } 

        if ($user->isInCharge($transfer->bodega_id_end and !$transfer->isDraft())) {

            return true;
        }

        return false;
    }


    public function update($user, $transfer)
    {
        return $user->isAuthor($transfer);
    }


    public function author($user, $transfer)
    {
        return $user->isAuthor($transfer);
    }


    public function transfer($user, $transfer)
    {
        if($user->isAdmin() and $user->isInCharge($transfer->bodega_id) and $transfer->isDraft()){
            return true;
        }

        if ($user->isInCharge($transfer->bodega_id) and $transfer->orderState('for_transfer')){
           return true;
        }

        return false;
    }



    public function requestTransfer($user, $transfer){
        return !$user->isInCharge($transfer->bodega_id) and $transfer->isDraft();
    }


    public function confirm($user, $transfer)
    {
        return $user->isInCharge($transfer->bodega_id_end) and $transfer->orderState('confirmed');
    }
}
