<?php

namespace TradeMarketing\Validation;

use Illuminate\Support\Arr;
use Illuminate\Validation\Validator as LaravelValidator;

class Validator extends LaravelValidator
{

    public function validateImageBase64($attribute, $value, $parameters)
    {

        if (base64_decode($value, true)) {
            return false;
        }
        return true;
    }


    public function validateRequiredIf($attribute, $value, $parameters)
    {

        $this->requireParameterCount(2, $parameters, 'required_if');

        $data = Arr::get($this->data, $parameters[0]);

        $values = array_slice($parameters, 1);

        if ($values[0] == 'not_null') {
            if ($data != null) {
                return $this->validateRequired($attribute, $value);
            }
        }

        if (in_array($data, $values)) {
            return $this->validateRequired($attribute, $value);
        }

        return true;
    }


    /**
     * Validate that an attribute is an integer.
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @return bool
     */
    protected function validateId($attribute, $value)
    {
        return filter_var($value, FILTER_VALIDATE_INT) !== false;
    }
}