<?php namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

use DB;

class Inventory extends Model {

    
    protected $table = 'tmk_inventories';
    protected $fillable = ['id', 'description', 'date', 'quantity_items', 'amount', 'active'];
    
    
    public static function destruir(){
//        DB::table('tmk_inventories')->where('amount', '=', '0.00')->delete();
        return  DB::table('tmk_inventories')
            ->join('tmk_inventory_details', 'tmk_inventory_details.inventory_id', '=', 'tmk_inventories.id')
            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_inventory_details.bodega_id') 
            ->where('amount', '=', '0.00')
            ->select('tmk_inventories.id', 'tmk_inventories.date', 'tmk_inventories.quantity_items', 'tmk_inventories.amount', 'tmk_bodegas.description')
            ->groupby('tmk_inventories.id', 'tmk_inventories.date', 'tmk_inventories.quantity_items', 'tmk_inventories.amount', 'tmk_bodegas.description')
            ->delete();
    }
    
    public static function Annual_Inventory(){
        $year = 2016;
        return DB::select('Exec  tmk_inventory ?', array($year));
    }
//    
//     return DB::table('tmk_inventories')
//            ->join('tmk_inventory_details', 'tmk_inventory_details.inventory_id', '=', 'tmk_inventories.id')
//            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_inventory_details.bodega_id')  
//            ->select('tmk_inventories.id', 'tmk_inventories.date', 'tmk_inventories.quantity_items', 'tmk_inventories.amount', 'tmk_bodegas.description')
//            ->groupby('tmk_inventories.id', 'tmk_inventories.date', 'tmk_inventories.quantity_items', 'tmk_inventories.amount', 'tmk_bodegas.description')
//            ->get();
//    
    
    /** 
     * SELECT tmk_articles.id, tmk_articles.description as 'article', tmk_articles.barcode, tmk_bodegas.id as 'idbodega',      tmk_bodegas.description as 'bodega', TBL_SUCURSALES.DESCR_SUCURSAL as 'center', tmk_brands.description as 'brand',     tmk_classes.description  'clase', tmk_categories.description as 'category', tmk_article_bodegas.quantity as 'cant', tmk_articles.unit_cost, tmk_articles.unit_cost * tmk_article_bodegas.quantity  as 'subtotal'
     * FROM tmk_article_bodegas
     * INNER JOIN tmk_articles ON tmk_articles.id = tmk_article_bodegas.article_id
     * INNER JOIN tmk_bodegas ON tmk_bodegas.id = tmk_article_bodegas.bodega_id 
     * INNER JOIN TBL_SUCURSALES ON TBL_SUCURSALES.ID_SUCURSAL = tmk_bodegas.sucursal_id
     * INNER JOIN tmk_brands ON tmk_brands.id = tmk_articles.brand_id
     * INNER JOIN tmk_classes ON tmk_classes.id = tmk_articles.class_id
     * INNER JOIN tmk_categories ON tmk_categories.id = tmk_classes.category_id
     * WHERE tmk_bodegas.id = $id
     * GROUP BY tmk_articles.id, tmk_articles.description, 
     tmk_articles.barcode, tmk_bodegas.id, tmk_bodegas.description, TBL_SUCURSALES.DESCR_SUCURSAL, tmk_brands.description,   tmk_classes.description, tmk_categories.description, tmk_article_bodegas.quantity
     * ORDER BY tmk_articles.description ASC
     */
    public static function List_Inventory_Bodega($id){
        return DB::table('tmk_article_bodegas')
            ->join('tmk_articles','tmk_articles.id','=','tmk_article_bodegas.article_id')
            ->join('tmk_bodegas','tmk_bodegas.id','=','tmk_article_bodegas.bodega_id')  
            ->join('tmk_brands','tmk_brands.id','=','tmk_articles.brand_id')
            ->join('TBL_SUCURSALES','TBL_SUCURSALES.ID_SUCURSAL','=','tmk_bodegas.sucursal_id')
            ->join('tmk_classes','tmk_classes.id','=','tmk_articles.class_id')
            ->join('tmk_categories','tmk_categories.id','=','tmk_classes.category_id')
            ->select('tmk_articles.id',
                     'tmk_articles.description as article', 
                     'tmk_articles.barcode', 
                     'tmk_brands.description as brand',
                     'tmk_bodegas.id as idbodega',
                     'tmk_bodegas.description as bodega',
                     'TBL_SUCURSALES.DESCR_SUCURSAL as center', // center = sucursal
                     'tmk_classes.description as class',
                     'tmk_categories.description as category',
                     'tmk_articles.unit_cost as cost',
                     'tmk_article_bodegas.quantity as cant',
                     DB::raw('tmk_articles.unit_cost  * tmk_article_bodegas.quantity as subtotal')
                    )
            
            ->where('tmk_bodegas.id','=', $id)
            ->groupBy('tmk_articles.id',
                      'tmk_articles.description', 
                      'tmk_articles.barcode',
                      'tmk_brands.description',
                      'tmk_bodegas.id',
                      'tmk_bodegas.description',
                      'TBL_SUCURSALES.DESCR_SUCURSAL', 
                      'tmk_classes.description',
                      'tmk_categories.description',
                      'tmk_articles.unit_cost',
                      'tmk_article_bodegas.quantity'
                     )
            ->orderBy('tmk_articles.description', 'asc')
            ->get();
    }

    /*    
    SELECT 
    barcode, 
    article_id, 
    article, 
    min_stock, 
    max_stock, 
    SUM(entry) AS cant_entry , 
    SUM([exit]) AS cant_exit, 
    SUM(entry) - SUM([exit]) AS stock, 
    ( SELECT MAX(updated_at) FROM tmk_article_bodegas where tmk_article_bodegas.article_id = tmk_stock_bodega_view.article_id
    GROUP BY article_id ) AS last_update
    FROM tmk_stock_bodega_view 
    GROUP BY article_id, barcode, article, min_stock, max_stock
    */
    public static function list_inventory_all(){
        return DB::table('tmk_inventory_view')
        ->select( 'article_id', 'article', 'barcode', 'max_stock', 'min_stock', 'initial', DB::raw('SUM(stock) AS stock'), 'last_purchase', 'last_update')
        ->groupBy('article_id', 'article', 'barcode','max_stock', 'min_stock', 'initial', 'last_purchase', 'last_update')
        ->orderBy('article')
        ->get();
    }



}
