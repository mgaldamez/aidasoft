<?php namespace TradeMarketing\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Requests\ProviderStoreRequest;
use TradeMarketing\Http\Requests\ProviderUpdateRequest;
use TradeMarketing\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;

// Models
use TradeMarketing\Provider;
use TradeMarketing\Traits\ResponseHandler;

use Session;
use Redirect;

class ProviderController extends Controller
{

    use ResponseHandler;


    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('admin');
        $this->beforeFilter('@isAjax', ['only' => ['destroy']]);
        $this->beforeFilter('@find', ['only' => ['edit', 'update', 'destroy']]);
    }

    public function find(Route $route, Request $request)
    {

        $this->provider = Provider::find($route->getParameter('provider'));
        if (!$this->provider) {

            if ($request->ajax()) {
                abort(404);
            }

            return back();
        }
    }


    public function isAjax(Route $route, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response;
     */
    public function index(Request $request)
    {
        $providers = Provider::name($request->get('search'))
            ->company($request->get('search'))
            ->type($request->get('search'))
            ->orderBy('id', 'desc')
            ->paginate(50);

        if ($request->ajax()) {
            if (count($providers) != 0) {
                return response()->json(view('providers.providers', compact('providers'))->render());
            } else {
                return response()->json('<p style="text-align:center">No hay resultados que coincidan con "' . $request->get('search') . '"</p>');
            }

        } else {
            return view('providers.index', compact('providers'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('providers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->only('firstname', 'lastname', 'address', 'email', 'phone', 'company', 'type');

        // If data is valid
        $provider = new Provider;

        if ($provider->isValid($data)) {

            Provider::create($data);

            ResponseHandler::alertView(1, 'El proveedor se ha registrado correctamente.', '', false);
            return redirect()->route('admin.provider.create');
        }

        ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);

        return redirect()->route('admin.provider.create')
            ->withErrors($provider->errors)
            ->withInput($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $provider = Provider::find($id);
        $purchases = Provider::find($id)->purchases;

        return view('providers.show', compact('provider', 'purchases'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit()
    {
        return view('providers.edit', ['provider' => $this->provider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request)
    {

        $data = $request->only('firstname', 'lastname', 'address', 'email', 'phone', 'company', 'type');

        // If data is valid
        if ($this->provider->isValid($data)) {

            $this->provider->fill($data);
            $this->provider->save();

            Session::flash('alert-status', 1);
            Session::flash('alert-message', $this->provider->fullname . ', ha sido editado correctamente.');
            return redirect()->route('admin.provider.show', [$this->provider->id]);
        }

        ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);

        return back()
            ->withErrors($this->provider->errors)
            ->withInput($data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {

//        $this->provider->delete();
        return response()->json([
            'alert' => $this->alertView(1, $this->provider->fullname . " fue eliminado.")
        ], 200);
    }

}
