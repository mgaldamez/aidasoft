<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use TradeMarketing\Models\Category;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Traits\ArrayHandler;
use Validator;

class CategoryController extends Controller
{


    public function __construct(){
        $this->beforeFilter('@isAjax', ['only' => 'store']);
    }



    public function isAjax(Route $route, Request $request){

        if(!$request->ajax()){
            abort(404);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('categories.index', compact('categories'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->only('description');

        $data = array_map('trim', $data);

        $messages = [
            'required' => 'El nombre de la categoría es obligatorio.',
            'unique' => 'El nombre de la categoría ya ha sido registrado.'
        ];

        $validator = Validator::make($data, [
            'description' => 'required|unique:tmk_categories,description'
        ], $messages);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = ArrayHandler::array_upper($data);

        $category = new Category;
        $category->fill($data);
        $category->save();


        return response()->json($category);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $data = ArrayHandler::array_upper($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
