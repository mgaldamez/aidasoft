<?php

namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;


/*Models*/
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\Models\Order;
use TradeMarketing\Models\Transfer;
use TradeMarketing\Models\TransferDetails;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ResponseHandler;

class TransferController extends Controller
{
    use ResponseHandler;

    public function __construct()
    {
		
        $this->beforeFilter('@find', ['only' => ['show', 'edit', 'update', 'transfer', 'cancel', 'destroy', 'confirmReception', 'nose']]);
        $this->beforeFilter('@orderExist', ['only' => 'create']);
        $this->beforeFilter('@isAuthor', ['only' => 'edit', 'update', 'nose']);
        $this->beforeFilter('@editing', ['only' => 'edit']);
        $this->beforeFilter('@validateRequest', ['only' => ['store', 'update']]);
    }


    public function find(Route $route, Request $request)
    {
        if (!$this->transfer = Transfer::find($route->getParameter('transfer'))) {
            abort(404);
        }
    }


    public function isAuthor(Route $route, Request $request)
    {
        if (Gate::denies('author', $this->transfer)) {

            ResponseHandler::alertView(2, 'No tienes permiso para editar esta trasferencia.', 'Accesso Aenegado!', false);

            if ($request->ajax()) {
                abort(404);
            }
            return back();
        }
    }


    public function orderExist(Route $route, Request $request)
    {
        if (!$this->order = Order::find($route->getParameter('order'))) {
            if ($request->ajax()) {
                abort(404);
            }
            return back();
        }
    }


    public function editing(Route $route)
    {
        if (!$this->transfer->isDraft()) {
            abort(404);
        }
    }


    public function validateRequest(Route $route, Request $request)
    {
        $data = $request->only('bodega_id', 'bodega_id_end', 'order', 'observation', 'transfer_confirm', 'transfer_reference', 'items');

        if (isset($this->transfer)) {
            $data['order_id'] = $this->transfer->order_id;
            $data['transfer_num'] = $this->transfer->transfer_num;
            $data['bodega_id'] = isset($data['bodega_id']) ? $data['bodega_id'] : $this->transfer->bodega_id;
        } else {
            $data['order_id'] = isset($data['order']) ? Order::where('order_num', $data['order'])->first()->id : '';
            $data['transfer_num'] = trans('app.folio.transfer') . date('misy');
            $data['status_id'] = 1; // Draft
            $data['created_by'] = currentUser()->id;

            $this->transfer = new Transfer();

        }

        $data['updated_by'] = auth()->user()->id;
        $data['transferred_from'] = 17;
        $data['transferred_to'] = 57;


        if (!$this->transfer->isValid($data)) {
            ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.Soluciona los errores en el formulario.', false);

            return back()
                ->withErrors($this->transfer->errors)
                ->withInput($request->all());
        }

        $this->data = $data;
    }


    public function create()
    {



        if ($transfer = $this->order->transfers()->draft()->first()) {
            return redirect()->route('transfer.show', $transfer->id);
        }

        $transfer = new Transfer([
            'order_id' => $this->order->id,
            'transferred_by' => $this->order->for_user_id,
            'transferred_for' => $this->order->from_user_id,
            'transferred_from' => 17,
            'transferred_to' => 57,
            'bodega_id' => $this->order->warehouse_destination,
            'bodega_id_end' => $this->order->warehouse_origen,
        ]);

        $details = [];

        foreach ($this->order->details as $detail) {
            array_push($details, new TransferDetails([
                'article_id' => $detail->article_id,
                'quantity' => $detail->quantity,
            ]));
        }

        $transfer->details = $details;

        return view('transfers.create', compact('transfer'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
	 public function guardar_transfer(Transfer $transfer,Request $request)
	 {
		
		 try {
			
            $transferences = [];

            foreach ($request['items'] as $key => $item) {
                array_push($transferences, new TransferDetails([
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
                ]));
            }
			
			
            DB::beginTransaction();
          
           



            // save details
            $trasnf->details()->saveMany($transferences);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se creaba el documento.',
                'Error en creación!', false);

            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se creaba la transferencia..',
                'Error en creación!', false);

            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1,
            'La se tranferencia  se ha creado correctamente.',
            'Transferencia creada!', false);
        return redirect()->route('transfer.show', $trasnf->id);
	 }
	 
    public function store()
    {
        try {
            $transferences = [];

            foreach ($this->data['items'] as $key => $item) {
                array_push($transferences, new TransferDetails([
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
                ]));
            }

            DB::beginTransaction();
          
            $this->transfer->fill($this->data);

            $this->transfer->save();


            // save details
            $this->transfer->details()->saveMany($transferences);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se creaba la transferencia.',
                'Error en creación!', false);

            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se creaba la transferencia.',
                'Error en creación!', false);

            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1,
            'La se tranferencia ' . $this->transfer->transfer_num . ' se ha creado correctamente.',
            'Transferencia creada!', false);
        return redirect()->route('transfer.show', $this->transfer->id);
		
    }


    public function show()
    {
        if (Gate::denies('show', $this->transfer)) {

            ResponseHandler::alertView(2, 'No puedes ver esta trasferencia.', '', false);
            return back();
        }

        $transfer = $this->transfer;

        return view('transfers.show', compact('transfer'));
    }


    public function edit()
    {
        $warehouse = Warehouse::find($this->transfer->bodega_id);
        $articles = $warehouse->articleMasterWarehousesView()->lists('article', 'article_id');

        return view('transfers.edit', ['transfer' => $this->transfer, 'articles']);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        try {

            $transferences = [];

            foreach ($this->data['items'] as $key => $item) {
                array_push($transferences, new TransferDetails([
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
                ]));
            }

            DB::beginTransaction();

            $this->transfer->fill($this->data);
            // save
            $this->transfer->save();

            $this->transfer->details()->delete();
            // save details
            $this->transfer->details()->saveMany($transferences);


        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se actualizaba la transferencia.',
                'Error en actualización!', false);
            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se actualizaba la transferencia.',
                'Error en actualización!', false);
            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1,
            'La se tranferencia ' . $this->transfer->transfer_num . ' ha actualizado correctamente.',
            'Actualización exitosa!', false);
        return redirect()->route('transfer.show', $this->transfer->id);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {
        try {
            DB::beginTransaction();

            $status = $this->transfer->status;

            $this->transfer->caceled_at = Carbon::now()->toDateTimeString();
            $this->transfer->caceled_by = currentUser()->id;
            $this->transfer->status_id = 7; // cancel
            $this->transfer->save();

            if ($status == 'in_transit') {

                // Movement =>  5 = Cancelacion de Salida a Sucursal
                $articles = ArticleBodega::movementsInWarehouse($this->transfer->details, $this->transfer->bodega_id, 5);

                $this->transfer->articleWarehouseMovements()->saveMany($articles);
            }

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se cancelaba la transferencia.',
                'Error!', false);
            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ocurrió un error mientras se cancelaba la transferencia.',
                'Error!', false);
            return back();
        }


        DB::commit();


        ResponseHandler::alertView(1,
            'La transferencia ' . $this->transfer->transfer_num . ' ha sido cancelada!',
            'Transferencia cancelada!', false);

        return back();
    }


    public function transfer(Request $request)
    {
        try {

            if (!currentUser()->isInCharge($this->transfer->bodega_id)) {

                ResponseHandler::alertView(3, 'Solo puede iniciar una transferencia el encargado de la bodega.', 'Accesso Denegado!', false);

                return back();
            }


            $data = $request->only('transfer_confirm', 'transfer_reference');
            $data['transferred_at'] = Carbon::now()->toDateTimeString();
            $data['transferred_by'] = currentUser()->id;
            $data['status_id'] = isset($data['transfer_confirm']) ? 10 : $this->transfer->status_id; // in_transit : actual status)
            $data['transfer_reference'] = isset($data['transfer_reference']) ? $data['transfer_reference'] : $this->transfer->transfer_reference;

            $this->transfer->fill($data);

            $rules = [
                'transfer_reference' => 'required_if:status_id,10|unique:tmk_transfers'
            ];

            if($this->transfer->exists){
                $rules['transfer_reference'] .= ',transfer_reference,'. $this->transfer->id;
            }

            $validator = Validator::make($data, $rules,
                [
                    'transfer_reference.required_if' => 'required',
                    'transfer_reference.unique' => 'unique',
                ]
            );

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator->errors(), 'transfer_reference')
                    ->withInput($request->all());
            }

            DB::beginTransaction();

            $this->transfer->save();

            // Si el estado de la transferencia es 10 => in_transit
            // se envia la transferencia
            // se crea un movimiento de salida en la bodega
            if ($this->transfer->status_id === 10) {

                // Movement =>  27 = Salida a Sucursal
                $articles = ArticleBodega::movementsInWarehouse($this->transfer->details, $this->transfer->bodega_id, 27);

                $this->transfer->articleWarehouseMovements()->saveMany($articles);
                $this->transfer->transferred_at = Carbon::now()->toDateTimeString();
                $this->transfer->save();


                if ($this->transfer->order->status == 'in_revision') {
                    $this->transfer->order->status_id = 13; // For confirm
                    $this->transfer->order->save();
                }

            }

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(1,
                'Ha ocurrido mientras se enviaba la transferencia.',
                'Error al transferir!',
                false
            );

            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(1,
                'Ha ocurrido mientras se enviaba la transferencia.',
                'Error al transferir!',
                false
            );

            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1,
            'Transferencia ' . $this->transfer->transfer_num . ' iniciada',
            currentUser()->fullName . ' Ha iniciado una transferancia',
            false
        );

        return back();
    }


    public function destroy()
    {
        try {

            DB::beginTransaction();

            if ($this->transfer->isDraft()) {

                $this->transfer->forceDelete();
            } else {

                $this->transfer->delete();
            }

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ha ocurrido un error mientras se eliminaba la transferencia ' . $this->transfer->transfer_num . '.',
                'Error al eliminar la transferencia!', false);

            return back();

        } catch (\Exception $e) {

            DB::rollback();

            Log::error($e->getMessage());

            ResponseHandler::alertView(2,
                'Ha ocurrido un error mientras se eliminaba la transferencia ' . $this->transfer->transfer_num,
                'Error al eliminar la transferencia!', false);

            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1,
            'La transferencia ' . $this->transfer->transfer_num . ' ha sido eliminada.',
            currentUser()->fullName . ' eliminó una transferencia!', false);

        return redirect()->route('order.show', $this->transfer->order_id);
    }


    public function confirmReception(Request $request, $id)
    {
        try {
			
				$users = DB::table('tmk_user_warehouses')->where('user_id',auth()->user()->id)
															->where('warehouse_id',$this->transfer->bodega_id_end)
															->get();
				
				
            if (count($users)==0) {

                ResponseHandler::alertView(3, 'Solo puede confirmar una transferencia el encargado de la bodega.', 'Accesso Denegado!', false);
                return back();
            }

            $data = [
                'receipt_num' => 'R' . substr($this->transfer->transfer_num, 1, -2) . date('my'),
                'transfer_id' => $this->transfer->id,
                'observation' => '',
                'received_at' => Carbon::now()->toDateTimeString(),
                'received_by' => auth()->user()->id,
                'status_id' => 5 // Confirm
            ];

            DB::beginTransaction();

            $this->transfer->fill($data);
            $this->transfer->save();


            foreach ($this->transfer->details as $detail) {
                $detail->received_quantity = $detail->quantity;
                $detail->save();
            }


            // Movement => 12 = Entrada de inventario
            $articles = ArticleBodega::movementsInWarehouse($this->transfer->details, $this->transfer->bodega_id_end, 12);


            $this->transfer->articleWarehouseMovements()->saveMany($articles);

            foreach ($this->transfer->order->details as $orderDetail) {

                $orderDetail->pending = isset($orderDetail->pending) ? $orderDetail->pending : $orderDetail->quantity;

                foreach ($articles as $transference) {
                    if ($transference->article_id === $orderDetail->article_id) {
                        $orderDetail->pending = $orderDetail->pending - $transference->quantity;
                    }
                }
                $orderDetail->save();
            }

            if ($this->transfer->order->isComplete()) {

                $this->transfer->order->status_id = 5; // Confirmed
            } else {

                if ($request->get('confirm_partial')) {

                    $this->transfer->order->status_id = 9; // Confirmed Parcial

                } else {

                    $this->transfer->order->status_id = 5; // Confirmed
                }
            }

            $this->transfer->order->save();

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            return response()->json($e, 500);

        } catch (Exception $e) {

            DB::rollback();
            return response()->json($e, 500);
        }

        DB::commit();

        ResponseHandler::alertView(1, 'Registrado', 'Recepción exitosa!', false);
        return redirect()->route('order.show', $this->transfer->order_id);
    }


    public function nose()
    {
        try {
            DB::beginTransaction();

            $this->transfer->status_id = 12; // for_transfer
            $this->transfer->requested_at = Carbon::now()->toDateTimeString();
            $this->transfer->requested_by = $this->transfer->transferred_by;
            $this->transfer->save();

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            return response()->json($e, 500);

        } catch (\Exception $e) {

            DB::rollback();
            return response()->json($e, 500);
        }

        DB::commit();

        ResponseHandler::alertView(1, 'Se ha enviado una transferencia por iniciar en la bodega ' . $this->transfer->warehouseOrigen, 'Por Transferir!', false);
        return back();
    }

}
