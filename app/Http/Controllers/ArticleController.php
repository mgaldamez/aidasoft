<?php

namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use TradeMarketing\Models\Article;
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\ArticleMaster;
use TradeMarketing\tmk_tipo_gestion_articulo;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\AttributeValue;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;
use TradeMarketing\Models\Sucursal;
use TradeMarketing\Models\Variant;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use Response;
use Session;
use Validator;
use Yajra\Datatables\Facades\Datatables;


class ArticleController extends Controller
{

 protected $hidden = ['img'];
    public function __construct()
    {
        // $this->beforeFilter('@isAjax', ['only' => ['filter']]);
        $this->beforeFilter('@find', ['only' => ['show', 'update', 'destroy']]);
    }


    public function isAjax(Route $route, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
    }


    public function find(Route $route, Request $request)
    {
        $this->article = ArticleMaster::find($route->getParameter('article'));

        if (!$this->article) {
            if ($request->ajax()) {
                abort(404);
            }

            return back();
        }
    }


    public function index()
    {
        $warehouse = Warehouse::main()->first()['id'];
        $articles  = ArticleMasterWarehousesView::warehouse($warehouse)->get();
        return view('articles.index', compact('articles'));
    }




    public function filter(Request $request)
    {

        $articles = [];
        $param = $request->get('param');
        $warehouse = Warehouse::main()->first()->id;

        if (isset($param))
         {

            $articles = ArticleMasterWarehousesView::warehouse($warehouse)
                ->orWhere('article', 'LIKE', "%$param%")
                ->orWhere('category', 'LIKE', "%$param%")
                ->orWhere('brand', 'LIKE', "%$param%")
                ->get();
        }
        else 
        {

            $articles = ArticleMasterWarehousesView::warehouse($warehouse)->get();
        }


        foreach ($articles as $key => $article) 
        {

            $article['article'] = view('partials.article_add_to_cart', compact('article'))->render();

        }

       
        return Datatables::of($articles)->make(true);
    }


    public function create()
    {
        $brandList = Brand::listing();
        $sucursalList = Sucursal::listing();
        $categoryList = Category::listing();
//        $providerList = Provider::listing();
        $attributeList = Attribute::listing();
        $measurementUnitList = MeasurementUnits::listing();
        $tipo_gestion = tmk_tipo_gestion_articulo::listing();
        return view('admin.articles.create', compact(
            'categoryList', 'sucursalList', 'brandList', 'measurementUnitList', 'attributeList','tipo_gestion'
        ));
    }


    public function show(Request $request, $id)
    {
        $master = $this->article;
        
        $attributes = [];

        foreach ($master->masterAttributes as $masterAttribute) {
            $attributes[] = $masterAttribute->attribute;
        }

        $variants = Variant::variantsByArticle($master->id);
        
        return view('articles.show', compact('master', 'variants', 'attributes'));
    }


    public function store(Request $request)
    {
      
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:tmk_article_masters|max:100',
                'brand_id' => 'required|exists:tmk_brands,id',
                'category_id' => 'required|exists:tmk_categories,id',
                'measurement_unit_id' => 'required|exists:tmk_measurement_units,id',
                'barcode' => 'unique:tmk_article_masters|min:6|max:100',
                'internal_reference' => 'required|unique:tmk_article_masters|min:6|max:100',
                'initial_stock' => 'numeric|min:0',
                'min_stock' => 'numeric|min:1',
                'max_stock' => 'numeric|min:1',
                'warehouse_id' => 'required_if:initial_stock,not_null|exists:tmk_bodegas,id',
                'long_description' => 'min:5|max:500',
                'image' => 'image_base64',
            ]);
                
              
            if ($validator->fails()) {
                return response()->json([
                    'alert' => ResponseHandler::alertView(2, "Soluciona los errores en el formulario.",false),
                    'errors' => $validator->errors()
                ], 422);
            }

            $imageBase64 = $request->get('image');

            $master = new ArticleMaster;


            $data = $request->only([
                'name', 'barcode', 'internal_reference', 'brand_id', 'category_id', 'measurement_unit_id','gestion', 'min_stock', 'max_stock', 'image', 'long_description'
            ]);

            $data['image'] = (isset($data['image']) && $data['image'] != '') ? $master->getImageName($request->get('image_name')) : '';
            $data['created_by'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->id;

            $data = ArrayHandler::array_upper($data);


            // If data is valid
            if ($master->isValid($data)) {

            }


            DB::beginTransaction();

            $master->fill($data);
            $master->save();

            if ($imageBase64) {
                $master->saveImage($imageBase64, $master->image);
            }

//            dd($request->all());
            // Asigna los attributos a el articulo
            if ($request->get('variant')) {


                $attributes = [];
                foreach ($request->get('attribute') as $attribute) {

                    if (Attribute::find($attribute['id'])) {
                        AttributeValue::addNewValues($attribute['id'], $attribute);

                        array_push($attributes, $attribute['id']);
                    }
                }


                $master->attributes()->sync($attributes);

                // Si la check-variants existe se crean las variantes
                // if ($request->get('check-variants')) {


                foreach ($request->get('variant') as $key => $variant) {

                    $article = new Article([
                        'internal_reference' => $master->internal_reference . "-" . strval(str_pad($key + 1, 3, "0", STR_PAD_LEFT)), // produces "001"
                        'reference_code' => $key + 1,
                        'description' => $variant['description'],
                        'created_by' => auth()->user()->id,
                        'updated_by' => auth()->user()->id
                    ]);

                    $master->articles()->save($article);

                    $article->attributeValues()->sync($variant['value']);

                    if ($request->get('initial_stock') != '') {

                        $articleWarehouse = new ArticleBodega([
                            'bodega_id' => $request->get('warehouse_id'),
                            'quantity' => $request->get('initial_stock'),
                            'movement_id' => 14, // Inventario inicial
                            'initial' => true,
                        ]);

                        $article->warehouses()->save($articleWarehouse);
                    }
                }
                // }

            } else {

                $article = Article::create([
                    'internal_reference' => $master->internal_reference,
                    'reference_code' => 0,
                    'description' => $master->name,
                    'article_master_id' => $master->id,
                    'created_by' => auth()->user()->id,
                    'updated_by' => auth()->user()->id
                ]);

                if ($request->get('initial_stock') != '') {

                    $articleWarehouse = new ArticleBodega([
                        'bodega_id' => $request->get('warehouse_id'),
                        'quantity' => $request->get('initial_stock'),
                        'movement_id' => 14, // Inventario inicial
                        'initial' => true,

                    ]);

                    $article->warehouses()->save($articleWarehouse);
                }
            }
        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => ResponseHandler::alertView(2, "Ocurrió un error mientras se creaba el artículo.",false)]
                , 500);

        } catch (Exception $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => ResponseHandler::alertView(2, "Ocurrió un error mientras se creaba el artículo.",false)]
                , 500);
        }

        DB::commit();
        return response()->json([
            'alert' => ResponseHandler::alertView(1, "El artículo se ha registrado correctamente.",false), 'redirectTo' => $master->id
        ], 200);
    }


    public function edit($id)
    {
        if (!ArticleMaster::find($id)) {
            return back();
        }

        $master = ArticleMaster::find($id);

        $brandList = Brand::listing();
        $sucursalList = Sucursal::listing();
        $categoryList = Category::listing();
//        $providerList = Provider::listing();
        $attributeList = Attribute::listing();
        $measurementUnitList = MeasurementUnits::listing();

        return view('admin.articles.edit',
            compact(
//                'providerList',
                'categoryList',
                'sucursalList',
                'brandList',
                'measurementUnitList',
                'attributeList',
                'master',
                'attributeValues'
            ));
    }


    public function update(Request $request)
    {
        $data = $request->only([
            'name', 'barcode', 'internal_reference', 'brand_id', 'category_id', 'measurement_unit_id','gestion', 'min_stock', 'max_stock', 'long_description'
        ]);

        $imageBase64 = $request->get('image');

        if ($request->get('image') != '') {
            $data['image'] = ($request->get('image')) ? $this->article->getImageName($request->get('image_name')) : '';
        }

        $data['updated_by'] = auth()->user()->id;

        $data = ArrayHandler::array_upper($data);

        // If data is valid
        if ($this->article->isValid($data)) {

            try {
                DB::beginTransaction();

                // If the data is valid, we assign the article
                $this->article->fill($data);

                // save
                $this->article->save();

                if ($imageBase64 != '') {
                    $this->article->saveImage($imageBase64, $this->article->image);
                }

                // Attributes
//            $newAttributes = AttributeValue::filterArray($request->get('attribute_id'));
//
//            $exists = ArticleMasterAttribute::where('article_master_id', $id)->lists('attribute_id')->toArray();
//
//            $diff = array_diff($newAttributes, $exists);
//
//            $remove = array_diff($exists, $newAttributes);
//
//            if ($remove) {
//                ArticleMasterAttribute::whereIn('attribute_id', $remove)->where('article_master_id', $id)->delete();
//            }
//
//            $reals = Attribute::whereIn('id', $diff)->lists('id')->toArray();
//
//            $noExist = array_diff($diff, $reals);
//
//            foreach ($noExist as $key => $value) {
//                unset($diff[$key]);
//            }
//
//            foreach ($diff as $attribute_id) {
//
//                ArticleMasterAttribute::create([
//                    'article_master_id' => $id,
//                    'attribute_id' => $attribute_id,
//                ]);
//            }


            } catch (\Illuminate\Database\QueryException $e) {

                DB::rollback();
                Log::info('Error Updating article: ' . $e);

                return response()->json([
                        'alert' => ResponseHandler::alertView(2, "Ocurrió un error mientras se actuallizaba el artículo.",false)]
                    , 500);

            } catch (Exception $e) {

                DB::rollback();
                Log::info('Error Updating article: ' . $e);

                return response()->json([
                        'alert' => ResponseHandler::alertView(2, "Ocurrió un error mientras se actuallizaba el artículo.",false)]
                    , 500);
            }

            DB::commit();
            return response()->json([
                'alert' =>ResponseHandler::alertView(1, "El artículo ha sido actualizado."), 'redirectTo' => $this->article->id
            ], 200);
        }


        return response()->json([
            'alert' => ResponseHandler::alertView(2, "Soluciona los errores en el formulario.",false),
            'errors' => $this->article->errors
        ], 422);
    }


    public function destroy(Request $request, $id)
    {
        try {

            DB::beginTransaction();

            DB::table('tmk_article_masters')
                ->where('id', $this->article->id)
                ->update([
                    'deleted_at' => Carbon::now()->toDateTimeString(),
                    'deleted_by' => auth()->user()->id
                ]);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Log::info('Error Updating article: ' . $e);

            ResponseHandler::alertView(2, '', 'Ocurrió un error mientras se actuallizaba el artículo.', false);
            return back();

        } catch (Exception $e) {

            DB::rollback();
            Log::info('Error Updating article: ' . $e);

            ResponseHandler::alertView(2, '', 'Ocurrió un error mientras se actuallizaba el artículo.', false);
            return back();
        }

        DB::commit();

        ResponseHandler::alertView(1, '', 'El artículo fue eliminado', false);

        return back();
    }

    public function findById($article)
    {
        
        $article = \TradeMarketing\Models\Views\ArticleView::where('article_id', $article)->first();
        return response()->json($article);
    }    
    
    public function referencia($referencia)
    {
        
        $article = \TradeMarketing\Models\Views\ArticleView::where('internal_reference', $referencia)->first();
  
        return response()->json($article);
    }  

    public function findseries($referencia,$num_doc,$num_item)
    {
        
        
        $series = \TradeMarketing\Purchase::buscar_series_num_doc($referencia,$num_doc,$num_item);
        return response()->json($series);

    }  
    public function findseries_edit($referencia,$num_doc,$num_item)
    {
        
      
        $series = \TradeMarketing\Purchase::buscar_series_edit($referencia,$num_doc,$num_item);
        return response()->json($series);

    }  
    public function findseriesall($referencia,$num_doc,$num_item)
    {
        
      
        $series = \TradeMarketing\Purchase::buscar_series_edit($referencia,$num_doc,$num_item);
        $key = 0;
        $view = view('purchases.tables.series',compact('series', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);
 

    } 


    public function articuloWarehouse( $warehouse, $article)
    {
        
    
            return ArticleMasterWarehousesView::where('warehouse_id', $warehouse)
                ->where('article_id', $article)
                ->firstOrFail();
    
        
    }

    public function addmovement($warehouse)
    {
        
    
            $key = 0;
               
            $articleList = Warehouse::find($warehouse)->articleListing();
            $view = view('movements.external.partials.table_row', compact('articleList', 'key'));
            $sections = $view->render();
    
            return Response::json($sections);
        
    }



}
