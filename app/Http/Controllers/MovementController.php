<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

// Models
use \TradeMarketing\Models\ArticleBodega;
use \TradeMarketing\Models\Category;
use \TradeMarketing\Models\Movement;
use TradeMarketing\Models\Views\MovementsView;
use \TradeMarketing\Provider;

class MovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $movements = '';
        if ($request->ajax()) {
            $movements = Movement::list_movements();
            return $movements;
        } else {
            $movements = Movement::All();
            return view('movements.index', compact('movements'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function createPurchase()
    {

        $providers = Provider::All();
        $category = Category::All();
        $pakages = Pakage::All();
        $invoices = Invoice::All();
        return view('articles.purchase', compact('providers', 'category', 'pakages', 'invoices'));
        // return view('articles.purchase', compact("providers"));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //   $this->data = ArrayHandler::array_upper($this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //   $this->data = ArrayHandler::array_upper($this->data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function show_movements_articles(Request $request, $id)
    {

        if ($request->ajax()) {
            $movements = Movement::list_movements_articles($id);
            return $movements;
        }
    }

    public function store_movement_article(Request $request)
    {

        $movement = ArticleBodega::create([
            'type_movement_id' => (int)$request->get('movement'),
            'article_id' => (int)$request->get('article'),
            'bodega_id' => (int)$request->get('bodega'),
            'entry_date' => $request->get('date'),
            'quantity' => (int)$request->get('quantity'),
            'unit_cost' => (double)$request->get('unit_cost'),
        ]);

        dd($movement);
    }

    public function showMovementList()
    {
        $this->movements = MovementsView::orderBy('id', 'desc')->get();
        return view('movements.index', ['movements' => $this->movements]);
    }

    public function export()
    {
        try {


            Excel::create('laravel_excel_movements', function ($excel) {

                $excel->sheet('Movimientos', function ($sheet) {

                    $movements = MovementsView::all();

                    $sheet->loadView('movements.table', compact('movements'));
                    // Set font with ->setStyle()
                    $sheet->setStyle(array(
                        'font' => array(
                            'upper'      =>  true,
                        )
                    ));
                });

            })->download('xlsx');
        } catch (\Exception $e) {
            return $e;
        }
    }
}
