<?php namespace TradeMarketing\Http\Controllers;

use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;

use Illuminate\Http\Request;

use TradeMarketing\Classes; /*Modelo Classes*/
class ClassController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// $data = ArrayHandler::array_upper($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}
    
    public function show_classes_actives(){
        $class = Classes::Show_Classes_Actives();
        return response()->json($class);
    }

    public function show_classes_category(Request $request, $id){

        $class = Classes::Get_Classes_Category($id);
        return response()->json($class);
    }
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// $data = ArrayHandler::array_upper($data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
