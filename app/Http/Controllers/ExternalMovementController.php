<?php

namespace TradeMarketing\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use TradeMarketing\Purchase;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\ArticleBodega;
use TradeMarketing\Models\ExternalMovement;
use TradeMarketing\Models\ExternalMovementDetail;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\User;
use TradeMarketing\Aprobar;
use Storage;
use Response;
class ExternalMovementController extends Controller
{

    public function __construct()
    {
        $this->beforeFilter('@validateRequest', ['only' => ['storeEntry', 'storeExit']]);
    }


    public function validateRequest(Route $route, Request $request)
    {
        $data = [
            'agent_id' => $request->get('agent'),
            'external_agent_id' => $request->get('external_agent'),
            'warehouse_id' => $request->get('warehouse'),
            'observation' => $request->get('observation'),
            'items' => $request->get('items'),
            'created_by' => currentUser()->id,
            'updated_by' => currentUser()->id,
            'status_id' =>4,
            'num_doc_temp'=>$request->get('num_doc_temp'),
            'actionController' => explode('@', $route->getAction()['controller'])[1],
        ];

        $this->external = new ExternalMovement;


        if (!$this->external->isValid($data)) {

            ResponseHandler::alertView(2, '', 'Soluciona los errores en el formulario.', false);

            return back()
                ->withErrors($this->external->errors)
                ->withInput($request->all());
        }


        $this->data = $data;
        $this->data['items'] = ArrayHandler::array_condensed($this->data['items'], 'article', 'quantity');
    }


    public function createExit(Request $request, $warehouse)
    {
        
        $userList = User::supervisorListing();

        $warehouse = Warehouse::findOrFail($warehouse);
        $warehouse_id = $warehouse->id;
        $articleList = $warehouse->articleListing();
        $num_doc=array("num_doc_temp"=>str_replace(' ','',str_replace(':','',str_replace('-','', Carbon::now()->toDateTimeString().Carbon::now()->micro))));
        
       


        $particularList = config('enums.external_agents');
        return view('movements.external.out_warehouse',
            compact('warehouse_id', 'userList', 'articleList', 'particularList','num_doc'));
    }

    public function createEntry(Request $request, $warehouse)
    {
        $userList = User::supervisorListing();
        $warehouse = Warehouse::find($warehouse);
        $warehouse_id = $warehouse->id;
        $articleList = $warehouse->articleListing();
        $particularList = config('enums.external_agents');
        $num_doc=array("num_doc_temp"=>str_replace(' ','',str_replace(':','',str_replace('-','', Carbon::now()->toDateTimeString().Carbon::now()->micro))));
        
        return view('movements.external.in_warehouse',
            compact('warehouse_id', 'userList', 'articleList', 'particularList','num_doc'));
    }


    public function storeEntry(Request $request)
    {
      
        $this->data['external_movement_num'] = config('enums.doc_references.external_movement_exit') . date('misy');
        $details = [];

        foreach ($this->data['items'] as $key => $item) {
            array_push($details, new ExternalMovementDetail([
                'article_id' => $item['article'],
                'quantity' => $item['quantity'],
//                'external_agent_id' => $item['agent']
            ]));
        }

        $this->data = ArrayHandler::array_upper($this->data);
        $this->data['movement'] = 'entry';

        try {

            DB::beginTransaction();

            $this->external->fill($this->data);

            $this->external->save();
            // save details
            $this->external->details()->saveMany($details);

            $articles = ArticleBodega::movementsInWarehouse($this->external->details, $this->external->warehouse_id, 30);

            $this->external->articleWarehouseMovements()->saveMany($articles);
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            abort(500, $e);

        } catch (Exception $e) {
            DB::rollback();
            abort(500, $e);
        }

        DB::commit();

        ResponseHandler::alertView(1,
            currentUser()->fullName . ' ha creado una entrada extra de artículos con éxito.',
            'Nueva Entrada Extra.',
            false);

return redirect()->route('warehouse.inventory', $this->external->warehouse_id);
       // return redirect()->route('external.movement.show', $this->external->id);
    }


    public function storeExit(Request $request)
    {
        try {
            
            $this->data['external_movement_num'] = config('enums.doc_references.external_movement_exit') . date('misy');

            $details = [];
            foreach ($this->data['items'] as $key => $item) {
                array_push($details, new ExternalMovementDetail([
                    'article_id' => $item['article'],
                    'quantity' => $item['quantity'],
//                    'external_agent_id' => $item['agent']
                ]));
                
            }
           



            $this->data = ArrayHandler::array_upper($this->data);
            $this->data['movement'] = 'exit';

            DB::beginTransaction();


        

            $this->external->fill($this->data);

            $this->external->save();
            // save details

            
            $this->external->details()->saveMany($details);

           $articles = ArticleBodega::movementsInWarehouse($this->external->details, $this->external->warehouse_id, 29);

            $this->external->articleWarehouseMovements()->saveMany($articles);

            $id_serie_exit = DB::table('tmk_purchase_series_out')
                                    ->select('item_id')
                                    ->where('num_doc_temp',$this->data['num_doc_temp'])
                                    ->get();
            
            foreach($id_serie_exit as $id_)
            {
                DB::table('tmk_purchase_series')
                ->where('id', $id_->item_id)
                ->update([
                    'num_doc_temp_out' => $this->data['num_doc_temp']
                    ,'status_in'=>0
                    ,'status_out'=>1
                    ,'salida_el'=>Carbon::now()->toDateTimeString()
                    
                    ]);
                    DB::table('tmk_purchase_series_out')
                    ->where('item_id', $id_->item_id)
                    ->update(['status_doc'=>1
                        ,'status_out'=>1
                        ,'actualizado'=>Carbon::now()->toDateTimeString()
                        
                        ]);
                
            }
           
           
            



        } 


        catch (\Illuminate\Database\QueryException $e) {
            DB::rollback();
            abort(500, $e);

        } 


        catch (Exception $e) {
            DB::rollback();
            abort(500, $e);
        }

        DB::commit();


        ResponseHandler::alertView(1,
            currentUser()->fullName . ' ha creado una salida extra de artículos con éxito.',
            'Nueva Salida Extra.',
            false);

      return redirect()->route('warehouse.inventory', $this->external->warehouse_id);
    }


    public function show($id)
    {
      
        $external = ExternalMovement::find($id);
        return view('movements.external.show', compact('external'));
    }

    public function create_series_out($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
       
        foreach($data as $key => $value)
        {
            DB::table('tmk_purchase_series_out')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'serie'=> $value['serie']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_out'=>1
                ,'creado'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
        
        return $data;
    }


    public function edit_series_out($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);

        Storage::disk('local')->put('request.txt',$request);
        DB::table('tmk_purchase_series_out')
            ->where('internal_reference', $internal_reference)
            ->where('num_doc_temp',$num_doc)
            ->where('article_id',$id)
            ->delete();
           // Storage::disk('local')->put('delete.txt',$request);
        foreach($data as $key => $value)
        {
            DB::table('tmk_purchase_series_out')->insert(
                ['item_id' => $value['id']
                , 'internal_reference' => $internal_reference
                , 'num_doc_temp' => $num_doc
                ,'serie'=> $value['serie']
                ,'article_id'=>$id
                ,'status_doc'=>0
                ,'status_out'=>1
                ,'creado'=>Carbon::now()->toDateTimeString()
                ,'actualizado'=>Carbon::now()->toDateTimeString()
                ]
            );
           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
       
        return $data;
    }



    
    public function findseries($referencia,$num_doc,$num_item)
    {
        
        
        $series = \TradeMarketing\Purchase::buscar_series_num_doc_out($referencia,$num_doc,$num_item);
        return response()->json($series);

    }  
    public function findseries_edit($referencia,$num_doc,$num_item)
    {
        
      
        $series = \TradeMarketing\Purchase::buscar_series_edit_out($referencia,$num_doc,$num_item);
        return response()->json($series);

    }  

    public function findseriesall($referencia,$num_doc,$num_item)
    {
      
  
        $series = \TradeMarketing\Purchase::buscar_series_edit_out($referencia,$num_doc,$num_item);

        $key = 0;
        $view = view('movements.external.partials.list_out_series',compact('series', 'key'));
        
        $sections = $view->render();
    
        return Response::json($sections);
 

    } 

    public function cancelar_out($num_doc,$warehouse_id)
    {
        DB::table('tmk_lotes_out')->where('num_doc_temp',$num_doc)->delete();
        DB::table('tmk_purchase_series_out')->where('num_doc_temp',$num_doc)->delete();
        return redirect()->route('warehouse.inventory', $warehouse_id);

    }  

    public function cancelar_in($num_doc,$warehouse_id)
    {
        DB::table('tmk_lotes')->where('num_doc_temp',$num_doc)->delete();
        DB::table('tmk_purchase_series')->where('num_doc_temp',$num_doc)->delete();
        return redirect()->route('warehouse.inventory', $warehouse_id);

    } 
    /*
    
        public function create_series_out($num_doc,$id,$internal_reference,Request $request)
    {
      
        $data=json_decode($request->getContent(), true);
       
        foreach($data as $key => $value)
        {
            DB::table('tmk_purchase_series')
            ->where('id', $value['id'])
            ->update([
                'num_doc_temp_out' => $num_doc
                ,'status_in'=>0
                ,'status_out'=>1
                ,'salida_el'=>Carbon::now()->toDateTimeString()
                
                ]);


           // Storage::disk('local')->put($key.'.txt',$value['serie']);
        }
        
        return $data;
    }
    
    
    */

}
