<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\MeasurementUnits;
use TradeMarketing\Traits\ArrayHandler;
use Validator;


class MeasurementController extends Controller
{


    public function __construct()
    {
        $this->beforeFilter('@isAjax');
    }


    public function isAjax(Route $route, Request $request)
    {

        if (!$request->ajax()) {
            abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('description', 'unity');

        $data = array_map('trim', $data);

        $messages = [
            'description.required' => 'El nombre de la medida es obligatorio.',
            'description.unique' => 'El nombre de la medida ya ha sido registrado.'
        ];

        $validator = Validator::make($data, [
            'description' => 'required|unique:tmk_measurement_units,description'
        ], $messages);


        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = ArrayHandler::array_upper($data);

        $measurement = new MeasurementUnits;
        $measurement->fill($data);
        $measurement->save();


        return response()->json($measurement);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  $this->data = ArrayHandler::array_upper($this->data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
