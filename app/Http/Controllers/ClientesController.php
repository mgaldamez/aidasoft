<?php

namespace TradeMarketing\Http\Controllers;
use GuzzleHttp;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Requests\ProviderStoreRequest;
use TradeMarketing\Http\Requests\ProviderUpdateRequest;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Clients;
use TradeMarketing\Sucursalclientes;
use TradeMarketing\ClientesContactos;
use TradeMarketing\ClientesTipocontacto;
use TradeMarketing\Paises;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use TradeMarketing\Provider;
use TradeMarketing\Traits\ResponseHandler;
use Session;
use Redirect;
use Carbon\Carbon;
use Gate;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use TradeMarketing\Traits\ArrayHandler;
use Yajra\Datatables\Facades\Datatables;
use TradeMarketing\ArticleMaster;

class ClientesController extends Controller
{
	 use ResponseHandler;
    /**
     * Controlador de clientes y proveedores
     */
	 
    public function index(Request $request)
    {
		
		$providers = DB::table('tmk_clientes_empresas')->paginate(50);
			/*$providers = Provider::name($request->get('search'))
            ->company($request->get('search'))
            ->type($request->get('search'))
            ->orderBy('id', 'desc')
            ->paginate(50);
			*/
			
        if ($request->ajax()) {
            if (count($providers) != 0) {
                return response()->json(view('providers.providers', compact('providers'))->render());
            } else {
                return response()->json('<p style="text-align:center">No hay resultados que coincidan con "' . $request->get('search') . '"</p>');
            }

        } else {
            return view('clients.index', compact('providers'));
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$ListaSucursales=Sucursalclientes::listing(0);
		$Listapaises=Paises::listing();
		
          return view('clients.create',compact('ListaSucursales','Listapaises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		try
		{

		 $validator = Validator::make($request->all(), [
                'Nombre' => 'required|unique:tmk_clientes_empresas|max:100',
                'id_tipo_cliente' => 'required|max:100',
                'Ruc' => 'required|unique:tmk_clientes_empresas|max:80',
            ]);


            if ($validator->fails()) {
                ResponseHandler::alertView(2, 'Error', '', false);

                return back()
                    ->withInput($request->all())
                    ->withErrors($validator->errors());
            }


		$ativ=0;
		$cliente_=0;
		$proveedor_=0;
        if($request->chk_active=='on')
		{
			$ativ=1;
		}
		if($request->chk_cliente==1)
		{
			$cliente_=1;
		}
		if($request->chk_provider==1)
		{
			$proveedor_=1;
		}
		
		$id_empresa=DB::table('tmk_clientes_empresas')->insertGetId(
		[		'id_tipo_cliente'=>$request->id_tipo_cliente
				,'Nombre'=>$request->Nombre
				,'direccion1'=>$request->direccion1
				,'direccion2'=>$request->direccion2
				,'id_pais'=>$request->id_pais
				,'ciudad'=>$request->estado
				,'estado'=>$request->estado
				,'codigo_postal'=>$request->codigo_postal
				,'Ruc'=>$request->Ruc
				,'telefono'=>$request->telefono
				,'movil'=>$request->movil
				,'email'=>$request->email
				,'titulo'=>$request->titulo
				,'pagina_web'=>$request->pagina_web
				,'cliente'=>$cliente_
				,'proveedor'=>$proveedor_
				,'active'=>$ativ
				,'empresa_id'=>$request->empresa_id
				,'sucursal_id'=>$request->sucursal_id
				,'referencia'=>$request->txt_referencia
				,'notas'=>$request->notas
				,'creado' => Carbon::now()->toDateTimeString()
                ,'agregado_por' => auth()->user()->id
				]
		);
		
		if(isset($request->sucursal_id))
		{
			
			DB::table('tmk_clientes_sucursales')
				->where('id', $request->sucursal_id)
				->update(['id_empresa'=>$id_empresa]);
		}
		
		ResponseHandler::alertView(1, 'El registro se ha registrado con exito.', ' Confirmada! .', false);
		return redirect()->route('clientes.index');
		}
		catch (Exception $e) {

            DB::rollback();
            Log::info('Error creating article: ' . $e);

            return response()->json([
                    'alert' => $this->alertView(2, "Ocurrió un error mientras se creaba el artículo.")]
                , 500);
        }
		
    }
    public function list()
    {
       $list=Clients::listing();
	   dd($list);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		
		$datos=Clients::buscar_cliente($id);
		$ListaSucursales=Sucursalclientes::listing($id);

		$items = [];
		$sucursales = [];
		
            foreach ($datos as $item) {
				
				$id_sucursal=$item->sucursal_id;
				$id_sucursal="";
				$suc_nombre="";
				$suc_telefono="";
				$suc_correo="";
				$suc_movil="";
				$suc_image="";
				$ativ=false;
				$cliente_=false;
				$proveedor_=false;
				$suc_id=0;
				if($item->active==1)
				{
					$ativ=true;
				}
		
				if($item->cliente==1)
				{
				$cliente_=true;
				$sucursal=Sucursalclientes::buscar_sucursalcliente($id);
				foreach($sucursal as $itm)
				{
					$suc_id=$itm->id;
					$suc_image=$itm->image;
					$suc_nombre=$itm->nombre;
					$suc_telefono=$itm->telefono;
					$suc_movil=$itm->movil;
					$suc_correo=$itm->correo;
				
				}
				}
				if($item->proveedor==1)
				{
				$proveedor_=true;
				}
				
				$items1= (object) array(
				'id'=>$item->id
				,'id_tipo_cliente'=>$item->id_tipo_cliente
				,'Nombre'=>$item->Nombre
				,'direccion1'=>$item->direccion1
				,'direccion2'=>$item->direccion2
				,'id_pais'=>$item->id_pais
				,'ciudad'=>$item->ciudad
				,'estado'=>$item->estado
				,'codigo_postal'=>$item->codigo_postal
				,'Ruc'=>$item->Ruc
				,'telefono'=>$item->telefono
				,'movil'=>$item->movil
				,'email'=>$item->email
				,'titulo'=>$item->titulo
				,'pagina_web'=>$item->pagina_web
				,'chk_cliente'=>$cliente_
				,'chk_provider'=>$proveedor_
				,'active'=>$ativ
				,'empresa_id'=>$item->empresa_id
				,'sucursal_id'=>$item->sucursal_id
				,'txt_referencia'=>$item->referencia
				,'notas'=>$item->notas
				,'suc_nombre'=>$suc_nombre
				,'suc_telefono'=>$suc_telefono
				,'suc_movil'=>$suc_movil
				,'suc_correo'=>$suc_correo
				,'suc_image'=>$suc_image
				,'suc_id'=>$suc_id
				);
                
			}
			$provider=$items1;
			
			$Listapaises=Paises::listing();
			$contactos=ClientesContactos::buscar_contactos($id);
			
        return view('clients.edit', compact('provider','ListaSucursales','Listapaises','contactos'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
       		$ativ=0;
			$cliente=0;
			$proveedor=0;
        if($request->chk_active=='on')
		{
			$ativ=1;
		}
		
		if(!isset($request->chk_cliente))
		{
			$request->chk_cliente=0;
		}
		if(!isset($request->chk_provider))
		{
			$request->chk_provider=0;
		}
		$arreglo=array(
				'id_tipo_cliente'=>$request->id_tipo_cliente
				,'Nombre'=>$request->Nombre
				,'direccion1'=>$request->direccion1
				,'direccion2'=>$request->direccion2
				,'id_pais'=>$request->id_pais
				,'ciudad'=>$request->estado
				,'estado'=>$request->estado
				,'codigo_postal'=>$request->codigo_postal
				,'Ruc'=>$request->Ruc
				,'telefono'=>$request->telefono
				,'movil'=>$request->movil
				,'email'=>$request->email
				,'titulo'=>$request->titulo
				,'pagina_web'=>$request->pagina_web
				,'cliente'=>$request->chk_cliente
				,'proveedor'=>$request->chk_provider
				,'active'=>$ativ
				,'empresa_id'=>$request->empresa_id
				,'sucursal_id'=>$request->sucursal_id
				,'referencia'=>$request->txt_referencia
				,'notas'=>$request->notas
				,'actualizado' => Carbon::now()->toDateTimeString()
                ,'actualizado_por' => auth()->user()->id
				);
		 	
		DB::table('tmk_clientes_empresas')
				->where('id', $id)
				->update($arreglo);
				
		DB::table('tmk_clientes_sucursales')
				->where('id', $request->sucursal_id)
				->update(['id_empresa'=>$id]);
		ResponseHandler::alertView(1, 'El registro se ha actualizado con exito.', ' Confirmada! .', false);
		return redirect()->route('clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

	public function addsucursal(Request $request)
    {
			$id=DB::table('tmk_clientes_sucursales')->insertGetId(
		[		'nombre'=>$request->suc_nombre
				,'telefono'=>$request->suc_correo
				,'movil'=>$request->suc_telefono
				,'correo'=>$request->suc_movil		
		]
		);
		
		$sucursal=Sucursalclientes::buscar_sucursales($id);
		//$category=Clients::listing();
		
	return response()->json($sucursal[0]);
    }
	
	
	
	public function image(Request $request,$id)
	{
		
		$imagen=$request->get('imagen');
		$master = new ArticleMaster;
		$imageBase64=$imagen;
		if ($imageBase64) {
			$imagenes=$master->saveImage($imageBase64, $master->image);
            }
		DB::table('tmk_clientes_sucursales')
		->where('id',$id)
		->update(['image'=>$imagenes]);
		$respuesta=array('dato' =>$imagenes,'id'=>$id);
		return response()->json($respuesta);	
	}
	
	
	
	
		public function editsucursal(Request $request)
	{
		
		
		
			DB::table('tmk_clientes_sucursales')
			->where('id',$request->suc_id)
			->update(
				['nombre'=>$request->suc_nombre
				,'telefono'=>$request->suc_correo
				,'movil'=>$request->suc_telefono
				,'correo'=>$request->suc_movil
				]
		);
		
		$sucursal=Sucursalclientes::buscar_sucursales($request->suc_id);

		
	return response()->json($sucursal[0]);
	}

	public function addcontactos(Request $request,$id)
	{
		$idk=DB::table('tmk_clientes_contactos')->insertGetId(
			[		'con_nombre'=>$request->con_nombre
					,'con_direccion'=>$request->con_direccion
					,'con_telefono'=>$request->con_telefono
					,'con_movil'=>$request->con_movil	
					,'con_correo'=>$request->con_correo		
					,'con_puesto'=>$request->con_puesto	
					,'id_empresa'=>$id
					,'con_tipo_contacto'=>$request->con_tipo_contacto				
			]
			);
		$va=ClientesTipocontacto::buscar_header($request->con_tipo_contacto);
		$contactos=ClientesContactos::buscar_contacto($idk);
		foreach($contactos as $item)
		{	
			$arreglo[]=array(
					'id'=>$item->id
					,'con_nombre'=>$item->con_nombre
					,'con_direccion'=>$item->con_direccion
					,'con_telefono'=>$item->con_telefono
					,'con_movil'=>$item->con_movil	
					,'con_correo'=>$item->con_correo		
					,'con_puesto'=>$item->con_puesto	
					,'con_tipo_contacto'=>$item->con_tipo_contacto
					,'tipo_contacto'=>$va);

		}
		return response()->json($arreglo[0]);

	}
	public function editcontactos(Request $request,$id)
	{
		DB::table('tmk_clientes_contactos')
		->where('id',$id)
		->update(
			[		'con_nombre'=>$request->con_nombre
					,'con_direccion'=>$request->con_direccion
					,'con_telefono'=>$request->con_telefono
					,'con_movil'=>$request->con_movil	
					,'con_correo'=>$request->con_correo		
					,'con_puesto'=>$request->con_puesto	
					,'con_tipo_contacto'=>$request->con_tipo_contacto				
			]
			);

		$va=ClientesTipocontacto::buscar_header($request->con_tipo_contacto);
		$contactos=ClientesContactos::buscar_contacto($id);
		foreach($contactos as $item)
		{	
			$arreglo[]=array(
					'id'=>$item->id
					,'con_nombre'=>$item->con_nombre
					,'con_direccion'=>$item->con_direccion
					,'con_telefono'=>$item->con_telefono
					,'con_movil'=>$item->con_movil	
					,'con_correo'=>$item->con_correo		
					,'con_puesto'=>$item->con_puesto	
					,'con_tipo_contacto'=>$item->con_tipo_contacto
					,'tipo_contacto'=>$va);

		}
		return response()->json($arreglo[0]);
	}
	
	
		
	public function insert_pru()
	{
		$va=ClientesTipocontacto::buscar_header(1);
		$contactos=ClientesContactos::buscar_contacto(7);
		$all_contactos=ClientesContactos::all_contactos();
		foreach($contactos as $item)
		{	
			$arreglo[]=array(
					'id'=>$item->id
					,'con_nombre'=>$item->con_nombre
					,'con_direccion'=>$item->con_direccion
					,'con_telefono'=>$item->con_telefono
					,'con_movil'=>$item->con_movil	
					,'con_correo'=>$item->con_correo		
					,'con_puesto'=>$item->con_puesto	
					,'con_tipo_contacto'=>$item->con_tipo_contacto
					,'tipo_contacto'=>$va);

		}
		dd($all_contactos);
		
	}

	public function destroy($id)
    {
		DB::table('tmk_clientes_contactos')->where('id', $id)->delete();
		$all_contactos=ClientesContactos::all_contactos();
		return response()->json($all_contactos);
    }

}
