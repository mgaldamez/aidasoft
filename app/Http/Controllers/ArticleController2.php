<?php

namespace TradeMarketing\Http\Controllers;

use Illuminate\Http\Response;
use TradeMarketing\Http\Requests;

use TradeMarketing\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

// database models
use \TradeMarketing\Article;
use \TradeMarketing\ArticleMaster;
use \TradeMarketing\Models\ArticleView;
use \TradeMarketing\ArticleBodega;

// use \TradeMarketing\Provider;
// use \TradeMarketing\Category;
// use \TradeMarketing\Pakage;
// use \TradeMarketing\Brand;
use \TradeMarketing\MeasurementUnits;
use TradeMarketing\Models\Sucursal;
use \TradeMarketing\Utility;
use \TradeMarketing\Invoice;
// use \Trademarketing\Classes;
use \TradeMarketing\Models\Variant;
use \TradeMarketing\Models\Attribute;
use \TradeMarketing\Models\AttributeValue;

// Session
use Session;
use DB;
use Auth;

class ArticleController2 extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $articles = '';

        if ($request->ajax()) {

            $array = $request->all();

            foreach ($array as $key => $value) {
                $array[$key] = trim($value);
            }

            $search = $array['search'];
            $filter = $array['filter'];


            /*
            *   select * from [tmk_articles_view]
            *   where [name] like '%Flayers%'
            *   or [description] like '%Flayers%'
            *   or category = 'Flayers'
            *   group by [id], [article_master_id], [name], [description], [image], [serial_code], [brand], [category]
            *   having [category] = 'Flayers'
            */
            if ($filter == 'Todos') {

                $articles = ArticleView::name($search)
                    ->category($search)
                    ->paginate(50);

            } else {
                $articles = ArticleView::name($search)
                    ->filterByCategory($request->get('filter'))
                    ->paginate(50);
            }

            return response()->json(view('articles.partials.articlesRender', compact('articles'))->render());
        }

        $articles = ArticleView::all();

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $brandList = \TradeMarketing\Brand::listing();
        $sucursalList = Sucursal::listing();
        $categoryList = \TradeMarketing\Category::listing();
        $providerList = \TradeMarketing\Provider::listing();
        $attributeList = Attribute::listing();
        $measurementUnitList = \TradeMarketing\MeasurementUnits::listing();

        return view('articles.create', compact('providerList', 'categoryList', 'sucursalList', 'brandList', 'measurementUnitList', 'attributeList'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ArticleStoreRequest $request)
    {
        try {

            DB::beginTransaction();

            $image = "";

            if ($request->get('image') != "") {
                $image = \TradeMarketing\Image::save($request->get('image'));
            }

            $array = $request->only(['name', 'barcode', 'brand_id', 'min_stock', 'max_stock', 'category_id', 'measurement_unit_id']);

            // Agregar la variable image dentro del array
            $array['image'] = $image;
            $array['created_by'] = \Auth::user()->id;
            $array['updated_by'] = \Auth::user()->id;


            $master = ArticleMaster::create($array);

            if ($request->get('variant')) {

                for ($i = 0; $i < count($request->get('variant')); $i++) {

                    $article = Article::create([
                        'serial_code' => $master->barcode . "-" . strval($i + 1),
                        'description' => $request->get('variant')[$i]['description'],
                        'article_master_id' => $master->id,
                        'created_by' => \Auth::user()->id,
                        'updated_by' => \Auth::user()->id
                    ]);

                    for ($j = 0; $j < count($request->get('variant')[$i]['value']); $j++) {

                        Variant::create([
                            'article_id' => $article->id,
                            'attribute_id' => $request->get('variant')[$i]['value'][$j]
                        ]);
                    }
                }
            } else {

                $article = Article::create([
                    'serial_code' => $master->barcode . '-0',
                    'description' => '',
                    'article_master_id' => $master->id,
                    'created_by' => \Auth::user()->id,
                    'updated_by' => \Auth::user()->id
                ]);
            }


            DB::commit();
            Session::flash('status', 1);
            Session::flash('message', 'Tu información ha sido registrada con éxito.');

            return redirect('/article/' . $master->id . '/master');

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();
            Session::flash('status', 2);
            Session::flash('message', $e->getMessage());

            return back()->withInput();

        } catch (Exception $e) {

            DB::rollback();
            Session::flash('status', 2);
            Session::flash('message', $e->getMessage());

            return back()->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {

            if ($request->get('data')) {

                // cargo la data en un array
                $array = $request->get('data');

                // Elimina el primer valor de array; array[0]
                unset($array[0]);

                // array
                $attr = [];

                // Extrae los valores del array y los almacena en un nuevo array
                for ($i = 1; $i <= count($array); $i++) {
                    $attr[] = $array[$i]['value'];
                }

                // Convierte el array en cadena de texto, separada por comas (,)
                // $attr = implode(', ', $attr);

                $article = \DB::table('tmk_variants')->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_variants.article_id')
                    ->select(\DB::raw('COUNT(article_id) as [count]'), 'article_id')
                    ->where('article_master_id', $request->get('data')[0]['value'])
                    ->whereIn('attribute_value_id', $attr)
                    ->groupBy('article_id')
                    ->orderBy('count', 'desc')
                    ->first();

                return response()->json($article);
            }
        }

        $article = Article::find($id);

        if (!$article) {
            return back();
        }

        $articleByWarehouses = $article->articleByWarehouses;
        $variants = $article->variants;

        $attributeValues = [];

        foreach ($variants as $key => $value) {
            $attributeValues[] = AttributeValue::findOrfail($value->attribute_value_id);
        }

        $variants = ArticleView::where('article_master_id', '=', $article->article_master_id)->get();

        return view('articles.show', compact('article', 'variants', 'attributeValues', 'articleByWarehouses'));
    }


    public function getArticles(Request $request)
    {
        if ($request->json()) {
            $articles = Article::get_articles_list();
            return response()->json($articles);
        }
    }


    public function show_article_bodegas($id)
    {
        $article_bodega = ArticleBodega::list_article_bodega($id);
        return response()->json($article_bodega);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {

        if (!Article::find($id)) {
            return back();
        }

        $article = Article::find($id);
        $variants = $article->variants;
        // dd($variants);
        $attributes = [];
        $attributeValues = [];

        foreach ($variants as $key => $value) {

            $attributeValues[] = AttributeValue::find($value->attribute_id);
        }


        foreach ($attributeValues as $key => $value) {
            $attributes[] = $value->attribute_id;
        }

        // // $attributes = Variant::where('article_id',  $article->id)->get();


        // dd($attributes);


        $brandList = \TradeMarketing\Brand::listing();
        $sucursalList = Sucursal::listing();
        $categoryList = \TradeMarketing\Category::listing();
        $providerList = \TradeMarketing\Provider::listing();
        $attributeList = Attribute::listing();
        $measurementUnitList = \TradeMarketing\MeasurementUnits::listing();


        if ($request->ajax()) {
            return response()->json(
                view('articles.partials.variantEdit',
                    compact(
                        'providerList',
                        'categoryList',
                        'sucursalList',
                        'brandList',
                        'measurementUnitList',
                        'attributeList',
                        'article',
                        'attributeValues'
                    )
                )->render());
        }

        return view('articles.edit',
            compact(
                'providerList',
                'categoryList',
                'sucursalList',
                'brandList',
                'measurementUnitList',
                'attributeList',
                'article',
                'attributeValues'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {


        $this->validate($request, [
            'description' => 'required|max:255',
            'article_id' => 'exists:tmk_articles,id',
            'article_master_id' => 'required|exists:tmk_articles,article_master_id'
        ]);

        $article = Article::find($id);

        if ($request->get('attribute') !== null) {

            $attributes = $request->get('attribute');

            $attributes = AttributeValue::filterExistAttribute($attributes);

            $exist = DB::table('tmk_variants')
                ->join('tmk_attribute_values', 'tmk_attribute_values.id', '=', 'tmk_variants.attribute_id')
                ->where('tmk_variants.article_id', $article->id)
                ->get(['tmk_attribute_values.id', 'tmk_attribute_values.attribute_id']);

            // crea un array con los articulos existentes
            $exist = array_pluck($exist, 'id', 'attribute_id');

            // obtiene la diferencia entre los attributos existentes y los agregados
            $attributes = array_diff($attributes, $exist);

            // verificar que el array sea distinto de cero
            if (count($attributes) != 0) {

                foreach ($attributes as $key => $attribute) {

                    Variant::create([
                        'attribute_id' => $attribute,
                        'article_id' => $article->id
                    ]);
                }
            }
        }


        $article->description = $request->get('description');
        $article->article_master_id = $request->get('article_master_id');
//		$article->image = $request->get('image');

        $article->active = ($request->get('active')) ? 1 : 0;

        $article->updated_by = Auth::user()->id;

        $article->save();


        return response()->json("El articulo fue actualizado...");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function getLastCostArticle(Request $request, $id)
    {

        // if( $request->ajax() ){

        $cost = Article::getLastCostByArticle($id);
        $unity = Article::getMeasurementUnitByArticle($id);
        $array = Utility::object_to_array($unity[0]);

        return response()->json(Utility::array_push_assoc($array, ['cost' => $cost]));
        // }

    }


    public function getArticlesAutocomplete()
    {
        $variable = Article::get_articles_list();

        $articles = '';
        foreach ($variable as $key => $value) {
            # code...
            $articles += $value->article_id;

        }

        dd($articles);
        return response()->json($articles);
    }

}
