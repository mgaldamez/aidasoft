<?php

namespace TradeMarketing\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Route;
use TradeMarketing\Http\Requests;
use TradeMarketing\Http\Controllers\Controller;
use TradeMarketing\Models\Article;
use TradeMarketing\Models\Order;
use TradeMarketing\Models\OrderDetail;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Warehouse;
use TradeMarketing\Traits\ResponseHandler;

class CartController extends Controller
{

    public function __construct()
    {
        $this->beforeFilter('@isAjax', ['only' => 'addItem']);
        $this->beforeFilter('@hasWarehouse', ['only' => 'addItem']);
        $this->beforeFilter('@hasOrder', ['only' => 'addItem']);
    }


    public function isAjax(Route $route, Request $request)
    {
        if (!$request->ajax()) {
            abort(404);
        }
    }


    public function hasWarehouse(Route $route, Request $request)
    {
        if (!count(currentUser()->warehouses)) {

            if ($request->ajax()) {
                $view = ResponseHandler::alertView(3,
                    'Comunicate con tu administrador para solicitar una bodega.',
                    'Necesitas tener una bodega para poder realizar pedidos!', true);
                return response()->json(['alert' => $view], 500);
            }
        }
    }


    public function hasOrder(Route $route, Request $request)
    {
        if (!$this->order = currentUser()->hasOrder()) {

            $data['warehouse_origen'] = currentUser()->warehouses()->first()->id;
            $data['warehouse_destination'] = Warehouse::where('main', 1)->first()->id;
            $data['order_num'] = config('enums.doc_references.order') . date('misy');
            $data['status_id'] = 1; // 1 = DRAFT
            $data['ordered_at'] = null;
            $data['from_user_id'] = currentUser()->id;
            $data['created_by'] = currentUser()->id;
//            $data['for_user_id'] = Warehouse::main()->first()->users->first()->id;

            $this->order = new Order();

            $this->order->fill($data);
            $this->order->save();
        }
    }


    public function addItem(Request $request)
    {
        try {

            $request = $request->only('article', 'quantity');


            if ($detail = $this->order->details()->article($request['article'])->first()) {

                $view = ResponseHandler::alertView(1, '', 'El artículo ya está en el carrito.', true);

                return response()->json(['alert' => $view], 200);
            }

            $data = $this->order->toArray();
            $data['items'] = [[
                'article' => $request['article'],
                'quantity' => $request['quantity']
            ]];

            if (!$this->order->isValid($data)) {
                throw new \Illuminate\Contracts\Validation\ValidationException($this->order->errors);
            }

            $details = [];

            foreach ($data['items'] as $key => $value) {
                array_push($details, new OrderDetail([
                    'article_id' => $value['article'],
                    'quantity' => $value['quantity'],
                    'pending' => isset($data['send_order']) ? $value['quantity'] : null,
                ]));
            }

            DB::beginTransaction();

            $this->order->details()->saveMany($details);


        } catch (\Illuminate\Contracts\Validation\ValidationException $e) {
            DB::rollback();

            Log::error($e->getMessage());

            $view = ResponseHandler::alertView(2, 'No se pudo agregar el articulo al carrito.', 'Ocurrio un error', true);
            return response()->json(['alert' => $view], 500);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollback();

            Log::info($e->getMessage());

            $view = ResponseHandler::alertView(2, 'No se pudo agregar el articulo al carrito.', 'Ocurrio un error', true);
            return response()->json(['alert' => $view], 500);

        } catch (\Exception $e) {

            DB::rollback();

            Log::warning($e->getMessage());

            $view = ResponseHandler::alertView(2, 'No se pudo agregar el articulo al carrito.', 'Ocurrio un error', true);
            return response()->json(['alert' => $view], 500);
        }

        DB::commit();

        $view = ResponseHandler::alertView(1, '', 'El artículo ha sido agregado al carrito.', true);
        $html = view('partials.article_add_to_cart',
            [
                'article' => ArticleMasterWarehousesView::warehouseMain()->where('article_id', $request['article'])->first()
            ])->render();

        return response()->json(['alert' => $view, 'html' => $html], 200);
    }
}