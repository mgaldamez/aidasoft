<?php

namespace TradeMarketing\Http\Requests;

use TradeMarketing\Http\Requests\Request;

class UserStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'            => 'required',
            'lastname'        => 'required',
            'username'        => 'required',
            'phone'           => 'required',
            'address'         => '',
            'email'           => 'required|email|unique:users',
            'password'        => 'required|min:8|confirmed',
            'password_confirmation' => 'required'
        ];
    }
}
