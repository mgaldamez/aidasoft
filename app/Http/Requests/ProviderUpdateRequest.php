<?php

namespace TradeMarketing\Http\Requests;

use TradeMarketing\Http\Requests\Request;

class ProviderUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required', 
            'address'   => 'required', 
            'email'     => 'required',
            'phone'     => 'required', 
            'company'   => 'required', 
            'type'      => 'required|in:'.implode(',', config('enums.provider_types')).''
        ];
    }
}
