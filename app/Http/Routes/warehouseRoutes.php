<?php


use \TradeMarketing\Models\Warehouse;



Route::pattern('article', '\d+');

Route::group(['middleware' => 'role:admin'], function () {

    Route::get('admin/warehouse', [
        'as' => 'admin.warehouse.index',
        'uses' => 'WarehouseController@index'
    ]);

    Route::get('admin/warehouse/create', [
        'as' => 'admin.warehouse.create',
        'uses' => 'WarehouseController@create'
    ]);

    Route::get('admin/warehouse/{warehouse}/edit', [
        'as' => 'admin.warehouse.edit',
        'uses' => 'WarehouseController@edit'
    ]);


    Route::post('admin/warehouse', [
        'as' => 'admin.warehouse.store',
        'uses' => 'WarehouseController@store'
    ]);


    Route::put('admin/warehouse/{warehouse}', [
        'as' => 'admin.warehouse.update',
        'uses' => 'WarehouseController@update'
    ]);


    Route::put('admin/warehouse/{warehouse}/active', [
        'as' => 'admin.warehouse.active',
        'uses' => 'WarehouseController@active'
    ]);
});

Route::group(['middleware' => 'role:editor'], function () {

    Route::get('admin/my/warehouse', [
        'as ' => 'admin.my.warehouse',
        'uses' => function () {

            $warehouses = currentUser()->warehouses;

            if($warehouses->count() > 1 || $warehouses->count() == 0 ){

                return view('warehouses.index', compact('warehouses'));
            }

            return redirect()->route('warehouse.show', $warehouses->first());
        }
    ]);
    Route::get('admin/my/warehouse_io', [
        'as ' => 'admin.my.warehouse_io',
        'uses' => function () {

            $warehouses = currentUser()->warehouses;

            if($warehouses->count() > 1 || $warehouses->count() == 0 ){

                return view('warehouses.index', compact('warehouses'));
            }

            return redirect()->route('warehouse.movements_io', $warehouses->first());
        }
    ]);

});


Route::get('warehouse/{warehouse}', [
    'as' => 'warehouse.show',
    'uses' => 'WarehouseController@inventory'
]);



Route::get('warehouse/{warehouse}/inventory', [
    'as' => 'warehouse.inventory',
    'uses' => 'WarehouseController@inventory'
]);


Route::get('warehouse/{warehouse}/movements', [
    'as' => 'warehouse.movements',
    'uses' => 'WarehouseController@movements'
]);
Route::get('warehouse/{warehouse}/movements_io', [
    'as' => 'warehouse.movements_io',
    'uses' => 'WarehouseController@movements_io'
]);


Route::get('warehouse/{warehouse}/transfers', [
    'as' => 'warehouse.transfers',
    'uses' => 'WarehouseController@transfers'
]);
Route::get('warehouse/{warehouse}/aprobaciones', [
    'as' => 'warehouse.aprobaciones',
    'uses' => 'WarehouseController@aprobaciones'
]);

/*
Lista de bodegas por usuario
param user
return List
*/
Route::get('bodegas/user/{id}', 'WarehouseController@show_bodegas_user');

Route::get('bodegas', 'WarehouseController@show_bodegas_actives');

Route::get('bodega_articles/{id}', 'WarehouseController@show_bodega_articles');

Route::any('bodega/stock/{id}', function ($id) {
    $bodega = Warehouse::find($id);

    $stockBodega = Warehouse::list_stock_bodega($id);
    return view('bodegas.stock', compact('stockBodega', 'bodega'));
});


Route::any('bodega/graphs/{id}', function ($id) {
    $bodega = Warehouse::Show_Bodega($id);
    $articleBodega = Warehouse::list_bodega_articles($id);
    return view('bodegas.graphs', compact('articleBodega', 'bodega'));
});