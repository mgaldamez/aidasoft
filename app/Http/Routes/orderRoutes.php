<?php

Route::pattern('order', '\d+');


Route::group(['middleware' => 'role:editor'], function () {

Route::resource('order', 'OrderController');




 Route::get('order/pendientes', [
        'as' => 'order.Pendientes.index',
        'uses' => 'OrderController@pendientes'
    ]);

 Route::get('order/cancelados', [
        'as' => 'order.Cancelados.index',
        'uses' => 'OrderController@cancelados'
    ]);

 Route::get('order/finalizado', [
        'as' => 'order.Finalizado.index',
        'uses' => 'OrderController@finalizado'
    ]);

 Route::get('order/transito', [
        'as' => 'order.Transito.index',
        'uses' => 'OrderController@transito'
    ]);















    Route::get('order/{order}/delete', [
        'as' => 'order.delete',
        'uses' => 'OrderController@destroy'
    ]);


    Route::put('order/{order}/send', [
        'as' => 'order.send',
        'uses' => 'OrderController@send'
    ]);


    Route::get('order/{order}/cancel', [
        'as' => 'order.cancel',
        'uses' => 'OrderController@cancel'
    ]);


    Route::get('order/{order}/transfers', [
        'as' => 'order.transfers',
        'uses' => 'OrderController@transfers'
    ]);

    Route::get('order/inbox', [
        'as' => 'order.inbox',
        'uses' => function () {

            $collection = collect();

            foreach (currentUser()->warehouses as $key => $warehouse) {
            
                $collection = $collection->merge($warehouse->ordersFor);
            }

            $orders = $collection->filter(function ($item) {
                return $item['ordered_at'] != null;
            });


            $orders = $orders->sortByDesc('ordered_at');



            return view('orders.inbox', compact('orders'));
        }
    ]);


    Route::get('order/drafts', [
        'as' => 'order.drafts',
        'uses' => function () {

            $orders = \TradeMarketing\Models\Warehouse::orders()->filter(function ($item) {
                return $item['status_id'] == 1;
            });

            return view('orders.drafts', ['orders' => $orders->sortByDesc('created_at')]);
        }
    ]);


    Route::get('order/sent', [
        'as' => 'order.sent',
        'uses' => function () {

            $collection = collect();

            foreach (currentUser()->warehouses as $key => $warehouse) {
                $collection = $collection->merge($warehouse->ordersFrom);
            }


            $orders = $collection->filter(function ($item) {
                return $item['ordered_at'] != null;
            });


            $orders = $orders->sortByDesc('ordered_at');

            return view('orders.sent', compact('orders'));
        }
    ]);


    Route::get('orders/for/approve', [
        'as' => 'orders.for.approve',
        'uses' => function () {

            $orders = currentUser()->ordersIn()->forApprove()->get();

            return view('orders.for_approve', compact('orders'));
        }
    ]);

});


Route::get('order/{order}/warehouse/{warehouse}/details', [
    'as' => 'order.warehouse.details',
    'uses' => function ($order, $warehouse) {

        $order = \TradeMarketing\Models\Order::findOrFail($order);

        $transfer = new \TradeMarketing\Models\Transfer([
            'order_id' => $order->id,
            'bodega_id' => $warehouse,
        ]);

        $details = [];
        foreach ($order->details as $detail) {
            array_push($details, new \TradeMarketing\Models\TransferDetails([
                'article_id' => $detail->article_id,
                'quantity' => $detail->quantity,
            ]));
        }

        $transfer->details = $details;

        return view('transfers.partials.table_row', compact('transfer'))->render();
    }
]);


Route::post('add/article/cart', [
    'as' => 'add.article.cart',
    'uses' => 'CartController@addItem'
]);


Route::get('order/cart/refresh', [
    'as' => 'order.cart.refresh',
    'uses' => function () {
        return response()->json(['cart' => view('partials.order_cart')->render()]);
    }
]);


