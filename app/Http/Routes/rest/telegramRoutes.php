
<?php



Route::get('/micrositio/get-me','TelegramController@getMe');
Route::get('/micrositio/set-hook', 'TelegramController@setWebHook');
Route::post(env('TELEGRAM_BOT_TOKEN') . '/webhook', 'TelegramController@handleRequest');
Route::post('/prueba', 'TelegramController@prueba');
Route::get('/micrositio/xm','TelegramController@xm');
Route::get('/micrositio/master','TelegramController@masterOpPrueba');
Route::get('/micrositio/getprueba','TelegramController@getprueba');

Route::get('/micrositio/ws/ml','ApiController@ml');
Route::get('/micrositio/ws/root','ApiController@root');

Route::get('/crontab','TelegramController@crontab');

Route::get('/micrositio/ws/pru','ApiController@pru');

Route::post('/micrositio/ws/inv/','ApiController@inv');
Route::get('/micrositio/ws/inv','ApiController@inv');
Route::get('/micrositio/ws/inventario','ApiController@inventario');








