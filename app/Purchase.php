<?php

namespace TradeMarketing;

use Illuminate\Database\Eloquent\Model;

use DB;

class Purchase extends Model
{
    protected $table = 'tmk_purchases';

	protected $fillable = [ 'id', 'provider_id', 'purchase_order', 'date', 'subtotal', 'tax', 'discount', 'total', 'order_purchase_id', 'user_id'];


	public static function insert_purchase_details( $purchase, $article, $quantity, $unit_cost, $tax){
		$last_detail = Purchase::last_purchase_detail();
		$id =  (int)$last_detail[0]->id + 1;

		// dd( $unit_cost);
		DB::table('tmk_purchase_details')->insert(array(
			'id' 		 => $id, 
			'purchase_id'=> $purchase,
			'article_id' => $article,
			'quantity'   => $quantity,
			'unit_cost'  => $unit_cost,
			'tax' 		 => $tax,
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
			));
		return $id;
	}


	public static function insert_purchase_article_bodega($article, $purchase){
		DB::table('tmk_purchase_articles')->insert( array(
			'article_id' => $article,
			'purchase_id' => $purchase,
			// 'created_at' => date("Y-m-d H:i:s"),
			// 'updated_at' => date("Y-m-d H:i:s")
			));
	}


	public static function last_purchase_detail(){
		return  DB::table('tmk_purchase_details')
		->orderBy('id', 'desc')
		->take(1)
		->get();
	}


    public static function get_purchase_by_id( $id ){
    	return DB::table('tmk_purchase_details_view')
    	->where('purchase_id', '=', $id)
    	->get();
    }

    /*
    SELECT 
	tmk_articles.id AS article_id , 
	tmk_articles.name, 
	tmk_articles.barcode, 
	tmk_purchase_details.quantity, 
	tmk_purchase_details.unit_cost, 
	tmk_purchase_details.tax,
	( ROUND( tmk_purchase_details.unit_cost * tmk_purchase_details.quantity, 2) ) AS amount
	FROM tmk_purchase_details 
	INNER JOIN tmk_purchase_details ON tmk_purchase_details.id =  tmk_purchase_details.article_id
	INNER JOIN tmk_articles ON tmk_articles.id = tmk_purchase_details.article_id
	WHERE tmk_purchase_details.purchase_id = 1
	*/
    public static function list_articles_by_purchase( $id ){
    	return DB::table('tmk_purchase_details')
		->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_purchase_details.article_id')
		->where('tmk_purchase_details.purchase_id', '=', $id )
		->select(
			'tmk_articles.id as article_id',
			'tmk_articles.name',
			'tmk_articles.barcode',
			'tmk_purchase_details.purchase_id',
			'tmk_purchase_details.quantity',
			'tmk_purchase_details.unit_cost',
			'tmk_purchase_details.tax',
			DB::raw(' CAST(tmk_purchase_details.unit_cost * tmk_purchase_details.quantity AS decimal (10,2)) AS amount') 
			)
		->get();
    }
	public static function buscar_series($id)
    {
		
         return DB::table('tmk_purchase_series')
            ->where('num_doc_temp',$id)
            ->get();
	}
	public static function buscar_series_num_doc($id,$num,$num_item)
    {
		
         return DB::table('tmk_purchase_series')
			->where('num_doc_temp',$num)
			->where('internal_reference',$id)
			->where('article_id',$num_item)
            ->first();
	}
	public static function buscar_series_edit($id,$num,$num_item)
    {
		
         return DB::table('tmk_purchase_series')
			->where('num_doc_temp',$num)
			->where('internal_reference',$id)
			->where('article_id',$num_item)
            ->get();
	}
	public static function buscar_series_num_doc_out($id,$num,$num_item)
    {
		
         return DB::table('tmk_purchase_series_out')
			->where('num_doc_temp',$num)
			->where('internal_reference',$id)
			->where('article_id',$num_item)
            ->first();
	}
	public static function buscar_series_edit_out($id,$num,$article)
    {
		
         return DB::table('tmk_series_all_view')
			->where('internal_reference',$id)
			->where('status_in',1)
			->where('num_doc_temp_out',null)
			->orwhere('num_doc_temp_out',$num)
			->where('article_id',$article)
			->orwhere('article_id',null)
            ->get();
    }
	//
	public static function buscar_lotes_num_doc($id,$num,$num_item)
    {
		
         return DB::table('tmk_lotes_out')
			->where('num_doc_temp',$num)
			->where('internal_reference',$id)
			->where('article_id',$num_item)
            ->first();
	}
	public static function buscar_lotes_edit($id,$num,$num_item)
    {
		
         return DB::table('tmk_lotes')
			->where('num_doc_temp',$num)
			->where('internal_reference',$id)
			->where('article_id',$num_item)
            ->get();
	}
	
	
	public static function buscarlotes_out($id,$num,$article)
    {
		
        return DB::table('tmk_lotes_create_view')
			->where('internal_reference',$id)
			->where('status_in',1)
			->where('cantidad_disponible','>',0)
			->orwhere('article_id',null)
            ->get();
	}
	public static function buscarlotes_create($id,$num,$article)
    {
		
         return DB::table('tmk_lotes_create_view')
			->where('internal_reference',$id)
			->where('status_in',1)
			->where('cantidad_disponible','>',0)
			->orwhere('article_id',null)
			->orderBy('id', 'asc')
            ->get();
	}
	public static function buscarlotes_edit($id,$num,$article)
    {
		
         return DB::select('select * from (select *,null cantidad_out from tmk_lotes_create_view
		 where id not in(
		 select item_id from tmk_lotes_out where num_doc_temp=?)
		 union all
		 select a.*,b.cantidad AS `cantidad_out` from tmk_lotes_create_view a
		 inner join
		 tmk_lotes_out b on a.id=b.item_id
		 where b.num_doc_temp=?) as c
		 order by c.id',array($num,$num));
	}

}
