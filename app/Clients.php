<?php

namespace TradeMarketing;

use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Traits\ImageHandler;
use Validator;
use DB;
class Clients extends Model
{
     protected $table = "tmk_clientes_empresas";


    protected $fillable = [
		'id_tipo_cliente','Nombre','direccion1','direccion2','id_pais','ciudad','estado','codigo_postal','Ruc','telefono','movil','email','titulo','pagina_web','etiquetas','cliente','proveedor'
    ];
	
	 public function isValid($data)
    {
		 $rules = 
		 [
		 'id_tipo_cliente'=> 'required|max:100|unique:tmk_tipo_clientes',
		 'Nombre' => 'min:5|max:255',
		 ];
		    // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['id_tipo_cliente'] .= ',id_tipo_cliente,' . $this->id;
            $rules['Nombre'] .= ',Nombre,' . $this->id;
        }
		 $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
		  if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
	}
	 public static function listing()
    {
        return static::orderBy('Nombre')->lists('Nombre', 'id');
    }
    public static function lista_proveedores()
    {
        return static::orderBy('Nombre')->where('proveedor',1)->lists('Nombre', 'id');
    }
	 public static function buscar_cliente($id)
    {
         return DB::table('tmk_clientes_empresas')
            ->where('id',$id)
            ->get();
    }
}
