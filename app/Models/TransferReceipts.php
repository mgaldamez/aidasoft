<?php
namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use TradeMarketing\ArticleBodega;


class TransferReceipts extends Model {


    protected $table = "tmk_transfer_receipts";

    protected $fillable = ['receipt_number', 'receipt_file', 'transfer_id', 'received_by', 'updated_by', 'deleted_by'];


    public function details(){
        return $this->hasMany(TransferReceiptDetail::class);
    }



    public function articleWarehouseMovements(){
        return $this->belongsToMany(ArticleBodega::class, 'tmk_transfer_receipt_article_bodegas', 'transfer_receipt_id');
    }
}