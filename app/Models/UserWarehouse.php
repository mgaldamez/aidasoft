<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\User;

class UserWarehouse extends Model
{

    use SoftDeletes;

    protected $table = 'tmk_user_warehouses';

    protected $fillable = ['user_id', 'warehouse_id'];



    public function User(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userWarehouse(){
        return $this->belongsTo(Warehouse::class);
    }
}
