<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use PhpSpec\Exception\Exception;


class AttributeValue extends Model
{
    protected $table = "tmk_attribute_values";
    protected $fillable = ['attribute_id', 'description', 'value', 'created_by', 'updated_by'];


    public static function listing($attribute = '')
    {

        if ($attribute != '') {
            if (is_numeric($attribute)) {

                if (static::find($attribute)) {
                    return static::where('attribute_id', $attribute)->lists('value', 'id');
                }

                return [];
            }

            return [];
        }

        return static::all()->lists('description', 'id');
    }



    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }



    public function articles(){
        return $this->belongsToMany(Article::class, 'tmk_variants');
    }


    /**
     * @param array $array
     * @param bool $isNumeric
     * @return array
     */
    public static function filterArray($array = [], $isNumeric = true)
    {

        if(is_null($array)){
            return [];
        }

        $array = ($isNumeric == true)
            ? array_filter($array, function ($value) {
                return is_numeric($value);
            })
            : array_filter($array, function ($value) {
                return !is_numeric($value);
            });


        $array = array_unique($array);

        array_walk($array, 'trim');

        if($isNumeric != true){
            $array = array_filter($array, function ($value) {
                return strlen($value) >= 2;
            });
        }

        return $array;
    }



    /**
     * @param array $array
     * @return array
     */
    public static function filterNewValues($array = [])
    {

        $array = static::filterArray($array, false);

        $array = static::strTransform($array, 'strtolower');

        $exist = static::whereIn('value', $array)->lists('value')->toArray();

        $exist = static::strTransform($exist, 'strtolower');

        return array_diff($array, $exist);
    }


    /**
     * @param $attribute
     * @param $array
     */
    public static function addNewValues($attribute, $array)
    {

        $values = static::filterNewValues($array['value']);

        if(isset($array['description'])){

            $diff = array_diff_key(static::filterArray($array['description'], false), $values);

            foreach ($diff as $key => $value) {
                unset($array['description'][$key]);
            }
        }


        foreach ($values as $key => $value) {
            static::create([
                'attribute_id' => $attribute,
                'description' => (isset($array['description'])) ? $array['description'][$key] : $value,
                'value' => $value,
                'created_by' => auth()->user()->id,
                'updated_by' => auth()->user()->id]);
        }
    }



    /**
     * Transform String to Upper or lower
     *
     * @param $array
     * @param $function
     * @return mixed
     */
    public static function strTransform($array, $function)
    {
        foreach ($array as $key => $valor) {
            $array[$key] = $function($valor);
        }

        return $array;
    }


    /**
     * @param $array
     * @param bool $isNumeric
     * @return mixed
     */
    public static function filterExistAttribute($array, $isNumeric = true)
    {

        $attribute_id = static::filterArray($array['id']);

        $attribute_value_id = static::filterArray($array['value_id']);

        $diff = [];

        // verifica cual array deve ir en la primera entrada de la funcion
        if(count($attribute_id) >= count($attribute_value_id)){
            $diff = array_diff_key($attribute_id, $attribute_value_id);
            // comprueba si el array contienen algun valor
            // y elimina del array los valores relacionados
            if($diff){
                foreach ($diff as $key => $value) {
                    unset($attribute_id[$key]);
                }
            }

        } else {
            $diff = array_diff_key($attribute_value_id, $attribute_id);
            // comprueba si el array contienen algun valor
            // y elimina del array los valores relacionados
            if($diff){
                foreach ($diff as $key => $value) {
                    unset($attribute_value_id[$key]);
                }
            }
        }

        return static::whereIn('id', $attribute_value_id)->whereIn('attribute_id', $attribute_id)->lists('id', 'attribute_id')->toArray();
    }
}
