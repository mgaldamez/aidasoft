<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'tmk_roles';
    protected $fillable = ['id', 'description', 'read', 'write', 'modify'];


    public static function listing(){

        $roles = static::orderby('description')->lists('description', 'id');

        $roles->transform(function ($item, $key) {
            return trans('auth.roles.' . $item );
        });

        return $roles;
    }
}
