<?php
namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Brand extends Model {


    protected $table = 'tmk_brands';

    protected $fillable = ['description'];

    public static function listing(){
        return static::where('description', '<>', 'Desconocido')->orderBy('description')->lists('description', 'id');
    }


    public static function Show_Brands_Actives(){
        return DB::table('tmk_brands')
        ->select('tmk_brands.id','tmk_brands.description as brand')
        ->where('tmk_brands.active','=','1')
        ->orderBy('brand')
        ->get();
    }

    public static function Get_Brands_Provider($id){
    	return DB::table('tmk_brands')
        ->join('tmk_provider_brands', 'tmk_provider_brands.brand_id', '=', 'tmk_brands.id' )
        ->join('tmk_providers', 'tmk_provider_brands.provider_id', '=', 'tmk_providers.id')
        ->select('tmk_provider_brands.id', DB::raw("CONCAT(tmk_brands.description, ' - ',tmk_provider_brands.company) as description") )
        ->where('tmk_providers.id','=', $id)
        ->where('tmk_brands.active','=','1')
        ->orderBy('description')
        ->get();
    }

    /* 
    SELECT 
    tmk_providers.id 'provider_id', 
    CONCAT(tmk_providers.name,' ',tmk_providers.lastname) 'provider', 
    tmk_brands.description 'brand'
    --tmk_provider_brands.id 'brand_id', 
    --tmk_provider_brands.company
    FROM tmk_providers 
    INNER JOIN tmk_provider_brands ON tmk_providers.id =  tmk_provider_brands.provider_id
    INNER JOIN tmk_brands ON tmk_brands.id =  tmk_provider_brands.brand_id 
    */
    public static function list_brand_to_provider(){
        return DB::table('tmk_providers')
        ->join('tmk_provider_brands', 'tmk_provider_brands.provider_id', '=', 'tmk_providers.id' )
        ->join('tmk_brands', 'tmk_provider_brands.brand_id', '=', 'tmk_brands.id')
        ->select(
            'tmk_providers.id as provider_id', 
            DB::raw("CONCAT(tmk_providers.name, ' - ',tmk_providers.lastname) as provider"), 
            'tmk_brands.description as brand'
            // 'tmk_provider_brands.id as brand_id', 
            // 'tmk_provider_brands.company'
            )
        ->get();   
    }

    public static function insert_provider_brand_article($article, $providerBrand){
        DB::table('tmk_provider_brand_articles')->insert(array(
            'article_id'        => $article,
            'provider_brand_id' => $providerBrand,
            'created_at'        => date("Y-m-d H:i:s"),
            'updated_at'        => date("Y-m-d H:i:s"),
            ));
    }
}
