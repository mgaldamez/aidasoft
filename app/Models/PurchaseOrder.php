<?php

namespace TradeMarketing\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Provider;
use TradeMarketing\Traits\FormatDates;
use TradeMarketing\User;

class PurchaseOrder extends Model
{

    use SoftDeletes, FormatDates;

    protected $table = 'tmk_purchase_orders';
    protected $fillable = ['order', 'require_date', 'warehouse_id', 'provider_id', 'observation', 'order_status_id', 'created_by', 'updated_by', 'deleted_by', 'confirmed_at', 'canceled_at','num_doc_temp'];
    protected $date = ['require_date', 'confirmed_at', 'canceled_at'];
    protected $dates = ['deleted_at'];


    public function isValid($data)
    {
        $rules = [
            'order' => 'unique:tmk_purchase_orders',
            'provider_id' => 'id|exists:tmk_clientes_empresas,id',
            'require_date' => 'date_format:"Y-m-d"|after:' . Carbon::yesterday()->format('Y-m-d') . '|date_format:Y-m-d',
            'warehouse_id' => 'id|exists:tmk_bodegas,id',
            'items' => 'required|min:1',
            'observation' => 'max:255',
            'created_by' => 'id|exists:tmk_users,id',
            'updated_by' => 'id|exists:tmk_users,id',
            'created_by_id' => 'id|exists:tmk_users,id',
            'num_doc_temp' => 'required|min:1',
        ];


        $data['items'] = $data['items'] ? $data['items'] : [];

        foreach ($data['items'] as $key => $value) {
            $rules['items.' . $key . '.article'] = 'required|id|exists:tmk_articles,id';
            $rules['items.' . $key . '.quantity'] = 'required|numeric|min:1';
            $rules['items.' . $key . '.unit_cost'] = 'required:|numeric|min:0';
        }
       

        $validator = Validator::make($data, $rules);


        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();
        return false;
    }



    public function orderState($stateRequired)
    {
        $status = OrderStatus::statusHierarchy();

        return $status[$this->status] < $status[$stateRequired];
    }



    public static function getOrderPurchaseByOrder($order)
    {
        return DB::table('tmk_purchase_orders')
            ->where('order', '=', $order)
            ->get();
    }

    public static function getLastOrderPurchase()
    {
        return DB::table('tmk_purchase_orders')
            ->orderBy('id', 'desc')
            ->first();
    }


    public function getTotalAttribute()
    {
        return DB::table('tmk_purchase_order_details')
            ->select(DB::raw('SUM(amount) as amount'))
            ->where('order_id', $this->id)
            ->whereNull('deleted_at')
            ->first()->amount;
    }


    public function provider()
    {
        return $this->hasOne(Provider::class, 'id', 'provider_id');
    }


    public function sucursal()
    {
        return $this->hasOne(Sucursal::class, 'id_sucursal', 'require_place_id');
    }


    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }


    public function details()
    {
        return $this->hasMany(PurchaseOrderDetail::class, 'order_id');
    }


    public function articles()
    {
        return $this->belongsToMany(Article::class, 'tmk_purchase_order_details', 'order_id');
    }



//    public function status()
//    {
//        return $this->belongsTo(OrderStatus::class, 'order_status_id')->first()->description;
//    }

    public function getStatusAttribute()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id')->first()->description;
    }


    public function articleWarehouseMovements()
    {
        return $this->morphMany(ArticleBodega::class, 'movementable');
    }


    public function purchaseReceipts()
    {
			
        return $this->hasMany(PurchaseReceipt::class);
    }


    public function isDraft()
    {
        return $this->status == 'draft';
    }

}
