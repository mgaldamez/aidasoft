<?php namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Aprobar;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\ArticleMaster;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\Views\MovementsView;
use TradeMarketing\Models\Views\TransferView;
use TradeMarketing\User;
use TradeMarketing\Models\UserWarehouse;
class Warehouse extends Model
{
    use SoftDeletes;

    protected $table = 'tmk_bodegas';

    protected $fillable = ['description', 'colour', 'sucursal_id', 'main', 'active'];


    public function isValid($data)
    {
        $rules = [
            'description' => 'required|min:2|max:100|unique:tmk_bodegas',
            'sucursal_id' => 'required|exists:tbl_sucursales,ID_SUCURSAL',
            'users' => '',
        ];


        if ($this->exists) {
            $rules['description'] .= ',description,' . $this->id;
        }


        $messages = [];

        if ($data['users']) {

            foreach ($data['users'] as $key => $user) {
                $rules['users.' . $key . ''] = 'exists:tmk_users,id,deleted_at,NULL';
                $messages['users.' . $key . '.exists'] = 'El usuario no es valido.';
            }
        }


        $validator = Validator::make($data, $rules, $messages);

        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();
        return false;
    }


    /**
     * @param $data => users array
     * @return array
     */
    public function attachUsers($data)
    {
        // Compara los usuarios ingresados con los usuarios activos relacionados a la bodega
        // y retorna un array con los usuarios nuevos
        $usersDiff = array_diff($data, $this->usersActive->lists('id')->toArray());
		

        // Elimina los usuarios activos que no fueron agregados como parametros

        if (isset($usersDiff)) {

            $users = [];

            foreach ($usersDiff as $user) {
                array_push($users, new UserWarehouse(['user_id' => (int)$user
                ]));
            }
			
			       // $this->userWarehouses()->whereNotIn('user_id', $data)->delete(['deleted_by' => auth()->user()->id]);

            return $users;
        }

        return [];
    }


    public static function listing()
    {
        if (currentUser()->isAdmin()) {
            return static::active()->orderBy('description')->lists('description', 'id');
        }

        return currentUser()->warehouses()->active()->orderBy('description')->lists('description', 'id');
    }


    public function articleListing()
    {
        $articles = [];

        foreach ($this->articles as $article) {
            $articles[$article->article_id] =ArticleView::findOrFail($article->article_id)->description;


        }

        return $articles;
    }
    public function articleListing_()
    {
        $articles = [];

        foreach ($this->articles as $article) {
            $articles[$article->article_id] = ArticleView::findOrFail($article->article_id)->description;
        }
dd($articles);
        return $articles;
    }


    public function inventory()
    {
        return $this->hasMany(ArticleMasterWarehousesView::class, 'warehouse_id');
    }


    public function transfers()
    {
        return Transfer::where('bodega_id', $this->id)->orWhere('bodega_id_end', $this->id)->get();
    }
  public function aproba()
    {
        return Aprobar::where('warehouse_id', $this->id)->where('status_id',2)->where('movement','exit')->get();
    }


    public function transfersViewAll()
    {
        return TransferView::where('transfer_warehouse_destination', $this->description)
            ->orWhere('transfer_warehouse_origen', $this->description)
            ->orWhere('created_by', auth()->user()->fullName)
            ->get();
    }


    public function transfersViewExit()
    {
        return TransferView::where('transfer_warehouse_destination', $this->description)
            ->orWhere('transfer_warehouse_origen', $this->description)->get();
    }


    /**
     * Eloquent: Relationships
     *
     *
     * Las tablas de base de datos suelen estar relacionadas entre sí.
     * Por ejemplo, una entrada de blog puede tener muchos comentarios o un pedido podría estar relacionado
     * con el usuario que lo colocó.
     * Eloquent facilita la gestión y el trabajo con estas relaciones y apoya varios tipos diferentes de relaciones:
     *
     * https://laravel.com/docs/5.1/eloquent-relationships
     */

    public function articleMasters()
    {
        return $this->belongsToMany(ArticleMaster::class, 'tmk_article_master_warehouses', 'warehouse_id');
    }


    public function articleMasterWarehousesView()
    {
        return $this->hasMany(ArticleMasterWarehousesView::class);
    }


    public function articles()
    {
        return $this->hasMany(ArticleMasterWarehousesView::class, 'warehouse_id');
    }

    public function movements()
    {
        return $this->hasMany(MovementsView::class, 'warehouse_id');
    }


    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'sucursal_id', 'ID_SUCURSAL');
    }


    public function transfersViewEntry()
    {
        return $this->hasMany(TransferView::class, 'transfer_warehouse_destination', 'description');
    }


    public function users()
    {
        return $this->belongsToMany(User::class, 'tmk_user_warehouses');
    }


    public function usersActive()
    {
        return $this->belongsToMany(User::class, 'tmk_user_warehouses')->whereNull('tmk_user_warehouses.deleted_at');
    }


    public function userWarehouses()
    {
        return $this->hasMany(UserWarehouse::class, 'warehouse_id');
    }


    public function ordersFrom()
    {
        return $this->hasMany(Order::class, 'warehouse_origen');
    }


    public function ordersFor()
    {
        return $this->hasMany(Order::class, 'warehouse_destination');
    }


    public static function orders()
    {
        $collection = collect();

        foreach (currentUser()->warehouses as $key => $warehouse) {
            $collection = $collection->merge($warehouse->ordersFor);
            $collection = $collection->merge($warehouse->ordersFrom);
        }

        return $collection->unique();
    }




//return Order::where('warehouse_destination', $this->id)->orWhere('warehouse_origen', $this->id)->get();

    /**
     * Eloquent: Mutators
     *
     *
     * Los accesores y mutadores le permiten dar formato a valores de atributos de Eloquent cuando los recupera
     * o los configura en instancias de modelo.
     * Por ejemplo, puede utilizar el cifrador de Laravel para cifrar un valor mientras se almacena en la base de datos y,
     * a continuación, descifrar automáticamente el atributo cuando se accede a él en un modelo Eloquent.
     *
     * https://laravel.com/docs/5.1/eloquent-mutators
     */

    public function getSucursalNameAttribute()
    {
        return $this->belongsTo(Sucursal::class, 'sucursal_id', 'ID_SUCURSAL')->first()->DESCR_SUCURSAL;
    }


    public function getRegionNameAttribute()
    {
        return $this->sucursal->belongsTo(Region::class, 'ID_REGION')->first()->DESCR_REGION;
    }


    public static function mainId()
    {
        return Warehouse::main()->firstOrFail()->id;
    }


    /**
     * Eloquent: Local Scopes Queries
     *
     *
     * Los ámbitos locales le permiten definir conjuntos comunes de restricciones
     * que puede reutilizar fácilmente en toda la aplicación.
     *
     * https://laravel.com/docs/5.1/eloquent#query-scopes
     */

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeMain($query)
    {
        return $query->where('main', 1);
    }


    /**
     * SELECT bodegas.id, bodegas.description as 'bodega', bodegas.colour, SUM (tmk_article_bodegas.quantity) AS 'cant'
     * FROM bodegas
     * INNER JOIN  tmk_article_bodegas ON  tmk_article_bodegas.bodega_id = bodegas.id
     * WHERE bodegas.active = 1
     * GROUP BY  bodegas.id, bodegas.description , bodegas.colour
     * ORDER BY bodegas.description
     */
    public static function Show_Bodegas_Actives()
    {
        return DB::table('tmk_bodegas')
            ->join('tmk_article_bodegas', 'tmk_article_bodegas.bodega_id', '=', 'tmk_bodegas.id')
            ->select('tmk_bodegas.id', 'tmk_bodegas.description as bodega', 'tmk_bodegas.colour',
                DB::raw('SUM(tmk_article_bodegas.quantity) as cant'))
            // ->where('tmk_bodegas.active','=','1')
            ->groupBy('tmk_bodegas.id', 'tmk_bodegas.description', 'tmk_bodegas.colour')
            ->orderBy('bodega')
            ->get();
    }


    /**
     * SELECT bodegas.description as 'bodega', TBL_SUCURSALES.DESCR_SUCURSAL as 'center', TBL_REGIONES.DESCR_REGION as 'region', bodegas.active
     * FROM bodegas
     * INNER JOIN TBL_SUCURSALES ON TBL_SUCURSALES.ID_SUCURSAL = bodegas.sucursal_id
     * INNER JOIN TBL_REGIONES ON TBL_REGIONES.ID_REGION = TBL_SUCURSALES.ID_REGION
     * WHERE bodegas.id = $id
     */
    public static function Show_Bodega($id)
    {
        return DB::table('tmk_bodegas')
            ->join('TBL_SUCURSALES', 'TBL_SUCURSALES.ID_SUCURSAL', '=', 'tmk_bodegas.sucursal_id')
            ->join('TBL_REGIONES', 'TBL_REGIONES.ID_REGION', '=', 'TBL_SUCURSALES.ID_REGION')
            ->select('tmk_bodegas.id', 'tmk_bodegas.description as bodega', 'TBL_SUCURSALES.DESCR_SUCURSAL as center', 'TBL_REGIONES.DESCR_REGION as region')
            ->where('tmk_bodegas.id', '=', $id)
            // ->where('tmk_bodegas.user_id', '=', Auth::user()->id)
            ->get();
    }


    public static function list_bodega_articles($id)
    {
        return DB::table('tmk_bodegas')
            ->join('tmk_article_bodegas', 'tmk_article_bodegas.bodega_id', '=', 'tmk_bodegas.id')
            ->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_article_bodegas.article_id')
            ->join('tmk_article_masters', 'tmk_article_masters.id', '=', 'tmk_articles.article_id')
            ->leftjoin('tmk_purchase_article_bodegas', 'tmk_purchase_article_bodegas.article_bodega_id', '=', 'tmk_article_bodegas.id')
            ->leftjoin('tmk_purchase_order_details', 'tmk_purchase_order_details.id', '=', 'tmk_purchase_article_bodegas.purchase_detail_id')
            ->leftjoin('tmk_purchase_orders', 'tmk_purchase_orders.id', '=', 'tmk_purchase_order_details.order_id')
            ->select(
                'tmk_article_bodegas.article_id',
                'tmk_article_masters.barcode',
                'tmk_articles.name',
                'tmk_articles.image',
                'tmk_purchase_order_details.unit_cost',
                'tmk_article_bodegas.active',
                'tmk_article_bodegas.created_at',
                'tmk_article_masters.min_stock',
                'tmk_article_bodegas.quantity',
                'tmk_bodegas.colour',
                'tmk_bodegas.description as bodega',
                DB::raw('tmk_purchase_order_details.unit_cost * tmk_article_bodegas.quantity as amount')
            )
            ->where('tmk_bodegas.id', '=', $id)
            ->get();
    }


//    public static function list_movements_bodega($id)
//    {
//        return DB::table('tmk_movements_view')
//            ->where('bodega_id', '=', $id)
//            ->select('tmk_movements_view.*',
//                DB::raw('unit_cost * quantity as amount')
//            )
//            ->get();
//    }

    /*
    SELECT * 
    FROM tmk_inventory_view
    WHERE bodega_id = { id }
    */
    public static function list_stock_bodega($id)
    {
        return DB::table('tmk_inventory_view')
            ->join('tmk_article_masters', 'tmk_article_masters.id', '=', 'tmk_inventory_view.article_master_id')
            ->where('bodega_id', '=', $id)
            ->select('article_id', 'barcode', 'name as article', 'entry', 'exit', 'tmk_inventory_view.min_stock', 'tmk_inventory_view.max_stock', 'tmk_inventory_view.stock')
            ->get();
        // dd( $res );
    }


    /*
    SELECT tmk_bodegas.id, tmk_bodegas.description FROM tmk_bodegas 
    INNER JOIN tmk_user_bodegas ON tmk_user_bodegas.bodega_id = tmk_bodegas.id
    INNER JOIN tmk_users ON tmk_users.id = tmk_user_bodegas.user_id
    WHERE tmk_users.agent_id =  {agent_id}
    */
    public static function list_bodegas_user($id)
    {
        return DB::table('tmk_bodegas')
            ->join('tmk_user_bodegas', 'tmk_user_bodegas.bodega_id', '=', 'tmk_bodegas.id')
            ->join('tmk_users', 'tmk_users.id', '=', 'tmk_user_bodegas.user_id')
            ->select(
                'tmk_bodegas.id',
                'tmk_bodegas.description'
            )
            ->where('tmk_users.id', '=', $id)
            ->get();
    }

}