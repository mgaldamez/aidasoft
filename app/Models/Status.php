<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

    protected $table = 'tmk_order_status';
    protected $fillable = [];

}