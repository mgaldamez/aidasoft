<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Support\Facades\DB;
use TradeMarketing\ArticleMaster;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\Views\ArticleBodegaView;

class Article extends Model
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tmk_articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'active', 'barcode', 'internal_reference', 'unit_cost_initial', 'reference_code', 'image',
        'article_master_id', 'created_by', 'updated_by'
    ];

    protected $dates = ['deleted_at'];


    public static function listing()
    {
        return static::orderBy('description')->lists('description', 'id');
    }


    public function master()
    {
        return $this->hasOne(ArticleMaster::class, 'id', 'article_master_id');
    }


    public function variants()
    {
        return $this->hasMany(Variant::class);
    }


    public function attributeValues()
    {
        return $this->belongsToMany(AttributeValue::class, 'tmk_variants');
    }


    public function articleByWarehouses()
    {
        return $this->hasMany(ArticleBodegaView::class);
    }


    public function warehouses()
    {
        return $this->hasMany(ArticleBodegaView::class);
    }


    public function view()
    {
        return $this->hasOne(ArticleView::class, 'id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articleWarehouse()
    {
        return $this->hasMany(ArticleWarehouse::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function articleMasterWarehouse()
    {
        return $this->hasManyThrough(ArticleMasterWarehouse::class, ArticleWarehouse::class, 'article_id', 'id');
    }




    public function availableForOrders(){
        return ArticleMasterWarehousesView::where('article_id', $this->id)->warehouseMain()->first()->availableForOrders();
    }


    /**
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeName($query, $name)
    {
        return $query->where('description', $name);
    }


    public function getAvailableQuantityAttribute()
    {
        return ArticleMasterWarehousesView::where('article_id', $this->id)->get();
    }


    public function getBrandAttribute(){
        return $this->master->brand->description;
    }


    public function getImageAttribute(){
        return $this->master->image;
    }


    public function availableByWarehouse($warehouse = null )
    {
        if($warehouse == null){
            $warehouse = Warehouse::mainId();
        }

        if($article = ArticleMasterWarehousesView::where('article_id', $this->id)
            ->where('warehouse_id', $warehouse)
            ->first()){
            return $article->available_quantity;
        }
        return 0;

    }


    public function availableToTransfer($quantity, $warehouse)
    {
        $available = $this->availableByWarehouse($warehouse);

        if ($quantity <= $available) {
            return $quantity;
        }

        return $available;
    }



    /**
     * @return mixed
     */
    public function getMeasurementUnitAttribute()
    {
        return DB::table('tmk_articles')
            ->join('tmk_article_masters', 'tmk_article_masters.id', '=', 'tmk_articles.article_master_id')
            ->join('tmk_measurement_units', 'tmk_measurement_units.id', '=', 'tmk_article_masters.measurement_unit_id')
            ->select('tmk_measurement_units.*')
            ->where('tmk_articles.id', $this->id)
            ->first()->description;
    }


    public function getDescriptionAttribute(){
        return ArticleView::findOrFail($this->id)->description;
    }



    public function getFullDescriptionAttribute(){
        return $this->master->name .' '.$this->description;
    }



    /********************************************************
    */
    public static function get_articles_list()
    {
        return DB::table('tmk_articles_view')
            ->orderBy('article', 'asc')
            ->get();
    }


    /*
     SELECT * FROM tmk_articles
     INNER JOIN tmk_article_bodegas ON tmk_article_bodegas.article_id =  tmk_articles.id
     INNER JOIN tmk_provider_brand_articles ON tmk_provider_brand_articles.article_id = tmk_articles.id
     INNER JOIN tmk_provider_brands ON tmk_provider_brands.id =  tmk_provider_brand_articles.provider_brand_id
     INNER JOIN tmk_brands ON tmk_brands.id  = tmk_provider_brands.brand_id INNER JOIN tmk_classes ON tmk_classes.id = tmk_articles.class_id
     INNER JOIN tmk_categories ON tmk_categories.id = tmk_classes.category_id
     WHERE tmk_articles.id = { id del  articulo }
    */
    public static function list_article($id)
    {
        return DB::table('tmk_articles')
            ->join('tmk_article_bodegas', 'tmk_article_bodegas.article_id', '=', 'tmk_articles.id')
            ->join('tmk_provider_brand_articles', 'tmk_provider_brand_articles.article_id', '=', 'tmk_articles.id')
            ->join('tmk_provider_brands', 'tmk_provider_brands.id', '=', 'tmk_provider_brand_articles.provider_brand_id')
            ->join('tmk_brands', 'tmk_brands.id', '=', 'tmk_provider_brands.brand_id')
            ->join('tmk_classes', 'tmk_classes.id', '=', 'tmk_articles.class_id')
            ->join('tmk_categories', 'tmk_categories.id', '=', 'tmk_classes.category_id')
            ->select(
                'tmk_articles.*',
                'tmk_article_bodegas.quantity',
                'tmk_classes.description as clase',
                'tmk_categories.description as category',
                'tmk_brands.description as brand'
            )
            ->where('tmk_articles.id', '=', $id)
            ->get();
    }

//    public static function insert_article ($request){
//        return DB::table('tmk_articles')->insert(['description'=> $request['name'], 'barcode'=> $request['barcode'], 'class_id'=> $request['tmk_classes'], 'brand_id'=> $request['brand']]
//                                                );
//        
//        return DB::table('article_bodegas')->insert([
//            'article_id' => $id,
//            'bodega_id'=> $request['bodega']
//        ]);
//    }


    /* Calcular el promedio de los 5 ultimos costos de un articulo
	SELECT ISNULL ( 
			CAST (  
				AVG( subquery.unit_cost ) AS decimal (10,2)  
			), 0.00 
	) AS unit_cost 
	FROM ( 
		SELECT DISTINCT TOP 5 unit_cost, updated_at, article_id
		FROM tmk_purchase_order_details 
		WHERE tmk_purchase_order_details.article_id = 37 
		ORDER BY updated_at DESC 
	) AS subquery
    */
    public static function getLastCostByArticle($id)
    {

        $subquery = DB::table('tmk_purchase_order_details')
            ->select(DB::raw('top 5  unit_cost ,  updated_at'))
            ->whereRaw('article_id = ' . $id)
            ->orderBy('updated_at', 'desc')
            ->distinct()
            ->toSql();

        $query = DB::table(DB::raw("( $subquery ) as subquery"))
            ->avg('subquery.unit_cost');

        return $query;
    }


    /*
    */
    public static function getMeasurementUnitByArticle($id)
    {
        return DB::table('tmk_article_masters')
            ->join('tmk_measurement_units', 'tmk_measurement_units.id', '=', 'tmk_article_masters.measurement_unit_id')
            ->select('tmk_measurement_units.id', 'tmk_measurement_units.description', 'tmk_measurement_units.unity')
            ->where('tmk_article_masters.id', $id)
            ->get();
    }

}