<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class PurchaseOrderDetail extends Model
{

    protected $table = 'tmk_purchase_order_details';

    protected $fillable = ['order_id', 'article_id', 'quantity', 'unit_cost', 'amount'];

    protected $dates = ['deleted_at'];


    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }



    public function getReceivedAttribute()
    {
        $received = DB::table('tmk_purchase_receipts')
            ->select('tmk_purchase_receipts.article_id', DB::raw('SUM(tmk_purchase_receipts.quantity) as quantity'))
            ->where('tmk_purchase_receipts.purchase_order_id', $this->order_id)
            ->where('tmk_purchase_receipts.article_id', $this->article_id)
            ->groupBy('tmk_purchase_receipts.article_id')
            ->first();

        return isset($received->quantity)? $received->quantity : 0;
    }



    public function getPendingAttribute()
    {
        return $this->quantity - $this->received;
    }



    public function scopeOrder($query, $id)
    {
        return $query->where('order_id', $id);
    }


    public function scopeArticleMaster($query)
    {
        return $query
            ->select('tmk_purchase_order_details.*', 'tmk_measurement_units.description as unity_desc', 'tmk_measurement_units.unity')
            ->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_purchase_order_details.article_id')
            ->join('tmk_article_masters', 'tmk_article_masters.id', '=', 'tmk_articles.article_master_id')
            ->join('tmk_measurement_units', 'tmk_measurement_units.id', '=', 'tmk_article_masters.measurement_unit_id');
    }



    public static function pendingArticlesByPurchase($purchase, $article){

       $article =  static::where('order_id', $purchase)->where('article_id', $article)->first();

       return $article->pending;
    }

}
