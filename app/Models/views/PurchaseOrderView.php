<?php
namespace TradeMarketing\Models\Views;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Models\OrderStatus;
use TradeMarketing\Traits\FormatDates;

class PurchaseOrderView extends Model
{
    use SoftDeletes, FormatDates;


	protected $table = 'tmk_purchase_orders_view';

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

//	public $timestamps = false;



	public function scopeNoStatus($query, $status){
		$query->where('order_status_id', '<>', $status);
	}


	public function detailsView(){
		return $this->hasMany(PurchaseOrderDetailView::class, 'order_id');
	}

}
