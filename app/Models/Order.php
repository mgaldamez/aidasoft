<?php

namespace TradeMarketing\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\Views\TrackingOrdersView;
use TradeMarketing\Traits\FormatDates;
use TradeMarketing\User;


class Order extends Model
{

    use FormatDates, SoftDeletes;

    protected $table = 'tmk_orders';
    protected $fillable = [
        'subject', 'from_user_id', 'for_user_id', 'order_num', 'sucursal_destination', 'require_date',  'observation', 'status_id',
        'warehouse_origen', 'warehouse_destination', 'created_by', 'canceled_by', 'updated_by', 'deleted_by', 'ordered_at', 'canceled_at'
    ];
    protected $date = ['require_date', 'ordered_at', 'canceled_at'];
    protected $dates = ['deleted_at'];

    public function isComplete()
    {
        $collection = collect($this->details);

        $pending = $collection->filter(function ($item) {
            return $item->pending;
        });

        if (count($pending)) {
            return false;
        }

        return true;
    }




    public function isDraft(){
        return $this->status == 'draft';
    }


    public function isCanceled(){
        return $this->canceled_at != null;
    }



    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    public function sucursal($sucursal)
    {
        return Sucursal::where('ID_SUCURSAL', $sucursal)->first()->DESCR_SUCURSAL;
    }

    public function tracking()
    {
        return $this->hasMany(TrackingOrdersView::class)
            ->select('made_at', 'warehouse', 'description')
            ->groupBy('made_at', 'warehouse', 'description')
            ->orderBy('made_at', 'asc');
    }


    public function transfers()
    {
        return $this->hasMany(Transfer::class);
    }


    public function transferCount()
    {
        //  10 => in_transit
        return Transfer::where('order_id', $this->id)->where('status_id', 10)->count();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function forUser()
    {
        return $this->belongsTo(User::class, 'for_user_id');
    }

    public function isValid($data)
    {
        $rules = [
            'subject' => 'min:6|max:100',
            'for_user_id' => 'id|exists:tmk_users,id',
            'order' => 'unique:tmk_orders,order',
            'warehouse_origen' => 'required_unless:status_id,1|id|exists:tmk_bodegas,id',
            'warehouse_destination' => 'required_unless:status_id,1|id|exists:tmk_bodegas,id',
            'require_date' => 'date_format:"Y-m-d"|after:' . Carbon::yesterday()->format('Y-m-d').'|date_format:Y-m-d',
            'original_reference' => '',
            'observation' => 'min:6|max:250',
            'items' => 'required_unless:status_id,1',
        ];

        $items = isset($data['items']) ? $data['items'] : [];

        foreach ($items as $key => $value) {
            $rules['items.' . $key . '.article'] = 'required|id|exists:tmk_articles,id,deleted_at,NULL';
            $rules['items.' . $key . '.quantity'] = 'required|numeric|min:1';
        }

        $messages = [];

        foreach ($items as $key => $value) {
            $messages['items.' . $key . '.article.required'] = 'El artículo es obligatorio.';
            $messages['items.' . $key . '.article.exists'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.article.id'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.quantity.required'] = 'La cantidad es obligatoria.';
            $messages['items.' . $key . '.quantity.numeric'] = 'La cantidad debe ser numérico.';
            $messages['items.' . $key . '.quantity.min'] = 'El tamaño de la cantidad debe ser de al menos :min';
        }

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {

            $this->errors = $validator->errors();

            return false;
        }

        return true;
    }


    public function scopeForApprove($query)
    {
        return $query->where('status_id', 2);
    }


    public function scopeInbox($query, $user = null)
    {
        if ($user) {
            return $query->where('for_user_id', $user)->whereNotNull('ordered_at');
        }

        return $query->where('for_user_id', currentUser()->id)->whereNotNull('ordered_at');
    }


    public function scopeDraft($query)
    {
        return $query->where('status_id', 1);
    }


    public function scopeNotDraft($query)
    {
        return $query->where('status_id', '<>', 1);
    }


    public function scopeSent($query)
    {
        return $query->notDraft()->whereNotNull('ordered_at');
    }


    public function warehouseOrigen()
    {
        return Warehouse::find($this->warehouse_origen)['description'];
    }


    public function warehouseDestination()
    {
        return Warehouse::find($this->warehouse_destination)['description'];
    }


    public function getStatusAttribute()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id')->first()->description;
    }





    public function warehousesId($warehouse)
    {
        $warehouses = currentUser()->warehouses->pluck('id')->toarray();

        return  in_array($warehouse, $warehouses);
    }



    public function orderState($stateRequired)
    {
        $status = OrderStatus::statusHierarchy();

        return $status[$this->status] < $status[$stateRequired];
    }



    public static function getListOrderPurchases()
    {
        return DB::table('tmk_order_purchases_view')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public static function getOrderPurchaseByOrder($order)
    {
        return DB::table('tmk_purchase_orders')
            ->where('order', '=', $order)
            ->get();
    }

    public static function getLastOrderPurchase()
    {
        return DB::table('tmk_order_purchases')
            ->orderBy('id', 'desc')
            ->first();
    }

    public static function getOrderPurchaseById($id)
    {
        return DB::table('tmk_order_purchases_view')
            ->where('tmk_order_purchases_view.id', '=', $id)
            ->first();
    }

    public static function getArticlesByOrderPurchase($id)
    {
        return DB::table('tmk_purchase_order_details')
            ->join('tmk_articles', 'tmk_articles.id', '=', 'tmk_purchase_order_details.article_id')
            ->where('tmk_purchase_order_details.order_id', '=', $id)
            ->select('tmk_purchase_order_details.*', 'tmk_articles.name as article', DB::raw("CAST(tmk_purchase_order_details.unit_cost * tmk_purchase_order_details.quantity AS decimal (10,2)) AS amount"))
            ->get();
    }


}
