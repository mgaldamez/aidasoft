<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleWarehouse extends Model{

    protected $table = "tmk_article_warehouses";

    protected $fillable = ['article_id', 'quantity', 'stock'];



    public function article(){
        return $this->belongsTo(Article::class);
    }


    /**
     *  SCOPES
     */
//    public function scopeWarehouse($query, $id){
//        return $query->where('bodega_id', $id);
//    }

}