<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Traits\ArrayHandler;
use TradeMarketing\Traits\FormatDates;
use TradeMarketing\User;

class ExternalMovement extends Model
{


    use  FormatDates;

    protected $table = 'tmk_external_movements';
    protected $fillable = ['external_movement_num', 'agent_id', 'external_agent_id', 'warehouse_id', 'observation', 'movement', 'status_id', 'created_by', 'updated_by', 'deleted_by'];


    public function isValid($data)
    {
        $array = [];
        $messages = [];
        $rules = [
            'agent_id' => 'required|exists:tmk_clientes_empresas,id,cliente,1',
            'external_agent_id' => 'required|in:' . implode(',', array_keys(config('enums.external_agents'))) . '',
            'warehouse_id' => 'required|exists:tmk_bodegas,id',
            'observation' => 'min:5|max:255',
            'items' => 'required'
        ];


        $data['items'] = isset($data['items']) ? $data['items'] : [];


        foreach ($data['items'] as $key => $item) {

            if (!array_key_exists($item['article'], $array)) {
                $array[$item['article']] = 0;
            }

            $available = $this->availableQuantity($item['article'], $data['warehouse_id']) - $array[$item['article']];

            if ($available < 0) {
                $available = 0;
            }

            $rules['items.' . $key . '.article'] = 'required|id|exists:tmk_articles,id,deleted_at,NULL';
            $rules['items.' . $key . '.quantity'] = 'required|numeric|min:1';


            // Si es una accion de Salida => storeExist;
            // se agrega la regla "max" para validar la cantidad maxinma disponible de articulos
            if ($data['actionController'] == 'storeExit') {
                $rules['items.' . $key . '.quantity'] .= '|max:' . $available;
            }


            if (array_key_exists($item['article'], $array)) {
                $array[$item['article']] += $item['quantity'];
            }


            $messages['items.' . $key . '.article.required'] = 'El artículo es obligatorio.';
            $messages['items.' . $key . '.article.exists'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.article.id'] = 'El artículo es inválido.';
            $messages['items.' . $key . '.quantity.required'] = 'La cantidad es obligatoria.';
            $messages['items.' . $key . '.quantity.numeric'] = 'La cantidad debe ser numérico.';
            $messages['items.' . $key . '.quantity.min'] = 'La cantidad es inválida.';
            $messages['items.' . $key . '.quantity.max'] = 'No cuentas con stock suficiente.';
        }


        $validator = Validator::make($data, $rules, $messages);

        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }


    public function availableQuantity($article, $warehouse)
    {
        if ($quantity = ArticleMasterWarehousesView::where('warehouse_id', $warehouse)->where('article_id', (int)$article)->first()) {
            $old = ($this->details()->where('article_id', $article)->first())
                ? $this->details()->where('article_id', $article)->first()->quantity
                : 0;

            return $quantity->available_quantity + $old;
        }

        return 0;
    }


    public function details()
    {
        return $this->hasMany(ExternalMovementDetail::class);
    }


    public function articleWarehouseMovements()
    {
        return $this->morphMany(ArticleBodega::class, 'movementable');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }


    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }


    public function getAgentNameAttribute()
    {
        return DB::table('tbl_usuarios')
            ->where('COD_AGENTE', $this->agent_id)
            ->first()->nombre;
    }

}

