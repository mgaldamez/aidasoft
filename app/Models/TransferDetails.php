<?php

namespace TradeMarketing\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use TradeMarketing\Models\Views\ArticleMasterWarehousesView;
use TradeMarketing\Models\Views\OrderDetailView;

class TransferDetails extends Model
{

    protected $table = 'tmk_transfer_details';

    protected $fillable = ['article_id', 'quantity', 'received_quantity', 'available_quantity', 'measurement_unit'];


    public function isValid($data)
    {
        $rules = [
            'article_id' => 'required|exists:tmk_articles,id',
            'transfer_id' => 'required|exists:tmk_transfers,id',
            'quantity' => 'numeric|min:1'
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }


    public function stock($warehouse)
    {
        if ($article = ArticleMasterWarehousesView::where('warehouse_id', $warehouse)->where('article_id', $this->article_id)->first()) {
            return $article->stock;
        }
        return 0;
    }


    /**
     * Eloquent: Relationships
     *
     *
     * Las tablas de base de datos suelen estar relacionadas entre sí.
     * Por ejemplo, una entrada de blog puede tener muchos comentarios o un pedido podría estar relacionado
     * con el usuario que lo colocó.
     * Eloquent facilita la gestión y el trabajo con estas relaciones y apoya varios tipos diferentes de relaciones:
     *
     * https://laravel.com/docs/5.1/eloquent-relationships
     */

    public function article()
    {
        return $this->belongsTo(Article::class);
    }


    public function transfer()
    {
        return $this->belongsTo(Transfer::class);
    }



    /**
     * Eloquent: Mutators
     *
     *
     * Los accesores y mutadores le permiten dar formato a valores de atributos de Eloquent cuando los recupera
     * o los configura en instancias de modelo.
     * Por ejemplo, puede utilizar el cifrador de Laravel para cifrar un valor mientras se almacena en la base de datos y,
     * a continuación, descifrar automáticamente el atributo cuando se accede a él en un modelo Eloquent.
     *
     * https://laravel.com/docs/5.1/eloquent-mutators
     */

    public function originalQuantity($order)
    {
        return DB::table('tmk_order_details')
            ->where('order_id', $order)
            ->where('article_id', $this->article_id)
            ->first()->quantity;
    }


    public function pending($order = null)
    {
       $order =  isset($order)? $order : $this->transfer->order_id;

        return $this->originalQuantity($order) - $this->received($order);
    }


    public function received($order)
    {
        return DB::table('tmk_transfers')
            ->join('tmk_transfer_details', 'tmk_transfer_details.transfer_id', '=', 'tmk_transfers.id')
            ->select(DB::raw('SUM(tmk_transfer_details.received_quantity) as received'))
            ->where('tmk_transfer_details.article_id', $this->article_id)
            ->where('tmk_transfers.order_id', $order)
            ->first()->received;
    }
}