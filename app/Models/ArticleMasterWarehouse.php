<?php

namespace TradeMarketing\Models;


use Illuminate\Database\Eloquent\Model;

class ArticleMasterWarehouse extends Model
{

    protected $table = "tmk_article_master_warehouses";
}