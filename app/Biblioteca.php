<?php

namespace TradeMarketing;

use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use TradeMarketing\Models\ArticleMasterAttribute;
use TradeMarketing\Models\ArticleMasterWarehouse;
use TradeMarketing\Models\Views\ArticleView;
use TradeMarketing\Models\ArticleWarehouse;
use TradeMarketing\Traits\ResponseHandler;
use TradeMarketing\Traits\ImageHandler;
use TradeMarketing\Models\Attribute;
use TradeMarketing\Models\Brand;
use TradeMarketing\Models\Category;
use TradeMarketing\Models\MeasurementUnits;

use Validator;
use DB; 

class Biblioteca extends Model
{

  use ResponseHandler;
    protected $table = "Tmk_libros";


    protected $fillable = [
        'COD_LIBRO', 'NOMBRE', 'EDITORIAL', 'AUTOR', 'GENERO', 'PAIS_AUTOR',  'ANIO_EDICION', 'CODIGO_BARRAS','IMAGE','FEC_MAX'
    ];

    public function isValid($data)
    {

        $rules = [
            'COD_LIBRO' => 'required|max:100|unique:tmk_libros',
            'NOMBRE' => 'required|max:100',
            'EDITORIAL' => 'required|max:100',
            'AUTOR' => 'required|max:100',

            'IMAGE' => 'image_base64'
        ];


        // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['COD_LIBRO'] .= ',COD_LIBRO,' . $this->id;
            $rules['NOMBRE'] .= ',NOMBRE,' . $this->id;

        }

        $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);


        if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }

  public function master()
    {
        return $this->hasOne(ArticleMaster::class, 'id', 'id');
    }

 public function article(){
     return $this->belongsTo(ArticleMaster::class);
    }


}
