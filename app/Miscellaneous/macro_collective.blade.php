<?php

{!! Form::macro('button', function($name = '', $value = null, $attributes = [] )
{

	$name = isset($name)? 'name="'. $name . '"' : ''; 
	$id = isset($attributes['id'])? 'id="'. $attributes['id'] . '"' : '';
	// $value =  isset($value)? 'value="'. $value . '"' : ''; 
	$class =  isset($attributes['class'])? 'class="'. $attributes['class'] . '"' : '';
	$placeholder = isset($attributes['placeholder'])? 'placeholder="'. $attributes['placeholder'] .'"': '';

    return '<button type="button"  '. $name .' '. $id .' '. $class .' '. $placeholder .' '. $value .'></button>';
});
!!}