<?php
namespace TradeMarketing;

use Illuminate\Auth\Access\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TradeMarketing\Traits\ImageHandler;
use Validator;
use DB;

class ClientesContactos extends Model
{

    protected $table = "tmk_clientes_contactos";

    protected $fillable = [
		'id','con_nombre','con_direccion','con_telefono','con_movil','con_correo','con_puesto','id_empresa','con_tipo_contacto'
    ];
    public function isValid($data)
    {
		 $rules = 
		 [
		 'id'=> 'required|max:100|unique:tmk_clientes_contactos',
		 'con_nombre' => 'min:5|max:255',
		 ];
		    // Si el articulo existe:
        if ($this->exists) {
            //Evitamos que la regla “unique” tome en cuenta el nombre y el codigo de barras
            $rules['id'] .= ',id,' . $this->id;
            $rules['con_nombre'] .= ',nombre,' . $this->id;
        }
		 $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);
		  if ($validator->passes()) {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
	}
	public static function listing()
    {
        return static::orderBy('con_nombre')->lists('con_nombre', 'id');
    }
    public static function buscar_contactos($id)
    {
         return DB::table('tmk_clientes_contactos')
            ->where('id_empresa',$id)
            ->get();
    }
    public static function buscar_contacto($id)
    {
         return DB::table('tmk_clientes_contactos')
            ->where('id',$id)
            ->get();
    }
    public static function all_contactos()
    {
         return DB::table('tmk_clientes_contactos')
            ->get();
    }
    public static function buscar_inventario($id)
    {
         return DB::table('tmk_inventario_general')
            ->where('article_master_id',$id)
            ->sum('stock');
    }
}
