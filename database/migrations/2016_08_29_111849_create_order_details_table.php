p<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_order_details', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('tmk_orders')->onDelete('cascade');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles')->onDelete('cascade');

            $table->integer('quantity');

            $table->integer('pending')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_order_details');
    }
}
