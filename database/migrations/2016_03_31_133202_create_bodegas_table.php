<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodegasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_bodegas', function(Blueprint $table)
		{
			$table->increments('id');
            
            $table->integer('sucursal_id')->unsigned();
            //$table->foreign('sucursal_id')->references('id_sucursal')->on('tbl_sucursales')->onDelete('cascade');

            // $table->integer('center_id')->unsigned();
            // $table->foreign('center_id')->references('id')->on('tmk_centers')->onDelete('cascade');

            $table->string('description');
            $table->string('colour')->nullable();

            $table->integer('main')->nullable();
            $table->boolean('editable')->default(true);
            $table->boolean('active')->default(true); // true = activo, false = inactivo
            $table->string('observation')->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_bodegas');
	}

}
