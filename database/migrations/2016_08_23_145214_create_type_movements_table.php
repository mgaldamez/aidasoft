<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_type_movements', function (Blueprint $table) {
            $table->increments('id');

            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDelete('cascade');

            $table->string('description', 100);
            $table->string('colour')->nullable();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_type_movements');
    }
}
