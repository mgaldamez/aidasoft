<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_variants', function (Blueprint $table) {
            // $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles');
            $table->integer('attribute_value_id')->unsigned();
            $table->foreign('attribute_value_id')->references('id')->on('tmk_attribute_values');

            $table->primary(['article_id', 'attribute_value_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_variants');
    }
}
