<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_provider_brands', function (Blueprint $table) {

            // $table->increments('id');
            
            $table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('tmk_providers')->onDelete('cascade');

            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('tmk_brands')->onDelete('cascade');

            $table->primary(['provider_id', 'brand_id']);

            // $table->string('company')->nullable();
            // $table->string('email')->nullable();
            // $table->string('phone')->nullable();
            // $table->string('address')->nullable();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_provider_brands');
    }
}
