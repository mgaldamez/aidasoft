<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('transfer_num');

            $table->string('transfer_reference')->nullable();

            $table->integer('order_id')->unsigned()->nullable();
            //$table->foreign('order_id')->references('id')->on('tmk_orders')->onDelete('cascade');

            $table->string('subject', 100)->nullable();


            $table->integer('transferred_from')->unsigned()->nullable();
//            $table->foreign('transferred_from')->references('ID_SUCURSAL')->on('TBL_SUCURSALES');

            $table->integer('transferred_to')->unsigned()->nullable();
//            $table->foreign('transferred_to')->references('ID_SUCURSAL')->on('TBL_SUCURSALES');

            $table->integer('bodega_id')->unsigned();
            $table->foreign('bodega_id')->references('id')->on('tmk_bodegas');

            $table->integer('bodega_id_end')->unsigned()->nullable();

//            $table->enum('status', config('enums.transfer_status'))->default('draft');

            $table->string('observation')->nullable();

            $table->string('receipt_num')->nullable();
            $table->string('receipt_file')->nullable();
            $table->string('receipt_observation')->nullable();


//            $table->foreign('received_by')->references('id')->on('tmk_users');

            $table->integer('transferred_by')->nullable();
//            $table->integer('transferred_by')->unsigned();
//            $table->foreign('transferred_by')->references('id')->on('tmk_users');

            $table->integer('transferred_for')->nullable();
//            $table->integer('transferred_for')->unsigned();
//            $table->foreign('transferred_for')->references('id')->on('tmk_users');

            $table->integer('created_by')->index()->nullable();
            $table->integer('updated_by')->index()->nullable();
            $table->integer('deleted_by')->index()->nullable();
            $table->integer('requested_by')->index()->nullable();
            $table->integer('received_by')->index()->nullable();
            $table->integer('canceled_by')->index()->nullable();

            $table->timestamp('transferred_at')->nullable();
            $table->timestamp('requested_at')->nullable();
            $table->timestamp('received_at')->nullable();
            $table->timestamp('canceled_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_transfers');
    }
}
