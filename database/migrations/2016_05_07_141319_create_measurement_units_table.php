<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementUnitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_measurement_units', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('description');
            $table->string('unity');
            $table->boolean('active')->default(true); // true = activo, false = inactivo
            
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_measurement_units');
	}

}
