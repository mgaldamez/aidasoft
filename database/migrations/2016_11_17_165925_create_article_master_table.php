<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_article_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);

            $table->string('barcode', 100)->nullable();

            $table->string('internal_reference', 100)->nullable();

            $table->integer('min_stock')->unsigned()->nullable()->default(1);

            $table->integer('max_stock')->unsigned()->nullable();

            $table->string('image')->nullable();

//            $table->integer('category_id')->unsigned();
//            $table->foreign('category_id')->references('id')->on('tmk_categories')->onDelete('cascade');
            $table->integer('category_id')->nullable();

//            $table->integer('brand_id')->unsigned();
//            $table->foreign('brand_id')->references('id')->on('tmk_brands')->onDelete('cascade');
            $table->integer('brand_id')->nullable();

            $table->integer('measurement_unit_id')->unsigned();

            $table->foreign('measurement_unit_id')->references('id')->on('tmk_measurement_units')->onDelete('cascade');

            $table->text('long_description')->nullable();

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_article_masters');
    }
}
