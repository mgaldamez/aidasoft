<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('warehouse_id')->unsigned()->index();
            $table->integer('modelable_id')->nullable();
            $table->string('modelable_type')->nullable();
            $table->enum('type', ['success', 'info', 'warning', 'error']);
            $table->string('title')->nullable();
            $table->longText('body')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_activities');
    }
}
