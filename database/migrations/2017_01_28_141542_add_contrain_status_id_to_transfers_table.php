<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContrainStatusIdToTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_transfers', function (Blueprint $table) {
            $table->integer('status_id')->unsigned()->default(1);
            //$table->foreign('status_id')->references('id')->on('tmk_order_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_transfers', function (Blueprint $table) {
            $table->dropForeign('tmk_transfers_status_id_foreign');
        });
    }
}
