<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContrainStatusIdToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_orders', function (Blueprint $table) {
            $table->integer('status_id')->unsigned();
           // $table->foreign('status_id')->references('id')->on('tmk_order_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_orders', function (Blueprint $table) {
            $table->dropForeign('tmk_orders_status_id_foreign');
        });
    }
}
