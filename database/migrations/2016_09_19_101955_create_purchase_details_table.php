<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_purchase_order_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('tmk_purchase_orders')->onDelete('cascade');

            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('tmk_articles');

            $table->integer('quantity')->default(0);
            $table->decimal('unit_cost', 10, 2)->default(0.00);
            $table->decimal('tax', 10, 2)->nullable()->default(0.00);
            $table->decimal('amount', 10, 2)->default(0.00);

            $table->boolean('complete')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_purchase_order_details');
    }
}
