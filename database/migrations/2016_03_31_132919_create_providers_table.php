<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_providers', function(Blueprint $table)
		{
			$table->increments('id');
           
            // $table->integer('type_provider_id')->unsigned();
            // $table->foreign('type_provider_id')->references('id')->on('tmk_type_providers')->onDelete('cascade');
           
            // $table->integer('user_id')->unsigned();
            // $table->foreign('user_id')->references('id')->on('tmk_users')->onDeletes('cascade');
            
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->string('company', 100);
            $table->string('email')->unique();
            $table->string('phone', 50);
            $table->string('address');
            $table->boolean('state')->default(true);
            $table->string('image')->nullable();
            $table->enum('type', Config('enums.provider_types'));

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
			
			$table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_providers');
	}

}
