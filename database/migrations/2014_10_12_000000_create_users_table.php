<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->string('address')->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password', 60);
            $table->boolean('active')->default(true); // true = activo, false = inactivo
            $table->integer('agent_id')->unique();
            $table->integer('sucursal_id')->unsigned()->index()->nullable();

//            $table->integer('warehouse_id')->index()->unsigned()->nullable();

            $table->string('confirmation_token')->nullable();

//          	$table->integer('agent_id')->unsigned();
//            $table->foreign('agent_id')->references('cod_agente')->on('tbl_usuarios')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('last_logged_at')->nullable();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_users');
	}

}
