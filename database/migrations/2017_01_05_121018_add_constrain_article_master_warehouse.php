<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstrainArticleMasterWarehouse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tmk_article_warehouses', function (Blueprint $table) {
            $table->integer('article_master_warehouse_id')->unsigned();
           // $table->foreign('article_master_warehouse_id')->references('id')->on('tmk_article_master_warehouses');

          //  $table->index(['article_id', 'article_master_warehouse_id'])->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmk_article_warehouses', function (Blueprint $table) {
            //
           // $table->dropForeign('tmk_article_warehouses_article_master_warehouse_id_foreign');

        });
    }
}
