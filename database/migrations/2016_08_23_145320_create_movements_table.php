<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_movements', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('type_movement_id')->unsigned();
            $table->foreign('type_movement_id')->references('id')->on('tmk_type_movements')->onDelete('cascade');

            $table->string('code', 5);
            $table->string('description', 100);
            $table->string('concerned');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_movements');
    }
}
