<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_external_movements', function (Blueprint $table) {
            $table->increments('id');

            $table->string('external_movement_num')->index();

            $table->integer('agent_id')->index()->unsigned();

            $table->integer('external_agent_id')->index()->unsigned();

            $table->integer('warehouse_id')->index()->unasigned();

            $table->string('observation')->nullable();

            $table->enum('movement', ['entry', 'exit']);

            $table->integer('status_id')->unsigned()->default(1);
           // $table->foreign('status_id')->references('id')->on('tmk_order_status');

            $table->integer('created_by')->index()->unsigned()->nullable();
            $table->integer('updated_by')->index()->unsigned()->nullable();
            $table->integer('deleted_by')->index()->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_external_movements');
    }
}
