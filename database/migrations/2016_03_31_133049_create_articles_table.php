<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tmk_articles', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('barcode')->nullable();
            $table->string('internal_reference')->nullable();
            $table->integer('reference_code');
            $table->index(['reference_code', 'id'])->unique();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->decimal('unit_cost_initial', 10, 2)->nullable()->default(0.00);
            $table->boolean('active')->default(true); // true = activo, false = inactivo

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();

            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tmk_articles');
	}

}
