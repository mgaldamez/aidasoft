<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalMovementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmk_external_movement_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('external_movement_id')->unsigned();
            $table->foreign('external_movement_id')->references('id')->on('tmk_external_movements')->onDelete('cascade');

            $table->integer('article_id')->index()->unsigned();

            $table->integer('quantity')->unsigned();

//            $table->integer('external_agent_id')->index()->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tmk_external_movement_details');
    }
}
