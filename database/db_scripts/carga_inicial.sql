USE [COM_SYSTRADE]
GO
INSERT INTO `tbl_regiones`(`ID_REGION`, `DESCR_REGION`, `EMAIL`, `GERENTE`)  VALUES 
(0, N'NO INFORMADO', NULL, NULL)
(1, N'REGION 1', N'ventas.region.1@telefonica.com', 15)
(2, N'REGION 2', N'adminR2.PA@telefonica.com', 14)
(3, N'REGION 3', N'AdminR3.PA@telefonica.com', 6)
(4, N'REGION 4', N'AdminComercialR4.PA@telefonica.com', 13)
(5, N'REGION 5', N'adminR5.PA@telefonica.com', 14)


INSERT INTO `tbl_sucursales`(`ID_SUCURSAL`, `DESCR_SUCURSAL`, `ID_ESTADO`, `ID_REGION`, `FLAG_TK`)
VALUES(1, N'AGUADULCE', 1, 3, N'S'),
(2, N'ALBROOK CINE', 1, 1, N'S'),
(3, N'ALBROOK PINGUINO', 1, 1, N'S'),
(4, N'ALBROOK TERMINAL', 1, 1, N'S'),
(5, N'ALMIRANTE', 1, 4, N'S'),
(6, N'ATENTO', 3, 0, NULL),
(7, N'BUGABA', 1, 4, N'S'),
(8, N'CALIDONIA', 3, 0, NULL),
(9, N'CELLREY', 3, 0, NULL),
(10, N'CHANGUINOLA', 1, 4, N'S'),
(11, N'CHIRIQUÍ OBALDÍA', 3, 4, NULL),
(12, N'CHITRÉ', 1, 3, N'S'),
(13, N'CHORRERA', 1, 5, N'S'),
(14, N'COLÓN 2000', 1, 2, N'S'),
(15, N'COLÓN AVE. CENTRAL', 1, 2, N'S'),
(16, N'CORONADO', 1, 5, N'S'),
(17, N'COSTA DEL ESTE', 1, 1, N'S'),
(18, N'DAVID CENTRO', 1, 4, N'S'),
(19, N'DAVID TERRONAL', 1, 4, N'S'),
(20, N'EL DORADO', 1, 1, N'S'),
(21, N'EL VALLE', 1, 5, N'S'),
(22, N'GRAN ESTACIÓN DE S.M.', 3, 0, NULL),
(23, N'ISLA COLÓN', 1, 4, N'S'),
(24, N'KIOSCO 4 ALTOS', 1, 2, N'S'),
(25, N'KIOSCO ALBROOK CONWAY', 1, 1, N'S'),
(26, N'KIOSCO ALBROOK COSTO', 1, 1, N'S'),
(27, N'KIOSCO ALBROOK FELIX', 3, 1, NULL),
(28, N'KIOSCO ALBROOK MALL MP', 1, 1, N'S'),
(29, N'KIOSCO ALBROOK TERMINAL', 1, 1, N'S'),
(30, N'KIOSCO CHIRIQUÍ TERMINAL', 3, 4, NULL),
(31, N'KIOSCO DORADO', 3, 0, NULL),
(32, N'KIOSCO GALERÍA', 3, 0, NULL),
(33, N'KIOSCO LOS ANDES', 1, 1, N'S'),
(34, N'KIOSCO LOS ANDES MALL', 3, 1, NULL),
(35, N'KIOSCO METROMALL', 1, 1, N'S'),
(36, N'KIOSCO MULTICENTRO', 1, 1, N'S'),
(37, N'KIOSCO MULTIPLAZA', 1, 1, N'S'),
(38, N'KIOSCO REDES SOCIALES', 1, 1, N'S'),
(39, N'KIOSCO TERMINAL CHITRÉ', 1, 3, N'S'),
(40, N'KIOSCO TERMINAL DE COLÓN', 3, 2, NULL),
(41, N'KIOSCO TERMINAL SANTIAGO', 1, 3, N'S'),
(42, N'KIOSCO TERMINAL SONÁ', 1, 3, N'S'),
(43, N'KIOSCO WESTLAND MALL', 1, 5, N'S'),
(44, N'LA DOÑA', 1, 1, N'S'),
(45, N'LAS TABLAS', 1, 3, N'S'),
(46, N'LOS ANDES', 1, 1, N'S'),
(47, N'LOS PUEBLOS', 3, 0, NULL),
(48, N'METETÍ', 1, 1, N'S'),
(49, N'METROMALL', 1, 1, N'S'),
(50, N'MULTICANAL', 3, 0, NULL),
(51, N'MULTIPLAZA', 1, 1, N'S'),
(52, N'NO LABORA', 3, 0, NULL),
(53, N'OBARRIO', 3, 0, NULL),
(54, N'PEATONAL', 3, 0, NULL),
(55, N'PENONOMÉ', 1, 3, N'S'),
(56, N'PLAZA CONCORDIA', 1, 1, N'S'),
(57, N'SANTIAGO AVE. CENTRAL', 1, 3, N'S'),
(58, N'TIENDA WESTLAND MALL', 1, 5, N'S'),
(60, N'KIOSCO BOQUETE', 1, 4, N'S'),
(61, N'COOPERATIVA DE PROFESIONALES', 1, 1, N'S'),
(62, N'SIN ESPECIFICAR', 1, 0, N'S'),
(63, N'COSTA DEL ESTE OFICINA', 1, 1, NULL),
(64, N'BOC ', 1, 1, NULL),
(65, N'TIENDA MALL PASEO ARRAIJAN', 1, 5, NULL)

INSERT INTO `tmk_users`(`id`, `firstname`, `lastname`, `address`, `phone`, `email`, `username`, `password`, `active`, `agent_id`, `sucursal_id`, `confirmation_token`, `remember_token`, `created_at`, `updated_at`, `last_logged_at`, `deleted_at`, `role_id`) 
VALUES (1, N'JOHNNY', N'GONZALEZ', NULL, NULL, N'johnny.gonzalez@telefonica.com', N'NCT02167', N'$2a$06$LCUHNnoYYuQuSoVKUdQc9uz0s6Wiq7ZfbIbScW9V.Pi3hvjzQsuHW', 1, 2167, NULL, NULL, N'2uh9qfZCbx6XHUKE3FURugFJtOLGN4HZQS3UvukqaVIQCivW5f0mNJj9EZ9c', CAST(N'2017-03-24' AS Date), CAST(N'2017-11-22' AS Date), CAST(N'2017-11-22' AS Date), NULL, 4)
(2, N'Ariel', N'Muñoz', N'', N'', N'aemg12@gmail.com', N'ncn03839', N'$2y$10$EXb6L0Xmd2mMmkspClzq0evmAsj5MMph0CN35wGKqkKNix4E1kF76', 1, 9999993, NULL, NULL, N'Dt0Z5Gu5d0Jridj2ETs9rcn8zsV2sUMYO8YwPMOWa9CWrQAFNS3Pd2wHasK3', CAST(N'2016-07-24' AS Date), CAST(N'2017-06-16' AS Date), CAST(N'2017-06-16' AS Date), NULL, 1)
,(3, N'Fernan', N'González', NULL, NULL, N'fernan.gonzalez@telefonica.com', N'NCT02005', N'e10adc3949ba59abbe56e057f20f883e', 1, 2005, NULL, NULL, N'JFM4kKuSKUzAFYk9l4wUsCMBdM6qI43r00F08TzuhagH9LTbnr2hnRhtAH3r', CAST(N'2016-12-20' AS Date), CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), NULL, 3)


,(4, N'SUPER', N'SUPER', N'', N'', N'super.admin@telefonica.com', N'NCTSA', N'$2a$06$LCUHNnoYYuQuSoVKUdQc9uz0s6Wiq7ZfbIbScW9V.Pi3hvjzQsuHW', 1, 99999999, NULL, NULL, N'P1h5xqbNButLMjnos36BAR9ANHgXYdbNR5tZLm2Mt0LViT4Kizg8oCbumb2F', CAST(N'2017-03-15' AS Date), CAST(N'2018-07-04' AS Date), CAST(N'2018-07-04' AS Date), NULL, 1)
,(5, N'MARIA', N'NAVARRO NCT', N'', N'', N'maria.navarro@telefonica.com', N'NCT01965', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 1965, NULL, NULL, N'KEE4tHgpXfputeAF4AGLzj2WoraIAeSUfiJfCxDtDABNj2CNJek9hSbF3Rz7', CAST(N'2017-03-15' AS Date), CAST(N'2018-03-22' AS Date), CAST(N'2018-03-22' AS Date), NULL, 4)
,(6, N'TERESA', N'MONTENEGRO', N'', N'', N'teresa.montenegro@telefonica.com', N'NCT01896', N'$2y$10$sbMT3obw/H04ULoBinD2Ou2JmJbXAejSpkoVBiWdMDjHkHWmirSWO', 1, 901896, NULL, NULL, N'S2RaJwr9uvZjv41uJxyCP8MIsNWWjikGMX1ezDgEjgQBi1kIc0soV5revoCB', CAST(N'2017-03-15' AS Date), CAST(N'2017-12-04' AS Date), CAST(N'2017-12-04' AS Date), NULL, 2)
,(7, N'ALCIBIADES', N'GARCIA', N'', N'65137490', N'alcibiades.garcia@telefonica.com', N'NCT02036', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 0, 2036, NULL, NULL, NULL, CAST(N'2017-03-15' AS Date), CAST(N'2018-03-14' AS Date), CAST(N'2017-04-28' AS Date), NULL, 3)
,(8, N'JUAN', N'ELIZONDO', NULL, NULL, N'juan.elizondo.ext@telefonica.com', N'NCN03791', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 3791, NULL, NULL, N'4f5SohKIq6HX4LirJYwIj3zZF5lmfbxfRtbCLoAYAdVtdNFXngeIu1POzx3e', CAST(N'2017-03-15' AS Date), CAST(N'2018-01-30' AS Date), CAST(N'2018-01-30' AS Date), NULL, 4)
,(9, N'JEFFRY', N'FRANCO', NULL, NULL, N'Jeffry.franco.ext@telefonica.com', N'NCN03884', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 3884, NULL, NULL, N'oYrfnOFBgajJLxTONYmB8673hhkF01LTmux3hIGmzYIdKUKI6gOt9OYfMEZS', CAST(N'2017-03-15' AS Date), CAST(N'2017-08-17' AS Date), CAST(N'2017-08-17' AS Date), NULL, 4)
,(10, N'FERNANDO', N'MONTAÑEZ', NULL, NULL, N'fernando.montanez.ext@telefonica.com', N'NCN03452', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 0, 903452, NULL, NULL, N'vatKB7PD03mgf3rA9SYK65zjqPWTrKWwvTDfZfBtVJgy1U2Kk3YlWPSMaWXq', CAST(N'2017-03-15' AS Date), CAST(N'2017-07-12' AS Date), CAST(N'2017-07-12' AS Date), NULL, 4)
,(11, N'VIELKA', N'ALAIN', NULL, NULL, N'vielka.alain.ext@telefonica.com', N'NCN01999', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 1999, NULL, NULL, N'lQYgdLbOi5ki9ZB9aAILZuNV1JeaIWI3zcV0KTnNLSMq46R9XVDKTwi0R80I', CAST(N'2017-03-15' AS Date), CAST(N'2018-07-16' AS Date), CAST(N'2018-07-16' AS Date), NULL, 4)
,(12, N'ROXANA', N'ORTEGA', N'', N'', N'roxana.ortega@telefonica.com', N'NCT01121', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 901121, NULL, NULL, N'Peh2AloiytizvMXsxfPWqeDz6FcCA1WO013PfuC5jLz8LpLvUL21UM9PdNiX', CAST(N'2017-03-15' AS Date), CAST(N'2018-03-14' AS Date), CAST(N'2017-05-04' AS Date), NULL, 3)
,(13, N'CARLOS', N'SANTOS', NULL, NULL, N'carlos.santos@telefonica.com', N'NCT00738', N'$2y$10$agziqKrDCpix3CFfw4HrvekCPrdQIj3/.31zM/btY.eWGEleANZ6q', 1, 900738, NULL, N'9VcVwNYmAFOuQP5kpDVfHv9ZioaFRuh1SC0SpY2hjuta98BNQoa29ZX26tPH', NULL, CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), NULL, NULL, 4)
,(21, N'VICTOR', N'GONZÁLEZ', NULL, NULL, N'victor.gonzalezdelaflor@telefonica.com', N'NCT01817', N'bRFsoH3Uh2QB8DE2LOJtQQYvu3jYxS', 0, 901817, NULL, N'fC7JkJY5QdfdsYSaUW7mgAVjPCg36fWPJrTUDakm4myZOeDaeYlLt1TE8m8N', NULL, CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), NULL, NULL, 4)
,(22, N'TATIANA', N'SAAVEDRA', NULL, NULL, N'tatiana.saavedra.ext@telefonica.com', N'NCN03049', N'639b9a014c9bde830f4362270862d6a5', 1, 903049, NULL, N'w80mWhf3QCCiT0pvaiJsay9KmlG0HB5TNxVyfdcSSTMAVVXpM6gD1Gydi1on', NULL, CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), NULL, NULL, 4)
,(23, N'JOHNNY', N'GONZÁLEZ', NULL, NULL, N'johnny.gonzalez.PRUEBA@telefonica.com', N'NCT021679', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 21679, NULL, NULL, NULL, CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), CAST(N'2017-03-24' AS Date), NULL, 4)
,(24, N'JOSE', N'ANGEL CERRUD', NULL, NULL, N'jose.cerrud.ext@telefonica.com', N'NCN03882', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 3882, NULL, NULL, NULL, CAST(N'2017-03-30' AS Date), CAST(N'2018-01-31' AS Date), CAST(N'2018-01-31' AS Date), NULL, 4)
,(25, N'ALEJANDRA', N'MORENO', NULL, NULL, N'alejandra.moreno@telefonica.com', N'NCT01995', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 1995, NULL, NULL, N'aeLBwBV8MVNP3icqQq17R4wXShHDvD8XYNq0NAyW1k7qbly0i4ztD0lj69R3', CAST(N'2017-03-30' AS Date), CAST(N'2017-06-14' AS Date), CAST(N'2017-06-14' AS Date), NULL, 4)
,(26, N'MARCELO', N'MOUZO', N'', N'', N'marcelo.mouzo@telefonica.com', N'NCT01943', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 901943, NULL, NULL, NULL, CAST(N'2017-04-06' AS Date), CAST(N'2017-04-08' AS Date), CAST(N'2017-04-08' AS Date), NULL, 4)
,(27, N'Christian', N'Eduardo Sentmat Sanchez', N'', N'', N'chirtian.sentmat.ext@telefonica.com', N'NCN04157', N'$2y$10$.wgZ2tM7XzjlHyFiLK3MxOrKbodhA3wA/IwWeGEeuNn3HVwGVeztq', 1, 904157, NULL, NULL, N'XpchYSx0WeXOOyPTop0z1rQ4huKvvrIr7eWEkKbmcpm5FWYFCEitl6jwHrnM', CAST(N'2017-06-01' AS Date), CAST(N'2018-08-14' AS Date), CAST(N'2018-08-14' AS Date), NULL, 4)
,(44, N'GREGORY', N'DE LEON MORENO', NULL, NULL, N'', N'NCT02117', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 2117, NULL, NULL, NULL, CAST(N'2017-04-08' AS Date), CAST(N'2017-04-08' AS Date), CAST(N'2017-04-08' AS Date), NULL, 4)
,(51, N'JAHDIEL', N'SANTAMARIA', N'', N'', N'jahdiel.santamaria.ext@telefonica.com', N'NCN04165', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 904165, NULL, NULL, N'CHBVn9EKvg0TPKiYNTAjf5XhuZvuanMGB4UWGq0zziaCJwwH0Foxr28rnVPe', CAST(N'2017-06-13' AS Date), CAST(N'2018-07-12' AS Date), CAST(N'2018-07-12' AS Date), NULL, 1)
,(65, N'JAHDIEL', N'SANTAMARIA 1', N'', N'', N'jahdiel.santamaria.ext1@telefonica.com', N'NCN041650', N'$2y$10$bKMTHJB9SwIrlmukCcEMTuiK9FQ0eCNcZgbXxjub1sKYXJ12qIbly', 1, 99904165, NULL, NULL, N'tlVy0HYoFHP3zjIzkKPdqSUr6SZWn95kJ3lThNlApC9nafwsT53tIxchL1mx', CAST(N'2017-08-18' AS Date), CAST(N'2018-03-15' AS Date), CAST(N'2018-03-15' AS Date), NULL, 4)
,(84, N'JOHN', N'SAMUDIO PRUEBA', N'', N'', N'john.samudio@telefonica.com', N'NCN03877', N'$2y$10$3IGI0sFNnUK5ql3Dofu/iu1mRTHYsgH.hwaGwjFaRlDGMvSnMRYPy', 1, 99999999, NULL, NULL, N'AFFDszUVKRubq4aEfYegO9HX2k0MiFSe4GYICi0WsJK3bDcgSvDtflSKraNv', CAST(N'2017-03-15' AS Date), CAST(N'2017-09-14' AS Date), CAST(N'2017-09-14' AS Date), NULL, 1)
,(85, N'LOURDES', N'LEZCANO', NULL, NULL, N'lourdes.lezcano@telefonica.com', N'NCT01698', N'$2y$10$l9YtHZyrj./NiXTUCgEnUeY0nYu.pziXBOfBm68RTbGp/JoZPEdIW', 1, 901698, NULL, N'oySB5bSYojFm0ljiXxezhv22cwdrCA5XGSGfTrTWz3JSSQANA0xs7YVMjJlX', NULL, CAST(N'2018-02-01' AS Date), CAST(N'2018-02-01' AS Date), NULL, NULL, 4)
,(86, N'LOURDES', N'LEZCANO', NULL, NULL, N'lourdes.lezcano@telefonica.com', N'NCT01698', N'$2a$06$LCUHNnoYYuQuSoVKUdQc9uz0s6Wiq7ZfbIbScW9V.Pi3hvjzQsuHW', 1, 901698, NULL, NULL, N'yhZh8LEStco9BGgAUJIjineMu5XuFUhzJunRrIn566r9f99kkNKRuJ4S1US2', CAST(N'2018-02-01' AS Date), CAST(N'2018-07-27' AS Date), CAST(N'2018-07-27' AS Date), NULL, 4)
,(87, N'MARIA', N'NAVARRO', NULL, NULL, N'maria.navarro_ext@telefonica.com', N'NCN04316', N'$2y$10$pmcJz4jel0GKOOvCLC0M/.yRzo9xUAVSeJpRRYU0ssemcPB5kNXLS', 1, 904316, NULL, N'z1j502ZC5iQ5KyvrVsObXnxobuCP0cj64Pg5Ujzd9qQbv704jKcbR0kR0Zrx', NULL, CAST(N'2018-03-15' AS Date), CAST(N'2018-03-15' AS Date), NULL, NULL, 4)

