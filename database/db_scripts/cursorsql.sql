

use bi_comercial
go

/**
* TRAKING 
*
*/
IF OBJECT_ID ('tmk_traking') IS NOT NULL
BEGIN
	DROP VIEW tmk_traking
END
GO

CREATE VIEW tmk_traking AS 
SELECT  
tmk_orders.id AS order_id, 
tmk_orders.order_num, 
(
	SELECT tmk_bodegas.description 
	FROM tmk_bodegas 
	WHERE tmk_bodegas.id = tmk_orders.warehouse_origen 
)AS order_warehouse_origen, 
(
	SELECT tmk_bodegas.description 
	FROM tmk_bodegas 
	WHERE tmk_bodegas.id = tmk_orders.warehouse_destination
)AS order_warehouse_destination,
tmk_orders.ordered_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.created_by
) AS ordered_by,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.for_user_id
) AS ordered_for,

tmk_orders.canceled_at AS order_canceled_at,

(
select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) FROM tmk_users WHERE tmk_users.id = tmk_orders.canceled_by
) AS order_canceled_by,

tmk_transfers.id AS transfer_id, 
tmk_transfers.transfer_num, 
tmk_transfers.transfer_reference,
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id 
)AS transfer_warehouse_origen, 
(
	select tmk_bodegas.description 
	from tmk_bodegas 
	where tmk_bodegas.id = tmk_transfers.bodega_id_end 
)AS transfer_warehouse_destination,
tmk_transfers.created_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.created_by
) AS created_by,
tmk_transfers.updated_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.updated_by
) AS updated_by,
tmk_transfers.transferred_at,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_by
) AS transferred_by,
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.transferred_for
) AS transferred_for,

tmk_transfers.received_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.received_by
) AS received_by,
tmk_transfers.canceled_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.canceled_by
) AS canceled_by,
tmk_transfers.requested_at, 
(
	select CONCAT(tmk_users.firstname,' ',tmk_users.lastname) from tmk_users where tmk_users.id = tmk_transfers.requested_by
) AS requested_by,
( 
	select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_orders.status_id 
) As [order_status],
( 
	select tmk_order_status.description from tmk_order_status where tmk_order_status.id =  tmk_transfers.status_id 
) As [transfer_status]
FROM tmk_orders
LEFT JOIN tmk_transfers ON tmk_transfers.order_id = tmk_orders.id 
GO 

/**
 * CURSOR
 */

if OBJECT_ID('tmk_tracking_orders_func') IS NOT NULL
begin
	drop function tmk_tracking_orders_func
end
go

create function tmk_tracking_orders_func ()
returns @table table(
[made_at] datetime,
[made_by] nvarchar (255),
[transfer_id] int,
[transfer_num] nvarchar (255),
[order_id] int,
[order_num] nvarchar (255),
[warehouse] nvarchar (255),
[warehouse_origen] nvarchar (255),
[warehouse_destination] nvarchar (255),
[description] nvarchar (255)
--,[status] nvarchar (20)
) 
as begin

declare 
	@transfer_id int, 
	@transfer_num nvarchar (255), 
	@order_id int,
	@order_num nvarchar (255),
	@ordered_at DateTime,
	@ordered_by nvarchar (255),
	@ordered_for nvarchar (255),

	@order_canceled_at DateTime,
	@order_canceled_by nvarchar (255),
	
	@transferred_at DateTime,
	@transferred_by nvarchar (255),
	@received_at DateTime,
	@received_by nvarchar (255),
	@canceled_at DateTime,
	@canceled_by nvarchar (255),
	@requested_at DATETIME,
	@requested_by NVARCHAR (255),
	@order_warehouse_origen nvarchar (255),
	@order_warehouse_destination nvarchar (255),
	@transfer_warehouse_origen nvarchar (255),
	@transfer_warehouse_destination nvarchar (255),
	@made_at DATETIME,
	@made_by nvarchar (255),
	@warehouse NVARCHAR (255),
	@warehouse_origen NVARCHAR (255),
	@warehouse_destination NVARCHAR (255),
	@description NVARCHAR (255)

	declare [cursor] cursor FOR select 
	transfer_id, 
	transfer_num, 
	order_id,
	order_num,
	ordered_at,
	ordered_by,
	ordered_for,

	order_canceled_at,
	order_canceled_by,

	transferred_at,
	transferred_by,
	received_at,
	received_by,
	canceled_at,
	canceled_by,
	requested_at,
	requested_by,
	order_warehouse_origen,
	order_warehouse_destination,
	transfer_warehouse_origen,
	transfer_warehouse_destination
	from tmk_traking

	open [cursor]
	fetch [cursor] into  
	@transfer_id, 
	@transfer_num, 
	@order_id,
	@order_num,
	@ordered_at,
	@ordered_by,
	@ordered_for,

	@order_canceled_at,
	@order_canceled_by,

	@transferred_at,
	@transferred_by,
	@received_at,
	@received_by,
	@canceled_at,
	@canceled_by,
	@requested_at,
	@requested_by,
	@order_warehouse_origen,
	@order_warehouse_destination,
	@transfer_warehouse_origen,
	@transfer_warehouse_destination

	WHILE (@@FETCH_STATUS = 0)
	BEGIN

	IF @ordered_at IS NOT NULL 
	BEGIN
		SET @made_at = @ordered_at
		SET @made_by = @ordered_by
		SET @warehouse = @order_warehouse_origen
		SET @warehouse_origen = @order_warehouse_origen
		SET @warehouse_destination = @order_warehouse_destination
		SET @description =  CONCAT(@made_by,' cre� un pedido con n�mero ', @order_num, '.')
		
		INSERT INTO @table VALUES (@made_at, @made_by,@transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END

	IF @requested_at IS NOT NULL 
	BEGIN
		SET @made_at = @requested_at
		SET @made_by = @requested_by
		SET @warehouse = @order_warehouse_destination
		SET @warehouse_origen = @transfer_warehouse_origen
		SET @warehouse_destination = @transfer_warehouse_destination
		SET @description =  CONCAT(@ordered_for, ' cre� una solicitud de transferencia para ', @warehouse_origen, ' ', @order_num, '.')
		
		INSERT INTO @table VALUES (@made_at, @made_by, @transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END
	
	IF @transferred_at IS NOT NULL 
	BEGIN 
		SET @made_at = @transferred_at
		SET @made_by = @transferred_by
		SET @warehouse = @transfer_warehouse_origen
		SET @warehouse_origen = @transfer_warehouse_origen
		SET @warehouse_destination = @transfer_warehouse_destination
		SET @description = CONCAT(@made_by, ' cre� una transferencia hacia ', @warehouse_destination, '.')

		INSERT INTO @table VALUES (@made_at, @made_by,@transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END
	
	IF @received_at IS NOT NULL
	BEGIN 
		SET @made_at = @received_at
		SET @made_by = @received_by
		SET @warehouse = @transfer_warehouse_destination
		SET @warehouse_origen = @transfer_warehouse_origen
		SET @warehouse_destination = @transfer_warehouse_destination
		SET @description = CONCAT(@made_by, ' recibi� la transferencia.')

		INSERT INTO @table VALUES (@made_at, @made_by,@transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END
	
	IF @canceled_at IS NOT NULL
	BEGIN 
		SET @made_at = @canceled_at
		SET @made_by = @canceled_by
		SET @warehouse = @transfer_warehouse_origen
		SET @warehouse_origen = @transfer_warehouse_origen
		SET @warehouse_destination = @transfer_warehouse_destination
		SET @description = CONCAT(@made_by, ' cancel� la transferencia.')

		INSERT INTO @table VALUES (@made_at, @made_by,@transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END

	IF @order_canceled_at IS NOT NULL
	BEGIN 
		SET @made_at = @order_canceled_at
		SET @made_by = @order_canceled_by
		SET @warehouse = @order_warehouse_origen
		SET @warehouse_origen = @order_warehouse_origen
		SET @warehouse_destination = @order_warehouse_destination
		SET @description = CONCAT(@made_by, ' cancel� la orden ', @order_num, '.')

		INSERT INTO @table VALUES (@made_at, @made_by,@transfer_id, @transfer_num, @order_id, @order_num, @warehouse, @warehouse_origen, @warehouse_destination, @description)
	END

		fetch [cursor] into  
		@transfer_id, 
		@transfer_num, 
		@order_id,
		@order_num,
		@ordered_at,
		@ordered_by,
		@ordered_for,

		@order_canceled_at,
		@order_canceled_by,

		@transferred_at,
		@transferred_by,
		@received_at,
		@received_by,
		@canceled_at,
		@canceled_by,
		@requested_at,
		@requested_by,
		@order_warehouse_origen,
		@order_warehouse_destination,
		@transfer_warehouse_origen,
		@transfer_warehouse_destination
	END

	close [cursor]	
	deallocate [cursor]	 
	return 
end 
go




/**
* TRAKING ORDERS VIEW
*
*/
IF OBJECT_ID ('tmk_tracking_orders_view') IS NOT NULL
BEGIN
	DROP VIEW tmk_tracking_orders_view
END
GO

CREATE VIEW tmk_tracking_orders_view AS 
	select * from tmk_tracking_orders_func()
GO

select * from tmk_tracking_orders_view where transfer_id = 14

