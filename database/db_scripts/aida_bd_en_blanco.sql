-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2020 at 05:00 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aida_bd`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `a_view`
-- (See below for the actual view)
--
CREATE TABLE `a_view` (
`id` int(10) unsigned
,`ordenes` varchar(255)
,`provider_id` int(10) unsigned
,`require_date` timestamp
,`warehouse_id` int(10) unsigned
,`subtotal` decimal(10,2)
,`tax` decimal(10,2)
,`discount` decimal(10,2)
,`observation` varchar(255)
,`created_by` int(11)
,`updated_by` int(11)
,`deleted_by` int(11)
,`confirmed_at` timestamp
,`canceled_at` timestamp
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
,`order_status_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `bx_view`
-- (See below for the actual view)
--
CREATE TABLE `bx_view` (
`article_id` int(10) unsigned
,`purchase_order_id` int(10) unsigned
,`quantity` decimal(32,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `cx_view`
-- (See below for the actual view)
--
CREATE TABLE `cx_view` (
`article_id` int(10) unsigned
,`purchase_order_id` int(10) unsigned
,`Cantpend` decimal(32,2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `dx_view`
-- (See below for the actual view)
--
CREATE TABLE `dx_view` (
`cantrec` decimal(32,2)
,`purchase_order_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `external_movement_view`
-- (See below for the actual view)
--
CREATE TABLE `external_movement_view` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(100)
,`article` varchar(100)
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(11)
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(10,0)
,`reserved` binary(0)
,`available_quantity` binary(0)
,`active` int(1)
,`last_movement` binary(0)
,`created_by` int(11)
,`updated_by` int(11)
,`deleted_by` int(11)
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ex_view`
-- (See below for the actual view)
--
CREATE TABLE `ex_view` (
`cantpen2` decimal(41,0)
,`movementable_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `fx_view`
-- (See below for the actual view)
--
CREATE TABLE `fx_view` (
`article_id` int(10) unsigned
,`quantityBod` decimal(41,0)
,`movementable_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `gx_view`
-- (See below for the actual view)
--
CREATE TABLE `gx_view` (
`creado` timestamp
,`article_id` int(10) unsigned
,`purchase_order_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `logtriggers`
--

CREATE TABLE `logtriggers` (
  `id` int(11) NOT NULL,
  `cadena` varchar(550) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_03_31_132911_create_type_providers_table', 1),
('2016_03_31_132919_create_providers_table', 1),
('2016_03_31_132928_create_brands_table', 1),
('2016_03_31_133002_create_categories_table', 1),
('2016_03_31_133041_create_classes_table', 1),
('2016_03_31_133049_create_articles_table', 1),
('2016_03_31_133117_create_regions_table', 1),
('2016_03_31_133145_create_provinces_table', 1),
('2016_03_31_133146_create_centers_table', 1),
('2016_03_31_133202_create_bodegas_table', 1),
('2016_03_31_133235_create_article_bodegas_table', 1),
('2016_04_28_162845_create_inventories_table', 1),
('2016_04_28_164908_create_inventory_details_table', 1),
('2016_05_07_141319_create_measurement_units_table', 1),
('2016_07_26_162032_create_roles_table', 1),
('2016_07_26_174124_add_column_role_id_to_table_users', 1),
('2016_08_06_131646_create_article_details_table', 1),
('2016_08_06_134607_create_states_table', 1),
('2016_08_06_135332_drop_constrain_brand_to_provider_id', 1),
('2016_08_06_135816_create_provider_brands_table', 1),
('2016_08_18_175013_create_user_bodegas_table', 1),
('2016_08_23_145214_create_type_movements_table', 1),
('2016_08_23_145320_create_movements_table', 1),
('2016_08_23_160432_add_constrain_movement_id_on_article_bodegas', 1),
('2016_08_23_161757_create_transfer_table', 1),
('2016_08_24_130246_create_orders_table', 1),
('2016_08_29_102226_create_transfer_details_table', 1),
('2016_08_29_103039_create_devolution_purchases_table', 1),
('2016_08_29_110551_create_devolution_purchase_details_table', 1),
('2016_08_29_111245_create_devolution_transfer_table', 1),
('2016_08_29_111849_create_order_details_table', 1),
('2016_09_19_101925_create_purchases_table', 1),
('2016_09_19_101955_create_purchase_details_table', 1),
('2016_09_28_171959_create_taxes_table', 1),
('2016_09_28_172105_create_order_status_table', 1),
('2016_09_28_172149_create_purchase_status_table', 1),
('2016_10_24_165840_create_purchase_receipt_table', 1),
('2016_10_25_102922_create_purchase_bodega_table', 1),
('2016_10_27_152620_add_constain_order_status_id_to_puchase_orders_table', 1),
('2016_11_16_115208_create_receipts_table', 1),
('2016_11_16_115707_add_constrain_receipt_id_to_purchase_receipts', 1),
('2016_11_17_165925_create_article_master_table', 1),
('2016_11_19_164113_create_attributes_table', 1),
('2016_11_19_164155_create_attribute_values_table', 1),
('2016_11_19_165010_create_variants_table', 1),
('2016_11_23_104457_add_constrain_article_master_id_toarticles_table', 1),
('2016_12_15_111206_create_article_master_attribute_table', 1),
('2016_12_23_153505_create_transfer_article_bodega_table', 1),
('2016_12_23_154447_create_transfer_receipt_table', 1),
('2016_12_23_155226_create_transfer_receipt_details_table', 1),
('2016_12_23_155818_create_transfer_receipt_article_bodega_table', 1),
('2016_12_23_165843_create_transfer_order_transfer_table', 1),
('2016_12_26_120106_create_article_warehouses_table', 1),
('2017_01_05_042149_create_article_warehouse_details_table', 1),
('2017_01_05_110437_create_article_master_warehouses_table', 1),
('2017_01_05_121018_add_constrain_article_master_warehouse', 1),
('2017_01_18_165414_add_contrain_status_id_to_order_table', 1),
('2017_01_28_141542_add_contrain_status_id_to_transfers_table', 1),
('2017_02_01_094335_create_external_movements_table', 1),
('2017_02_01_094457_create_external_movement_details_table', 1),
('2017_03_10_115425_create_activities_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_regiones`
--

CREATE TABLE `tbl_regiones` (
  `ID_REGION` int(11) NOT NULL,
  `DESCR_REGION` varchar(50) NOT NULL,
  `EMAIL` varchar(550) NOT NULL,
  `GERENTE` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_regiones`
--

INSERT INTO `tbl_regiones` (`ID_REGION`, `DESCR_REGION`, `EMAIL`, `GERENTE`) VALUES
(1, 'REGION 1', 'ventas.region.1@telefonica.com', 15),
(2, 'REGION 2', 'adminR2.PA@telefonica.com', 14),
(3, 'REGION 3', 'AdminR3.PA@telefonica.com', 6),
(4, 'REGION 4', 'AdminComercialR4.PA@telefonica.com', 13),
(5, 'REGION 5', 'adminR5.PA@telefonica.com', 14),
(0, 'NO INFORMADO', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sucursales`
--

CREATE TABLE `tbl_sucursales` (
  `ID_SUCURSAL` int(11) NOT NULL,
  `DESCR_SUCURSAL` varchar(200) NOT NULL,
  `ID_ESTADO` int(11) NOT NULL,
  `ID_REGION` int(11) NOT NULL,
  `FLAG_TK` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sucursales`
--

INSERT INTO `tbl_sucursales` (`ID_SUCURSAL`, `DESCR_SUCURSAL`, `ID_ESTADO`, `ID_REGION`, `FLAG_TK`) VALUES
(1, 'AGUADULCE', 1, 3, 'S'),
(2, 'ALBROOK CINE', 1, 1, 'S'),
(3, 'ALBROOK PINGUINO', 1, 1, 'S'),
(4, 'ALBROOK TERMINAL', 1, 1, 'S'),
(5, 'ALMIRANTE', 1, 4, 'S'),
(6, 'ATENTO', 3, 0, ''),
(7, 'BUGABA', 1, 4, 'S'),
(8, 'CALIDONIA', 3, 0, ''),
(9, 'CELLREY', 3, 0, ''),
(10, 'CHANGUINOLA', 1, 4, 'S'),
(11, 'CHIRIQUÍ OBALDÍA', 3, 4, ''),
(12, 'CHITRÉ', 1, 3, 'S'),
(13, 'CHORRERA', 1, 5, 'S'),
(14, 'COLÓN 2000', 1, 2, 'S'),
(15, 'COLÓN AVE. CENTRAL', 1, 2, 'S'),
(16, 'CORONADO', 1, 5, 'S'),
(17, 'COSTA DEL ESTE', 1, 1, 'S'),
(18, 'DAVID CENTRO', 1, 4, 'S'),
(19, 'DAVID TERRONAL', 1, 4, 'S'),
(20, 'EL DORADO', 1, 1, 'S'),
(21, 'EL VALLE', 1, 5, 'S'),
(22, 'GRAN ESTACIÓN DE S.M.', 3, 0, ''),
(23, 'ISLA COLÓN', 1, 4, 'S'),
(24, 'KIOSCO 4 ALTOS', 1, 2, 'S'),
(25, 'KIOSCO ALBROOK CONWAY', 1, 1, 'S'),
(26, 'KIOSCO ALBROOK COSTO', 1, 1, 'S'),
(27, 'KIOSCO ALBROOK FELIX', 3, 1, ''),
(28, 'KIOSCO ALBROOK MALL MP', 1, 1, 'S'),
(29, 'KIOSCO ALBROOK TERMINAL', 1, 1, 'S'),
(30, 'KIOSCO CHIRIQUÍ TERMINAL', 3, 4, ''),
(31, 'KIOSCO DORADO', 3, 0, ''),
(32, 'KIOSCO GALERÍA', 3, 0, ''),
(33, 'KIOSCO LOS ANDES', 1, 1, 'S'),
(34, 'KIOSCO LOS ANDES MALL', 3, 1, ''),
(35, 'KIOSCO METROMALL', 1, 1, 'S'),
(36, 'KIOSCO MULTICENTRO', 1, 1, 'S'),
(37, 'KIOSCO MULTIPLAZA', 1, 1, 'S'),
(38, 'KIOSCO REDES SOCIALES', 1, 1, 'S'),
(39, 'KIOSCO TERMINAL CHITRÉ', 1, 3, 'S'),
(40, 'KIOSCO TERMINAL DE COLÓN', 3, 2, ''),
(41, 'KIOSCO TERMINAL SANTIAGO', 1, 3, 'S'),
(42, 'KIOSCO TERMINAL SONÁ', 1, 3, 'S'),
(43, 'KIOSCO WESTLAND MALL', 1, 5, 'S'),
(44, 'LA DOÑA', 1, 1, 'S'),
(45, 'LAS TABLAS', 1, 3, 'S'),
(46, 'LOS ANDES', 1, 1, 'S'),
(47, 'LOS PUEBLOS', 3, 0, ''),
(48, 'METETÍ', 1, 1, 'S'),
(49, 'METROMALL', 1, 1, 'S'),
(50, 'MULTICANAL', 3, 0, ''),
(51, 'MULTIPLAZA', 1, 1, 'S'),
(52, 'NO LABORA', 3, 0, ''),
(53, 'OBARRIO', 3, 0, ''),
(54, 'PEATONAL', 3, 0, ''),
(55, 'PENONOMÉ', 1, 3, 'S'),
(56, 'PLAZA CONCORDIA', 1, 1, 'S'),
(57, 'SANTIAGO AVE. CENTRAL', 1, 3, 'S'),
(58, 'TIENDA WESTLAND MALL', 1, 5, 'S'),
(60, 'KIOSCO BOQUETE', 1, 4, 'S'),
(61, 'COOPERATIVA DE PROFESIONALES', 1, 1, 'S'),
(62, 'SIN ESPECIFICAR', 1, 0, 'S'),
(63, 'COSTA DEL ESTE OFICINA', 1, 1, ''),
(64, 'BOC ', 1, 1, ''),
(65, 'TIENDA MALL PASEO ARRAIJAN', 1, 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `tmk_activities`
--

CREATE TABLE `tmk_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `modelable_id` int(11) DEFAULT NULL,
  `modelable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('success','info','warning','error') COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_ajustes_general`
--

CREATE TABLE `tmk_ajustes_general` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmk_ajustes_general`
--

INSERT INTO `tmk_ajustes_general` (`id`, `descripcion`, `estado`) VALUES
(1, 'recibir compra directa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_articles`
--

CREATE TABLE `tmk_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internal_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_code` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_cost_initial` decimal(10,2) DEFAULT 0.00,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `article_master_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_articles_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_articles_view` (
`article_id` int(10) unsigned
,`barcode` varchar(255)
,`internal_reference` varchar(255)
,`description` text
,`article_master_id` int(10) unsigned
,`article_master` varchar(100)
,`article_master_barcode` varchar(100)
,`article_master_internal_reference` varchar(100)
,`image` varchar(255)
,`long_description` text
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`gestion` int(11)
,`avg_unit_cost` decimal(6,2)
,`created_by` int(11)
,`updated_by` int(11)
,`deleted_by` int(11)
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_bodegas`
--

CREATE TABLE `tmk_article_bodegas` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `bodega_id` int(10) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL DEFAULT 0,
  `quantity_exit` bigint(20) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `initial` tinyint(1) NOT NULL DEFAULT 0,
  `movementable_id` int(11) DEFAULT NULL,
  `movementable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `movement_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `tmk_article_bodegas`
--
DELIMITER $$
CREATE TRIGGER `TransactNotifycation` AFTER INSERT ON `tmk_article_bodegas` FOR EACH ROW IF EXISTS(SELECT * FROM tmk_article_warehouses
                      WHERE article_id = NEW.article_id AND article_master_warehouse_id = (SELECT id_2 FROM aida_bd.tmk_id_articles where id_1=NEW.article_id and warehouse_id=NEW.bodega_id LIMIT 1)
             
             ) THEN
             
INSERT INTO `LOGTRIGGERS`(`id`, `cadena`) VALUES  (NULL,CONCAT('ARTICLE: ',NEW.article_id,' BODEGA : ',NEW.bodega_id,' CANTIDAD : ',NEW.quantity)) ;            
             
             
IF NEW.quantity = 0 THEN
set @Stock_warehouse :=(SELECT case when (A.stock>0) then (a.stock-NEW.quantity_exit) else 0 end as value FROM tmk_article_warehouses AS A INNER JOIN tmk_article_master_warehouses AS B
						ON B.id = A.article_master_warehouse_id 
                        WHERE A.article_id=NEW.article_id and B.warehouse_id=NEW.bodega_id limit 1);
UPDATE tmk_article_warehouses  AS UPD
SET UPD.stock = (@Stock_warehouse)
, UPD.updated_at = NEW.updated_at 
WHERE UPD.article_id = NEW.article_id AND UPD.article_master_warehouse_id = (SELECT id_2 FROM aida_bd.tmk_id_articles where id_1=NEW.article_id and warehouse_id=NEW.bodega_id LIMIT 1);
ELSE
set @Stock_warehouse_2 :=(SELECT  (a.stock+NEW.quantity) value FROM tmk_article_warehouses AS A INNER JOIN tmk_article_master_warehouses AS B
						ON B.id = A.article_master_warehouse_id 
                        WHERE A.article_id=NEW.article_id and B.warehouse_id=NEW.bodega_id limit 1);
UPDATE tmk_article_warehouses  AS UPD
SET UPD.stock = (@Stock_warehouse_2)
, UPD.updated_at = NEW.updated_at 
WHERE UPD.article_id = NEW.article_id AND UPD.article_master_warehouse_id = (SELECT id_2 FROM aida_bd.tmk_id_articles where id_1=NEW.article_id and warehouse_id=NEW.bodega_id LIMIT 1);

          
END IF;

ELSE

IF NEW.quantity> 0 THEN

IF EXISTS(SELECT * FROM tmk_article_master_warehouses WHERE article_master_id = (SELECT article_master_id FROM tmk_articles WHERE id = NEW.article_id) AND warehouse_id = NEW.bodega_id) THEN



			INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id,(SELECT id_2 FROM aida_bd.tmk_id_articles where id_1=NEW.article_id and warehouse_id=NEW.bodega_id),NEW.quantity, NEW.updated_at, NEW.updated_at);

ELSE
INSERT INTO tmk_article_master_warehouses (article_master_id, warehouse_id) VALUES ((SELECT article_master_id FROM tmk_articles WHERE id = NEW.article_id LIMIT 1), NEW.bodega_id);
				
INSERT INTO tmk_article_warehouses (article_id, article_master_warehouse_id, stock, created_at, updated_at) 
			VALUES ( NEW.article_id, (SELECT id_2 FROM aida_bd.tmk_id_articles where id_1=NEW.article_id and warehouse_id=NEW.bodega_id), NEW.quantity,  NEW.updated_at, NEW.updated_at);
            
END IF;




END IF;
 
 
 
END IF
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_article_bodegas_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_article_bodegas_view` (
`article_master_id` int(10) unsigned
,`article_id` int(10) unsigned
,`bodega_id` int(10) unsigned
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`entry` decimal(41,0)
,`exit` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_masters`
--

CREATE TABLE `tmk_article_masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `barcode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internal_reference` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min_stock` int(10) UNSIGNED DEFAULT 1,
  `max_stock` int(10) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `measurement_unit_id` int(10) UNSIGNED NOT NULL,
  `gestion` int(11) NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_master_attributes`
--

CREATE TABLE `tmk_article_master_attributes` (
  `article_master_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_master_warehouses`
--

CREATE TABLE `tmk_article_master_warehouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_master_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `min_stock` int(10) UNSIGNED DEFAULT NULL,
  `max_stock` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_article_master_warehouses_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_article_master_warehouses_view` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(44,0)
,`s` decimal(32,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(44,0)
,`active` tinyint(1)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_warehouses`
--

CREATE TABLE `tmk_article_warehouses` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `article_master_warehouse_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_article_warehouse_details`
--

CREATE TABLE `tmk_article_warehouse_details` (
  `article_warehouse_id` int(10) UNSIGNED NOT NULL,
  `stock` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_attributes`
--

CREATE TABLE `tmk_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_attributes`
--

INSERT INTO `tmk_attributes` (`id`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'COLOR', 1, 1, NULL, '2017-01-20 00:16:21', '2019-08-04 22:38:26', NULL),
(2, 'TALLA', 1, 1, NULL, '2017-01-20 00:16:21', '2017-02-23 18:27:00', NULL),
(3, 'SEXO', 1, 1, NULL, '2017-01-20 00:16:21', '2017-02-23 18:27:00', NULL),
(4, 'DIMENCIONES', 1, 1, NULL, '2017-01-20 00:16:21', '2019-08-04 22:38:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_attribute_values`
--

CREATE TABLE `tmk_attribute_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_attribute_values`
--

INSERT INTO `tmk_attribute_values` (`id`, `attribute_id`, `description`, `value`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'BLANCO', 'Blanco', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(2, 1, 'AZUL OSCURO', 'Amarillo', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(3, 1, 'VERDE', 'Verde', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(4, 1, 'AZUL/NEGRO', 'Azul', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(5, 1, 'Fucsia', 'Fucsia', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(6, 1, 'AZUL/NEGRO/GRIS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(7, 2, 'Small', 'Small', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(8, 2, 'Medium', 'Medium', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(9, 2, 'Large', 'Large', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(10, 2, 'xLarge', 'xLarge', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(11, 3, 'Hombre', 'Hombre', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(12, 3, 'Mujer', 'Mujer', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(13, 1, 'AZUL/VERDE/BLANCO', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(14, 1, 'CELESTE', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(15, 1, 'AZUL', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(16, 1, 'ROSADO', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(17, 1, 'TRANSPARENTE', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(18, 1, 'AZUL/ROSADO', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(19, 1, 'NEGRO', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(20, 1, 'GRIS/AZUL', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(21, 4, 'ESTANDAR', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(22, 4, '3X3 METROS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(23, 4, '7 METROS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(24, 4, 'CHICOS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(25, 4, '4X4 METROS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(26, 4, '4X8 METROS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(27, 4, '6 METROS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(28, 4, '9 PULGADAS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(29, 4, '12 PULGADAS', 'Negro', 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_bodegas`
--

CREATE TABLE `tmk_bodegas` (
  `id` int(10) UNSIGNED NOT NULL,
  `sucursal_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main` int(11) DEFAULT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT 1,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_bodegas`
--

INSERT INTO `tmk_bodegas` (`id`, `sucursal_id`, `description`, `colour`, `main`, `editable`, `active`, `observation`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 57, 'SANTIAGO', '', 0, 1, 1, '', 0, 2147483647, 2147483647, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 46, 'BODEGA CENTRAL', NULL, 1, 1, 1, NULL, NULL, NULL, NULL, '2019-07-18 05:27:18', '2019-07-18 05:27:18', NULL),
(3, 53, 'BODEGA 12 OCTUBRE', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2019-08-14 15:39:58', '2019-08-14 15:39:58', NULL),
(4, 1, 'BODEGA AGUADULCE', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, '2019-08-23 01:45:45', '2019-08-23 01:45:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_brands`
--

CREATE TABLE `tmk_brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_brands`
--

INSERT INTO `tmk_brands` (`id`, `description`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Desconocido', 1, '2016-09-16 19:45:18', '2016-02-22 22:17:38', NULL),
(2, 'MOVISTAR', 1, '2017-01-20 00:32:06', '2017-01-20 00:32:06', NULL),
(3, 'UNICRECE', 1, '2017-01-31 16:47:51', '2017-01-31 16:47:51', NULL),
(4, 'RECORD', 1, '2017-02-23 18:20:58', '2017-02-23 18:20:58', NULL),
(5, 'ARGO', 1, '2019-08-13 00:37:15', '2019-08-13 00:37:15', NULL),
(6, 'desconocido', 1, '2019-05-01 22:52:15', '2019-04-14 02:06:01', NULL),
(7, 'Adidas', 1, '2019-02-06 08:07:17', '2019-08-29 14:13:14', NULL),
(8, 'Marca 2', 1, '2019-04-25 20:23:47', '2019-03-02 16:38:20', NULL),
(9, 'Marca 3', 1, '2019-06-02 12:43:58', '2018-09-22 04:17:59', NULL),
(10, 'Marca 4', 1, '2018-09-29 10:28:17', '2018-10-29 21:58:47', NULL),
(11, 'Marca 5', 1, '2019-02-23 19:49:03', '2019-05-26 01:48:00', NULL),
(12, 'ASUS', 1, '2019-09-25 13:34:59', '2019-09-25 13:34:59', NULL),
(13, 'DELL', 1, '2019-09-25 14:17:02', '2019-09-25 14:17:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_categories`
--

CREATE TABLE `tmk_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_categories`
--

INSERT INTO `tmk_categories` (`id`, `description`, `active`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Desconocido', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(2, 'Promocional', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(3, 'Oficina', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(4, 'Promocionales', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(5, 'Vestuarios', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(6, 'Accesorios de computadoras', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(7, 'Flayers', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(8, 'Inflables', 1, 1, 1, NULL, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(9, 'Estructuras', 1, NULL, NULL, NULL, '2017-01-31 17:45:22', '2017-01-31 17:45:22', NULL),
(10, 'P.OPE PROMOCIONES', 1, NULL, NULL, NULL, '2017-02-10 01:05:35', '2017-02-10 01:05:35', NULL),
(11, 'BATERIAS', 1, 1, 1, NULL, '2017-02-23 18:20:47', '2017-02-23 18:20:47', NULL),
(12, 'MATERIALES', 1, 1, 1, NULL, '2019-08-13 00:36:51', '2019-08-13 00:36:51', NULL),
(13, 'ARGO', 1, 1, 1, NULL, '2019-08-13 00:37:04', '2019-08-13 00:37:04', NULL),
(14, 'ROPA', 1, 1, 1, NULL, '2019-09-01 05:05:33', '2019-09-01 05:05:33', NULL),
(15, 'COCINA', 1, 1, 1, NULL, '2019-09-01 08:02:00', '2019-09-01 08:02:00', NULL),
(16, 'PRUEBA2', 1, 1, 1, NULL, '2019-09-03 04:06:57', '2019-09-03 04:06:57', NULL),
(17, 'COMPUTADORAS', 1, 1, 1, NULL, '2019-09-25 13:34:53', '2019-09-25 13:34:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_classes`
--

CREATE TABLE `tmk_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_clientes_contactos`
--

CREATE TABLE `tmk_clientes_contactos` (
  `id` int(11) NOT NULL,
  `con_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_movil` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_correo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `con_puesto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `con_tipo_contacto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_clientes_empresas`
--

CREATE TABLE `tmk_clientes_empresas` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` varchar(50) DEFAULT NULL,
  `Nombre` varchar(255) NOT NULL,
  `direccion1` longtext DEFAULT NULL,
  `direccion2` longtext DEFAULT NULL,
  `id_pais` int(11) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  `codigo_postal` int(11) DEFAULT NULL,
  `Ruc` varchar(255) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `movil` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `titulo` varchar(15) DEFAULT NULL,
  `pagina_web` varchar(255) DEFAULT NULL,
  `etiquetas` varchar(255) DEFAULT NULL,
  `cliente` int(11) DEFAULT NULL,
  `proveedor` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `sucursal_id` int(11) DEFAULT NULL,
  `referencia` varchar(255) DEFAULT NULL,
  `notas` text DEFAULT NULL,
  `creado` date DEFAULT NULL,
  `actualizado` date DEFAULT NULL,
  `eliminado` date DEFAULT NULL,
  `agregado_por` varchar(255) DEFAULT NULL,
  `actualizado_por` varchar(255) DEFAULT NULL,
  `eliminado_por` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_clientes_individual`
--

CREATE TABLE `tmk_clientes_individual` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `area_trabajo` varchar(50) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_clientes_sucursales`
--

CREATE TABLE `tmk_clientes_sucursales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `movil` varchar(50) DEFAULT NULL,
  `correo` varchar(255) NOT NULL,
  `image` text DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_clientes_tipo_contactos`
--

CREATE TABLE `tmk_clientes_tipo_contactos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmk_clientes_tipo_contactos`
--

INSERT INTO `tmk_clientes_tipo_contactos` (`id`, `descripcion`) VALUES
(1, 'Contacto'),
(2, 'Direccion de factura'),
(3, 'Direccion de entrega'),
(4, 'otras');

-- --------------------------------------------------------

--
-- Table structure for table `tmk_cliente_tipos`
--

CREATE TABLE `tmk_cliente_tipos` (
  `id` int(11) NOT NULL,
  `id_tipo_cliente` int(11) NOT NULL,
  `descripcion` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmk_cliente_tipos`
--

INSERT INTO `tmk_cliente_tipos` (`id`, `id_tipo_cliente`, `descripcion`) VALUES
(1, 1, 'Individual'),
(2, 2, 'Empresa');

-- --------------------------------------------------------

--
-- Table structure for table `tmk_devolution_purchases`
--

CREATE TABLE `tmk_devolution_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_devolution_purchase_details`
--

CREATE TABLE `tmk_devolution_purchase_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `devolution_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `reference_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_devolution_transfers`
--

CREATE TABLE `tmk_devolution_transfers` (
  `id` int(10) UNSIGNED NOT NULL,
  `transfer_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_entradas_salidas_inicio`
-- (See below for the actual view)
--
CREATE TABLE `tmk_entradas_salidas_inicio` (
`fecha` date
,`movement_type` varchar(100)
,`movement` varchar(100)
,`movement_code` varchar(5)
,`cantidad` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_external_movements`
--

CREATE TABLE `tmk_external_movements` (
  `id` int(10) UNSIGNED NOT NULL,
  `external_movement_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `agent_id` int(10) UNSIGNED NOT NULL,
  `external_agent_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `movement` enum('entry','exit') COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `deleted_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_external_movement_details`
--

CREATE TABLE `tmk_external_movement_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `external_movement_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_external_movement_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_external_movement_view` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(100)
,`article` varchar(100)
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(11)
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(32,0)
,`reserved` binary(0)
,`available_quantity` binary(0)
,`active` int(1)
,`last_movement` binary(0)
,`created_by` int(11)
,`updated_by` int(11)
,`deleted_by` int(11)
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_id_articles`
-- (See below for the actual view)
--
CREATE TABLE `tmk_id_articles` (
`id_1` int(10) unsigned
,`article_master_id` int(10) unsigned
,`id_2` int(10) unsigned
,`warehouse_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_inventario_general`
-- (See below for the actual view)
--
CREATE TABLE `tmk_inventario_general` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(43,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(43,0)
,`active` tinyint(1)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_inventario_general_union`
-- (See below for the actual view)
--
CREATE TABLE `tmk_inventario_general_union` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` longtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(43,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(43,0)
,`active` tinyint(4)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_inventario_global`
-- (See below for the actual view)
--
CREATE TABLE `tmk_inventario_global` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(43,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(43,0)
,`active` tinyint(1)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_inventario_inicio`
-- (See below for the actual view)
--
CREATE TABLE `tmk_inventario_inicio` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(43,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(43,0)
,`active` tinyint(1)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
,`fecha` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_inventario_stock`
-- (See below for the actual view)
--
CREATE TABLE `tmk_inventario_stock` (
`article_master_id` int(10) unsigned
,`barcode` varchar(100)
,`article_master` varchar(100)
,`image` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`brand` varchar(255)
,`category` varchar(255)
,`measurement_unit` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`stock` decimal(43,0)
,`reserved` decimal(32,0)
,`available_quantity` decimal(43,0)
,`active` tinyint(1)
,`last_movement` timestamp
,`created_by` varchar(201)
,`created_at` timestamp
,`updated_by` varchar(201)
,`updated_at` timestamp
,`deleted_by` varchar(201)
,`deleted_at` timestamp
,`fecha` date
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_inventories`
--

CREATE TABLE `tmk_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `quantity_items` bigint(20) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_inventory_details`
--

CREATE TABLE `tmk_inventory_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_bodega_id` int(10) UNSIGNED NOT NULL,
  `inventory_id` int(10) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `unit_cost` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_lotes`
--

CREATE TABLE `tmk_lotes` (
  `id` int(11) NOT NULL,
  `item_id` varchar(50) NOT NULL,
  `internal_reference` varchar(100) NOT NULL,
  `num_doc_temp` varchar(255) NOT NULL,
  `num_doc_temp_out` varchar(255) DEFAULT NULL,
  `lote` varchar(255) NOT NULL,
  `cantidad_in` int(11) NOT NULL,
  `cantidad_out` int(11) DEFAULT NULL,
  `article_id` int(11) NOT NULL,
  `status_doc` int(11) NOT NULL,
  `status_doc_out` int(11) NOT NULL,
  `status_in` int(11) NOT NULL,
  `status_out` int(11) NOT NULL,
  `caducidad` date NOT NULL,
  `creado_el` date NOT NULL,
  `salida_el` date NOT NULL,
  `actualizado_el` date NOT NULL,
  `eliminado_el` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_lotes_all_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_lotes_all_view` (
`existe` varchar(50)
,`id` int(11)
,`item_id` varchar(50)
,`internal_reference` varchar(100)
,`num_doc_temp` varchar(255)
,`lote` varchar(255)
,`cantidad` bigint(12)
,`cantidad_out` int(11)
,`caducidad` date
,`status_doc` int(11)
,`status_in` int(11)
,`num_doc_temp_out` varchar(255)
,`article_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_lotes_create_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_lotes_create_view` (
`id` int(11)
,`item_id` varchar(50)
,`internal_reference` varchar(100)
,`num_doc_temp_out` varchar(255)
,`num_doc_temp` varchar(255)
,`lote` varchar(255)
,`cantidad_disponible` decimal(33,0)
,`caducidad` date
,`status_doc` int(11)
,`status_in` int(11)
,`article_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_lotes_out`
--

CREATE TABLE `tmk_lotes_out` (
  `id` int(11) NOT NULL,
  `item_id` varchar(50) NOT NULL,
  `internal_reference` varchar(100) NOT NULL,
  `num_doc_temp` varchar(255) NOT NULL,
  `lote` varchar(255) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `status_doc` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `caducidad` date NOT NULL,
  `creado_el` date NOT NULL,
  `salida_el` date NOT NULL,
  `actualizado_el` date NOT NULL,
  `eliminado_el` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_measurement_units`
--

CREATE TABLE `tmk_measurement_units` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_measurement_units`
--

INSERT INTO `tmk_measurement_units` (`id`, `description`, `unity`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Unidad', 'U', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(2, 'Caja 6', '6/1', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(3, 'Caja 12', '12/1', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(4, 'Caja 24', '24/1', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(5, 'Kilogramos', 'Kg', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(6, 'Gramos', 'g', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(7, 'Libras', 'lb', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(8, 'Toneladas', 'T', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(9, 'Milimetros cubicos', 'mm3', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(10, 'Centimetros cubicos', 'cm3', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(11, 'Metros', 'm', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(12, 'Centimetros', 'cm', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(13, 'Milimetros', 'mm', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(14, 'Fahrenheit', '°F', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(15, 'Kelvi', '°K', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(16, 'Celsius', '°C', 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(17, 'PAKETE', '', 1, '2019-09-01 05:12:50', '2019-09-01 05:12:50', NULL),
(18, 'PAKETED', '', 1, '2019-09-01 05:12:56', '2019-09-01 05:12:56', NULL),
(19, 'PAKETEDDSDSDS', '', 1, '2019-09-01 05:13:03', '2019-09-01 05:13:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_movements`
--

CREATE TABLE `tmk_movements` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_movement_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `concerned` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_movements_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_movements_view` (
`id` int(10) unsigned
,`created_at` timestamp
,`movement_type` varchar(100)
,`movement` varchar(100)
,`movement_code` varchar(5)
,`movement_tag` varchar(16)
,`article_master_id` int(10) unsigned
,`article_master` varchar(100)
,`image` varchar(255)
,`barcode` varchar(100)
,`article_id` int(10) unsigned
,`article` mediumtext
,`internal_reference` varchar(255)
,`unity` varchar(255)
,`quantity` bigint(20)
,`quantity_exit` bigint(20)
,`unit_cost` decimal(10,2)
,`reference_id` int(10) unsigned
,`reference` varchar(255)
,`desde` varchar(201)
,`para` varchar(201)
,`initial` tinyint(1)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_movement_references_sub_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_movement_references_sub_view` (
`reference_id` int(10) unsigned
,`reference` varchar(255)
,`article_id` int(10) unsigned
,`quantity` decimal(10,0)
,`unit_cost` decimal(10,2)
,`desde` varchar(201)
,`para` varchar(201)
,`movement_tag` varchar(16)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_movement_references_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_movement_references_view` (
`reference_id` int(10) unsigned
,`reference` varchar(255)
,`article_id` int(10) unsigned
,`quantity` decimal(10,0)
,`unit_cost` decimal(10,2)
,`desde` varchar(201)
,`para` varchar(201)
,`movement_tag` varchar(16)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_orders`
--

CREATE TABLE `tmk_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_user_id` int(11) DEFAULT NULL,
  `for_user_id` int(11) DEFAULT NULL,
  `require_date` timestamp NULL DEFAULT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sucursal_destination` int(10) UNSIGNED DEFAULT NULL,
  `warehouse_origen` int(10) UNSIGNED DEFAULT NULL,
  `warehouse_destination` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  `ordered_at` timestamp NULL DEFAULT NULL,
  `canceled_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_order_details`
--

CREATE TABLE `tmk_order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `pending` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_order_details_sub_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_order_details_sub_view` (
`id` int(10) unsigned
,`order_id` int(10) unsigned
,`order_num` varchar(255)
,`article_id` int(10) unsigned
,`quantity` int(11)
,`pending` decimal(33,0)
,`transfer_id` int(10) unsigned
,`transfer_num` varchar(255)
,`transferred` decimal(32,0)
,`received` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_order_details_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_order_details_view` (
`id` int(10) unsigned
,`order_id` int(10) unsigned
,`order_num` varchar(255)
,`article_id` int(10) unsigned
,`quantity` int(11)
,`pending` decimal(33,0)
,`transferred` decimal(32,0)
,`received` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_order_status`
--

CREATE TABLE `tmk_order_status` (
  `id` int(11) NOT NULL,
  `acronym` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL DEFAULT 1,
  `observation` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `colour` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_paises`
--

CREATE TABLE `tmk_paises` (
  `id` int(11) NOT NULL,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmk_paises`
--

INSERT INTO `tmk_paises` (`id`, `iso`, `nombre`) VALUES
(1, 'AF', 'Afganistán'),
(2, 'AX', 'Islas Gland'),
(3, 'AL', 'Albania'),
(4, 'DE', 'Alemania'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antártida'),
(9, 'AG', 'Antigua y Barbuda'),
(10, 'AN', 'Antillas Holandesas'),
(11, 'SA', 'Arabia Saudí'),
(12, 'DZ', 'Argelia'),
(13, 'AR', 'Argentina'),
(14, 'AM', 'Armenia'),
(15, 'AW', 'Aruba'),
(16, 'AU', 'Australia'),
(17, 'AT', 'Austria'),
(18, 'AZ', 'Azerbaiyán'),
(19, 'BS', 'Bahamas'),
(20, 'BH', 'Bahréin'),
(21, 'BD', 'Bangladesh'),
(22, 'BB', 'Barbados'),
(23, 'BY', 'Bielorrusia'),
(24, 'BE', 'Bélgica'),
(25, 'BZ', 'Belice'),
(26, 'BJ', 'Benin'),
(27, 'BM', 'Bermudas'),
(28, 'BT', 'Bhután'),
(29, 'BO', 'Bolivia'),
(30, 'BA', 'Bosnia y Herzegovina'),
(31, 'BW', 'Botsuana'),
(32, 'BV', 'Isla Bouvet'),
(33, 'BR', 'Brasil'),
(34, 'BN', 'Brunéi'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'CV', 'Cabo Verde'),
(39, 'KY', 'Islas Caimán'),
(40, 'KH', 'Camboya'),
(41, 'CM', 'Camerún'),
(42, 'CA', 'Canadá'),
(43, 'CF', 'República Centroafricana'),
(44, 'TD', 'Chad'),
(45, 'CZ', 'República Checa'),
(46, 'CL', 'Chile'),
(47, 'CN', 'China'),
(48, 'CY', 'Chipre'),
(49, 'CX', 'Isla de Navidad'),
(50, 'VA', 'Ciudad del Vaticano'),
(51, 'CC', 'Islas Cocos'),
(52, 'CO', 'Colombia'),
(53, 'KM', 'Comoras'),
(54, 'CD', 'República Democrática del Congo'),
(55, 'CG', 'Congo'),
(56, 'CK', 'Islas Cook'),
(57, 'KP', 'Corea del Norte'),
(58, 'KR', 'Corea del Sur'),
(59, 'CI', 'Costa de Marfil'),
(60, 'CR', 'Costa Rica'),
(61, 'HR', 'Croacia'),
(62, 'CU', 'Cuba'),
(63, 'DK', 'Dinamarca'),
(64, 'DM', 'Dominica'),
(65, 'DO', 'República Dominicana'),
(66, 'EC', 'Ecuador'),
(67, 'EG', 'Egipto'),
(68, 'SV', 'El Salvador'),
(69, 'AE', 'Emiratos Árabes Unidos'),
(70, 'ER', 'Eritrea'),
(71, 'SK', 'Eslovaquia'),
(72, 'SI', 'Eslovenia'),
(73, 'ES', 'España'),
(74, 'UM', 'Islas ultramarinas de Estados Unidos'),
(75, 'US', 'Estados Unidos'),
(76, 'EE', 'Estonia'),
(77, 'ET', 'Etiopía'),
(78, 'FO', 'Islas Feroe'),
(79, 'PH', 'Filipinas'),
(80, 'FI', 'Finlandia'),
(81, 'FJ', 'Fiyi'),
(82, 'FR', 'Francia'),
(83, 'GA', 'Gabón'),
(84, 'GM', 'Gambia'),
(85, 'GE', 'Georgia'),
(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur'),
(87, 'GH', 'Ghana'),
(88, 'GI', 'Gibraltar'),
(89, 'GD', 'Granada'),
(90, 'GR', 'Grecia'),
(91, 'GL', 'Groenlandia'),
(92, 'GP', 'Guadalupe'),
(93, 'GU', 'Guam'),
(94, 'GT', 'Guatemala'),
(95, 'GF', 'Guayana Francesa'),
(96, 'GN', 'Guinea'),
(97, 'GQ', 'Guinea Ecuatorial'),
(98, 'GW', 'Guinea-Bissau'),
(99, 'GY', 'Guyana'),
(100, 'HT', 'Haití'),
(101, 'HM', 'Islas Heard y McDonald'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong'),
(104, 'HU', 'Hungría'),
(105, 'IN', 'India'),
(106, 'ID', 'Indonesia'),
(107, 'IR', 'Irán'),
(108, 'IQ', 'Iraq'),
(109, 'IE', 'Irlanda'),
(110, 'IS', 'Islandia'),
(111, 'IL', 'Israel'),
(112, 'IT', 'Italia'),
(113, 'JM', 'Jamaica'),
(114, 'JP', 'Japón'),
(115, 'JO', 'Jordania'),
(116, 'KZ', 'Kazajstán'),
(117, 'KE', 'Kenia'),
(118, 'KG', 'Kirguistán'),
(119, 'KI', 'Kiribati'),
(120, 'KW', 'Kuwait'),
(121, 'LA', 'Laos'),
(122, 'LS', 'Lesotho'),
(123, 'LV', 'Letonia'),
(124, 'LB', 'Líbano'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libia'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lituania'),
(129, 'LU', 'Luxemburgo'),
(130, 'MO', 'Macao'),
(131, 'MK', 'ARY Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MY', 'Malasia'),
(134, 'MW', 'Malawi'),
(135, 'MV', 'Maldivas'),
(136, 'ML', 'Malí'),
(137, 'MT', 'Malta'),
(138, 'FK', 'Islas Malvinas'),
(139, 'MP', 'Islas Marianas del Norte'),
(140, 'MA', 'Marruecos'),
(141, 'MH', 'Islas Marshall'),
(142, 'MQ', 'Martinica'),
(143, 'MU', 'Mauricio'),
(144, 'MR', 'Mauritania'),
(145, 'YT', 'Mayotte'),
(146, 'MX', 'México'),
(147, 'FM', 'Micronesia'),
(148, 'MD', 'Moldavia'),
(149, 'MC', 'Mónaco'),
(150, 'MN', 'Mongolia'),
(151, 'MS', 'Montserrat'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NI', 'Nicaragua'),
(158, 'NE', 'Níger'),
(159, 'NG', 'Nigeria'),
(160, 'NU', 'Niue'),
(161, 'NF', 'Isla Norfolk'),
(162, 'NO', 'Noruega'),
(163, 'NC', 'Nueva Caledonia'),
(164, 'NZ', 'Nueva Zelanda'),
(165, 'OM', 'Omán'),
(166, 'NL', 'Países Bajos'),
(167, 'PK', 'Pakistán'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestina'),
(170, 'PA', 'Panamá'),
(171, 'PG', 'Papúa Nueva Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Perú'),
(174, 'PN', 'Islas Pitcairn'),
(175, 'PF', 'Polinesia Francesa'),
(176, 'PL', 'Polonia'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'GB', 'Reino Unido'),
(181, 'RE', 'Reunión'),
(182, 'RW', 'Ruanda'),
(183, 'RO', 'Rumania'),
(184, 'RU', 'Rusia'),
(185, 'EH', 'Sahara Occidental'),
(186, 'SB', 'Islas Salomón'),
(187, 'WS', 'Samoa'),
(188, 'AS', 'Samoa Americana'),
(189, 'KN', 'San Cristóbal y Nevis'),
(190, 'SM', 'San Marino'),
(191, 'PM', 'San Pedro y Miquelón'),
(192, 'VC', 'San Vicente y las Granadinas'),
(193, 'SH', 'Santa Helena'),
(194, 'LC', 'Santa Lucía'),
(195, 'ST', 'Santo Tomé y Príncipe'),
(196, 'SN', 'Senegal'),
(197, 'CS', 'Serbia y Montenegro'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leona'),
(200, 'SG', 'Singapur'),
(201, 'SY', 'Siria'),
(202, 'SO', 'Somalia'),
(203, 'LK', 'Sri Lanka'),
(204, 'SZ', 'Suazilandia'),
(205, 'ZA', 'Sudáfrica'),
(206, 'SD', 'Sudán'),
(207, 'SE', 'Suecia'),
(208, 'CH', 'Suiza'),
(209, 'SR', 'Surinam'),
(210, 'SJ', 'Svalbard y Jan Mayen'),
(211, 'TH', 'Tailandia'),
(212, 'TW', 'Taiwán'),
(213, 'TZ', 'Tanzania'),
(214, 'TJ', 'Tayikistán'),
(215, 'IO', 'Territorio Británico del Océano Índico'),
(216, 'TF', 'Territorios Australes Franceses'),
(217, 'TL', 'Timor Oriental'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad y Tobago'),
(222, 'TN', 'Túnez'),
(223, 'TC', 'Islas Turcas y Caicos'),
(224, 'TM', 'Turkmenistán'),
(225, 'TR', 'Turquía'),
(226, 'TV', 'Tuvalu'),
(227, 'UA', 'Ucrania'),
(228, 'UG', 'Uganda'),
(229, 'UY', 'Uruguay'),
(230, 'UZ', 'Uzbekistán'),
(231, 'VU', 'Vanuatu'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Islas Vírgenes Británicas'),
(235, 'VI', 'Islas Vírgenes de los Estados Unidos'),
(236, 'WF', 'Wallis y Futuna'),
(237, 'YE', 'Yemen'),
(238, 'DJ', 'Yibuti'),
(239, 'ZM', 'Zambia'),
(240, 'ZW', 'Zimbabue');

-- --------------------------------------------------------

--
-- Table structure for table `tmk_password_resets`
--

CREATE TABLE `tmk_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_password_resets`
--

INSERT INTO `tmk_password_resets` (`email`, `token`, `created_at`) VALUES
('johnny.gonzalez@telefonica.com', '5419232a2800c7953ae416d96aa326b26ba1b2bbf3e6d10fa12c2381c3c390c1', '2019-07-17 18:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `tmk_providers`
--

CREATE TABLE `tmk_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 1,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('Bienes','Servicios','Recursos') COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_provider_brands`
--

CREATE TABLE `tmk_provider_brands` (
  `provider_id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_orders`
--

CREATE TABLE `tmk_purchase_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` int(10) UNSIGNED DEFAULT NULL,
  `require_date` timestamp NULL DEFAULT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `subtotal` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tax` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `confirmed_at` timestamp NULL DEFAULT NULL,
  `canceled_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `num_doc_temp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_purchase_orders_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_purchase_orders_view` (
`id` int(10) unsigned
,`order` varchar(255)
,`require_date` timestamp
,`subtotal` decimal(10,2)
,`tax` decimal(10,2)
,`discount` decimal(10,2)
,`total` decimal(32,2)
,`observation` varchar(255)
,`warehouse_id` int(10) unsigned
,`warehouse` varchar(255)
,`provider_id` int(10) unsigned
,`provider` varchar(201)
,`provider_addres` varchar(255)
,`provider_email` varchar(255)
,`provider_phone` varchar(50)
,`created_by` int(11)
,`agent_id` int(50)
,`order_status_id` int(10) unsigned
,`order_status` varchar(50)
,`order_status_acronym` varchar(20)
,`created_at` timestamp
,`updated_at` timestamp
,`confirmed_at` timestamp
,`canceled_at` timestamp
,`deleted_at` timestamp
,`missing` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_order_details`
--

CREATE TABLE `tmk_purchase_order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 0,
  `unit_cost` decimal(10,2) NOT NULL DEFAULT 0.00,
  `tax` decimal(10,2) DEFAULT 0.00,
  `amount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `complete` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_purchase_order_details_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_purchase_order_details_view` (
`id` int(10) unsigned
,`order_id` int(10) unsigned
,`ordenes` varchar(255)
,`article_id` int(10) unsigned
,`internal_reference` varchar(255)
,`article` mediumtext
,`measuremnt_unit` varchar(255)
,`quantity` int(11)
,`unit_cost` decimal(10,2)
,`amount` decimal(10,2)
,`tax` decimal(10,2)
,`received` decimal(32,2)
,`pending` decimal(33,2)
,`returned` int(1)
,`unassigned` decimal(44,2)
,`in_bodega` decimal(41,0)
,`complete` tinyint(1)
,`last_receipt` timestamp
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_receipts`
--

CREATE TABLE `tmk_purchase_receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchase_order_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `quantity` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_series`
--

CREATE TABLE `tmk_purchase_series` (
  `id` int(11) NOT NULL,
  `item_id` varchar(50) NOT NULL,
  `internal_reference` varchar(100) NOT NULL,
  `num_doc_temp` varchar(255) NOT NULL,
  `num_doc_temp_out` varchar(255) DEFAULT NULL,
  `serie` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  `status_doc` int(11) NOT NULL,
  `status_doc_out` int(11) NOT NULL,
  `status_in` int(11) NOT NULL,
  `status_out` int(11) NOT NULL,
  `creado_el` date NOT NULL,
  `salida_el` date NOT NULL,
  `actualizado_el` date NOT NULL,
  `eliminado_el` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_series_out`
--

CREATE TABLE `tmk_purchase_series_out` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_row` int(11) NOT NULL,
  `internal_reference` int(11) NOT NULL,
  `num_doc_temp` varchar(255) NOT NULL,
  `serie` varchar(255) NOT NULL,
  `article_id` int(11) NOT NULL,
  `status_doc` int(11) NOT NULL,
  `status_out` int(11) NOT NULL,
  `creado` datetime NOT NULL,
  `actualizado` datetime NOT NULL,
  `eliminado` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_purchase_status`
--

CREATE TABLE `tmk_purchase_status` (
  `status_id` int(11) NOT NULL DEFAULT 2,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_reserva_salida_extra`
-- (See below for the actual view)
--
CREATE TABLE `tmk_reserva_salida_extra` (
`status_id` int(10) unsigned
,`warehouse_id` int(11)
,`quantity` decimal(32,0)
,`article_id` int(10) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_roles`
--

CREATE TABLE `tmk_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` int(11) NOT NULL DEFAULT 0,
  `write` int(11) NOT NULL DEFAULT 0,
  `modify` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_roles`
--

INSERT INTO `tmk_roles` (`id`, `description`, `read`, `write`, `modify`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'superadmin', 1, 1, 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(2, 'admin', 1, 1, 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(3, 'user', 1, 1, 0, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(4, 'editor', 1, 0, 1, '2017-01-20 00:16:21', '2017-01-20 00:16:21', NULL),
(5, 'superadmin', 1, 1, 1, '2019-09-10 02:08:01', '2019-09-10 02:08:01', NULL),
(6, 'admin', 1, 1, 1, '2019-09-10 02:08:01', '2019-09-10 02:08:01', NULL),
(7, 'user', 1, 1, 0, '2019-09-10 02:08:01', '2019-09-10 02:08:01', NULL),
(8, 'editor', 1, 0, 1, '2019-09-10 02:08:01', '2019-09-10 02:08:01', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_series_all_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_series_all_view` (
`existe` int(11)
,`id` int(11)
,`serie` varchar(255)
,`internal_reference` varchar(100)
,`num_doc_temp` varchar(255)
,`status_doc` int(11)
,`status_in` int(11)
,`num_doc_temp_out` varchar(255)
,`article_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_states`
--

CREATE TABLE `tmk_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_stock_bodegas_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_stock_bodegas_view` (
`article_id` int(10) unsigned
,`bodega_id` int(10) unsigned
,`barcode` varchar(100)
,`article` varchar(100)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`entry` decimal(41,0)
,`exit` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_stock_bodega_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_stock_bodega_view` (
`article_id` int(10) unsigned
,`bodega_id` int(10) unsigned
,`barcode` varchar(100)
,`article` varchar(100)
,`min_stock` int(10) unsigned
,`max_stock` int(10) unsigned
,`entry` decimal(41,0)
,`exit` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_taxes`
--

CREATE TABLE `tmk_taxes` (
  `id` int(11) NOT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `acronym` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_tipo_gestion_articulos_table`
--

CREATE TABLE `tmk_tipo_gestion_articulos_table` (
  `id` int(11) NOT NULL,
  `gestion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmk_tipo_gestion_articulos_table`
--

INSERT INTO `tmk_tipo_gestion_articulos_table` (`id`, `gestion`) VALUES
(1, 'Series'),
(2, 'Lotes');

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_tracking_orders_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_tracking_orders_view` (
`made_at` timestamp
,`made_by` varchar(201)
,`transfer_id` int(10) unsigned
,`transfer_num` varchar(255)
,`order_id` int(10) unsigned
,`order_num` varchar(255)
,`warehouse` varchar(255)
,`warehouse_origen` varchar(255)
,`warehouse_destination` varchar(255)
,`description` varchar(758)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_traking`
-- (See below for the actual view)
--
CREATE TABLE `tmk_traking` (
`order_id` int(10) unsigned
,`order_num` varchar(255)
,`order_warehouse_origen` varchar(255)
,`order_warehouse_destination` varchar(255)
,`ordered_at` timestamp
,`ordered_by` varchar(201)
,`ordered_for` varchar(201)
,`order_canceled_at` timestamp
,`order_canceled_by` varchar(201)
,`transfer_id` int(10) unsigned
,`transfer_num` varchar(255)
,`transfer_reference` varchar(255)
,`transfer_warehouse_origen` varchar(255)
,`transfer_warehouse_destination` varchar(255)
,`created_at` timestamp
,`created_by` varchar(201)
,`updated_at` timestamp
,`updated_by` varchar(201)
,`transferred_at` timestamp
,`transferred_by` varchar(201)
,`transferred_for` varchar(201)
,`received_at` timestamp
,`received_by` varchar(201)
,`canceled_at` timestamp
,`canceled_by` varchar(201)
,`requested_at` timestamp
,`requested_by` varchar(201)
,`order_status` varchar(50)
,`transfer_status` varchar(50)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_transfers`
--

CREATE TABLE `tmk_transfers` (
  `id` int(10) UNSIGNED NOT NULL,
  `transfer_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transfer_reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferred_from` int(10) UNSIGNED DEFAULT NULL,
  `transferred_to` int(10) UNSIGNED DEFAULT NULL,
  `bodega_id` int(10) UNSIGNED NOT NULL,
  `bodega_id_end` int(10) UNSIGNED DEFAULT NULL,
  `observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receipt_num` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receipt_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receipt_observation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferred_by` int(11) DEFAULT NULL,
  `transferred_for` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `requested_by` int(11) DEFAULT NULL,
  `received_by` int(11) DEFAULT NULL,
  `canceled_by` int(11) DEFAULT NULL,
  `transferred_at` timestamp NULL DEFAULT NULL,
  `requested_at` timestamp NULL DEFAULT NULL,
  `received_at` timestamp NULL DEFAULT NULL,
  `canceled_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_transfers`
--

INSERT INTO `tmk_transfers` (`id`, `transfer_num`, `transfer_reference`, `order_id`, `subject`, `transferred_from`, `transferred_to`, `bodega_id`, `bodega_id_end`, `observation`, `receipt_num`, `receipt_file`, `receipt_observation`, `transferred_by`, `transferred_for`, `created_by`, `updated_by`, `deleted_by`, `requested_by`, `received_by`, `canceled_by`, `transferred_at`, `requested_at`, `received_at`, `canceled_at`, `created_at`, `updated_at`, `deleted_at`, `status_id`) VALUES
(1, 'TF-02065617', '123456', 4, NULL, 17, 57, 5, 1, '', 'RF-0206560217', NULL, NULL, 1, 3, 1, 3, NULL, NULL, 3, NULL, '2017-02-23 19:17:54', NULL, '2017-02-23 19:23:16', NULL, '2017-02-23 19:06:56', '2017-02-23 19:23:16', NULL, 11),
(2, 'TF-02202817', '1222334', 6, NULL, 17, 57, 5, 1, '', 'RF-0220280217', NULL, NULL, 1, 3, 1, 3, NULL, NULL, 3, NULL, '2017-02-23 19:21:28', NULL, '2017-02-23 19:27:49', NULL, '2017-02-23 19:20:28', '2017-02-23 19:27:49', NULL, 11),
(3, 'TF-02291317', '646464764', 6, NULL, 17, 57, 5, 1, '', 'RF-0229130217', NULL, NULL, 1, 3, 1, 3, NULL, NULL, 3, NULL, '2017-02-23 19:29:36', NULL, '2017-02-23 19:29:57', NULL, '2017-02-23 19:29:13', '2017-02-23 19:29:57', NULL, 11),
(4, 'TF-02423117', '4567893786', 7, NULL, 17, 57, 5, 2, '', 'RF-0242310217', NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, NULL, '2017-02-23 19:42:36', NULL, '2017-02-23 20:02:14', NULL, '2017-02-23 19:42:31', '2017-02-23 20:02:14', NULL, 11),
(5, 'TF-02032217', '76474674', 7, NULL, 17, 57, 5, 2, '', NULL, NULL, NULL, 1, 1, 1, 1, NULL, 1, NULL, NULL, '2017-03-02 23:05:59', '2017-02-23 20:03:24', NULL, NULL, '2017-02-23 20:03:22', '2017-03-02 23:05:59', NULL, 10),
(6, 'TF-02221817', '785367896', 8, NULL, 17, 57, 5, 1, '', 'RF-0222180217', NULL, NULL, 1, 3, 1, 3, NULL, NULL, 3, NULL, '2017-02-23 20:22:26', NULL, '2017-02-23 20:23:00', NULL, '2017-02-23 20:22:18', '2017-02-23 20:23:00', NULL, 11),
(9, '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-16 05:29:22', '2019-08-16 05:29:22', NULL, 1),
(10, '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-16 05:33:11', '2019-08-16 05:33:11', NULL, 1),
(11, 'TF-08462119', '04892', 21, NULL, 17, 57, 2, 3, '', 'RF-0846210819', NULL, NULL, 1, NULL, 1, 2, NULL, NULL, 2, NULL, '2019-08-17 05:46:39', NULL, '2019-08-17 07:04:31', NULL, '2019-08-17 05:46:22', '2019-08-17 07:04:31', NULL, 5),
(12, 'TF-08111119', '0102223', 22, NULL, 17, 57, 2, 3, '', 'RF-0811110819', NULL, NULL, 1, NULL, 1, 2, NULL, NULL, 2, NULL, '2019-08-17 07:11:26', NULL, '2019-08-17 07:12:05', NULL, '2019-08-17 07:11:11', '2019-08-17 07:12:05', NULL, 5);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tmk_transfers_view`
-- (See below for the actual view)
--
CREATE TABLE `tmk_transfers_view` (
`id` int(10) unsigned
,`transfer_num` varchar(255)
,`transfer_reference` varchar(255)
,`transfer_warehouse_origen` varchar(255)
,`transfer_warehouse_destination` varchar(255)
,`status` varchar(50)
,`created_at` timestamp
,`created_by` varchar(201)
,`updated_at` timestamp
,`updated_by` varchar(201)
,`transferred_at` timestamp
,`transferred_by` varchar(201)
,`transferred_for` varchar(201)
,`received_at` timestamp
,`received_by` varchar(201)
,`canceled_at` timestamp
,`canceled_by` varchar(201)
,`order_id` int(10) unsigned
,`order_num` varchar(255)
,`order_warehouse_origen` varchar(255)
,`order_warehouse_destination` varchar(255)
,`ordered_at` timestamp
,`ordered_by` varchar(201)
,`requested_at` timestamp
,`requested_by` varchar(201)
);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_transfer_article_bodegas`
--

CREATE TABLE `tmk_transfer_article_bodegas` (
  `transfer_id` int(10) UNSIGNED NOT NULL,
  `article_bodega_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmk_transfer_details`
--

CREATE TABLE `tmk_transfer_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `transfer_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `measurement_unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `available_quantity` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `received_quantity` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_transfer_details`
--

INSERT INTO `tmk_transfer_details` (`id`, `transfer_id`, `article_id`, `measurement_unit`, `available_quantity`, `quantity`, `received_quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, 2, NULL, NULL, 5, 5, '2017-02-23 19:17:18', '2017-02-23 19:23:16', NULL),
(5, 1, 4, NULL, NULL, 3, 3, '2017-02-23 19:17:18', '2017-02-23 19:23:16', NULL),
(6, 1, 7, NULL, NULL, 1, 1, '2017-02-23 19:17:18', '2017-02-23 19:23:16', NULL),
(7, 2, 2, NULL, NULL, 5, 5, '2017-02-23 19:20:28', '2017-02-23 19:27:49', NULL),
(8, 2, 4, NULL, NULL, 10, 10, '2017-02-23 19:20:28', '2017-02-23 19:27:49', NULL),
(9, 3, 2, NULL, NULL, 3, 3, '2017-02-23 19:29:13', '2017-02-23 19:29:57', NULL),
(10, 3, 4, NULL, NULL, 8, 8, '2017-02-23 19:29:13', '2017-02-23 19:29:57', NULL),
(11, 4, 4, NULL, NULL, 5, 5, '2017-02-23 19:42:31', '2017-02-23 20:02:14', NULL),
(12, 5, 4, NULL, NULL, 5, 0, '2017-02-23 20:03:22', '2017-02-23 20:03:22', NULL),
(13, 6, 4, NULL, NULL, 50, 50, '2017-02-23 20:22:18', '2017-02-23 20:23:00', NULL),
(14, 6, 7, NULL, NULL, 10, 10, '2017-02-23 20:22:18', '2017-02-23 20:23:00', NULL),
(15, 7, 4, NULL, NULL, 50, 50, '2017-02-23 20:23:51', '2017-02-23 20:24:26', NULL),
(16, 7, 7, NULL, NULL, 40, 40, '2017-02-23 20:23:51', '2017-02-23 20:24:26', NULL),
(17, 8, 2, NULL, NULL, 1, 1, '2017-02-23 21:09:59', '2017-02-23 21:16:08', NULL),
(18, 9, 14, NULL, NULL, 8, 0, '2019-08-16 05:29:22', '2019-08-16 05:29:22', NULL),
(19, 10, 14, NULL, NULL, 8, 0, '2019-08-16 05:33:11', '2019-08-16 05:33:11', NULL),
(20, 11, 14, NULL, NULL, 1, 1, '2019-08-17 05:46:22', '2019-08-17 07:04:31', NULL),
(21, 12, 15, NULL, NULL, 40, 40, '2019-08-17 07:11:11', '2019-08-17 07:12:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_type_movements`
--

CREATE TABLE `tmk_type_movements` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `colour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_type_movements`
--

INSERT INTO `tmk_type_movements` (`id`, `description`, `colour`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'entry', NULL, '2016-12-13 10:55:07', '2016-12-13 10:55:07', NULL),
(2, 'exit', NULL, '2016-12-13 10:55:07', '2016-12-13 10:55:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_type_providers`
--

CREATE TABLE `tmk_type_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_type_providers`
--

INSERT INTO `tmk_type_providers` (`id`, `description`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bienes', 1, '2016-02-16 23:26:06', '2016-12-15 19:30:17', NULL),
(2, 'Servicios', 1, '2016-08-31 20:59:17', '2016-06-28 21:36:56', NULL),
(3, 'Recursos', 1, '2016-12-22 21:59:53', '2016-07-31 02:42:06', NULL),
(4, 'Bienes', 1, '2019-03-07 14:05:47', '2019-05-24 15:30:15', NULL),
(5, 'Servicios', 1, '2019-03-27 23:33:24', '2019-01-25 06:59:07', NULL),
(6, 'Recursos', 1, '2019-05-13 09:49:16', '2019-01-10 03:52:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_users`
--

CREATE TABLE `tmk_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `agent_id` int(50) NOT NULL,
  `sucursal_id` int(10) UNSIGNED DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_logged_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_users`
--

INSERT INTO `tmk_users` (`id`, `firstname`, `lastname`, `address`, `phone`, `email`, `username`, `password`, `active`, `agent_id`, `sucursal_id`, `confirmation_token`, `remember_token`, `created_at`, `updated_at`, `last_logged_at`, `deleted_at`, `role_id`) VALUES
(1, 'JOHNNY', 'GONZALEZ', NULL, NULL, 'johnny.gonzalez@telefonica.com', 'NCT02167', '$2y$12$GLwWxixAlY2YF8.wQz5NaeazdeHUm5GQH1Y3u.bNBAnhHDcApY9Re', 1, 2167, NULL, NULL, '1P9O2cVdSaHpOwSdbEzuuZrHPcN07zkkZQBiZt5tzHWPTKYEFXlrKiOJ0nJM', '2017-03-24 07:00:00', '2020-06-14 01:46:44', '2020-06-14 01:46:44', NULL, 1),
(2, 'JUAN', 'GONZALEZ', NULL, NULL, 'johnny.gonzalez2@telefonica.com', 'NCT02168', '$2y$12$GLwWxixAlY2YF8.wQz5NaeazdeHUm5GQH1Y3u.bNBAnhHDcApY9Re', 1, 2168, NULL, NULL, '8OrPCXjPOFtCS5TwoT5mGyCtQtCkEfxpiJ26cor0DiCJ5cmGp6Y5hGlIhssv', '2017-03-24 14:00:00', '2019-08-17 07:11:50', '2019-08-17 07:11:50', NULL, 4),
(91, 'SIMONA', 'RODRIGUEZ', 'SAMARIA SINAI CASA 387', '68773334', 'simona@123.com', 'US001', '$2y$12$GLwWxixAlY2YF8.wQz5NaeazdeHUm5GQH1Y3u.bNBAnhHDcApY9Re', 1, 1566251982, NULL, NULL, '0hS9660SbeJq1BbYd1qDXvoJyDPjqMKAGoTDWCb9NsnQrs17k3U2bq7xaFBH', '2019-08-19 23:59:42', '2019-08-21 01:26:22', '2019-08-21 00:57:50', NULL, 4),
(92, 'johnny', 'gonzalez', '32576', '60634535', 'jhonny_sg10@hotmail.com', 'ncn03839', '$2y$10$UyiAV3mVShcsnqBYCNam5uprQIXxsTAbEhOQDqbPgJ400wJGOgeuW', 1, 9999993, NULL, NULL, NULL, '2016-07-24 13:11:16', '2019-09-10 15:25:01', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_user_warehouses`
--

CREATE TABLE `tmk_user_warehouses` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_user_warehouses`
--

INSERT INTO `tmk_user_warehouses` (`user_id`, `warehouse_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, NULL, NULL, NULL, '2017-02-23 18:53:08', '2017-02-23 18:53:08', NULL),
(73, 3, NULL, NULL, NULL, '2019-08-19 21:50:47', '2019-08-19 21:50:47', NULL),
(2, 3, NULL, NULL, NULL, '2019-08-14 18:52:49', '2019-08-14 18:52:49', NULL),
(1, 2, NULL, NULL, NULL, '2019-08-14 19:04:32', '2019-08-14 19:04:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tmk_variants`
--

CREATE TABLE `tmk_variants` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `attribute_value_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tmk_variants`
--

INSERT INTO `tmk_variants` (`article_id`, `attribute_value_id`) VALUES
(1, 3),
(1, 27),
(2, 2),
(2, 22),
(4, 5),
(4, 7),
(4, 11),
(5, 5),
(5, 7),
(5, 12),
(6, 5),
(6, 8),
(6, 11),
(7, 5),
(7, 8),
(7, 12),
(8, 5),
(8, 9),
(8, 11),
(9, 5),
(9, 9),
(9, 12),
(10, 5),
(10, 10),
(10, 11),
(11, 5),
(11, 10),
(11, 12);

-- --------------------------------------------------------

--
-- Structure for view `a_view`
--
DROP TABLE IF EXISTS `a_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `a_view`  AS  select `tmk_purchase_orders`.`id` AS `id`,`tmk_purchase_orders`.`order` AS `ordenes`,`tmk_purchase_orders`.`provider_id` AS `provider_id`,`tmk_purchase_orders`.`require_date` AS `require_date`,`tmk_purchase_orders`.`warehouse_id` AS `warehouse_id`,`tmk_purchase_orders`.`subtotal` AS `subtotal`,`tmk_purchase_orders`.`tax` AS `tax`,`tmk_purchase_orders`.`discount` AS `discount`,`tmk_purchase_orders`.`observation` AS `observation`,`tmk_purchase_orders`.`created_by` AS `created_by`,`tmk_purchase_orders`.`updated_by` AS `updated_by`,`tmk_purchase_orders`.`deleted_by` AS `deleted_by`,`tmk_purchase_orders`.`confirmed_at` AS `confirmed_at`,`tmk_purchase_orders`.`canceled_at` AS `canceled_at`,`tmk_purchase_orders`.`created_at` AS `created_at`,`tmk_purchase_orders`.`updated_at` AS `updated_at`,`tmk_purchase_orders`.`deleted_at` AS `deleted_at`,`tmk_purchase_orders`.`order_status_id` AS `order_status_id` from `tmk_purchase_orders` ;

-- --------------------------------------------------------

--
-- Structure for view `bx_view`
--
DROP TABLE IF EXISTS `bx_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `bx_view`  AS  select `bx`.`article_id` AS `article_id`,`bx`.`purchase_order_id` AS `purchase_order_id`,sum(`bx`.`quantity`) AS `quantity` from `tmk_purchase_receipts` `bx` group by `bx`.`article_id`,`bx`.`purchase_order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `cx_view`
--
DROP TABLE IF EXISTS `cx_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cx_view`  AS  select `cx`.`article_id` AS `article_id`,`cx`.`purchase_order_id` AS `purchase_order_id`,sum(`cx`.`quantity`) AS `Cantpend` from `tmk_purchase_receipts` `cx` group by `cx`.`article_id`,`cx`.`purchase_order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `dx_view`
--
DROP TABLE IF EXISTS `dx_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dx_view`  AS  select sum(`query`.`quantity`) AS `cantrec`,`query`.`purchase_order_id` AS `purchase_order_id` from `tmk_purchase_receipts` `query` group by `query`.`purchase_order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `external_movement_view`
--
DROP TABLE IF EXISTS `external_movement_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `external_movement_view`  AS  select `a`.`id` AS `article_master_id`,`a`.`barcode` AS `barcode`,`a`.`name` AS `article_master`,`a`.`image` AS `image`,`b`.`id` AS `article_id`,`a`.`internal_reference` AS `internal_reference`,`a`.`name` AS `article`,`c`.`description` AS `brand`,`d`.`description` AS `category`,`e`.`description` AS `measurement_unit`,`h`.`warehouse_id` AS `warehouse_id`,`i`.`description` AS `warehouse`,`a`.`min_stock` AS `min_stock`,`a`.`max_stock` AS `max_stock`,case `h`.`movement` when 'exit' then -`g`.`quantity` else `g`.`quantity` end AS `stock`,NULL AS `reserved`,NULL AS `available_quantity`,1 AS `active`,NULL AS `last_movement`,`a`.`created_by` AS `created_by`,`a`.`updated_by` AS `updated_by`,`a`.`deleted_by` AS `deleted_by`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_at` AS `deleted_at` from (((((((`tmk_article_masters` `a` join `tmk_articles` `b` on(`a`.`id` = `b`.`article_master_id`)) join `tmk_brands` `c` on(`c`.`id` = `a`.`brand_id`)) join `tmk_categories` `d` on(`d`.`id` = `a`.`category_id`)) join `tmk_measurement_units` `e` on(`a`.`measurement_unit_id` = `e`.`id`)) join `tmk_external_movement_details` `g` on(`b`.`id` = `g`.`article_id`)) join `tmk_external_movements` `h` on(`h`.`id` = `g`.`external_movement_id`)) join `tmk_bodegas` `i` on(`i`.`id` = `h`.`warehouse_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `ex_view`
--
DROP TABLE IF EXISTS `ex_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ex_view`  AS  select sum(`query`.`quantity`) AS `cantpen2`,`query`.`movementable_id` AS `movementable_id` from (`tmk_article_bodegas` `query` join `tmk_purchase_orders` on(`tmk_purchase_orders`.`id` = `query`.`movementable_id` and `query`.`movementable_type` = 'TradeMarketingModelsPurchaseOrder')) group by `query`.`movementable_id` ;

-- --------------------------------------------------------

--
-- Structure for view `fx_view`
--
DROP TABLE IF EXISTS `fx_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fx_view`  AS  select `fx`.`article_id` AS `article_id`,sum(`fx`.`quantity`) AS `quantityBod`,`fx`.`movementable_id` AS `movementable_id` from `tmk_article_bodegas` `fx` where reverse(left(reverse(`fx`.`movementable_type`),locate('sledoM',reverse(`fx`.`movementable_type`)) - 1)) = 'PurchaseOrder' group by `fx`.`article_id`,`fx`.`movementable_id` ;

-- --------------------------------------------------------

--
-- Structure for view `gx_view`
--
DROP TABLE IF EXISTS `gx_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gx_view`  AS  select max(`query`.`created_at`) AS `creado`,`query`.`article_id` AS `article_id`,`query`.`purchase_order_id` AS `purchase_order_id` from `tmk_purchase_receipts` `query` group by `query`.`article_id`,`query`.`purchase_order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_articles_view`
--
DROP TABLE IF EXISTS `tmk_articles_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_articles_view`  AS  select `tmk_articles`.`id` AS `article_id`,`tmk_articles`.`barcode` AS `barcode`,`tmk_articles`.`internal_reference` AS `internal_reference`,`tmk_articles`.`description` AS `description`,`tmk_article_masters`.`id` AS `article_master_id`,`tmk_article_masters`.`name` AS `article_master`,`tmk_article_masters`.`barcode` AS `article_master_barcode`,`tmk_article_masters`.`internal_reference` AS `article_master_internal_reference`,`tmk_article_masters`.`image` AS `image`,`tmk_article_masters`.`long_description` AS `long_description`,`tmk_article_masters`.`min_stock` AS `min_stock`,`tmk_article_masters`.`max_stock` AS `max_stock`,`tmk_brands`.`description` AS `brand`,`tmk_categories`.`description` AS `category`,`tmk_measurement_units`.`description` AS `measurement_unit`,`tmk_article_masters`.`gestion` AS `gestion`,ifnull((select cast(sum(`query`.`amount`) / sum(`query`.`quantity`) as decimal(6,2)) from `tmk_purchase_order_details` `query` where `query`.`article_id` = `tmk_articles`.`id` and `query`.`unit_cost` > 0 and `query`.`unit_cost` is not null),0.00) AS `avg_unit_cost`,`tmk_article_masters`.`created_by` AS `created_by`,`tmk_article_masters`.`updated_by` AS `updated_by`,`tmk_articles`.`deleted_by` AS `deleted_by`,`tmk_article_masters`.`created_at` AS `created_at`,`tmk_article_masters`.`updated_at` AS `updated_at`,`tmk_articles`.`deleted_at` AS `deleted_at` from ((((`tmk_articles` join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) left join `tmk_brands` on(`tmk_brands`.`id` = `tmk_article_masters`.`brand_id`)) left join `tmk_categories` on(`tmk_categories`.`id` = `tmk_article_masters`.`category_id`)) left join `tmk_measurement_units` on(`tmk_measurement_units`.`id` = `tmk_article_masters`.`measurement_unit_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_article_bodegas_view`
--
DROP TABLE IF EXISTS `tmk_article_bodegas_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_article_bodegas_view`  AS  select distinct `tmk_article_masters`.`id` AS `article_master_id`,`ab2`.`article_id` AS `article_id`,`ab2`.`bodega_id` AS `bodega_id`,`tmk_article_masters`.`min_stock` AS `min_stock`,`tmk_article_masters`.`max_stock` AS `max_stock`,sum(`ab2`.`quantity`) AS `entry`,(select sum(`ab1`.`quantity`) from (`tmk_article_bodegas` `ab1` join `tmk_movements` on(`tmk_movements`.`id` = `ab1`.`movement_id`)) where `ab1`.`article_id` = `ab2`.`article_id` and `ab1`.`bodega_id` = `ab2`.`bodega_id` and `tmk_movements`.`id` = 2) AS `exit` from (((`tmk_article_bodegas` `ab2` join `tmk_movements` on(`tmk_movements`.`id` = `ab2`.`movement_id`)) join `tmk_articles` on(`tmk_articles`.`id` = `ab2`.`article_id`)) join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) where `tmk_movements`.`id` = 1 group by `ab2`.`article_id`,`ab2`.`bodega_id`,`tmk_article_masters`.`id`,`tmk_article_masters`.`min_stock`,`tmk_article_masters`.`max_stock` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_article_master_warehouses_view`
--
DROP TABLE IF EXISTS `tmk_article_master_warehouses_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_article_master_warehouses_view`  AS  select `tmk_inventario_general`.`article_master_id` AS `article_master_id`,`tmk_inventario_general`.`barcode` AS `barcode`,`tmk_inventario_general`.`article_master` AS `article_master`,`tmk_inventario_general`.`image` AS `image`,`tmk_inventario_general`.`article_id` AS `article_id`,`tmk_inventario_general`.`internal_reference` AS `internal_reference`,`tmk_inventario_general`.`article` AS `article`,`tmk_inventario_general`.`brand` AS `brand`,`tmk_inventario_general`.`category` AS `category`,`tmk_inventario_general`.`measurement_unit` AS `measurement_unit`,`tmk_inventario_general`.`warehouse_id` AS `warehouse_id`,`tmk_inventario_general`.`warehouse` AS `warehouse`,`tmk_inventario_general`.`min_stock` AS `min_stock`,`tmk_inventario_general`.`max_stock` AS `max_stock`,`tmk_inventario_general`.`stock` - ifnull((select `rs`.`quantity` from `tmk_reserva_salida_extra` `rs` where `rs`.`warehouse_id` = `tmk_inventario_general`.`warehouse_id` and `rs`.`article_id` = `tmk_inventario_general`.`article_id`),0) AS `stock`,ifnull((select `rs`.`quantity` from `tmk_reserva_salida_extra` `rs` where `rs`.`warehouse_id` = `tmk_inventario_general`.`warehouse_id` and `rs`.`article_id` = `tmk_inventario_general`.`article_id`),0) AS `s`,`tmk_inventario_general`.`reserved` AS `reserved`,`tmk_inventario_general`.`available_quantity` - ifnull((select `rs`.`quantity` from `tmk_reserva_salida_extra` `rs` where `rs`.`warehouse_id` = `tmk_inventario_general`.`warehouse_id` and `rs`.`article_id` = `tmk_inventario_general`.`article_id`),0) AS `available_quantity`,`tmk_inventario_general`.`active` AS `active`,`tmk_inventario_general`.`last_movement` AS `last_movement`,`tmk_inventario_general`.`created_by` AS `created_by`,`tmk_inventario_general`.`created_at` AS `created_at`,`tmk_inventario_general`.`updated_by` AS `updated_by`,`tmk_inventario_general`.`updated_at` AS `updated_at`,`tmk_inventario_general`.`deleted_by` AS `deleted_by`,`tmk_inventario_general`.`deleted_at` AS `deleted_at` from `tmk_inventario_general` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_entradas_salidas_inicio`
--
DROP TABLE IF EXISTS `tmk_entradas_salidas_inicio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_entradas_salidas_inicio`  AS  select cast(`tmk_movements_view`.`created_at` as date) AS `fecha`,`tmk_movements_view`.`movement_type` AS `movement_type`,`tmk_movements_view`.`movement` AS `movement`,`tmk_movements_view`.`movement_code` AS `movement_code`,count(`tmk_movements_view`.`quantity`) AS `cantidad` from `tmk_movements_view` group by cast(`tmk_movements_view`.`created_at` as date),`tmk_movements_view`.`movement_type`,`tmk_movements_view`.`movement`,`tmk_movements_view`.`movement_code` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_external_movement_view`
--
DROP TABLE IF EXISTS `tmk_external_movement_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_external_movement_view`  AS  select `external_movement_view`.`article_master_id` AS `article_master_id`,`external_movement_view`.`barcode` AS `barcode`,`external_movement_view`.`article_master` AS `article_master`,`external_movement_view`.`image` AS `image`,`external_movement_view`.`article_id` AS `article_id`,`external_movement_view`.`internal_reference` AS `internal_reference`,`external_movement_view`.`article` AS `article`,`external_movement_view`.`brand` AS `brand`,`external_movement_view`.`category` AS `category`,`external_movement_view`.`measurement_unit` AS `measurement_unit`,`external_movement_view`.`warehouse_id` AS `warehouse_id`,`external_movement_view`.`warehouse` AS `warehouse`,`external_movement_view`.`min_stock` AS `min_stock`,`external_movement_view`.`max_stock` AS `max_stock`,sum(`external_movement_view`.`stock`) AS `stock`,`external_movement_view`.`reserved` AS `reserved`,`external_movement_view`.`available_quantity` AS `available_quantity`,`external_movement_view`.`active` AS `active`,`external_movement_view`.`last_movement` AS `last_movement`,`external_movement_view`.`created_by` AS `created_by`,`external_movement_view`.`updated_by` AS `updated_by`,`external_movement_view`.`deleted_by` AS `deleted_by`,`external_movement_view`.`created_at` AS `created_at`,`external_movement_view`.`updated_at` AS `updated_at`,`external_movement_view`.`deleted_at` AS `deleted_at` from `external_movement_view` group by `external_movement_view`.`article_master_id`,`external_movement_view`.`barcode`,`external_movement_view`.`article_master`,`external_movement_view`.`image`,`external_movement_view`.`article_id`,`external_movement_view`.`internal_reference`,`external_movement_view`.`article`,`external_movement_view`.`brand`,`external_movement_view`.`category`,`external_movement_view`.`measurement_unit`,`external_movement_view`.`warehouse_id`,`external_movement_view`.`warehouse`,`external_movement_view`.`min_stock`,`external_movement_view`.`max_stock`,`external_movement_view`.`reserved`,`external_movement_view`.`available_quantity`,`external_movement_view`.`active`,`external_movement_view`.`last_movement`,`external_movement_view`.`created_by`,`external_movement_view`.`updated_by`,`external_movement_view`.`deleted_by`,`external_movement_view`.`created_at`,`external_movement_view`.`updated_at`,`external_movement_view`.`deleted_at` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_id_articles`
--
DROP TABLE IF EXISTS `tmk_id_articles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_id_articles`  AS  select `a`.`id` AS `id_1`,`a`.`article_master_id` AS `article_master_id`,`b`.`id` AS `id_2`,`b`.`warehouse_id` AS `warehouse_id` from (`tmk_articles` `a` join `tmk_article_master_warehouses` `b` on(`a`.`article_master_id` = `b`.`article_master_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_inventario_general`
--
DROP TABLE IF EXISTS `tmk_inventario_general`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_inventario_general`  AS  select `tmk_inventario_global`.`article_master_id` AS `article_master_id`,`tmk_inventario_global`.`barcode` AS `barcode`,`tmk_inventario_global`.`article_master` AS `article_master`,`tmk_inventario_global`.`image` AS `image`,`tmk_inventario_global`.`article_id` AS `article_id`,`tmk_inventario_global`.`internal_reference` AS `internal_reference`,`tmk_inventario_global`.`article` AS `article`,`tmk_inventario_global`.`brand` AS `brand`,`tmk_inventario_global`.`category` AS `category`,`tmk_inventario_global`.`measurement_unit` AS `measurement_unit`,`tmk_inventario_global`.`warehouse_id` AS `warehouse_id`,`tmk_inventario_global`.`warehouse` AS `warehouse`,`tmk_inventario_global`.`min_stock` AS `min_stock`,`tmk_inventario_global`.`max_stock` AS `max_stock`,`tmk_inventario_global`.`stock` AS `stock`,`tmk_inventario_global`.`reserved` AS `reserved`,`tmk_inventario_global`.`available_quantity` AS `available_quantity`,`tmk_inventario_global`.`active` AS `active`,`tmk_inventario_global`.`last_movement` AS `last_movement`,`tmk_inventario_global`.`created_by` AS `created_by`,`tmk_inventario_global`.`created_at` AS `created_at`,`tmk_inventario_global`.`updated_by` AS `updated_by`,`tmk_inventario_global`.`updated_at` AS `updated_at`,`tmk_inventario_global`.`deleted_by` AS `deleted_by`,`tmk_inventario_global`.`deleted_at` AS `deleted_at` from `tmk_inventario_global` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_inventario_general_union`
--
DROP TABLE IF EXISTS `tmk_inventario_general_union`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_inventario_general_union`  AS  select `tmk_inventario_global`.`article_master_id` AS `article_master_id`,`tmk_inventario_global`.`barcode` AS `barcode`,`tmk_inventario_global`.`article_master` AS `article_master`,`tmk_inventario_global`.`image` AS `image`,`tmk_inventario_global`.`article_id` AS `article_id`,`tmk_inventario_global`.`internal_reference` AS `internal_reference`,`tmk_inventario_global`.`article` AS `article`,`tmk_inventario_global`.`brand` AS `brand`,`tmk_inventario_global`.`category` AS `category`,`tmk_inventario_global`.`measurement_unit` AS `measurement_unit`,`tmk_inventario_global`.`warehouse_id` AS `warehouse_id`,`tmk_inventario_global`.`warehouse` AS `warehouse`,`tmk_inventario_global`.`min_stock` AS `min_stock`,`tmk_inventario_global`.`max_stock` AS `max_stock`,`tmk_inventario_global`.`stock` AS `stock`,`tmk_inventario_global`.`reserved` AS `reserved`,`tmk_inventario_global`.`available_quantity` AS `available_quantity`,`tmk_inventario_global`.`active` AS `active`,`tmk_inventario_global`.`last_movement` AS `last_movement`,`tmk_inventario_global`.`created_by` AS `created_by`,`tmk_inventario_global`.`created_at` AS `created_at`,`tmk_inventario_global`.`updated_by` AS `updated_by`,`tmk_inventario_global`.`updated_at` AS `updated_at`,`tmk_inventario_global`.`deleted_by` AS `deleted_by`,`tmk_inventario_global`.`deleted_at` AS `deleted_at` from `tmk_inventario_global` union all select `a`.`article_master_id` AS `article_master_id`,`a`.`barcode` AS `barcode`,`a`.`article_master` AS `article_master`,`a`.`image` AS `image`,`a`.`article_id` AS `article_id`,`a`.`internal_reference` AS `internal_reference`,`a`.`article` AS `article`,`a`.`brand` AS `brand`,`a`.`category` AS `category`,`a`.`measurement_unit` AS `measurement_unit`,`a`.`warehouse_id` AS `warehouse_id`,`a`.`warehouse` AS `warehouse`,`a`.`min_stock` AS `min_stock`,`a`.`max_stock` AS `max_stock`,`b`.`stock` AS `stock`,`a`.`reserved` AS `reserved`,`a`.`available_quantity` AS `available_quantity`,`a`.`active` AS `active`,`a`.`last_movement` AS `last_movement`,`a`.`created_by` AS `created_by`,`a`.`created_at` AS `created_at`,`a`.`updated_by` AS `updated_by`,`a`.`updated_at` AS `updated_at`,`a`.`deleted_by` AS `deleted_by`,`a`.`deleted_at` AS `deleted_at` from (`tmk_inventario_global` `a` join `tmk_external_movement_view` `b` on(`a`.`article_id` = `b`.`article_id` and `a`.`warehouse_id` = `b`.`warehouse_id` and `a`.`internal_reference` = `b`.`internal_reference`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_inventario_global`
--
DROP TABLE IF EXISTS `tmk_inventario_global`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_inventario_global`  AS  select distinct `tmk_article_masters`.`id` AS `article_master_id`,`tmk_article_masters`.`barcode` AS `barcode`,`tmk_article_masters`.`name` AS `article_master`,`tmk_article_masters`.`image` AS `image`,`tmk_articles`.`id` AS `article_id`,`tmk_articles`.`internal_reference` AS `internal_reference`,concat(`tmk_article_masters`.`name`,' ',`tmk_articles`.`description`) AS `article`,`tmk_brands`.`description` AS `brand`,`tmk_categories`.`description` AS `category`,`tmk_measurement_units`.`description` AS `measurement_unit`,`tmk_bodegas`.`id` AS `warehouse_id`,`tmk_bodegas`.`description` AS `warehouse`,`tmk_article_master_warehouses`.`min_stock` AS `min_stock`,`tmk_article_master_warehouses`.`max_stock` AS `max_stock`,ifnull((select sum(`a`.`quantity`) - sum(`a`.`quantity_exit`) from (`tmk_article_bodegas` `a` join `tmk_articles` `b` on(`b`.`id` = `a`.`article_id`)) where `b`.`article_master_id` = `tmk_article_masters`.`id` and `a`.`bodega_id` = `tmk_bodegas`.`id`),0) - ifnull((select sum(`query2`.`quantity`) from (`tmk_orders` `query` join `tmk_order_details` `query2` on(`query2`.`order_id` = `query`.`id`)) where `query`.`status_id` = 1 and `query`.`warehouse_destination` = `tmk_bodegas`.`id` and `query2`.`article_id` = `tmk_articles`.`id`),0) AS `stock`,(select sum(`query2`.`quantity`) from (`tmk_orders` `query` join `tmk_order_details` `query2` on(`query2`.`order_id` = `query`.`id`)) where `query`.`status_id` = 1 and `query`.`warehouse_destination` = `tmk_bodegas`.`id` and `query2`.`article_id` = `tmk_articles`.`id`) AS `reserved`,ifnull((select sum(`a`.`quantity`) - sum(`a`.`quantity_exit`) from (`tmk_article_bodegas` `a` join `tmk_articles` `b` on(`b`.`id` = `a`.`article_id`)) where `b`.`article_master_id` = `tmk_article_masters`.`id` and `a`.`bodega_id` = `tmk_bodegas`.`id`),0) - ifnull((select sum(`query2`.`quantity`) from (`tmk_orders` `query` join `tmk_order_details` `query2` on(`query2`.`order_id` = `query`.`id`)) where `query`.`status_id` = 1 and `query`.`warehouse_destination` = `tmk_bodegas`.`id` and `query2`.`article_id` = `tmk_articles`.`id`),0) AS `available_quantity`,`tmk_articles`.`active` AS `active`,`tmk_article_warehouses`.`updated_at` AS `last_movement`,(select concat(`query`.`firstname`,' ',`query`.`lastname`) from `tmk_users` `query` where `query`.`id` = `tmk_article_masters`.`created_by`) AS `created_by`,`tmk_article_masters`.`created_at` AS `created_at`,(select concat(`query`.`firstname`,' ',`query`.`lastname`) from `tmk_users` `query` where `query`.`id` = `tmk_article_masters`.`updated_by`) AS `updated_by`,`tmk_article_masters`.`updated_at` AS `updated_at`,(select concat(`query`.`firstname`,' ',`query`.`lastname`) from `tmk_users` `query` where `query`.`id` = `tmk_articles`.`deleted_by`) AS `deleted_by`,`tmk_articles`.`deleted_at` AS `deleted_at` from (((((((`tmk_article_masters` left join `tmk_article_master_warehouses` on(`tmk_article_master_warehouses`.`article_master_id` = `tmk_article_masters`.`id`)) left join `tmk_article_warehouses` on(`tmk_article_warehouses`.`article_master_warehouse_id` = `tmk_article_master_warehouses`.`id`)) left join `tmk_brands` on(`tmk_brands`.`id` = `tmk_article_masters`.`brand_id`)) left join `tmk_categories` on(`tmk_categories`.`id` = `tmk_article_masters`.`category_id`)) left join `tmk_measurement_units` on(`tmk_measurement_units`.`id` = `tmk_article_masters`.`measurement_unit_id`)) left join `tmk_bodegas` on(`tmk_bodegas`.`id` = `tmk_article_master_warehouses`.`warehouse_id`)) left join `tmk_articles` on(`tmk_articles`.`id` = `tmk_article_warehouses`.`article_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_inventario_inicio`
--
DROP TABLE IF EXISTS `tmk_inventario_inicio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_inventario_inicio`  AS  select `tmk_inventario_general`.`article_master_id` AS `article_master_id`,`tmk_inventario_general`.`barcode` AS `barcode`,`tmk_inventario_general`.`article_master` AS `article_master`,`tmk_inventario_general`.`image` AS `image`,`tmk_inventario_general`.`article_id` AS `article_id`,`tmk_inventario_general`.`internal_reference` AS `internal_reference`,`tmk_inventario_general`.`article` AS `article`,`tmk_inventario_general`.`brand` AS `brand`,`tmk_inventario_general`.`category` AS `category`,`tmk_inventario_general`.`measurement_unit` AS `measurement_unit`,`tmk_inventario_general`.`warehouse_id` AS `warehouse_id`,`tmk_inventario_general`.`warehouse` AS `warehouse`,`tmk_inventario_general`.`min_stock` AS `min_stock`,`tmk_inventario_general`.`max_stock` AS `max_stock`,`tmk_inventario_general`.`stock` AS `stock`,`tmk_inventario_general`.`reserved` AS `reserved`,`tmk_inventario_general`.`available_quantity` AS `available_quantity`,`tmk_inventario_general`.`active` AS `active`,`tmk_inventario_general`.`last_movement` AS `last_movement`,`tmk_inventario_general`.`created_by` AS `created_by`,`tmk_inventario_general`.`created_at` AS `created_at`,`tmk_inventario_general`.`updated_by` AS `updated_by`,`tmk_inventario_general`.`updated_at` AS `updated_at`,`tmk_inventario_general`.`deleted_by` AS `deleted_by`,`tmk_inventario_general`.`deleted_at` AS `deleted_at`,cast(`tmk_inventario_general`.`last_movement` as date) AS `fecha` from `tmk_inventario_general` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_inventario_stock`
--
DROP TABLE IF EXISTS `tmk_inventario_stock`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_inventario_stock`  AS  select `tmk_inventario_general`.`article_master_id` AS `article_master_id`,`tmk_inventario_general`.`barcode` AS `barcode`,`tmk_inventario_general`.`article_master` AS `article_master`,`tmk_inventario_general`.`image` AS `image`,`tmk_inventario_general`.`article_id` AS `article_id`,`tmk_inventario_general`.`internal_reference` AS `internal_reference`,`tmk_inventario_general`.`article` AS `article`,`tmk_inventario_general`.`brand` AS `brand`,`tmk_inventario_general`.`category` AS `category`,`tmk_inventario_general`.`measurement_unit` AS `measurement_unit`,`tmk_inventario_general`.`warehouse_id` AS `warehouse_id`,`tmk_inventario_general`.`warehouse` AS `warehouse`,`tmk_inventario_general`.`min_stock` AS `min_stock`,`tmk_inventario_general`.`max_stock` AS `max_stock`,`tmk_inventario_general`.`stock` AS `stock`,`tmk_inventario_general`.`reserved` AS `reserved`,`tmk_inventario_general`.`available_quantity` AS `available_quantity`,`tmk_inventario_general`.`active` AS `active`,`tmk_inventario_general`.`last_movement` AS `last_movement`,`tmk_inventario_general`.`created_by` AS `created_by`,`tmk_inventario_general`.`created_at` AS `created_at`,`tmk_inventario_general`.`updated_by` AS `updated_by`,`tmk_inventario_general`.`updated_at` AS `updated_at`,`tmk_inventario_general`.`deleted_by` AS `deleted_by`,`tmk_inventario_general`.`deleted_at` AS `deleted_at`,cast(`tmk_inventario_general`.`last_movement` as date) AS `fecha` from `tmk_inventario_general` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_lotes_all_view`
--
DROP TABLE IF EXISTS `tmk_lotes_all_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_lotes_all_view`  AS  select distinct ifnull(`b`.`item_id`,0) AS `existe`,`a`.`id` AS `id`,`a`.`item_id` AS `item_id`,`a`.`internal_reference` AS `internal_reference`,`a`.`num_doc_temp` AS `num_doc_temp`,`a`.`lote` AS `lote`,`a`.`cantidad_in` - ifnull(`b`.`cantidad`,0) AS `cantidad`,`b`.`cantidad` AS `cantidad_out`,`a`.`caducidad` AS `caducidad`,`a`.`status_doc` AS `status_doc`,`a`.`status_in` AS `status_in`,`b`.`num_doc_temp` AS `num_doc_temp_out`,`b`.`article_id` AS `article_id` from (`tmk_lotes` `a` left join `tmk_lotes_out` `b` on(`a`.`id` = `b`.`item_id`)) where `a`.`cantidad_in` - ifnull(`b`.`cantidad`,0) > 0 ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_lotes_create_view`
--
DROP TABLE IF EXISTS `tmk_lotes_create_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_lotes_create_view`  AS  select `a`.`id` AS `id`,`a`.`item_id` AS `item_id`,`a`.`internal_reference` AS `internal_reference`,`a`.`num_doc_temp_out` AS `num_doc_temp_out`,`a`.`num_doc_temp` AS `num_doc_temp`,`a`.`lote` AS `lote`,`a`.`cantidad_in` - ifnull((select sum(`x`.`cantidad`) from `tmk_lotes_out` `x` where `x`.`item_id` = `a`.`id`),0) AS `cantidad_disponible`,`a`.`caducidad` AS `caducidad`,`a`.`status_doc` AS `status_doc`,`a`.`status_in` AS `status_in`,`a`.`article_id` AS `article_id` from `tmk_lotes` `a` order by `a`.`caducidad` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_movements_view`
--
DROP TABLE IF EXISTS `tmk_movements_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_movements_view`  AS  select `tmk_article_bodegas`.`id` AS `id`,`tmk_article_bodegas`.`created_at` AS `created_at`,`tmk_type_movements`.`description` AS `movement_type`,`tmk_movements`.`description` AS `movement`,`tmk_movements`.`code` AS `movement_code`,`referencia`.`movement_tag` AS `movement_tag`,`tmk_article_masters`.`id` AS `article_master_id`,`tmk_article_masters`.`name` AS `article_master`,`tmk_article_masters`.`image` AS `image`,`tmk_article_masters`.`barcode` AS `barcode`,`tmk_articles`.`id` AS `article_id`,concat(`tmk_article_masters`.`name`,' ',`tmk_articles`.`description`) AS `article`,`tmk_articles`.`internal_reference` AS `internal_reference`,`tmk_measurement_units`.`description` AS `unity`,`tmk_article_bodegas`.`quantity` AS `quantity`,`tmk_article_bodegas`.`quantity_exit` AS `quantity_exit`,`referencia`.`unit_cost` AS `unit_cost`,`referencia`.`reference_id` AS `reference_id`,`referencia`.`reference` AS `reference`,`referencia`.`desde` AS `desde`,`referencia`.`para` AS `para`,`tmk_article_bodegas`.`initial` AS `initial`,`tmk_bodegas`.`id` AS `warehouse_id`,`tmk_bodegas`.`description` AS `warehouse` from (((((((`tmk_article_bodegas` left join `tmk_movement_references_sub_view` `referencia` on(`tmk_article_bodegas`.`movementable_id` = `referencia`.`reference_id` and reverse(left(reverse(`tmk_article_bodegas`.`movementable_type`),locate('sledoM',reverse(`tmk_article_bodegas`.`movementable_type`)) - 1)) collate utf8_general_ci = convert(`referencia`.`movement_tag` using utf8) and `tmk_article_bodegas`.`article_id` = `referencia`.`article_id`)) join `tmk_bodegas` on(`tmk_bodegas`.`id` = `tmk_article_bodegas`.`bodega_id`)) join `tmk_articles` on(`tmk_articles`.`id` = `tmk_article_bodegas`.`article_id`)) join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) join `tmk_measurement_units` on(`tmk_measurement_units`.`id` = `tmk_article_masters`.`measurement_unit_id`)) join `tmk_movements` on(`tmk_movements`.`id` = `tmk_article_bodegas`.`movement_id`)) join `tmk_type_movements` on(`tmk_type_movements`.`id` = `tmk_movements`.`type_movement_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_movement_references_sub_view`
--
DROP TABLE IF EXISTS `tmk_movement_references_sub_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_movement_references_sub_view`  AS  select distinct `tmk_transfers`.`id` AS `reference_id`,`tmk_transfers`.`transfer_num` AS `reference`,`tmk_transfer_details`.`article_id` AS `article_id`,`tmk_transfer_details`.`quantity` AS `quantity`,NULL AS `unit_cost`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_by`) AS `desde`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_for`) AS `para`,'transfer' AS `movement_tag` from (`tmk_transfers` join `tmk_transfer_details` on(`tmk_transfer_details`.`transfer_id` = `tmk_transfers`.`id`)) union select `tmk_purchase_orders`.`id` AS `id`,`tmk_purchase_orders`.`order` AS `order`,`tmk_purchase_order_details`.`article_id` AS `article_id`,`tmk_purchase_order_details`.`quantity` AS `quantity`,`tmk_purchase_order_details`.`unit_cost` AS `unit_cost`,'' AS `Name_exp_14`,'' AS `Name_exp_15`,'purchaseorder' AS `movement_tag` from (`tmk_purchase_orders` join `tmk_purchase_order_details` on(`tmk_purchase_order_details`.`order_id` = `tmk_purchase_orders`.`id`)) union select `x`.`id` AS `id`,`x`.`external_movement_num` AS `orders`,`y`.`article_id` AS `article_id`,`y`.`quantity` AS `quantity`,NULL AS `unit_cost`,'' AS `Name_exp_22`,'' AS `Name_exp_23`,'ExternalMovement' AS `movement_tag` from (`tmk_external_movements` `x` join `tmk_external_movement_details` `y` on(`x`.`id` = `y`.`external_movement_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_movement_references_view`
--
DROP TABLE IF EXISTS `tmk_movement_references_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_movement_references_view`  AS  select `tmk_transfers`.`id` AS `reference_id`,`tmk_transfers`.`transfer_num` AS `reference`,`tmk_transfer_details`.`article_id` AS `article_id`,`tmk_transfer_details`.`quantity` AS `quantity`,NULL AS `unit_cost`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_by`) AS `desde`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_for`) AS `para`,'transfer' AS `movement_tag` from (`tmk_transfers` join `tmk_transfer_details` on(`tmk_transfer_details`.`transfer_id` = `tmk_transfers`.`id`)) union all select `tmk_purchase_orders`.`id` AS `id`,`tmk_purchase_orders`.`order` AS `order`,`tmk_purchase_order_details`.`article_id` AS `article_id`,`tmk_purchase_order_details`.`quantity` AS `quantity`,`tmk_purchase_order_details`.`unit_cost` AS `unit_cost`,'' AS `Name_exp_14`,'' AS `Name_exp_15`,'purchaseorder' AS `movement_tag` from (`tmk_purchase_orders` join `tmk_purchase_order_details` on(`tmk_purchase_order_details`.`order_id` = `tmk_purchase_orders`.`id`)) union all select `x`.`id` AS `id`,`x`.`external_movement_num` AS `ordern`,`y`.`article_id` AS `article_id`,`y`.`quantity` AS `quantity`,NULL AS `unit_cost`,'' AS `Name_exp_22`,'' AS `Name_exp_23`,'ExternalMovement' AS `movement_tag` from (`tmk_external_movements` `x` join `tmk_external_movement_details` `y` on(`x`.`id` = `y`.`external_movement_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_order_details_sub_view`
--
DROP TABLE IF EXISTS `tmk_order_details_sub_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_order_details_sub_view`  AS  select `tmk_order_details`.`id` AS `id`,`tmk_orders`.`id` AS `order_id`,`tmk_orders`.`order_num` AS `order_num`,`tmk_order_details`.`article_id` AS `article_id`,`tmk_order_details`.`quantity` AS `quantity`,`tmk_order_details`.`quantity` - ifnull((select sum(`subquery`.`quantity`) from `tmk_transfer_details` `subquery` where `subquery`.`transfer_id` = `tmk_transfers`.`id` and `subquery`.`article_id` = `tmk_order_details`.`article_id`),0) AS `pending`,`tmk_transfers`.`id` AS `transfer_id`,`tmk_transfers`.`transfer_num` AS `transfer_num`,(select sum(`subquery`.`quantity`) from `tmk_transfer_details` `subquery` where `subquery`.`transfer_id` = `tmk_transfers`.`id` and `subquery`.`article_id` = `tmk_order_details`.`article_id`) AS `transferred`,(select sum(`subquery`.`received_quantity`) from `tmk_transfer_details` `subquery` where `subquery`.`transfer_id` = `tmk_transfers`.`id` and `subquery`.`article_id` = `tmk_order_details`.`article_id`) AS `received` from (((`tmk_orders` join `tmk_order_details` on(`tmk_order_details`.`order_id` = `tmk_orders`.`id`)) left join `tmk_transfers` on(`tmk_transfers`.`order_id` = `tmk_orders`.`id`)) left join `tmk_transfer_details` on(`tmk_transfer_details`.`transfer_id` = `tmk_transfers`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_order_details_view`
--
DROP TABLE IF EXISTS `tmk_order_details_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_order_details_view`  AS  select `tmk`.`id` AS `id`,`tmk`.`order_id` AS `order_id`,`tmk`.`order_num` AS `order_num`,`tmk`.`article_id` AS `article_id`,`tmk`.`quantity` AS `quantity`,`tmk`.`pending` AS `pending`,`tmk`.`transferred` AS `transferred`,`tmk`.`received` AS `received` from `tmk_order_details_sub_view` `tmk` group by `tmk`.`id`,`tmk`.`order_id`,`tmk`.`order_num`,`tmk`.`article_id`,`tmk`.`quantity`,`tmk`.`pending`,`tmk`.`transferred`,`tmk`.`received` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_purchase_orders_view`
--
DROP TABLE IF EXISTS `tmk_purchase_orders_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_purchase_orders_view`  AS  select `tmk_purchase_orders`.`id` AS `id`,`tmk_purchase_orders`.`order` AS `order`,`tmk_purchase_orders`.`require_date` AS `require_date`,`tmk_purchase_orders`.`subtotal` AS `subtotal`,`tmk_purchase_orders`.`tax` AS `tax`,`tmk_purchase_orders`.`discount` AS `discount`,(select sum(`tmk_purchase_order_details`.`amount`) from `tmk_purchase_order_details` where `tmk_purchase_order_details`.`order_id` = `tmk_purchase_orders`.`id`) AS `total`,`tmk_purchase_orders`.`observation` AS `observation`,`tmk_bodegas`.`id` AS `warehouse_id`,`tmk_bodegas`.`description` AS `warehouse`,`tmk_purchase_orders`.`provider_id` AS `provider_id`,concat(`tmk_providers`.`firstname`,' ',`tmk_providers`.`lastname`) AS `provider`,`tmk_providers`.`address` AS `provider_addres`,`tmk_providers`.`email` AS `provider_email`,`tmk_providers`.`phone` AS `provider_phone`,`tmk_purchase_orders`.`created_by` AS `created_by`,`tmk_users`.`agent_id` AS `agent_id`,`tmk_purchase_orders`.`order_status_id` AS `order_status_id`,`tmk_order_status`.`description` AS `order_status`,`tmk_order_status`.`acronym` AS `order_status_acronym`,`tmk_purchase_orders`.`created_at` AS `created_at`,`tmk_purchase_orders`.`updated_at` AS `updated_at`,`tmk_purchase_orders`.`confirmed_at` AS `confirmed_at`,`tmk_purchase_orders`.`canceled_at` AS `canceled_at`,`tmk_purchase_orders`.`deleted_at` AS `deleted_at`,(select count(ifnull(`query`.`id`,0)) from `tmk_purchase_order_details` `query` where (select ifnull(sum(`tmk_purchase_order_details`.`quantity`),0) from `tmk_purchase_order_details` where `tmk_purchase_order_details`.`id` = `query`.`id`) > (select ifnull(sum(`tmk_purchase_receipts`.`quantity`),0) from `tmk_purchase_receipts` where `tmk_purchase_receipts`.`purchase_order_id` = `query`.`id`) and `query`.`order_id` = `tmk_purchase_orders`.`id` and `query`.`deleted_at` is null) AS `missing` from ((((`tmk_purchase_orders` left join `tmk_providers` on(`tmk_providers`.`id` = `tmk_purchase_orders`.`provider_id`)) join `tmk_users` on(`tmk_users`.`id` = `tmk_purchase_orders`.`created_by`)) join `tmk_bodegas` on(`tmk_bodegas`.`id` = `tmk_purchase_orders`.`warehouse_id`)) join `tmk_order_status` on(`tmk_order_status`.`id` = `tmk_purchase_orders`.`order_status_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_purchase_order_details_view`
--
DROP TABLE IF EXISTS `tmk_purchase_order_details_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_purchase_order_details_view`  AS  select `tmk_purchase_order_details`.`id` AS `id`,`a`.`id` AS `order_id`,`a`.`ordenes` AS `ordenes`,`tmk_purchase_order_details`.`article_id` AS `article_id`,`tmk_articles`.`internal_reference` AS `internal_reference`,concat(`tmk_article_masters`.`name`,' ',`tmk_articles`.`description`) AS `article`,`tmk_measurement_units`.`description` AS `measuremnt_unit`,ifnull(`tmk_purchase_order_details`.`quantity`,0) AS `quantity`,ifnull(`tmk_purchase_order_details`.`unit_cost`,0.00) AS `unit_cost`,ifnull(cast(`tmk_purchase_order_details`.`unit_cost` * `tmk_purchase_order_details`.`quantity` as decimal(10,2)),0.00) AS `amount`,ifnull(`tmk_purchase_order_details`.`tax`,0.00) AS `tax`,`bx1`.`quantity` AS `received`,ifnull(`tmk_purchase_order_details`.`quantity` - `cx1`.`Cantpend`,0) AS `pending`,0 AS `returned`,ifnull(`dx1`.`cantrec`,0) - ifnull(`ex1`.`cantpen2`,0) AS `unassigned`,ifnull(`fx1`.`quantityBod`,0) AS `in_bodega`,`tmk_purchase_order_details`.`complete` AS `complete`,`gx1`.`creado` AS `last_receipt`,`tmk_purchase_order_details`.`created_at` AS `created_at`,`tmk_purchase_order_details`.`updated_at` AS `updated_at`,`tmk_purchase_order_details`.`deleted_at` AS `deleted_at` from (((((((((((`a_view` `a` left join `tmk_purchase_order_details` on(`a`.`id` = `tmk_purchase_order_details`.`order_id`)) left join `tmk_article_bodegas` on(`tmk_article_bodegas`.`movementable_id` = `a`.`id` and reverse(left(reverse(`tmk_article_bodegas`.`movementable_type`),locate('sledoM',reverse(`tmk_article_bodegas`.`movementable_type`)) - 1)) = 'PurchaseOrder')) join `tmk_articles` on(`tmk_articles`.`id` = `tmk_purchase_order_details`.`article_id`)) join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) join `tmk_measurement_units` on(`tmk_measurement_units`.`id` = `tmk_article_masters`.`measurement_unit_id`)) left join `bx_view` `bx1` on(`bx1`.`purchase_order_id` = `a`.`id` and `bx1`.`article_id` = `tmk_purchase_order_details`.`article_id`)) left join `cx_view` `cx1` on(`cx1`.`purchase_order_id` = `a`.`id` and `cx1`.`article_id` = `tmk_purchase_order_details`.`article_id`)) left join `dx_view` `dx1` on(`dx1`.`purchase_order_id` = `a`.`id`)) left join `ex_view` `ex1` on(`ex1`.`movementable_id` = `a`.`id`)) left join `fx_view` `fx1` on(`fx1`.`movementable_id` = `a`.`id` and `fx1`.`article_id` = `tmk_purchase_order_details`.`article_id`)) left join `gx_view` `gx1` on(`gx1`.`purchase_order_id` = `a`.`id` and `gx1`.`article_id` = `tmk_purchase_order_details`.`article_id`)) where `tmk_purchase_order_details`.`deleted_at` is null group by `tmk_purchase_order_details`.`id`,`a`.`id`,`a`.`ordenes`,`bx1`.`quantity`,`cx1`.`Cantpend`,`dx1`.`cantrec`,`ex1`.`cantpen2`,`fx1`.`quantityBod`,`gx1`.`creado`,`tmk_purchase_order_details`.`article_id`,`tmk_articles`.`internal_reference`,`tmk_article_masters`.`name`,`tmk_articles`.`description`,`tmk_purchase_order_details`.`quantity`,`tmk_purchase_order_details`.`unit_cost`,`tmk_purchase_order_details`.`tax`,`tmk_measurement_units`.`unity`,`tmk_measurement_units`.`description`,`tmk_purchase_order_details`.`complete`,`tmk_purchase_order_details`.`created_at`,`tmk_purchase_order_details`.`updated_at`,`tmk_purchase_order_details`.`deleted_at` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_reserva_salida_extra`
--
DROP TABLE IF EXISTS `tmk_reserva_salida_extra`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_reserva_salida_extra`  AS  select `a`.`status_id` AS `status_id`,`a`.`warehouse_id` AS `warehouse_id`,sum(`b`.`quantity`) AS `quantity`,`b`.`article_id` AS `article_id` from (`tmk_external_movement_details` `b` join `tmk_external_movements` `a` on(`b`.`external_movement_id` = `a`.`id`)) group by `a`.`status_id`,`a`.`warehouse_id`,`b`.`article_id` having `a`.`status_id` = 2 or `a`.`status_id` = 2 ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_series_all_view`
--
DROP TABLE IF EXISTS `tmk_series_all_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_series_all_view`  AS  select distinct ifnull(`b`.`item_id`,0) AS `existe`,`a`.`id` AS `id`,`a`.`serie` AS `serie`,`a`.`internal_reference` AS `internal_reference`,`a`.`num_doc_temp` AS `num_doc_temp`,`a`.`status_doc` AS `status_doc`,`a`.`status_in` AS `status_in`,`b`.`num_doc_temp` AS `num_doc_temp_out`,`b`.`article_id` AS `article_id` from (`tmk_purchase_series` `a` left join `tmk_purchase_series_out` `b` on(`a`.`id` = `b`.`item_id`)) where `a`.`status_in` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_stock_bodegas_view`
--
DROP TABLE IF EXISTS `tmk_stock_bodegas_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_stock_bodegas_view`  AS  select distinct `ab2`.`article_id` AS `article_id`,`ab2`.`bodega_id` AS `bodega_id`,`tmk_article_masters`.`barcode` AS `barcode`,`tmk_article_masters`.`name` AS `article`,`tmk_article_masters`.`min_stock` AS `min_stock`,`tmk_article_masters`.`max_stock` AS `max_stock`,sum(`ab2`.`quantity`) AS `entry`,(select sum(`ab1`.`quantity`) from (`tmk_article_bodegas` `ab1` join `tmk_movements` on(`tmk_movements`.`id` = `ab1`.`movement_id`)) where `ab1`.`article_id` = `ab2`.`article_id` and `ab1`.`bodega_id` = `ab2`.`bodega_id` and `tmk_movements`.`id` = 2) AS `exit` from (((`tmk_article_bodegas` `ab2` join `tmk_movements` on(`tmk_movements`.`id` = `ab2`.`movement_id`)) join `tmk_articles` on(`tmk_articles`.`id` = `ab2`.`article_id`)) join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) where `tmk_movements`.`id` = 1 group by `ab2`.`article_id`,`ab2`.`bodega_id`,`tmk_article_masters`.`name`,`tmk_article_masters`.`barcode`,`tmk_article_masters`.`min_stock`,`tmk_article_masters`.`max_stock` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_stock_bodega_view`
--
DROP TABLE IF EXISTS `tmk_stock_bodega_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_stock_bodega_view`  AS  select distinct `ab2`.`article_id` AS `article_id`,`ab2`.`bodega_id` AS `bodega_id`,`tmk_article_masters`.`barcode` AS `barcode`,`tmk_article_masters`.`name` AS `article`,`tmk_article_masters`.`min_stock` AS `min_stock`,`tmk_article_masters`.`max_stock` AS `max_stock`,sum(`ab2`.`quantity`) AS `entry`,(select sum(`ab1`.`quantity`) from (`tmk_article_bodegas` `ab1` join `tmk_movements` on(`tmk_movements`.`id` = `ab1`.`movement_id`)) where `ab1`.`article_id` = `ab2`.`article_id` and `ab1`.`bodega_id` = `ab2`.`bodega_id` and `tmk_movements`.`id` = 2) AS `exit` from (((`tmk_article_bodegas` `ab2` join `tmk_movements` on(`tmk_movements`.`id` = `ab2`.`movement_id`)) join `tmk_articles` on(`tmk_articles`.`id` = `ab2`.`article_id`)) join `tmk_article_masters` on(`tmk_article_masters`.`id` = `tmk_articles`.`article_master_id`)) where `tmk_movements`.`id` = 1 group by `ab2`.`article_id`,`ab2`.`bodega_id`,`tmk_article_masters`.`name`,`tmk_article_masters`.`barcode`,`tmk_article_masters`.`min_stock`,`tmk_article_masters`.`max_stock` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_tracking_orders_view`
--
DROP TABLE IF EXISTS `tmk_tracking_orders_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_tracking_orders_view`  AS  select `tmk_traking`.`ordered_at` AS `made_at`,`tmk_traking`.`ordered_by` AS `made_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`ordered_by`,' creÃƒÂ³ un pedido con nÃƒÂºmero ',`tmk_traking`.`order_num`,'.') AS `description` from `tmk_traking` where `tmk_traking`.`ordered_at` is not null union all select `tmk_traking`.`requested_at` AS `requested_at`,`tmk_traking`.`requested_by` AS `requested_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`requested_by`,' creÃƒÂ³ una solicitud de transferencia para ',`tmk_traking`.`order_warehouse_origen`,' ',`tmk_traking`.`order_num`,'.') AS `description` from `tmk_traking` where `tmk_traking`.`requested_at` is not null union all select `tmk_traking`.`transferred_at` AS `transferred_at`,`tmk_traking`.`transferred_by` AS `transferred_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`transferred_by`,' creÃƒÂ³ una transferencia hacia ',`tmk_traking`.`order_warehouse_destination`,'.') AS `description` from `tmk_traking` where `tmk_traking`.`transferred_at` is not null union all select `tmk_traking`.`received_at` AS `received_at`,`tmk_traking`.`received_by` AS `received_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`received_by`,' recibiÃƒÂ³ la transferencia.') AS `description` from `tmk_traking` where `tmk_traking`.`received_at` is not null union all select `tmk_traking`.`canceled_at` AS `canceled_at`,`tmk_traking`.`canceled_by` AS `canceled_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`ordered_by`,' cancelÃƒÂ³ la transferencia.') AS `description` from `tmk_traking` where `tmk_traking`.`canceled_at` is not null union all select `tmk_traking`.`order_canceled_at` AS `order_canceled_at`,`tmk_traking`.`order_canceled_by` AS `order_canceled_by`,`tmk_traking`.`transfer_id` AS `transfer_id`,`tmk_traking`.`transfer_num` AS `transfer_num`,`tmk_traking`.`order_id` AS `order_id`,`tmk_traking`.`order_num` AS `order_num`,`tmk_traking`.`order_warehouse_origen` AS `warehouse`,`tmk_traking`.`order_warehouse_origen` AS `warehouse_origen`,`tmk_traking`.`order_warehouse_destination` AS `warehouse_destination`,concat(`tmk_traking`.`order_canceled_by`,' cancelÃƒÂ³ la transferencia.') AS `description` from `tmk_traking` where `tmk_traking`.`order_canceled_at` is not null order by `order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_traking`
--
DROP TABLE IF EXISTS `tmk_traking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_traking`  AS  select `tmk_orders`.`id` AS `order_id`,`tmk_orders`.`order_num` AS `order_num`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_orders`.`warehouse_origen`) AS `order_warehouse_origen`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_orders`.`warehouse_destination`) AS `order_warehouse_destination`,`tmk_orders`.`ordered_at` AS `ordered_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_orders`.`created_by`) AS `ordered_by`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_orders`.`for_user_id`) AS `ordered_for`,`tmk_orders`.`canceled_at` AS `order_canceled_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_orders`.`canceled_by`) AS `order_canceled_by`,`tmk_transfers`.`id` AS `transfer_id`,`tmk_transfers`.`transfer_num` AS `transfer_num`,`tmk_transfers`.`transfer_reference` AS `transfer_reference`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_transfers`.`bodega_id`) AS `transfer_warehouse_origen`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_transfers`.`bodega_id_end`) AS `transfer_warehouse_destination`,`tmk_transfers`.`created_at` AS `created_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`created_by`) AS `created_by`,`tmk_transfers`.`updated_at` AS `updated_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`updated_by`) AS `updated_by`,`tmk_transfers`.`transferred_at` AS `transferred_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_by`) AS `transferred_by`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_for`) AS `transferred_for`,`tmk_transfers`.`received_at` AS `received_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`received_by`) AS `received_by`,`tmk_transfers`.`canceled_at` AS `canceled_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`canceled_by`) AS `canceled_by`,`tmk_transfers`.`requested_at` AS `requested_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`requested_by`) AS `requested_by`,(select `tmk_order_status`.`description` from `tmk_order_status` where `tmk_order_status`.`id` = `tmk_orders`.`status_id`) AS `order_status`,(select `tmk_order_status`.`description` from `tmk_order_status` where `tmk_order_status`.`id` = `tmk_transfers`.`status_id`) AS `transfer_status` from (`tmk_orders` left join `tmk_transfers` on(`tmk_transfers`.`order_id` = `tmk_orders`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tmk_transfers_view`
--
DROP TABLE IF EXISTS `tmk_transfers_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tmk_transfers_view`  AS  select `tmk_transfers`.`id` AS `id`,`tmk_transfers`.`transfer_num` AS `transfer_num`,`tmk_transfers`.`transfer_reference` AS `transfer_reference`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_transfers`.`bodega_id`) AS `transfer_warehouse_origen`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_transfers`.`bodega_id_end`) AS `transfer_warehouse_destination`,(select `tmk_order_status`.`description` from `tmk_order_status` where `tmk_order_status`.`id` = `tmk_transfers`.`status_id`) AS `status`,`tmk_transfers`.`created_at` AS `created_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`created_by`) AS `created_by`,`tmk_transfers`.`updated_at` AS `updated_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`updated_by`) AS `updated_by`,`tmk_transfers`.`transferred_at` AS `transferred_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_by`) AS `transferred_by`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`transferred_for`) AS `transferred_for`,`tmk_transfers`.`received_at` AS `received_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`received_by`) AS `received_by`,`tmk_transfers`.`canceled_at` AS `canceled_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`canceled_by`) AS `canceled_by`,`tmk_orders`.`id` AS `order_id`,`tmk_orders`.`order_num` AS `order_num`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_orders`.`warehouse_origen`) AS `order_warehouse_origen`,(select `tmk_bodegas`.`description` from `tmk_bodegas` where `tmk_bodegas`.`id` = `tmk_orders`.`warehouse_destination`) AS `order_warehouse_destination`,`tmk_orders`.`ordered_at` AS `ordered_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_orders`.`created_by`) AS `ordered_by`,`tmk_transfers`.`requested_at` AS `requested_at`,(select concat(`tmk_users`.`firstname`,' ',`tmk_users`.`lastname`) from `tmk_users` where `tmk_users`.`id` = `tmk_transfers`.`requested_by`) AS `requested_by` from (`tmk_orders` join `tmk_transfers` on(`tmk_orders`.`id` = `tmk_transfers`.`order_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `logtriggers`
--
ALTER TABLE `logtriggers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_activities`
--
ALTER TABLE `tmk_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_activities_user_id_index` (`user_id`),
  ADD KEY `tmk_activities_warehouse_id_index` (`warehouse_id`);

--
-- Indexes for table `tmk_ajustes_general`
--
ALTER TABLE `tmk_ajustes_general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_articles`
--
ALTER TABLE `tmk_articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_articles_reference_code_id_index` (`reference_code`,`id`),
  ADD KEY `tmk_articles_article_master_id_foreign` (`article_master_id`);

--
-- Indexes for table `tmk_article_bodegas`
--
ALTER TABLE `tmk_article_bodegas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_article_bodegas_bodega_id_foreign` (`bodega_id`),
  ADD KEY `tmk_article_bodegas_movement_id_foreign` (`movement_id`);

--
-- Indexes for table `tmk_article_masters`
--
ALTER TABLE `tmk_article_masters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_article_masters_measurement_unit_id_foreign` (`measurement_unit_id`);

--
-- Indexes for table `tmk_article_master_warehouses`
--
ALTER TABLE `tmk_article_master_warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_article_warehouses`
--
ALTER TABLE `tmk_article_warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_article_warehouse_details`
--
ALTER TABLE `tmk_article_warehouse_details`
  ADD KEY `tmk_article_warehouse_details_article_warehouse_id_index` (`article_warehouse_id`);

--
-- Indexes for table `tmk_attributes`
--
ALTER TABLE `tmk_attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tmk_attributes_description_unique` (`description`);

--
-- Indexes for table `tmk_attribute_values`
--
ALTER TABLE `tmk_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tmk_attribute_values_description_unique` (`description`),
  ADD KEY `tmk_attribute_values_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `tmk_bodegas`
--
ALTER TABLE `tmk_bodegas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_brands`
--
ALTER TABLE `tmk_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_categories`
--
ALTER TABLE `tmk_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_classes`
--
ALTER TABLE `tmk_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_classes_category_id_foreign` (`category_id`);

--
-- Indexes for table `tmk_clientes_contactos`
--
ALTER TABLE `tmk_clientes_contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_clientes_empresas`
--
ALTER TABLE `tmk_clientes_empresas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Ruc` (`Ruc`);

--
-- Indexes for table `tmk_clientes_individual`
--
ALTER TABLE `tmk_clientes_individual`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_clientes_sucursales`
--
ALTER TABLE `tmk_clientes_sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_clientes_tipo_contactos`
--
ALTER TABLE `tmk_clientes_tipo_contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_devolution_purchases`
--
ALTER TABLE `tmk_devolution_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_devolution_purchase_details`
--
ALTER TABLE `tmk_devolution_purchase_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_devolution_purchase_details_devolution_id_foreign` (`devolution_id`),
  ADD KEY `tmk_devolution_purchase_details_article_id_foreign` (`article_id`),
  ADD KEY `tmk_devolution_purchase_details_reference_id_foreign` (`reference_id`);

--
-- Indexes for table `tmk_devolution_transfers`
--
ALTER TABLE `tmk_devolution_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_devolution_transfers_transfer_id_foreign` (`transfer_id`),
  ADD KEY `tmk_devolution_transfers_article_id_foreign` (`article_id`);

--
-- Indexes for table `tmk_external_movements`
--
ALTER TABLE `tmk_external_movements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_external_movements_external_movement_num_index` (`external_movement_num`),
  ADD KEY `tmk_external_movements_agent_id_index` (`agent_id`),
  ADD KEY `tmk_external_movements_external_agent_id_index` (`external_agent_id`),
  ADD KEY `tmk_external_movements_warehouse_id_index` (`warehouse_id`),
  ADD KEY `tmk_external_movements_created_by_index` (`created_by`),
  ADD KEY `tmk_external_movements_updated_by_index` (`updated_by`),
  ADD KEY `tmk_external_movements_deleted_by_index` (`deleted_by`);

--
-- Indexes for table `tmk_external_movement_details`
--
ALTER TABLE `tmk_external_movement_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_external_movement_details_external_movement_id_foreign` (`external_movement_id`),
  ADD KEY `tmk_external_movement_details_article_id_index` (`article_id`);

--
-- Indexes for table `tmk_inventories`
--
ALTER TABLE `tmk_inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_inventory_details`
--
ALTER TABLE `tmk_inventory_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_inventory_details_article_bodega_id_foreign` (`article_bodega_id`),
  ADD KEY `tmk_inventory_details_inventory_id_foreign` (`inventory_id`);

--
-- Indexes for table `tmk_lotes`
--
ALTER TABLE `tmk_lotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_lotes_out`
--
ALTER TABLE `tmk_lotes_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_measurement_units`
--
ALTER TABLE `tmk_measurement_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_movements`
--
ALTER TABLE `tmk_movements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_movements_type_movement_id_foreign` (`type_movement_id`);

--
-- Indexes for table `tmk_orders`
--
ALTER TABLE `tmk_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_order_details`
--
ALTER TABLE `tmk_order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_order_details_order_id_foreign` (`order_id`),
  ADD KEY `tmk_order_details_article_id_foreign` (`article_id`);

--
-- Indexes for table `tmk_order_status`
--
ALTER TABLE `tmk_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_paises`
--
ALTER TABLE `tmk_paises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_password_resets`
--
ALTER TABLE `tmk_password_resets`
  ADD KEY `tmk_password_resets_email_index` (`email`),
  ADD KEY `tmk_password_resets_token_index` (`token`);

--
-- Indexes for table `tmk_providers`
--
ALTER TABLE `tmk_providers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tmk_providers_email_unique` (`email`);

--
-- Indexes for table `tmk_provider_brands`
--
ALTER TABLE `tmk_provider_brands`
  ADD PRIMARY KEY (`provider_id`,`brand_id`),
  ADD KEY `tmk_provider_brands_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `tmk_purchase_orders`
--
ALTER TABLE `tmk_purchase_orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tmk_purchase_orders_order_unique` (`order`),
  ADD KEY `tmk_purchase_orders_warehouse_id_foreign` (`warehouse_id`);

--
-- Indexes for table `tmk_purchase_order_details`
--
ALTER TABLE `tmk_purchase_order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_purchase_order_details_order_id_foreign` (`order_id`),
  ADD KEY `tmk_purchase_order_details_article_id_foreign` (`article_id`);

--
-- Indexes for table `tmk_purchase_receipts`
--
ALTER TABLE `tmk_purchase_receipts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_purchase_receipts_purchase_order_id_foreign` (`purchase_order_id`),
  ADD KEY `tmk_purchase_receipts_article_id_index` (`article_id`);

--
-- Indexes for table `tmk_purchase_series`
--
ALTER TABLE `tmk_purchase_series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_purchase_series_out`
--
ALTER TABLE `tmk_purchase_series_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_roles`
--
ALTER TABLE `tmk_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_states`
--
ALTER TABLE `tmk_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_taxes`
--
ALTER TABLE `tmk_taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_tipo_gestion_articulos_table`
--
ALTER TABLE `tmk_tipo_gestion_articulos_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_transfers`
--
ALTER TABLE `tmk_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_transfers_bodega_id_foreign` (`bodega_id`),
  ADD KEY `tmk_transfers_created_by_index` (`created_by`),
  ADD KEY `tmk_transfers_updated_by_index` (`updated_by`),
  ADD KEY `tmk_transfers_deleted_by_index` (`deleted_by`),
  ADD KEY `tmk_transfers_requested_by_index` (`requested_by`),
  ADD KEY `tmk_transfers_received_by_index` (`received_by`),
  ADD KEY `tmk_transfers_canceled_by_index` (`canceled_by`);

--
-- Indexes for table `tmk_transfer_article_bodegas`
--
ALTER TABLE `tmk_transfer_article_bodegas`
  ADD KEY `tmk_transfer_article_bodegas_transfer_id_foreign` (`transfer_id`),
  ADD KEY `tmk_transfer_article_bodegas_article_bodega_id_foreign` (`article_bodega_id`);

--
-- Indexes for table `tmk_transfer_details`
--
ALTER TABLE `tmk_transfer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tmk_transfer_details_transfer_id_foreign` (`transfer_id`),
  ADD KEY `tmk_transfer_details_article_id_foreign` (`article_id`);

--
-- Indexes for table `tmk_type_movements`
--
ALTER TABLE `tmk_type_movements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_type_providers`
--
ALTER TABLE `tmk_type_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmk_users`
--
ALTER TABLE `tmk_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tmk_users_email_unique` (`email`),
  ADD UNIQUE KEY `tmk_users_username_unique` (`username`),
  ADD UNIQUE KEY `tmk_users_agent_id_unique` (`agent_id`),
  ADD KEY `tmk_users_sucursal_id_index` (`sucursal_id`),
  ADD KEY `tmk_users_role_id_foreign` (`role_id`);

--
-- Indexes for table `tmk_user_warehouses`
--
ALTER TABLE `tmk_user_warehouses`
  ADD KEY `tmk_user_warehouses_user_id_foreign` (`user_id`),
  ADD KEY `tmk_user_warehouses_warehouse_id_foreign` (`warehouse_id`);

--
-- Indexes for table `tmk_variants`
--
ALTER TABLE `tmk_variants`
  ADD PRIMARY KEY (`article_id`,`attribute_value_id`),
  ADD KEY `tmk_variants_attribute_value_id_foreign` (`attribute_value_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logtriggers`
--
ALTER TABLE `logtriggers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_activities`
--
ALTER TABLE `tmk_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_ajustes_general`
--
ALTER TABLE `tmk_ajustes_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tmk_articles`
--
ALTER TABLE `tmk_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tmk_article_bodegas`
--
ALTER TABLE `tmk_article_bodegas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `tmk_article_masters`
--
ALTER TABLE `tmk_article_masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tmk_article_master_warehouses`
--
ALTER TABLE `tmk_article_master_warehouses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tmk_article_warehouses`
--
ALTER TABLE `tmk_article_warehouses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tmk_attributes`
--
ALTER TABLE `tmk_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tmk_attribute_values`
--
ALTER TABLE `tmk_attribute_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tmk_bodegas`
--
ALTER TABLE `tmk_bodegas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tmk_brands`
--
ALTER TABLE `tmk_brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tmk_categories`
--
ALTER TABLE `tmk_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tmk_classes`
--
ALTER TABLE `tmk_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_clientes_contactos`
--
ALTER TABLE `tmk_clientes_contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tmk_clientes_empresas`
--
ALTER TABLE `tmk_clientes_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tmk_clientes_sucursales`
--
ALTER TABLE `tmk_clientes_sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `tmk_clientes_tipo_contactos`
--
ALTER TABLE `tmk_clientes_tipo_contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tmk_devolution_purchases`
--
ALTER TABLE `tmk_devolution_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_devolution_purchase_details`
--
ALTER TABLE `tmk_devolution_purchase_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_devolution_transfers`
--
ALTER TABLE `tmk_devolution_transfers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_external_movements`
--
ALTER TABLE `tmk_external_movements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tmk_external_movement_details`
--
ALTER TABLE `tmk_external_movement_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tmk_inventories`
--
ALTER TABLE `tmk_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_inventory_details`
--
ALTER TABLE `tmk_inventory_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_lotes`
--
ALTER TABLE `tmk_lotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tmk_lotes_out`
--
ALTER TABLE `tmk_lotes_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tmk_measurement_units`
--
ALTER TABLE `tmk_measurement_units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tmk_movements`
--
ALTER TABLE `tmk_movements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tmk_orders`
--
ALTER TABLE `tmk_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tmk_order_details`
--
ALTER TABLE `tmk_order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `tmk_paises`
--
ALTER TABLE `tmk_paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `tmk_providers`
--
ALTER TABLE `tmk_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tmk_purchase_orders`
--
ALTER TABLE `tmk_purchase_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `tmk_purchase_order_details`
--
ALTER TABLE `tmk_purchase_order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `tmk_purchase_receipts`
--
ALTER TABLE `tmk_purchase_receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `tmk_purchase_series`
--
ALTER TABLE `tmk_purchase_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=796;

--
-- AUTO_INCREMENT for table `tmk_purchase_series_out`
--
ALTER TABLE `tmk_purchase_series_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tmk_roles`
--
ALTER TABLE `tmk_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tmk_states`
--
ALTER TABLE `tmk_states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tmk_tipo_gestion_articulos_table`
--
ALTER TABLE `tmk_tipo_gestion_articulos_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tmk_transfers`
--
ALTER TABLE `tmk_transfers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tmk_transfer_details`
--
ALTER TABLE `tmk_transfer_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tmk_type_movements`
--
ALTER TABLE `tmk_type_movements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tmk_type_providers`
--
ALTER TABLE `tmk_type_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tmk_users`
--
ALTER TABLE `tmk_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
