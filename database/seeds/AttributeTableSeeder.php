<?php


use TradeMarketing\Models\Attribute;
use Faker\Generator;
use Styde\Seeder\Seeder;

class AttributeTableSeeder extends Seeder
{
    protected $total = 50;

    public function getModel()
    {
        return new Attribute();
    }

    public function getDummyData(Generator $faker, array $custom = [])
    {
        return [
            'description' => $faker->name
        ];
    }

}