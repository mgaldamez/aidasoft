<?php

use Illuminate\Database\Seeder;

// Faker
use Faker\Factory as Faker;

class StatusOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  

    	$faker = Faker::create();

    	$status = array('draft', 'in_revision', 'rejected', 'approved', 'confirmed', 'finalized', 'canceled', 'partially_approved', 'partially_confirmed', 'in_transit', 'received', 'for_transfer', 'completed');
        $acronym = array('BDR', 'REV', 'RVDO', 'APDO', 'CFDO', 'FNDO', 'CNDO', 'PADO', 'PCDO', 'TRNS', 'RVDO', 'FRTF', 'CPDO');

    	for( $i=0; $i<count($status); $i++ ){

    		DB::table('tmk_order_status')->insert(array (    
                'id'           => $i+1,                     
    			'description'  => $status[$i],
                'acronym'      => $acronym[$i],
    			'created_at'   => $faker->dateTimeThisYear($max = 'now'),
    			'updated_at'   => $faker->dateTimeThisYear($max = 'now'),
    			));
    	}
    }
}
