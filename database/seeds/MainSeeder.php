<?php

use Illuminate\Database\Seeder;

class MainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$categories =  [ 'Desconocido', 'Promocional', 'Oficina', 'Promocionales', 'Vestuarios', 'Accesorios de computadoras', 'Flayers', 'Inflables'];

    	$clases = [
	    	'Desconocido' => [
	    		'Desconocido'
	    	],
	    	'Promocional' => [
			    'Paraguas',
				'Bolsas',
				'Escenarios',
				'Llaveros',
				'Accesorios'
	    	],
	    	'Oficina' => [
		    	'Boligrafos',
		    	'Sillas',
		    	'Equipo de audio',
		    	'Vestuarios',
		    	'Accesorios de computadoras'
	    	],
	    	'Publicidad' => [
		    	'Flayers',
		    	'Inflables',
		    	'Mostradores'
    		]
    	];

    	for ($i=0; $i < count($categories); $i++) { 

    		$category = \TradeMarketing\Models\Category::create([
    			'description' => $categories[$i],
                'created_by'        => 1,
                'updated_by'      	=> 1,
    			]);
   
    		// for ($j=0; $j < count($clases[$categories[$i]]) ; $j++) { 
		    // 	\TradeMarketing\Classes::create([
		    // 		'category_id' => $category->id,
		    // 		'description' => $clases[$categories[$i]][$j]
		    // 		]);
    		// }
    	}
    	// end

		$measurementUnits = ['Unidad', 'Caja 6', 'Caja 12', 'Caja 24', 'Kilogramos', 'Gramos', 'Libras', 'Toneladas','Milimetros cubicos','Centimetros cubicos','Metros', 'Centimetros', 'Milimetros', 'Fahrenheit', 'Kelvin', 'Celsius'];
		$unities = ['U', '6/1', '12/1', '24/1', 'Kg', 'g', 'lb', 'T', 'mm3', 'cm3', 'm', 'cm', 'mm','°F','°K', '°C'];

	  	for ($i=0; $i < count($measurementUnits); $i++) { 

			\TradeMarketing\Models\MeasurementUnits::create([
				'description' => $measurementUnits[$i],
				'unity'	 => $unities[$i],
				]);

    	}
    	// end



	} // 

}
