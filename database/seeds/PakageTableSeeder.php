<?php 

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PakageTableSeeder extends Seeder {

	public function run(){

		$faker = Faker::create();

		DB::table('tmk_pakages')->insert(array(
			'description' => 'desconocido',
			'created_at'  => $faker->dateTimeThisYear($max = 'now'),
	        'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
		));

		DB::table('tmk_pakages')->insert(array(
			'description' => 'Unidad',
			'quantity' 	  => 1,
			'created_at'  => $faker->dateTimeThisYear($max = 'now'),
	        'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
		));

		for( $i=6; $i<30; $i+=6){
			DB::table('tmk_pakages')->insert(array( 
				'description' => 'Caja',
				'quantity' 	  => $i,
				'created_at'  => $faker->dateTimeThisYear($max = 'now'),
		        'updated_at'  => $faker->dateTimeThisYear($max = 'now'),
			));
		}

	}
}