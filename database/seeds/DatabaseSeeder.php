<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('RolesTableSeeder');
        $this->call('UserTableSeeder');
        $this->call('ProviderTableSeeder');
		$this->call('BrandTableSeeder');
		// $this->call('PakageTableSeeder');
		$this->call('StatusOrderSeeder');
		$this->call('AttributeSeeder');
		$this->call('MainSeeder');
		$this->call('BodegaTableSeeder');

	}

}
