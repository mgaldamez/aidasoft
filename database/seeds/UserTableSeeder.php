<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        // DB::table('tbl_usuarios')->truncate();
//        DB::table('tbl_usuarios')->insert(array (
//            'Cod_agente'       => '9999991',
//            'username' => 'NCN03839',
//            'nombre'       => 'Ariel Muñoz',
//            'email'      => 'aemg12@gmail.com',
//            'password'   => hash('md5', 'admin'),
//        ));

        DB::table('tmk_users')->insert(array(
            'firstname' => 'Ariel',
            'lastname' => 'Muñoz',
            'role_id'  => 1,
            'agent_id' => '9999993',
            'username' => 'ncn03839',
            'email' => 'aemg12@gmail.com',
            'created_at' => '2016-07-24 08:11:16.000',
            'updated_at' => '2016-07-24 08:11:16.000',
            'password' => bcrypt('admin'),
        ));


        // DB::table('tmk_users')->insert(array (
        //         'agent_id'       => '9999991',
        //         // 'username'       => 'Ariel',
        //         // 'email'      => 'aemg12@gmail.com',
        //         'created_at' => '2016-07-24 08:11:16.000',
        //             'updated_at' => '2016-07-24 08:11:16.000',
        //         'password'   => hash('md5', 'admin'),
        //     ));

        // DB::table('tbl_usuarios')->insert(array (
        //         'cod_agente'       => '9999991',
        //         'username'       => 'Ariel',
        //         'email'      => 'aemg12@gmail.com',
        //         'password'   => hash('md5', 'admin'),
        //     ));


        // DB::table('users')->insert(array (
        //     'name'       => 'Ariel',
        //     'lastname'   => 'Muñoz', 
        //     'email'      => 'aemg12@gmail.com',
        //     'password'   => bcrypt('admin'),
        //     'address'    => 'Ocu, Herrera',
        //     'phone'      => '6602-1358',
        //     'role'       => 'admin',
        //     'created_at' => '2016-07-13 12:50:58.393',
        //     'updated_at' => '2016-07-13 12:50:58.393',
        // ));

        // for($i = 0; $i<10; $i++){
        //     DB::table('users')->insert(array (
        //         'name'            => $faker->name,
        //         'lastname'        => $faker->lastName, 
        //         'email'           => $faker->email,
        //         'password'        => bcrypt('password'),
        //         'remember_token'  => str_random(10),
        //         'address'         => $faker->address,  
        //         'phone'           => $faker->tollFreePhoneNumber,                          
        //         'role'            => $faker->randomElement(['user', 'admin']),
        //         'created_at'      => $faker->dateTimeThisYear($max = 'now'),
        //         'updated_at'      => $faker->dateTimeThisYear($max = 'now'),
        //     ));
        // }

        //  	factory(App\User::class)->create([
        //  		'name' => 'Ariel',
        // 'lastname' => 'Muñoz',
        // 'email' => 'aemg12@gmail.com',
        // 'password' => bcrypt('admin'),
        // 'role' => 'admin'
        //  	]);


        // factory(App\User::class, 10)->create();
    }
}
