// $.validator.setDefaults( {
//     submitHandler: function () {
//         // llamada a la funcion para registrar articulos 
//         new_article(); 
//     }
// } );

$( document ).ready( function () {
/**
* Validacion con una complejidad mayor
* response "campo 1 y campo 2 deben tener al menos 3 caracteres"
*/
jQuery.validator.addMethod("validacionCompleja",function(value, element, param) {
    var result = true;
    var comparador = $(param).val();
    
    if (value == "" || param == "") {
        result = true;
    }

    if (value.length >= 3 && comparador >= 3) {
        result = true;
    } else {
        result = false; 
    }

    return result;
}, "validacionCompleja");

jQuery.validator.addMethod("required_if_check", function(value, element, param){
    var result = true;
    var item = param[0];
    var value = param[1];

    if($(item).is(':checked')) {  
         if( $(item).val() == value && $(element).val() == 0 ){
            result = false
        } else {
            result = true 
        }
    }

return result;
}, "required_if_check");

var parentClass = ".form-group";

$( "#form-article" ).validate( {
    rules: {
        image: {
            accept: "png"
        },  
        barcode:{
            required: true
        },
        name:{
            required: true
        },
        provider:{
            required: true
        },
        brand:{
            required: true
        },
        category: {
            required: true
        },
        class: {
            required: true
        },
        bodega: {
            required: true
        },
        pakageOption: {
            required: true
        },
        box:{
            required_if_check: ["#optionsRadios2", 2],
            min: 1
        },
        quantity:{
            required_if_check: ["#optionsRadios1", 1],
            min: 1
        }, 
        minvalue:{
            required: true
        },
        unit_cost: {
            required: true
        },
        muWeight: {
            required_if_check: ['#check_weight', 1]
        },
        muVolume:{
            required_if_check: ['#check_volume', 1]
        },
        muLength:{
            required_if_check: ['#check_length', 1]
        },
        muTemperature:{
            required_if_check: ['#check_temperature', 1]
        }
    },
    messages: {
        image:{
            accept: "No es un archivo valido."
        },
        barcode:{
            required: "El campo código es obligatorio."
        },
        name:{
            required: "El campo nombre es obligatorio." 
        },
        provider:{
            required: "Necesitas seleccionar un proveedor para asociar una marca al artículo."
        },
        brand:{
            required: "El campo marca es obligatorio."
        },
        category:{
            required: "Necesitas seleccionar una categoría para asociar una clase al artículo."
        },
        class:{
            required: "El campo clase es obligatorio."
        },
        bodega:{
            required: "El campo bodega es obligatorio."
        },
        pakageOption:{
            required: "El campo embajaje es obligatorio."
        },
        box: {
            required_if_check: "Este campo es obligatorio.",
            min: "El tamaño de la caja/empaque debe ser de al menos 1."
        },
        quantity:{
            required_if_check: "Este campo es obligatorio.",
            min: "El tamaño debe ser de al menos 1."
        },
        minvalue:{
            required: "El campo es obligatorio."
        },
        unit_cost: {
            required: "El campo es obligatorio."
        },
        muWeight:{
            required_if_check: "Es obligatorio si se marca la opción."
        },
        muVolume:{
            required_if_check: "Es obligatorio si se marca la opción."
        },
        muLength:{
            required_if_check: "Es obligatorio si se marca la opción."
        },
        muTemperature:{
            required_if_check: "Es obligatorio si se marca la opción."
        }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "radio" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( parentClass ).addClass( "has-error" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( parentClass ).removeClass( "has-error" );
        }
    })
});