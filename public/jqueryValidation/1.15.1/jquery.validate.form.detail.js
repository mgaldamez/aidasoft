$.validator.setDefaults( {
    submitHandler: function () {
        // activar la function 
        create_details()
    }
});

$( document ).ready( function () {

var parentClass = ".form-group";

$( "#formArticleDetailList" ).validate( {
    rules: {
        colour:{
            required: true
        },
        quantity:{
            required: true
        },
        size: {
            required: true
        }   
    },
    messages: {
        color:{
            required: "El campo es obligatorio."
        },
        quantity: {
            required: "El campo es obligatorio."
        },
        size: {
            required: "El campo es obligatorio."
        }
    },
    errorElement: "em",
    errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "help-block" );

            if ( element.prop( "type" ) === "radio" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).parents( parentClass ).addClass( "has-error" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).parents( parentClass ).removeClass( "has-error" );
        }
    })
});