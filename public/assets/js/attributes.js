
'use strict';


(function($){

	$.fn.handleAttributes = function(options){

		var defaults = {
			tbody: '',
			btn_update: '<a href="#" class="btn btn-success btn-fab btn-fab-sm mdb" role="update"><i class="material-icons">check</i></a>',
			btn_controls: '<div class="btn-group" style="margin:0;">'+
						   '<a href="#" class="btn btn-warning btn-fab btn-fab-sm mdb" role="edit"><i class="material-icons">mode_edit</i></a>'+
						   '<a href="#" class="btn btn-danger btn-fab btn-fab-sm mdb" role="delete"><i class="material-icons">delete</i></a>'+
					      '</div>',
			// input: ['<input type="text" name="attribute[descripction][]" class="form-control" placeholder="Descripctión">',
			// 		'<input type="text" name="attribute[value][]" class="form-control" placeholder="Valor">']
		};

		options = $.extend(defaults, options);
		
		var methods = {

			add: function(row){

				var inputs = Array.prototype.slice.apply( $(row).find('input') );

				if( !this.strTrim(inputs) ) {

					var tr = document.createElement('tr');
					var html = '<td>'+
									'<span>'+ inputs[0].value +'</span>'+ 
									'<input type="hidden" name="attrValue[descripction][]" class="form-control" placeholder="Descripctión" value="'+  inputs[0].value +'">'+
								'</td>'+
								'<td>'+
									'<span>'+ inputs[1].value +'</span>'+
									'<input type="hidden" name="attrValue[value][]" class="form-control" placeholder="Valor" value="'+ inputs[1].value +'">'+
								'</td>'+
								'<td width="79">'+ options.btn_controls +	
								'</td>';

					tr.innerHTML = html;
					row.parentNode.insertBefore(tr, row);

					this.cleanInputs(inputs);	
				}
			},

			edit: function(row){
				
				row.classList.add('edit');
				row.children[2].innerHTML = "";
				row.children[2].innerHTML = options.btn_update;

				var inputs = Array.prototype.slice.apply( $(row).find('span') );
				this.readonly(inputs, false);
				
				$(row).delegate("a[role='update']", 'click', function(event){
					event.preventDefault();
					
					inputs = Array.prototype.slice.apply( $(row).find('input') );
					
					if( !methods.strTrim(inputs) ) {

						methods.readonly(inputs);
						row.children[2].innerHTML = "";
						row.children[2].innerHTML = options.btn_controls;
					}					
				});
			},


			delete: function(row){

				$(row).fadeOut(300, function(){ 
					$(row).remove();
				});
			},


			strTrim: function(array){
				var cont = 0;
				
				array.map( function(elem, item){

					elem.value = elem.value.trim();
					
					if( elem.value === '')
						cont++;
				});
				
				return ( cont ) ? true : false; 
			},


			cleanInputs: function(array){
				array.map( function(elem ){
					 elem.value = '';
				});
			},


			readonly: function(array, value = true){

				array.map( function(elem, index ){

					var parent = elem.parentNode;

					if( !value ){
						parent = elem.parentNode;

						var span = $(parent).find('span')[0];
						span.style.display = "none";
						
						var input =  $(parent).find('input')[0];
						input.type = "text";	
					} else {

						var input = $(parent).find('input')[0];
						input.type = "hidden";

						// var new_element = document.createElement('span');
						// new_element.appendChild( document.createTextNode(input.value) );
						// parent.append( new_element );
						var span = $(parent).find('span')[0];
						span.style.display = "block";
					}
				});
			}
		}

		return this.each(function() {

			options.tbody = $(this).find('tbody')
			var rows = $(options.tbody).find('tr');


			$(options.tbody).delegate("a[role='add']", 'click', function(event){
				event.preventDefault();
				methods.add(event.target.closest('tr'));
			});

			$(options.tbody).delegate("a[role='edit']", 'click', function(event){
				event.preventDefault();
				methods.edit(event.target.closest('tr'));
			});

			$(options.tbody).delegate("a[role='delete']", 'click', function(event){
				event.preventDefault();
				methods.delete(event.target.closest('tr'));
			});

		});

	}

})(jQuery);


$(function(){
	$("#__attributes").handleAttributes();
});