
// (function(d){

'use strict';




// buscar un articulo en la tabla por su id
function searchTable( id ){

	var i = 0;
	$("table#tbl-article tbody> tr").each( function() {

		var tableBody = $(this).children('td');
		var article = $('input', tableBody[0])[2].value;
		
		if( article === id ){
			i++;
		} 

	});

	if( i > 0 ){
		return false;
	} else return true;
}


// Calcular el total de la suma de los articulos agregados a la tabla
function __calculate (table) {

	var table        = document.querySelector('table> tbody');
	var rows         = table.getElementsByTagName('tr');
	var amount 	     = 0.00;
	var subtotalItem = $('tfoot input[name="subtotal"]');
	var totalItem 	 = $('tfoot input[name="total"]');
	var taxItem			 = $('tfoot input[name="tax"]');
	var discountItem = $('tfoot input[name="discount"]');
	var subtotal 	   = 0.00;
	var total        = 0.00;
	var totalTax     = 0.00;
	var discount     = 0.00;

	for (var i=0; i<rows.length; i++ ){

		var column = rows[i].getElementsByTagName('td');
		amount = parseFloat( column[ column.length - 1 ].childNodes[0].value);		
		subtotal += amount;
		totalTax += parseFloat( column[ column.length - 2 ].hasChildNodes('input')  ? column[ column.length - 2 ].lastChild.value * amount  : 0  );
	}

	total = (discountItem.val() <= (subtotal + totalTax) )? parseFloat( (subtotal + totalTax) - discountItem.val() ) : parseFloat(subtotal + totalTax);


	if( $.isNumeric(subtotal) ) {
		// $("label[for='"+ subtotalItem.attr('id') +"']").text( subtotal.toFixed(2) );
		subtotalItem.val( subtotal.toFixed(2) );
	} else {
		// $("label[for='"+ subtotalItem.attr('id') +"']").text( '0.00' );
		subtotalItem.val( '0.00' );
	}
	
	if( $.isNumeric(totalTax) ) {
		// $("label[for='"+ taxItem.attr('id') +"']").text( totalTax.toFixed(2) );
		taxItem.val( totalTax.toFixed(2) );
	} else {
		// $("label[for='"+ taxItem.attr('id') +"']").text( '0.00' );
		taxItem.val( '0.00' );
	}

	if( $.isNumeric(total) ) {
		// $("label[for='"+ totalItem.attr('id') +"']").text( total.toFixed(2) );
		totalItem.val( total.toFixed(2) );
	} else {
		// $("label[for='"+ totalItem.attr('id') +"']").text( '0.00' );
		totalItem.val( '0.00' );
	}	

	if (discountItem.val() == 0){
		discountItem.val( '0.00' );
	}

}




/* ADD */
function add (){

	var form 		= $('#create-article');
	var article     = $('select[id="_article"]');
	var articleText = $('select[id="_article"] option:selected');
	var quantity    = $('input[id="_quantity"]');
	var unity    	= $('label[id="_unity"]');
	var unitCost    = $('input[id="_unit_cost"]');
	var tax     	= $('select[id="_tax"]');
	var taxText 	= $('select[id="_tax"] option:selected');
	var data  		= $('#tbl-article tbody');
	var amount 		= quantity.val() * unitCost.val();

	data.append(
		'<tr id='+ article.val() +' class="table-row">'+
		'<td data-label="">'+ 
		'<div class="checkbox checkbox-mini">'+
		'<label>'+
		'<input type="checkbox" name="check" value="0">'+
		'</label>'+
		'</div>'+
		'</td>'+

		'<td data-label="Descripcion" class="cell-description">'+ articleText.text() +
		'<input type="hidden" name="items[][description]" class="name_handler" value="'+ articleText.text() +'">'+
		'<input type="hidden" name="items[][id]" class="name_handler" value="'+ article.val() +'">'+
		'</td>'+

		'<td data-label="Cantidad">'+
		'<input type="text" name="items[][quantity]" class="name_handler" value="'+ ((quantity.val())? quantity.val() : 0) +'" readonly>'+
		'</td>'+

		'<td data-label="Unidad">'+
		'<input type="text" name="items[][unity]" class="name_handler" value="'+ unity.text() +'" readonly>'+
		'</td>'+

		'<td data-label="Costo">'+
		'<input type="text" name="items[][unit_cost]" class="name_handler" value="'+ parseFloat(unitCost.val()? unitCost.val() : 0.00).toFixed(2) +'" readonly>'+
		'</td>'+

		'<td data-label="Impuesto">'+
		'<input type="text" name="items[][taxText]" class="name_handler" value="'+ taxText.text() +'" readonly>'+
		'<input type="hidden" name="items[][taxId]" class="name_handler" value="'+ tax.val() +'">'+
		'</td>'+

		'<td data-label="Monto" class="hidden-xs">'+
		'<input type="text" name="items[][amount]" class="name_handler" value="'+ parseFloat(amount).toFixed(2) +'" readonly>'+
		'</td>'+
		'</tr>'
		);

	$.material.init();


	// Clear inputs
	article.val('');
	quantity.val('');
	unitCost.val('');
	tax.val('.00');
}


$(document).delegate("#btn-add", "click", function(event){
	event.preventDefault();


	var row = event.target.closest('tr');
    var select = $(row).find('select#_article')[0].value;
	if (select){
        add();
    }
});


// Trae la lista de costos de un ariculo 
$(document).delegate('#_article', 'change', function(event){
	var itemId = $(this).val();
	var uCost = $('#_unit_cost');
	var unity = $('#_unity');

	$.get( '/article/complete/'+itemId, function(res){
		
		unity.val( ( res['unity'] )?  parseFloat( res['unity'] ).toFixed(2)  : 'Unidad' );

		uCost.val( ( res['cost'] )? parseFloat( res['cost'] ).toFixed(2) : '' );

	});

	$('#_quantity').focus();

});



/* DELETE */ 
$(document).delegate('#remove-items', 'click', function(event){ 
	event.preventDefault();

	$("table#tbl-article tbody tr").each(function (index) {

		if (  $(this).find("input[type='checkbox']").is(':checked')  ){

			$(this).fadeOut(300, function(){ 
				$(this).remove();
				verifyCheck();
			});

		}

	});

});


/*SELECT CHECK */
$(document).delegate('table#tbl-article tbody tr input[type="checkbox"]', 'click', function( event ){

	var checkbox = $(this);

	var row 	 = event.target.closest('tr');

	if( checkbox.is(':checked') ) {
		checkbox.val(1)
		row.classList.add('row-active') 
	} else {
		checkbox.val(0);
		row.classList.remove('row-active')
	}

});




$(document).delegate("input[type='checkbox']#select-all", 'click', function( event){

	if ( $(this).is(':checked') ){

		$("table#tbl-article tbody tr").each( function (index) {

			if( $(this).find("input[type='checkbox']").not(':checked') ){

				$(this).find("input[type='checkbox']").prop('checked', 'checked');
				$(this).find("input[type='checkbox']").val(1);
				$(this).addClass('row-active');
			}
		})

	} else {
		$("table#tbl-article tbody tr").each( function (index) {
			if( $(this).find("input[type='checkbox']").is(':checked') ){

				$(this).find("input[type='checkbox']").prop('checked', false);
				$(this).find("input[type='checkbox']").val(0);
				$(this).removeClass('row-active');
				$(this).removeClass('edit');
			}
		});
	}
});


$(document).delegate('table#tbl-article tbody tr input[type="checkbox"]', 'change', function(){

	var checkAll = $('input[type="checkbox"]#select-all');

	checkAll.is(':checked') ? checkAll.prop('checked', false) : '';

});



/* EDIT */
$(document).delegate('#edit-item', 'click', function(event){
	event.preventDefault();

	console.log('edit');

	$("table#tbl-article tbody tr").each( function (index) {

		if ( $(this).find("input[type='checkbox']").is(':checked') &&  $(this).hasClass('edit') != true ){
			
			$(this).addClass('edit');

			var article  = $(this).attr('id');
			var quantity = $(this).find('td')[2];
			var unity 	 = $(this).find('td')[3];
			var unitCost = $(this).find('td')[4];
			var tax  	 	 = $(this).find('td')[5];
			var amount   = $(this).find('td')[6];
			var modal    = $('#articleModal');

			modal.find('.table-row').addClass('edit');

			modal.find('.table-row').attr('id', article);

			modal.find('select[id="_article"]').val( article ).change();

			modal.find('input[id="_quantity"]').val( quantity.textContent );

			modal.find('input[id="_unit_cost"]').val( unitCost.textContent ).change();

			modal.find('select[id="_tax"]').val(  tax.lastChild.value ).change();

			modal.find('.modal-title').text('Editar');

			modal.find('select[id="_article"]').focus();

			modal.modal('show');
		} 

	});

});


$(document).delegate('#edit-items', 'click', function(event){
	event.preventDefault();

	$("table#tbl-article tbody tr").each( function (index) {

		if ( $(this).find("input[type='checkbox']").is(':checked') &&  $(this).hasClass('edit') != true ){

			// console.log( $(this).find('input') );
			
			var article  = $(this).attr('id');
			var quantity = $(this).find('td input')[3];
			var unity 	 = $(this).find('td input')[4];
			var unitCost = $(this).find('td input')[5];
			var tax  	 = $(this).find('td input')[6];
			var amount   = $(this).find('td input')[7];
			var data 	 = $(this);

			for (var i = data.children().length - 1; i >= 1; i--) {
				data.children()[i].remove()
			}

			data.addClass('edit');
			data.append(
				'<td>'+
				'<select name="_article" class="form-control">' + $('select[id="_article"]').html() +
				'</select>'+
				'</td>'+

				'<td>'+
				'<input type="text" name="_quantity" class="form-control" placeholder="Cantidad de articulos" value='+ quantity.value +'>'+
				'</td>'+

				'<td data-label="Unidad">'+
				'<label name="_unity" class="form-control">'+ unity.value +'</label>'+
				'</td>'+

				'<td>'+
				'<input type="text" name="_unit_cost"  class="form-control" placeholder="Costo por unidad" value='+ unitCost.value +'>'+
				'</td>'+

				'<td>'+
				'<select name="_tax" class="form-control">'+ $('select[id="_tax"]').html() +'</select>'+
				'</td>'+

				'<td>'+ amount.value +'</td>'
				);

			data.find('select[name="_article"]').val( article ).change();
			data.find('select[name="_tax"]').val( tax.value ).change();

			// $('#update').removeClass('hidden');
			$('#update').closest('tr').removeClass('hidden');
			$('#create-article').addClass('hidden');

			$.material.init();

		} 

	}); 

});

/* UPDATE */
$(document).delegate('#update', 'click', function( event){
	event.preventDefault();
	update();
});


// var rowId 		= $(row).attr('id');
// var article 	=  $(form).find('select[id="_article"]');
// var articleText =  $(form).find('select[id="_article"] option:selected');
// var quantity 	=  $(form).find('input[id="_quantity"]');
// var unitCost 	=  $(form).find('input[id="_unit_cost"]');
// var tax 		=  $(form).find('select[id="_tax"]');
// var taxText 	=  $(form).find('select[id="_tax"] option:selected');
// var amount 		= quantity.val() * unitCost.val();
// var data 		= $(this);

function update( elemt ){
	
	$("table#tbl-article tbody tr").each( function (index) {

		var row =  this.closest('.table-row');
		
		console.log(row)
		
		if( row.classList.contains('edit') == true ){

			var rowId 	= ( elemt )? $(elemt).find('.table-row').attr('id') : $(row).attr('id');
			console.log( rowId )

			var article   	= $('#'+ rowId +' select[name="_article"]');
			var articleText = $('#'+ rowId +' select[name="_article"] option:selected');
			var quantity 	  = $('#'+ rowId +' input[name="_quantity"]'); 
			var unity 			= $('#'+ rowId +' label[name="_unity"]');
			var unitCost   	= $('#'+ rowId +' input[name="_unit_cost"]');
			var tax 		    = $('#'+ rowId +' select[name="_tax"]');
			var taxText   	= $('#'+ rowId +' select[name="_tax"] option:selected');
			var amount  		= quantity.val() * unitCost.val();
			var data 	     	= $(this);

			for (var i = data.children().length - 1; i >= 1; i--) {
				data.children()[i].remove();
			}

			data.append(	
				'<td class="cell-description">'+ articleText.text() +
				'<input type="hidden" name="item[article][text][]" value="'+ articleText.text() +'">'+
				'<input type="hidden" name="item[article][id][]" value="'+ article.val() +'">'+
				'</td>'+

				'<td>'+ quantity.val() +
				'<input type="hidden" name="item[quantity][]" value="'+ quantity.val() +'">'+
				'</td>'+

				'<td data-label="Unidad">'+ unity.text() +
				'<input type="hidden" name="item[unity][]" value="'+ unity.text() +'">'+
				'</td>'+

				'<td>'+ parseFloat(unitCost.val()).toFixed(2) +
				'<input type="hidden" name="item[unit_cost][]" value="'+ parseFloat(unitCost.val()).toFixed(2) +'">'+
				'</td>'+

				'<td>'+ taxText.text() +
				'<input type="hidden" name="item[tax][text][]" value="'+ taxText.text() +'">'+
				'<input type="hidden" name="item[tax][id][]" value="'+ tax.val() +'">'+
				'</td>'+

				'<td class="hidden-xs">'+ parseFloat(amount).toFixed(2) +
				'<input type="hidden" name="item[amount][]" value="'+ parseFloat(amount).toFixed(2) +'">'+
				'</td>'
				);

			$(row).attr('id', articleText.val());

			row.classList.remove('edit');
		}
	});

	// $('#update').addClass('hidden');
	$('#update').closest('tr').addClass('hidden');
	$('#create-article').removeClass('hidden'); 

	verifyCheck();	
}



function verifyCheck(){
	var countCheck = $('table#tbl-article tbody :checkbox:checked').length;

	var countSelect = $('#countSelect');

	if( countCheck > 0 ){

		$('.tab-control-bottom').removeClass('hidden');
		countSelect.text( '('+ countCheck +') seleccionados' );
		countSelect.show();

	} else {

		$('.tab-control-bottom').addClass('hidden');
		countSelect.hide();

	} 

	( countCheck > 1 ) ? $('#edit-item').hide() : $('#edit-items').show();
}



/* INIT */
$(function(){

	// Confirmar que hubo algun cambio  en la tabla
	$(document).delegate('table#tbl-article tbody', 'DOMSubtreeModified', function(event){

        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

        rows.forEach(function (items, key) {

            var elements = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

            elements.forEach(function (item) {

                var nameAttr = item.getAttribute('name');

                var split = nameAttr.split("]");

                // var split_2 = nameAttr.split("[")
                var newName = split[0].split("[")[0] + '[' + key + ']' + split[1] + ']';

                // change attribute name
                item.setAttribute('name', newName);
            });
        });

        __calculate( 'tbl-article' );


		$(document).delegate('table#tbl-article :checkbox', 'click', function(){

			verifyCheck();
		});
	});
});


/* DISCOUNT */
$(document).delegate('#discountModal', 'shown.bs.modal',  function( event ){
	event.preventDefault();

	var discount = $('#_discount');
	var discountItem = $('input[name="discount"]');

	if( $.isNumeric( discountItem.val() ) && discountItem.val() != 0 ){
		discount.val( parseFloat( discountItem.val() ).toFixed(2) )
	} 

	discount.focus();

	$('#add-discount').click( function( event ){
		event.preventDefault();

		if( $.isNumeric( discount.val() ) ){

			discountItem.val( parseFloat(  discount.val() ).toFixed(2) );
			$("label[for='"+ discountItem.attr('name')+"']").text( parseFloat( discount.val() ).toFixed(2) ); 			

		} else {

			discountItem.val( parseFloat( '0.00' ) );
			$("label[for='"+ discountItem.attr('name')+"']").text( '0.00' ); 
		}

		__calculate();
		
		$('#discountModal').modal('hide');
	});
});



// MODAL
$(document).delegate('#articleModal #agree', 'click',  function(event) {
	event.preventDefault();
	
	var modal = event.target.closest('#articleModal');

	if (  $(modal).find('.edit').length ) {
		update( modal );
	}
	else {
		add();
	}

	$(modal).modal('hide');
});



$(document).delegate('#articleModal', 'shown.bs.modal',  function( event ){

	$(this).find('select[id="_article"]').focus();
});


$(document).delegate('#articleModal', 'hidden.bs.modal', function () {

	$(this).find('select[id="_article"]').val('');

	$(this).find('input[id="_quantity"]').val('');

	$(this).find('input[id="_unit_cost"]').val('');

	$(this).find('select[id="_tax"]').val('');

	$(this).find('.modal-title').text('Agregar');
});


// })(document);