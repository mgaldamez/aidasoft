
(function($){

//Renumber table rows
function renumber_table(tableID) {
	$(tableID + " tr").each(function() {
		count = $(this).parent().children().index($(this)) + 1;
		$(this).find('.priority').html(count);
	});
}

})(jQuery);


$(document).ready(function() {
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function(e, tr) {
    	var $originals = tr.children();
    	var $helper = tr.clone();
    	$helper.children().each(function(index)
    	{

    		$(this).width($originals.eq(index).width())
    	});
    	return $helper;
    };

    //Make diagnosis table sortable
    $("table tbody").sortable({
    	helper: fixHelperModified,
    	cursor: "move",
    	// stop: function(event,ui) {renumber_table('#diagnosis_list')}
    }).disableSelection();

    //Delete button in table rows
    $('.table-reorder-row').on('click','.btn-delete',function() {
    	tableID = '#' + $(this).closest('table').attr('id');
    	r = confirm('Delete this item?');
    	if(r) {
    		$(this).closest('tr').remove();
    		renumber_table(tableID);
    	}
    });
});