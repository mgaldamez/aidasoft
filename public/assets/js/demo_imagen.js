'use strict';

(function(d){


// icono cargar imagen 
var iconBtnCargar = $("#btn-cargar > span"); 
var imgDefault = '<i class="material-icons">photo_size_select_actual</i>';
var imgPreview = $(".__img-preview");
var modal = $('#myModal');
var uploadCrop = $('#upload-demo');
var fileUpload = $("#upload");
var base64 = $("textarea[name='image']");
// var load = '<div id="spin" class="load-rotate"><span class="glyphicon glyphicon-refresh"></span></div>';
var spin = $('#spin.load-rotate');


$('#btn-cargar').click(function (event) {
	
	event.preventDefault();
	fileUpload.click();
});


$('#btn-quitar').click(function (e) {

	fileUpload[0].files[0]; 

	imgPreview.find('img').remove();
	imgPreview.html(imgDefault);
});


function demoHidden() {

	uploadCrop.croppie({
		enableExif: true,
		viewport: {
			width: 250,
			height: 250
		},
		boundary: {
			width: 300,
			height: 300
		},
		enableOrientation: true
	});


	fileUpload.on('change', function () { 
		console.log(this);
		
		var reader = new FileReader();

		reader.onload = function (e) {
			uploadCrop.croppie('bind', {
				url: e.target.result
			}).then(function(){
				console.log('jQuery bind complete');
			});
		}


		reader.readAsDataURL(this.files[0]);

		$(modal).modal({
			'backdrop': 'static',
			'show': true
		});
	});

	$(modal).on('shown.bs.modal', function () {
		uploadCrop.croppie('bind');
	});

	$(".cr-slider-wrap").addClass('hidden-xs');

	$('.rotate').on('click', function(event) {
		event.preventDefault();
		uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
	});
}


$('.upload-result').on('click', function (event) {
	event.preventDefault();

	uploadCrop.croppie('result', {
		type: 'canvas',
		size: 'viewport'
	}).then(function (resp) {

		spin.css('display', 'block');

		var img = '<img src="' + resp + '" style="width:100%;" />';


		setTimeout(function(){ 

			imgPreview.html(img);

			base64.val( resp );
			modal.modal('hide');
			spin.css('display', 'none');
		}, 2000);


		// $.ajax({
		// 	headers: { 'X-CSRF-TOKEN':  $("input[name='_token']").val() },
		// 	url: "/image/cropp",
		// 	type: "POST",
		// 	data: {"image":resp},
		// 	success: function (data) {
		// 		console.log(data);
		// 		var img = '<img src="' + resp + '" style="width:100%;" />';
		// 		imgPreview.html(img);
		// 		$("textarea[name='image']").val( resp );
		// 	}
		// });


	});
});


imgPreview.html(imgDefault);
demoHidden();

})(document);
