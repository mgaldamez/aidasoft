var miTabla = 'table-inventary'; // d de la tabla

// preparar la tabla para edición
function iniciarTabla() {
    tab = document.getElementById(miTabla);

    filas = tab.getElementsByTagName('tr');
    for (i = 0; fil = filas[i]; i++) {
        celdas = fil.getElementsByTagName('td');
        for (j = 0; cel = celdas[j]; j++) {
            //      if ((i>0 && j==celdas.length-1) || (i==filas.length-1 && j!=0)) continue; // La última columna  y la última fila no se pueden editar
           
            if (j == 6) {
                cel.onclick = function () {
                    crearInput(this);
                }
            }
        } // end for j 
    } //end for i
} // end function


// crear input para editar datos
function crearInput(celda) {
    celda.onclick = function () {
        return false
    }
    txt = celda.innerHTML;
    celda.innerHTML = '';
    obj = celda.appendChild(document.createElement('input'));
    obj.value = txt;
    obj.focus();
    obj.onblur = function () {
        txt = this.value;
        celda.removeChild(obj);
        celda.innerHTML = txt;
        celda.onclick = function () {
            crearInput(celda)
        }
        sumar()
    }

    $('td.form-group').attr('style', 'box-sizing: border-box !important; ');
    $('td.form-group input').attr('type', 'number');
    $('td.form-group input').attr('style', 'width:90% !important; margin: auto auto; padding: 0px !important');
    $('td.form-group input').addClass('form-control input-sm text-right');
}

// sumar los datos de la tabla
function sumar() {

    tab = 3, // tab inicial
    tab_costoU = tab,
    tab_actual = tab + 1,
    tab_stock  = tab + 2,
    tab_compr  = tab + 3,
    tab_saldo  = tab + 4,
    tab_subtl  = tab + 5,
    saldototal = 0,
    total = 0.00,
    table = document.getElementById(miTabla),
    filas = table.getElementsByTagName('tr'),
    subt = filas[filas.length - 1].getElementsByTagName('th'),
    sum = new Array(filas.length);

    for (i = 0; i < sum.length - 1; i++)
        sum[i] = 0;

    for (i = 1, tot = filas.length - 1; i < tot; i++) {

        saldo = 0;
        subtotal = 0.00;
        celdas = filas[i].getElementsByTagName('td');

        for (j = 3, to = celdas.length; j < to; j++) {
            num = parseFloat(celdas[j].innerHTML);

            if (isNaN(num)) num = 0;

            sum[j - 2] += num;

            // Calcular el saldo de articulos en el inventario
            saldo = parseFloat(celdas[tab_compr].innerHTML) - parseFloat(celdas[tab_actual].innerHTML);
            if (saldo == 0) {
                saldo = 0
            } else if (saldo > 0) {
                saldo = "+" + saldo
            }

            // Calcular subtotal por articulo 
            subtotal = parseFloat(celdas[tab_costoU].innerHTML) * parseInt(celdas[tab_compr].innerHTML);

        } // end for j

        celdas[celdas.length - 2].innerHTML = saldo;
        celdas[celdas.length - 1].innerHTML = subtotal.toFixed(2);
        saldototal += parseInt(saldo);
        total += subtotal;

        if (saldototal > 0) {
            sum[tab_saldo - 2] = "+" + saldototal
        } else sum[tab_saldo - 2] = saldototal;

        sum[tab_subtl - 2] = total;
    } // end for i

    // Calcular Totales 
    for (i = 1, tot = subt.length; i < tot; i++)
        if (i == tab_costoU - 2 || i == tab_stock - 2 || i == tab_subtl - 2)
            subt[i].innerHTML = "$" + sum[i].toFixed(2);
        else subt[i].innerHTML = sum[i];
} // end function