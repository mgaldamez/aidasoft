function chart_article_bodega(a) {
    // console.log(a[0])
    var dato = JSON.parse("[" + a[0] + "]");
    var ctx = document.getElementById('myChart').getContext("2d");

    var data = dato;
    var options = {
        // Boolean - whether or not the chart should be responsive and resize when the browser does.
        responsive: true,
        //Number - The width of each segment stroke
        segmentStrokeWidth: 1,
        //Number - The percentage of the chart that we cut out of the middle
        //            percentageInnerCutout : 80, // This is 0 for Pie charts
    }
    var myhChart = new Chart(ctx).Pie(data, options);
    document.getElementById('js-legend').innerHTML = myhChart.generateLegend();
}
