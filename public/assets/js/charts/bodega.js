 var bodega = window.location.pathname.split('bodega/graphs/')[1];

 function chart_bodega(a, b, c, bodega) {
     var jsonLabels = JSON.parse("[" + a[0] + "]");
     var jsonData = JSON.parse("[" + b[0] + "]");
     var jsonColour = JSON.parse("[" + c[0] + "]");
console.log( jsonColour)
     // DATA
     var data = {
         labels: jsonLabels,
         datasets: [
             {
                 label: '',
                 backgroundColor: jsonColour,
                 data: jsonData
            }
        ]
     };

     // OPTIONS
     var options = {
         responsive: true,
         title: {
             display: true,
             text: "Cantidades de articulos por bodegas"
         },
         legend: {
             display: false
         },
         animation: {
             onComplete: function () {
                 var chartInstance = this.chart;
                 var ctx = chartInstance.ctx;
                 ctx.textAlign = "center";

                 Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                     var meta = chartInstance.controller.getDatasetMeta(i);
                     Chart.helpers.each(meta.data.forEach(function (bar, index) {
                         ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 10);
                     }), this)
                 }), this);
             }
         }
     }

     var ctx = document.getElementById('chartBodega').getContext("2d");
     window.myHorizontalBar = new Chart(ctx, {
         type: 'bar',
         data: data,
         options: options
     });
 }


 function prueba() {
     var route = 'bodegas';
     var labels = [],
         data = [],
         colour = [];

     var _labels = '';
     var _data = '';
     var _colour = '';
     var _bodega;
     var colourDefault = "#ccc";
     $.get(route, function (res) {

         $(res).each(function (key, value) {
             _labels += '"' + value.bodega + '"';
             _data += '"' + value.articles + '"';

             if (value.id == bodega) {
                 _bodega = value.bodega;
                 _colour += '"' + value.colour + '"';
             } else {
                 _colour += '"' + colourDefault + '"';
             }

             if (key < (res.length - 1)) {
                 _labels += ',';
                 _data += ',';
                 _colour += ',';
             }
         });

         labels[0] = _labels;
         data[0] = _data;
         colour[0] = _colour;
         //        
         chart_bodega(labels, data, colour, _bodega);
     });

 }