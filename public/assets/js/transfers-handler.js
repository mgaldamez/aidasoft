'use strict';

(function ($) {

    $.fn.transfersHandler = function (options) {

        var defaults = {
            article_list: [],
            articles: [],
            transfer: ''
        };

        options = $.extend(defaults, options);

        var methods = {

            addRow: function (row, item) {

                var cell_article = row.getElementsByClassName("cell-article")[0];
                var cell_measurement_unit = row.getElementsByClassName("cell-measurement-unit")[0];
                var cell_quantity = row.getElementsByClassName("cell-quantity")[0];
                var cell_stock = row.getElementsByClassName("cell-stock")[0];
                var cell_available = row.getElementsByClassName("cell-available")[0];
                var article = this.findById(options.article_list, item.value);

                if (!article) {
                    return false;
                }

                cell_article.children[0].value = article.id;
                cell_article.append( article.serial_code +' / '+ article.description );
                cell_stock.textContent = article.stock;
                cell_available.textContent = article.available;
                cell_measurement_unit.textContent = article.measurement_unit;

                var table = document.querySelector('#stock-transfer tbody');

                table.append(row);
            },

            remove: function (row) {

                $(row).fadeOut(300, function () {
                    this.remove();
                });
            },

            findById: function (items, id) {
                for (var i in items) {
                    if (items[i].id == id) {
                        return items[i];
                    }
                }

                return null;
            },

            beforeAddRow: function (item) {

                var articles = [];
                var array = Array.prototype.slice.call(document.querySelectorAll('.cell-article input'));

                array.forEach(function (item) {
                    var element = {};
                    element.id = item.value;
                    articles.push(element);
                });

                if (methods.findById(articles, item)) {
                    return false;
                }

                return true;
            }
        };


        return this.each(function () {

            $(document).delegate('#add-row', 'click', function (event) {
                event.preventDefault();

                var article_select = document.getElementById('article');

                if (!methods.beforeAddRow(article_select.value)) {
                    alert("El articulo ya existe en la lista");
                    return false;
                }

                var new_row = document.querySelector('#line-article tbody tr').cloneNode(true);

                methods.addRow(new_row, article_select);
            });

            $(this).delegate('*[role="delete-row"]', 'click', function (event) {
                event.preventDefault();

                var row = event.target.closest('tr');
                methods.remove(row);
            });


            $(this).delegate('.cell-quantity .form-control', 'keyup', function (event) {
                event.preventDefault();

                var row = event.target.closest('tr');
                var article = $(row).find('.cell-article input').val();
                var available = methods.findById( options.article_list, article).available;
                var cell_available = $(row).find('.cell-available')[0];



                if( $.isNumeric(this.value) && parseInt(this.value) >= 0 ){

                    var quantity = parseInt(available) - parseInt(this.value);

                    if(quantity >= 0) {
                        cell_available.textContent = quantity;
                    } else {
                        cell_available.textContent = 0;
                    }

                } else {
                    cell_available.textContent = available;
                }
            });


            $.getJSON('/transfer/' + options.transfer + '/warehouse/stock', function (data) {


                data.forEach(function (item) {
                    var object = {};
                    object.id = item.article_id;
                    object.description = item.article;
                    object.serial_code = item.serial_code;
                    object.stock = item.stock;
                    object.available =  item.available_quantity;
                    object.measurement_unit = item.measurement_unit;

                    options.article_list.push(object);
                });
            }).fail(function(errors){
                console.log(errors)
            });


            // Confirmar que hubo algun cambio  en la tabla
            $(this).on('DOMSubtreeModified', function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                var rows = Array.prototype.slice.call(this.getElementsByTagName('tr'));

                rows.forEach(function (items, key) {

                    var inputs = Array.prototype.slice.call(items.querySelectorAll('.name_handler'));

                    inputs.forEach(function (items) {

                        var nameAttr = items.getAttribute('name');

                        var split = nameAttr.split("]");

                        var split2 = nameAttr.split("[")

                        var newName = split[0].split("[")[0] + '[' + (key - 1) + '][' + split2.pop();

                        // change attribute name
                        items.setAttribute('name', newName);
                    });
                });
            });
        });
    }

})(jQuery);