//Cantidad de hijos que hay dentro del padre
var countChild = $('.wizard-container').children(".step").size();
//console.log(countChild);

//var item1 = $('.__wizard .progressbar li.active');
//var item2 = item1.next();
//var item3 = item2.next();

var optionEnd = 2;
var countStep = 1; // paso 1
var btn       = $("#btn-next");

// muestra y oculta los pasos segun sea el caso
function show_Step(stepHidden, stepShow) {
    //console.log(stepHidden, stepShow)
    $(".step:nth-child(" + stepHidden + ")").addClass("hidden");
    $(".step:nth-child(" + stepShow + ")").removeClass("hidden");


    if ( stepShow == countChild ) {
        btn.html('Finalizar');
        btn.removeClass('btn-primary');
        btn.addClass('btn-success');
    } else {
        $("#btn-next").removeClass("hidden");
        btn.html('Siguiente <span class="glyphicon glyphicon-menu-right"></span>');
        btn.removeClass('btn-success');
        btn.addClass('btn-primary');
    }

    if (stepShow == 1) {
        $("#btn-previous").addClass("hidden");
    } else {
        $("#btn-previous").removeClass("hidden");
    }
}

//moverse al paso siguiente
$('#btn-next').click(function (e) {
    if (countStep < countChild) {
        show_Step(countStep, countStep + 1);
        countStep += 1;

        $('.__wizard .progressbar li:nth-child(' + countStep + ')').addClass("active"); //agregar clase active 
    } else if (countStep == countChild) {
        //Detener el comportamiento normal del evento click sobre el elemento clicado
        
        e.preventDefault();
    }
})

//moverse al paso anterior
$('#btn-previous').click(function () {
    $('.__wizard .progressbar li:nth-child(' + countStep + ')').removeClass("active"); //quitar clase active 

    if (countStep <= countChild && countStep > 1) {
        show_Step(countStep, countStep - 1);
        countStep -= 1;
    }
})