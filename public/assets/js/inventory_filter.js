'use strict';


(function ($) {

    $('form[role="search"] select[id="filter"]').change(function (event) {

        var filterBy = $(this)[0].selectedOptions[0].value;
        var input = $('form[role="search"] datalist[id="browsers"]');
        var input_param = $('form[role="search"] input[name="param"]');

        input_param.val('');
        $('#example_filter input[type="search"]').val('').keyup();

        $.get('/inventory/filter/' + filterBy, function (res) {
            var options = '';

            if (res.length == 0) {
                options = '<option value="">Todo</option>';

            } else {

                $.each(res, function (index, value) {

                    options += '<option value="' + value + '">' + value + '</option>';
                });
            }


            input.html(options);
        });
    });


})(jQuery);



(function($){


    //
    // $('form[role="search"]').submit(function(event){
    //    event.preventDefault();
    //
    //     $.get('inventory', $(this).serialize(), function(res){
    //         $('tbody#inventory-data').html(res);
    //     });
    //
    // });


})(jQuery);