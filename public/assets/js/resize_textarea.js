jQuery.each(jQuery('textarea[data-autoresize]'), function() {
	var offset = this.offsetHeight - this.clientHeight;
	var resizeTextarea = function(el) {	
		jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
	};

	resizeTextarea(this);
	jQuery(this).on('change input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
});



// var textarea = document.querySelector('textarea');

// textarea.addEventListener('keydown', autosize);

// function autosize(){
//   var el = this;
//   setTimeout(function(){
//     el.style.cssText = 'height:auto; padding:0';
//     // for box-sizing other than "content-box" use:
//     // el.style.cssText = '-moz-box-sizing:content-box';
//     el.style.cssText = 'height:' + el.scrollHeight + 'px';
//   },0);
// }