'use strict';



//
// $("#menu").hover( function () {
//        $(".ar-sidebar-menu").toggleClass('open-part');
////       $(".droupdown-menu").toggleClass('open');
//    })

/*EFECTO DROPDOWN DEL MENU LATERAL*/
$(function () {

	var DropdownMenu = function (el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		/*Variable privadas*/
		var link = this.el.find('.droupdown-menu > a');
		/*Evento*/
		link.on('click', {
			el: this.el,
			multiple: this.multiple
		}, this.dropdown);
	}

	DropdownMenu.prototype.dropdown = function (e) {
		var $el = e.data.el,
		$this = $(this),
		$next = $this.next();
        //                console.log("");
        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
        	$el.find('.sub-menu').not($next).slideUp().parent().removeClass('open');
        }
    }

    var dropdownMenu = new DropdownMenu( $('#menu'), false);

});




(function(d){
	
	// $('.menu li:has(ul)').click( function(e){ })
	var menu = Array.prototype.slice.apply(d.querySelectorAll('.menu > li ul') );

	for( var i = 0; i< menu.length; i++){
		
		menu[i].parentNode.addEventListener('click', function( e ){
			// e.preventDefault();

			if (this.classList.contains('active')){

				this.classList.remove('active');
				$(this).find('ul.sub-menu').css('display', 'none');
			} else {
				this.classList.add('active');
                $(this).find('ul').css('display', 'block');
			} 

		});

	}

})(document);



/*BOTON TOGGLE DEL MENU PRINCIPAL*/
$(document).ready(function () {

    $(".toggle-menu").on('click', function () {
        $(".sidebar-menu").toggleClass('open-part');
        $(".container-wrapper").toggleClass('open-part');
        // $(".__container header").toggleClass('open-part');
    });
});