"use stric";


(function ($) {

    $.fn.searching = function (options) {

        var methods = {
            filter: function (form) {

                var input = $(form).find('input[name="search"]');
                var filter  = $(form).find('select[name="filter"] :selected')[0].textContent;

                var param = $(input).val();

                var render =  document.getElementById('articlesRender');

                $.get('article?search=' + param +'&filter='+ filter, function(res){
                    render.innerHTML = res ;
                      // $('table').dataTable('refresh');
                } );
            }
        };
        return this.each(function () {
            var form = this;
            var button =  $(this).find('button');



            $(form).submit(function(event){
                event.preventDefault();
                methods.filter(form);

            });


            $(button).click(function (event) {
                event.preventDefault();

                 methods.filter(form);
            });
        });
    };

})(jQuery);


$(document).ready(function () {
    $('form[role="search"]').searching();
});

