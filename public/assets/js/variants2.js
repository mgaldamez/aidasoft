'use strict';

(function (d) {

    var el = "#tbl-atrributes";
    var variants = [];
    var attributeValueList = [];
    var attributeList = [];
    var attributes = "";
    var selecAttribute = $('select[name="attribute[]"]');
    var selecAttributeValue = $('select[name="attribute_value[]"]');
    var combinar = $('#combinar');
    var numbreOfVariants = $('#number-variants');
    var _delete = $('#tbl-atrributes a[role="_delete"]');
    var table = document.createElement('TABLE');
    table.id = "show-variants";
    // table.style.display = 'none  ';


    var createVariants = function (attrArray) {

        var x = 0;

        if (variants.length == 0) {

            for (var i = 0; i < attrArray.length; i++) {

                var array = [];

                array[x] = attrArray[i];

                variants[i] = array;
            }

            x++;

        } else {

            var arrayTemp = variants;
            variants = [];

            for (var i = 0; i < arrayTemp.length; i++) {

                var newAttr = arrayTemp[i].length;

                for (var j = 0; j < attrArray.length; j++) {

                    arrayTemp[i][newAttr] = attrArray[j];

                    var str = arrayTemp[i].toString();
                    var newArray = str.split(",");
                    variants.push(newArray);
                }
            }
        }
    };


    var filterValue = function (object, value) {

        return Object.keys(object).filter(function (key) {

            if (key == value)
                return object[key];

        }).map(function (key) {

            return object[key];
        });
    };

    var findById = function (items, id) {
        for (var i in items) {

            if (items[i].id == id) {
                return items[i];
            }
        }

        return null;
    };


    var createArray = function (attrValues) {

        var array = [];

        for (var i = 0; i < attrValues.length; i++) {

            array = [];

            if (attrValues[i] != null) {

                for (var j = 0; j < attrValues[i].length; j++) {

                    array[j] = attrValues[i][j];
                }
            }

            createVariants(array);
        }
    };


    var showVariants = function (array, n) {
        n || ( n = 0 );

        // var desc = ($('input[name="name"]').val() != '')? $('input[name="name"]').val() : '[No definido]';
        // var barcode = ($('input[name="barcode"]').val() != '')? $('input[name="barcode"]').val() : '[No definido]';


        if (!document.getElementById("show-variants")) {
            document.getElementById('article_create').append(table);
        }

        table.innerHTML = '';

        for (var i = 0; i < array.length; i++) {

            var tr = document.createElement('TR');

            for (var j = 0; j < 5; j++) {

                var td = document.createElement('TD');

                switch (j) {
                    case 0:
                        var attr = '';
                        for (var k = 0; k < array[i].length; k++) {

                            var value = findById(attributeValueList, array[i][k]);

                            var attribute = findById(attributeList, value.attribute_id);

                            if (value) {

                                attr += attribute.description + ': ' + value.description + ', ';
                            }
                        }

                        td.classList.add('hidden-xs');
                        td.innerHTML = '<strong>' + (attr.substr(0, attr.length - 2)) + '</strong><input hidden name="variant[' + ( i + n ) + '][description]" value="' + (attr.substr(0, attr.length - 2)) + '">' +
                            '<input type="hidden" name="variant[' + (i + n) + '][id]" value="">';
                        break;

                    // case 1:
                    // td.classList.add('hidden-xs');
                    // td.innerHTML = '<span class="serial-code">' + barcode + '</span>';
                    // break;

                    case 2:
                        var html = '';
                        for (var k = 0; k < array[i].length; k++) {

                            var value = findById(attributeValueList, array[i][k]);

                            if (value) {
                                html += '<span class="chip chip-sm">' + value.description + '</span>' +
                                    '<input type="hidden" name="variant[' + ( i + n ) + '][value][]" value="' + array[i][k] + '" class="attr-value">';
                            }
                            else {
                                html += '<input type="hidden" name="variant[' + ( i + n ) + '][value][]" value="' + array[i][k] + '" class="attr-value">';
                            }
                        }
                        td.innerHTML = html;
                        break;

                    // case 3:
                    // td.innerHTML = '<input class="form-control" name="variant[' + ( i + n ) + '][stock]" value="" style="max-width:200px">';
                    // break;

                    // case 4:
                    //     td.width = 40;
                    //     td.innerHTML = '<div class="togglebutton"><label><input type="checkbox" name="variant[' + ( i + n ) + '][active]" checked="checked"><span class="toggle"></span></label></div>';
                    // break;
                }

                tr.appendChild(td);
            }

            table.appendChild(tr);
        }
    };


    var loadAttributes = function (array) {
        array || ( array = [] )
            var attributesArray = array;

            $(el + " tbody tr").each(function (index) {

                if ($(this).find('select.attr').val())
                    attributesArray.push($(this).find('select.attr-values').val());
            });


            $(array).each(function (index) {

                if ($(this).is(':checked')) {
                    attributesArray.push($(this).val());
                }
            });

            variants = []; // vaciar la variable

            createArray(attributesArray)
            showVariants(variants);

            numbreOfVariants.text(variants.length);
    };


    var getValues = function () {

        $.get('/attribute/values/lists', function (response) {
            attributeValueList = response;
        });
    };


    var getAttributes = function () {

        $.get('/attribute/all/lists', function (response) {

            attributes = "";

            attributeList = response;

            Object.keys(response).forEach(function (key) {

                attributes += '<option value="' + response[key].id + '">' + response[key].description + '</option>';
            });
        });
    };


    $(d).delegate('select.attr', 'change', function (event) {

        var row = event.target.closest('tr');
        var select = $(row).find('select.attr-values')[0];

        $(select).empty();

        if (event.target.value) {
            $.get('/attribute/' + event.target.value + '/value', function (response, status) {

                response.forEach(function (element, index) {

                    $(select).append('<option value="' + element.id + '">' + element.description + '</option>');
                });
            });
        }
    });


    $(d).delegate('#add_atribute', 'click', function (event) {

        event.preventDefault();

        getAttributes();

        if (maxAttributes()) {

            var tbody = $(el + " tbody").append(
                '<tr>' +
                '<td>' +
                '<div class="">' +
                '<div class="form-inline">' +
                '<div class="form-group col-xs-12 col-sm-4" style="margin:0px; padding-right:3px;">' +
                '<select name="attribute[][id]" class="form-control attr">' + attributes + '</select>' +
                '</div>' +
                '<div class="form-group col-xs-12 col-sm-8" style="margin:0px; padding-right:3px;">' +
                '<select name="attribute[][value][]" class="form-control attr-values name_handler" multiple="multiple"></select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</td>' +
                '<td style="padding-left:8px; padding-right:8px;" width="20">' +
                '<div class="form-group" style="margin:0px;">' +
                '<a href="#" class="btn btn-xs btn-danger" role="_delete" data-toggle="tooltip" data-placement="top" title="Quitar el atributo"><i class="material-icons md-18">clear</i></a>' +
                '</div>' +
                '</td>' +
                '</tr>');

            var tr = $(tbody).find('tr').last();
            var attribute = $(tr).find('select.attr');
            var attribute_value = $(tr).find('select.attr-values');


            $(attribute).select2({
                tags: false,
                minimumResultsForSearch: Infinity,
                placeholder: 'Selecciona un atributo de la lista',
            }).val('').trigger('change');


            $(attribute_value).select2({
                tags: false,
                multiple: "multiple",
                // tokenSeparators: [',', ' '],
                minimumResultsForSearch: -1,
                placeholder: "Selecciona los valores para este atributo",
            });
        }
    });

    var maxAttributes = function () {

        if ($(el).find('tbody tr').length >= 3) {

            $('#add_atribute').attr('disabled', 'disabled');

            return false;
        }

        $('#add_atribute').removeAttr('disabled');
        return true;
    };


    var verifyCheck = function (checkbox) {

        if ($(checkbox).is(':checked')) {

            return true;
        }
        return false;
    };


    $("#check-variants").change(function (event) {
        event.preventDefault();

        if( $(el).find('tbody tr').length == 0 ) {


        }

        if (verifyCheck(this)) {
            loadAttributes();

        } else {
            table.remove();
        }
    });


    $(d).delegate(el, "change", function (event) {
        event.preventDefault();
        if (document.getElementById("show-variants"))
            loadAttributes();
    });


    $(el).delegate("a[role='_delete']", "click", function (event) {
        event.preventDefault();

        var row = event.target.closest('tr');

        $(row).fadeOut("normal", function () {
            $(this).remove();
            loadAttributes();
            maxAttributes();
        });
    });


    // $('#btnAutoVariants').click(function (event) {
    //
    //     var group = $('#variants .form-group');
    //
    //     var attributesArray = [];
    //
    //
    //     $(group).each(function (index) {
    //
    //         var variants = $(this).find('input[name="attributeValue[]"]');
    //         var array = [];
    //
    //         $(variants).each(function (index) {
    //             if ($(this).is(':checked')) {
    //
    //                 array.push($(this).val());
    //             }
    //         });
    //
    //         if (array.length != 0)
    //             attributesArray.push(array);
    //     });
    //
    //     loadAttributes(attributesArray);
    // });


    getValues();
    getAttributes();
})(document);