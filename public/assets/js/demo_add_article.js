

/*
*    Variables 
*/

var data        = $('#add_article');
var cant        = 0;
var minToRemove = 2; // Cantidad minima de items para que el boton remover se muestre 
var form        = $('#formArticleDetailList'); // formulario


function disabled_selected () { 
    formSerializeArray = form.serializeArray();
    // console.log(formSerializeArray)
    
    var y = 1;

    var disabled = []; // almacena 

    $.each(formSerializeArray, function(i, field){ 
      if (field.name == 'bodega'+y ){
        disabled[y] =  field.value;
        y++
      }
    })
    // console.log(disabled)
    
    y = 1; // inicializo la variable y en 1

    $.each(formSerializeArray, function(i, field){   // index , element

     if (field.name == 'bodega'+y ){
            // cambia el atributo css (disblay : block) para todos los items 
            $("select#"+field.name+"").find('option').each(function(){
              var val = $(this).val() 
              if (val > 0)
                $("#"+field.name+" option[value="+val+"]").css("display", "block")
                // console.log(val)
              })

            for (j = 1; j<= disabled.length; j++ ){
             $("select#"+field.name+"").find('option').each(function(){
              var val = $(this).val()

              if ( disabled[j] == val){
                $("#"+field.name+" option[value="+val+"]").css("display", "none") 
                        // $("#"+field.name+" option[value="+val+"]").prop("disabled",true)
                      } 
                    })
            } // for

           y++; // incremento de y en 1

       } // if 

     })
  }

// ***********************
// var cantBodegas = 1;

//  function function_name () {
//     $.get( '/bodegas', function(res){
//         cantBodegas = res.length
//     }); 
// }

// $("select.fila1").find('option').on('load', function_name())
// ***********************



$('#btn-addElement').click(function(){
    // if( cant < cantBodegas){
      cant ++;
      data.append('<div class="row'+cant+'">'+
        '<div class="form-group">'+
        '<label class="col-md-1 control-label">'+cant+'</label>'+
        '<div class="col-md-11">'+
        '<div class="row">'+   
        '<div class="col-md-2">'+
        '<div class="col-xs-4">'+
        '<div class="radio radio-primary">'+
        '<label><input type="radio" id="" class="" value="1" name="sexOption'+cant+'" checked="">M</label>'+
        '</div>'+
        '</div>'+
        '<div class="col-xs-4">'+
        '<div class="radio radio-primary">'+
        '<label><input type="radio" id="" class="" value="2" name="sexOption'+cant+'">F</label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<br class="visible-xs visible-sm">'+
        '<div class="col-md-2">'+
        '<select class="form-control _form-control" name="size" title="Seleccione" autofocus>'+
        '<option value="1">S </option>'+
        '<option value="2">M </option>'+
        '<option value="3">L </option>'+
        '<option value="4">XL</option>'+
        '</select>'+
        '</div>'+
        '<div class="col-md-2">'+  
        '<input type="number" class="form-control _form-control" name="quantity" value="1" min="1" placeholder="Cantidad">'+
        '</div>'+
        '<div class="col-md-6">'+  
        '<textarea class="form-control _form-control" name="" value="" placeholder="Observaciones" rows="1"></textarea>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<hr class="visible-xs visible-sm">'+
        '</div>');

        // Cargar el select con los datos de las bodegas desde la base de datos
        // bodegas('bodega'+cant+'')  
    // }


    $('select').selectpicker();
    // iniciar bootstrap material design
    // para dibujar elementos en tiempo de ejecucion
    $.material.init()
    
    if( cant== minToRemove ){
      $('<button type="button" id="btn-deleteElement" class="btn btn-raised btn-danger row'+cant+'"><span class="glyphicon glyphicon-minus"></span><span class="hidden-xs"> Quitar</span></button>').insertAfter('#btn-addElement');
    }     
  })

$(document).delegate('#btn-deleteElement','click', function(event){
  event.preventDefault();
  $('.row'+cant).remove();
  cant--;
    // disabled_selected()
  })

$(document).delegate("#add_article select", "change", function(event){    
  event.preventDefault();
    // disabled_selected ()
  })


// Iniciar el evento que lanza la funcion para cargar las bodegas 
// descomentar para que aparesca una bodega por defecto
$('#btn-addElement').click(); 