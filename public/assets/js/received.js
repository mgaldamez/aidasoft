'use stric';

(function($){

	$.fn.table = function(options){

		var defaults = {
		};

		options = $.extend(defaults, options);

		var methods = {

			confirm: function(table){

				var count = 0;

				table.each( function (index) {

					var td = ($(this).children());
					var received = $(td[1]).find('input')[0].value;
					var missing = $(td[1]).find('input')[1].value;

					if( received > missing || received < missing  ){
						count++;
					} 
				});

				return count;
			},


			send: function(form){

				$(form).submit();
				
				// setTimeout(function(){ 
 				// $("#myForm").submit();
				// }, 2000);

				// $.ajax({
				// 	url: form.action,
				// 	type: 'POST',
				// 	data: $(form).serialize(),
				// 	dataType: 'json',
				// 	success: function(res){
				// 		console.log(res);
				// 	},
				// 	error: function(err){
				// 		console.log(err)
				// 	}
				// });
			}, 

			createModal: function(){

				var modal = '<div class="modal fade" tabindex="-1" role="dialog">'+
								'<div class="modal-dialog" role="document">'+
									'<div class="modal-content">'+
										'<div class="modal-header">'+
											'<h4 class="modal-title">Tus cantidades recibidas no coinciden con las solicidadas en la orden.</h4>'+
										'</div>'+
										'<div class="modal-body">'+
											
											'Puedes procesar tu recepcion con pedidos pendientes si recibiras los articulos restantes despues;'+
											'de lo contrario indica No para procesar el pedido actual como completo.'+
											
										'</div>'+
										'<div class="modal-footer">'+
										
											'<button type="button" class="btn btn-default mdb" data-dismiss="modal">Cancelar</button>'+
											'<button type="button" id="btn-no" class="btn btn-primary btn-raised mdb">No</button>'+
											'<button type="button" id="btn-yes" class="btn btn-primary btn-raised mdb">Si</button>'+
									
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>';

				$("body").append( modal );

				return $(modal)[0];
			}
		};

		return this.each(function() {

			var table  = $(this).find('.table tbody tr');
			var button = $(this).find('button#confirm')[0];
			var backorder = $(this).find('input[name="backorder"]')[0];
			var form = this;

			if( button ){

				$(button).click(function(event){
					event.preventDefault();

					if ( methods.confirm(table) > 0 ){

						var el = $(methods.createModal());
						el.modal('show');

						el.delegate('#btn-yes','click', function(event){
							backorder.value = 1;
							el.modal('hide');
							methods.send(form);
						});

						el.delegate('#btn-no','click', function(event){
							backorder.value = 0;
							el.modal('hide');
							methods.send(form);
						});	
					} else {
						methods.send(form);
					}

				});
			}
		});
	}

})(jQuery);


$(function(){
	$("#myForm").table();
});

