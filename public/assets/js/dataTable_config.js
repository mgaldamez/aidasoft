// function dataTableConfig(element = "table") {
//     $(element).dataTable({
//         "language": {
//             "decimal": "",
//             "emptyTable": "No hay datos disponibles en la tabla",
//             "info": "Viendo _START_ a _END_ de _TOTAL_ entradas",
//             "infoEmpty": "Viendo 0 a 0 de 0 entradas",
//             "infoFiltered": "(filtrado de _MAX_ entradas totales)",
//             "infoPostFix": "",
//             "thousands": ",",
//             "lengthMenu": "Cantidad _MENU_ ",
//             "loadingRecords": "Cargando...",
//             "processing": "Procesando...",
//             "search": "Buscar:",
//             "zeroRecords": "No se encontraron registros coincidentes",
//             "paginate": {
//                 "first": "Primero",
//                 "last": "Ultimo",
//                 "next": "<span class='glyphicon glyphicon-chevron-right'></span>",
//                 "previous": "<span class='glyphicon glyphicon-chevron-left'></span>"
//             },
//             "aria": {
//                 "sortAscending": ": activar para ordenar la columna ascendente",
//                 "sortDescending": ": activar para ordenar la columna descendente"
//             }
//         },
//
//
//     });
//
// }
'use strict';
(function ($) {
    $.fn.dataTableConfig = function (options) {


        var messages = function () {
            return {
                "decimal": "",
                "emptyTable": "No hay datos disponibles en la tabla",
                "info": "Viendo _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty": "Viendo 0 a 0 de 0 entradas",
                "infoFiltered": "(filtrado de _MAX_ entradas totales)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Cantidad _MENU_ ",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "No se encontraron registros coincidentes",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "<span class='glyphicon glyphicon-chevron-right'></span>",
                    "previous": "<span class='glyphicon glyphicon-chevron-left'></span>"
                }
            };
        };

        var defaults = {
            "displayLength": 100,
            "colReorder": "true",
            "rowReorder": "true",
            "order": [[0, 'false']],
            "language": messages()
        };

        options = $.extend(defaults, options);

        // return this.each(function () {
        return $(this).DataTable(options);
        // });
    }
})(jQuery);