
'use strict';

var articles = [
	{ 'id': 1, 'description': 'Articulo de prueba  Blanco 32 Gigas' },
	{ 'id': 2, 'description': 'Articulo de prueba  Blanco 64 Gigas' },
	{ 'id': 3, 'description': 'Articulo de prueba  Azul 32 Gigas' },
	{ 'id': 4, 'description': 'Articulo de prueba  Azul 64 Gigas' },
	{ 'id': 5, 'description': 'Articulo de prueba  Negro 32 Gigas' },
	{ 'id': 6, 'description': 'Articulo de prueba  Negro 64 Gigas'}
];


var taxes = [

	{ 'id': '.00', 'description': '0%' }, 
	{ 'id': '.05', 'description': '5%' }, 
	{ 'id': '.07', 'description': '7%' }, 
	{ 'id': '.10', 'description': '10%' } 
];


var unities = [
	{ 'id': 1, 'description': 'Unidad	U' },
	{ 'id': 2, 'description': 'Caja 6	6/1' },
	{ 'id': 3, 'description': 'Caja 12	12/1' },
	{ 'id': 4, 'description': 'Caja 24	24/1' },
	{ 'id': 5, 'description': 'Kilogramos	Kg' },
	{ 'id': 6, 'description': 'Gramos	g' },
	{ 'id': 7, 'description': 'Libras	lb' },
	{ 'id': 8, 'description': 'Toneladas	T' },
	{ 'id': 9, 'description': 'Milimetros cubicos	mm3' },
	{ 'id': 10, 'description': 'Centimetros cubicos	cm3' },
	{ 'id': 11, 'description': 'Metros	m' },
	{ 'id': 12, 'description': 'Centimetros	cm' },
	{ 'id': 13, 'description': 'Milimetros	mm' },
	{ 'id': 14, 'description': 'Fahrenheit	°F' },
	{ 'id': 15, 'description': 'Kelvin	°K' },
	{ 'id': 16, 'description': 'Celsius	°C' }
];


(function($){


	$.fn._table = function(options){

		var defaults = {};

		options = $.extend(defaults, options);


		var methods = {

			add: function(elements){
				
				var tr =  document.createElement('TR');

				for (var i = 0; i < 6; i++) {
					var td = document.createElement('TD');
						
					switch (i) {
						case 0:
						td.innerHTML = '<div class="checkbox checkbox-mini"><label><input type="checkbox" name="check" value="0"></label></div>';
						break;
							case 1:
						td.innerHTML = $(elements[0]).context.selectedOptions[0].textContent + '<input type="hidden" name="item[article][id][]" value="'+ $(elements[0]).val() +'">'; 
						
						break;
							case 2:
						td.innerHTML = $(elements[1]).val() + '<input type="hidden" name="item[quantity][]" value="'+ $(elements[1]).val() +'" readonly>';
						break;
							case 3:
						td.innerHTML = 'Unidad' + '<input type="hidden" name="item[unity][]" value="Unidad" readonly>';
						break;
							case 4:
						td.innerHTML = parseFloat($(elements[2]).val()).toFixed(2) + '<input type="hidden" name="item[unit_cost][]" value="'+ parseFloat($(elements[2]).val()).toFixed(2) +'" readonly>';
						break;
							case 5:
						td.innerHTML = $(elements[3]).context.selectedOptions[0].textContent  + '<input type="hidden" name="item[tax][id][]" value="'+ $(elements[3]).val() +'">';
						break;
							case 6:
						td.innerHTML = parseFloat($(elements[4]).val()).toFixed(2) + '<input type="hidden" name="item[amount][]" value="'+ parseFloat($(elements[4]).val()).toFixed(2) +'" readonly>';
						break;

						default:
							console.log('Default');
							break;
						}	
					tr.appendChild(td);
				}


				$("#tbl-article tbody").append(tr);
			},


			edit: function(elements, columns ){
				var td = columns;
			
				for (var i = 0; i < columns.length; i++) {
	
				switch (i) {
					case 0:

					break;
						case 1:
					// td[i].setAttribute('colspan);
					td[i].innerHTML = '<select name="_article" class="form-control">' + optionsHandle(articles) + '</select>';
					$(td[i].childNodes[0]).val( $(elements[1]).val() ).change();
					break;
						case 2:
					td[i].innerHTML = '<input type="text" name="_quantity" class="form-control" placeholder="Cantidad de articulos" value='+ $(elements[2]).val() +'>';
					break;
						case 3:
					td[i].innerHTML = '<select name="_unity" class="form-control">'+ optionsHandle(unities) +'</select>';
					// $(td[i].childNodes[0]).val( $(elements[3]).val() ).change();
					 $(td[i].childNodes[0]).val( 1 ).change();
					break;
						case 4:
					td[i].innerHTML = '<input type="text" name="_unit_cost"  class="form-control" placeholder="Costo por unidad" value='+ $(elements[4]).val() +'>';
					break;
						case 5:
					td[i].innerHTML = '<select name="_tax" class="form-control">'+ optionsHandle(taxes) +'</select>';
					$(td[i].childNodes[0]).val( $(elements[5]).val() ).change();
					break;

						default:
						console.log('Default');
					break;
					}	
				}
			}
		};



		return this.each(function() {

			var check = $(this).find("input[type='checkbox']");

			$(this).delegate(check, 'change', function(event){
				event.preventDefault();

				if( $(event.target).prop('id') == "select-all" ){
	
					var th = event.target.closest('th');
					th = Array.prototype.slice.apply( $(th).siblings('th') );
					th.forEach( function(element, index) {
						
						$(element).toggleClass('hidden-head')
					});
				}

				else if( $(event.target).is(':checked')  ){

					var row = event.target.closest('tr');
					var elements = $(row).find('input');
					var columns = $(row).find('td');
					
					methods.edit(elements, columns);

					row.setAttribute('Class', 'edit');				
				}
			});


			$(this).delegate("#btn-add", "click", function(event){
				event.preventDefault();

				var form =  event.target.closest('.form-table');
				var inputs = $(form).find('input , select');

				methods.add(inputs);

				$.material.init();
			});
		});
	}
})(jQuery);


(function(doc){

	$(doc).ready(function(){
		$("#tbl-article")._table();
	});
})(document);


function optionsHandle(array = []){
	var options = '';
	if(array){

		array.forEach( function(element, index) {
			options += '<option value="'+ element.id +'">' + element.description;
		});

		return options;
	}
}